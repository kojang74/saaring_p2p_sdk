﻿#include "mainwindow.h"

#include <QDebug>

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void MainWindow::onSharedFolderChanged()
{
    qDebug() << "onSharedFolderChanged...";
}

void MainWindow::threadShareDirCallback(void *context, int command, QStringList &param)
{
    MainWindow *mainWindow = (MainWindow*)context;

    switch((saaring::SaaringShareFolderInterface::ShareDirCallbackCommand)command)
    {
    case saaring::SaaringShareFolderInterface::ShareDirCallbackCommand::SDCC_DIRECTORY_CHANGED :
        {
            QString path        = param.takeFirst();
            qDebug() << "path=" << path;
            // do nothing
        }
        break;
    case saaring::SaaringShareFolderInterface::ShareDirCallbackCommand::SDCC_JOB_FINISHED :
        {
            qDebug() << "Refreshed shared folder...";
        }
        break;
    case saaring::SaaringShareFolderInterface::ShareDirCallbackCommand::SDCC_JOB_ERROR :
        {
            // do nothing
        }
        break;
    }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void MainWindow::onSignalingConnected()
{
    qDebug() << "onSignalingConnected...";
}

void MainWindow::onSignalingClosed()
{
    qDebug() << "onSignalingClosed...";
}

void MainWindow::onSignalingLoginFailed()
{
    qDebug() << "onSignalingLoginFailed...";
}

void MainWindow::onSignalingNotifyDeviceLiveYN(qint64 fromUserNo, qint64 fromDeviceNo, QString liveYN)
{
    qDebug() << "onSignalingNotifyDeviceLiveYN..." <<
                ", fromUserNo=" << fromUserNo <<
                ", fromDeviceNo=" << fromDeviceNo <<
                ", liveYN=" << liveYN
                ;
}

void MainWindow::onSignalingNotifyRequestFriend(QString friendName)
{
    qDebug() << "onSignalingNotifyRequestFriend..." <<
                ", friendName=" << friendName
                ;
}

void MainWindow::onSignalingNotifyAddFriend(QString friendName, qint64 friendUserNo)
{
    qDebug() << "onSignalingNotifyAddFriend..." <<
                ", friendName=" << friendName <<
                ", friendUserNo=" << friendUserNo
                ;
}

void MainWindow::onPortmapFinished()
{
    qDebug() << "onPortmapFinished...";
}

void MainWindow::onP2pLoginFinished(qint64 friendDeviceNo, qint64 toRemoteShareNo)
{
    qDebug() << "onP2pLoginFinished..." <<
                ", friendDeviceNo=" << friendDeviceNo <<
                ", toRemoteShareNo=" << toRemoteShareNo
                ;
}

void MainWindow::onP2pWakeUpFinished(bool isSuccess)
{
    qDebug() << "onP2pWakeUpFinished..." <<
                ", isSuccess=" << isSuccess
                ;
}

void MainWindow::onP2pFinishDownload(qint64 transNo, qint64 fileSize, qint64 receivedSize)
{
    qDebug() << "onSignalingConnected..." <<
                ", transNo=" << transNo <<
                ", fileSize=" << fileSize <<
                ", receivedSize=" << receivedSize
                ;
}

void MainWindow::onP2pFinishUpload(qint64 transNo, qint64 fileSize, qint64 sentSize)
{
    qDebug() << "onSignalingConnected..." <<
                ", transNo=" << transNo <<
                ", fileSize=" << fileSize <<
                ", sentSize=" << sentSize
                ;
}

void MainWindow::onP2pProgressDownload(qint64 transNo, qint64 fileSize, qint64 receivedSize)
{
    qDebug() << "onSignalingConnected..." <<
                ", transNo=" << transNo <<
                ", fileSize=" << fileSize <<
                ", receivedSize=" << receivedSize
                ;
}

void MainWindow::onP2pProgressUpload(qint64 transNo, qint64 fileSize, qint64 sentSize)
{
    qDebug() << "onSignalingConnected..." <<
                ", transNo=" << transNo <<
                ", fileSize=" << fileSize <<
                ", sentSize=" << sentSize
                ;
}

void MainWindow::onP2pAbortDownload(qint64 transNo)
{
    qDebug() << "onSignalingConnected..." <<
                ", transNo=" << transNo
                ;
}

void MainWindow::onP2pAbortUpload(qint64 transNo)
{
    qDebug() << "onSignalingConnected..." <<
                ", transNo=" << transNo
                ;
}

void MainWindow::onP2pFinishDownloadOther(qint64 jobId, qint64 remoteDeviceNo)
{
    qDebug() << "onSignalingConnected..." <<
                ", jobId=" << jobId <<
                ", remoteDeviceNo=" << remoteDeviceNo
                ;
}

void MainWindow::onP2pFinishUploadOther(qint64 jobId, qint64 remoteDeviceNo)
{
    qDebug() << "onSignalingConnected..." <<
                ", jobId=" << jobId <<
                ", remoteDeviceNo=" << remoteDeviceNo
                ;
}

void MainWindow::onP2pProgressDownloadOther(qint64 jobId, qint64 remoteDeviceNo, qint64 fileSize, qint64 currSize, QString filePath, QString serverName, QString deviceName)
{
    qDebug() << "onSignalingConnected..." <<
                ", jobId=" << jobId <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", fileSize=" << fileSize <<
                ", currSize=" << currSize <<
                ", filePath=" << filePath <<
                ", serverName=" << serverName <<
                ", deviceName=" << deviceName
                ;
}

void MainWindow::onP2pProgressUploadOther(qint64 jobId, qint64 remoteDeviceNo, qint64 fileSize, qint64 currSize, QString filePath, QString serverName, QString deviceName)
{
    qDebug() << "onSignalingConnected..." <<
                ", jobId=" << jobId <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", fileSize=" << fileSize <<
                ", currSize=" << currSize <<
                ", filePath=" << filePath <<
                ", serverName=" << serverName <<
                ", deviceName=" << deviceName
                ;
}

void MainWindow::onP2pProcessedRequest(int processType, qint64 jobId, qint64 remoteDeviceNo, int resultCode)
{
    qDebug() << "onSignalingConnected..." <<
                ", jobId=" << jobId <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", resultCode=" << resultCode
                ;
}

void MainWindow::onP2pProcessedResponse(int processType, qint64 jobId, qint64 remoteDeviceNo, int resultCode)
{
    qDebug() << "onSignalingConnected..." <<
                ", jobId=" << jobId <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", resultCode=" << resultCode
                ;
}

void MainWindow::onP2pProcessResponseFolderList(QString strJson)
{
    qDebug() << "onSignalingConnected..." <<
                ", strJson=" << strJson
                ;
}

void MainWindow::onP2pProcessResponseFolderDetail(QString strJson)
{
    qDebug() << "onSignalingConnected..." <<
                ", strJson=" << strJson
                ;
}

void MainWindow::onP2pProcessResponseFolderDetailFinished()
{
    qDebug() << "onP2pProcessResponseFolderDetailFinished...";
}

void MainWindow::onP2pProcessResponseFolderDetailRecursive(QString strJson)
{
    qDebug() << "onSignalingConnected..." <<
                ", strJson=" << strJson
                ;
}

void MainWindow::onP2pProcessResponseFileUploadQuery(qint64 transNo, qint64 fileStartPosition)
{
    qDebug() << "onSignalingConnected..." <<
                ", transNo=" << transNo <<
                ", fileStartPosition=" << fileStartPosition
                ;
}

void MainWindow::onP2pProcessResponseFileDownloadInviteSendFilesQuery(QString strJson)
{
    qDebug() << "onSignalingConnected..." <<
                ", strJson=" << strJson
                ;
}

void MainWindow::onP2pProcessResponseSaveSendFilesInviteInfo(QString strJson)
{
    qDebug() << "onSignalingConnected..." <<
                ", strJson=" << strJson
                ;
}

void MainWindow::onP2pNotifyDownloadQueueStandBy(qint64 remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
{
    qDebug() << "onSignalingConnected..." <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", downloadingCount=" << downloadingCount <<
                ", downloadStandByCount=" << downloadStandByCount <<
                ", myTurn=" << myTurn
                ;
}

void MainWindow::onP2pNotifyDownloadQueueRelease(qint64 remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
{
    qDebug() << "onSignalingConnected..." <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", downloadingCount=" << downloadingCount <<
                ", downloadStandByCount=" << downloadStandByCount <<
                ", myTurn=" << myTurn
                ;
}

void MainWindow::onP2pNotifyUploadQueueStandBy(qint64 remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
{
    qDebug() << "onSignalingConnected..." <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", uploadingCount=" << uploadingCount <<
                ", uploadStandByCount=" << uploadStandByCount <<
                ", myTurn=" << myTurn
                ;
}

void MainWindow::onP2pNotifyUploadQueueRelease(qint64 remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
{
    qDebug() << "onSignalingConnected..." <<
                ", remoteDeviceNo=" << remoteDeviceNo <<
                ", uploadingCount=" << uploadingCount <<
                ", uploadStandByCount=" << uploadStandByCount <<
                ", myTurn=" << myTurn
                ;
}

void MainWindow::onP2pNotifyConnectionClosed(qint64 remoteDeviceNo)
{
    qDebug() << "onP2pNotifyConnectionClosed..." <<
                ", remoteDeviceNo=" << remoteDeviceNo
                ;
}

void MainWindow::onP2pNotifyStartNextTransfer(qint64 remoteDeviceNo)
{
    qDebug() << "onP2pNotifyStartNextTransfer..." <<
                ", remoteDeviceNo=" << remoteDeviceNo
                ;
}

void MainWindow::onP2pNotifyStatusMessage(QString text)
{
    qDebug() << "onSignalingConnected..." <<
                ", text=" << text
                ;
}

void MainWindow::p2pCallback(void *context, int command, QStringList &param)
{
    qDebug() << "command=" << command << ", param=" << param;
    MainWindow *mainWindow = (MainWindow*)context;

    switch((saaring::SaaringP2pInterface::P2pCallbackCommand)command)
    {
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_SIGNALING_CONNECTED :
        {
            mainWindow->onSignalingConnected();
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_SIGNALING_CLOSED :
        {
            mainWindow->onSignalingClosed();
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_SIGNALING_LOGIN_FAILED :
        {
            mainWindow->onSignalingLoginFailed();
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_SIGNALING_NOTIFY_DEVICE_LIVEYN :
        {
            qint64 fromUserNo           = param.takeFirst().toLongLong();
            qint64 fromDeviceNo         = param.takeFirst().toLongLong();
            QString liveYN              = param.takeFirst();

            mainWindow->onSignalingNotifyDeviceLiveYN(fromUserNo, fromDeviceNo, liveYN);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_SIGNALING_NOTIFY_REQUEST_FRIEND :
        {
            QString friendName          = param.takeFirst();

            mainWindow->onSignalingNotifyRequestFriend(friendName);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_SIGNALING_NOTIFY_ADD_FRIEND :
        {
            QString friendName          = param.takeFirst();
            qint64 friendUserNo         = param.takeFirst().toLongLong();

            mainWindow->onSignalingNotifyAddFriend(friendName, friendUserNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PORTMAP_FINISHED :
        {
            mainWindow->onPortmapFinished();
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_LOGIN_FINISHED :
        {
            qint64 friendDeviceNo       = param.takeFirst().toLongLong();
            qint64 toRemoteShareNo      = param.takeFirst().toLongLong();

            mainWindow->onP2pLoginFinished(friendDeviceNo, toRemoteShareNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_WAKEUP_FINISHED :
        {
            bool isSuccess              = param.takeFirst() == "0" ? false : true;

            mainWindow->onP2pWakeUpFinished(isSuccess);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_FINISH_DOWNLOAD :
        {
            qint64 transNo              = param.takeFirst().toLongLong();
            qint64 fileSize             = param.takeFirst().toLongLong();
            qint64 receivedSize         = param.takeFirst().toLongLong();

            mainWindow->onP2pFinishDownload(transNo, fileSize, receivedSize);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_FINISH_UPLOAD :
        {
            qint64 transNo              = param.takeFirst().toLongLong();
            qint64 fileSize             = param.takeFirst().toLongLong();
            qint64 sentSize             = param.takeFirst().toLongLong();

            mainWindow->onP2pFinishUpload(transNo, fileSize, sentSize);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROGRESS_DOWNLOAD :
        {
            qint64 transNo              = param.takeFirst().toLongLong();
            qint64 fileSize             = param.takeFirst().toLongLong();
            qint64 receivedSize         = param.takeFirst().toLongLong();

//            qDebug("============================== before ===============================");
            QElapsedTimer timer;
            timer.start();
            mainWindow->onP2pProgressDownload(transNo, fileSize, receivedSize);
            qDebug() << "============================== The slow operation took " << timer.elapsed() << " milliseconds";
//            qDebug("============================== after ===============================");
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROGRESS_UPLOAD :
        {
            qint64 transNo              = param.takeFirst().toLongLong();
            qint64 fileSize             = param.takeFirst().toLongLong();
            qint64 sentSize             = param.takeFirst().toLongLong();

            mainWindow->onP2pProgressUpload(transNo, fileSize, sentSize);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_ABORT_DOWNLOAD :
        {
            qint64 transNo              = param.takeFirst().toLongLong();

            mainWindow->onP2pAbortDownload(transNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_ABORT_UPLOAD :
        {
            qint64 transNo              = param.takeFirst().toLongLong();

            mainWindow->onP2pAbortUpload(transNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_FINISH_DOWNLOAD_OTHER :
        {
            qint64 jobId                = param.takeFirst().toLongLong();
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();

            mainWindow->onP2pFinishDownloadOther(jobId, remoteDeviceNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_FINISH_UPLOAD_OTHER :
        {
            qint64 jobId                = param.takeFirst().toLongLong();
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();

            mainWindow->onP2pFinishUploadOther(jobId, remoteDeviceNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROGRESS_DOWNLOAD_OTHER :
        {
            qint64 jobId                = param.takeFirst().toLongLong();
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            qint64 fileSize             = param.takeFirst().toLongLong();
            qint64 currSize             = param.takeFirst().toLongLong();
            QString filePath            = param.takeFirst();
            QString serverName          = param.takeFirst();
            QString deviceName          = param.takeFirst();

            mainWindow->onP2pProgressDownloadOther(jobId, remoteDeviceNo, fileSize, currSize, filePath, serverName, deviceName);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROGRESS_UPLOAD_OTHER :
        {
            qint64 jobId                = param.takeFirst().toLongLong();
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            qint64 fileSize             = param.takeFirst().toLongLong();
            qint64 currSize             = param.takeFirst().toLongLong();
            QString filePath            = param.takeFirst();
            QString serverName          = param.takeFirst();
            QString deviceName          = param.takeFirst();

            mainWindow->onP2pProgressUploadOther(jobId, remoteDeviceNo, fileSize, currSize, filePath, serverName, deviceName);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESSED_REQUEST :
        {
            int processType             = param.takeFirst().toInt();
            qint64 jobId                = param.takeFirst().toLongLong();
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            int resultCode              = param.takeFirst().toInt();

            mainWindow->onP2pProcessedRequest(processType, jobId, remoteDeviceNo, resultCode);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESSED_RESPONSE :
        {
            int processType             = param.takeFirst().toInt();
            qint64 jobId                = param.takeFirst().toLongLong();
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            int resultCode              = param.takeFirst().toInt();

            mainWindow->onP2pProcessedResponse(processType, jobId, remoteDeviceNo, resultCode);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_FOLDER_LIST :
        {
            QString strJson             = param.takeFirst();

            mainWindow->onP2pProcessResponseFolderList(strJson);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_FOLDER_DETAIL :
        {
            QString strJson             = param.takeFirst();

            mainWindow->onP2pProcessResponseFolderDetail(strJson);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_FOLDER_DETAIL_FINISHED :
        {
            mainWindow->onP2pProcessResponseFolderDetailFinished();
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE :
        {
            QString strJson             = param.takeFirst();

            mainWindow->onP2pProcessResponseFolderDetailRecursive(strJson);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_FILE_UPLOAD_QUERY :
        {
            qint64 transNo              = param.takeFirst().toLongLong();
            qint64 fileStartPosition    = param.takeFirst().toLongLong();

            mainWindow->onP2pProcessResponseFileUploadQuery(transNo, fileStartPosition);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY :
        {
            QString strJson             = param.takeFirst();

            mainWindow->onP2pProcessResponseFileDownloadInviteSendFilesQuery(strJson);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO :
        {
            QString strJson             = param.takeFirst();

            mainWindow->onP2pProcessResponseSaveSendFilesInviteInfo(strJson);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY :
        {
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            int downloadingCount        = param.takeFirst().toInt();
            int downloadStandByCount    = param.takeFirst().toInt();
            int myTurn                  = param.takeFirst().toInt();

            mainWindow->onP2pNotifyDownloadQueueStandBy(remoteDeviceNo, downloadingCount, downloadStandByCount, myTurn);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE :
        {
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            int downloadingCount        = param.takeFirst().toInt();
            int downloadStandByCount    = param.takeFirst().toInt();
            int myTurn                  = param.takeFirst().toInt();

            mainWindow->onP2pNotifyDownloadQueueRelease(remoteDeviceNo, downloadingCount, downloadStandByCount, myTurn);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY :
        {
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            int uploadingCount          = param.takeFirst().toInt();
            int uploadStandByCount      = param.takeFirst().toInt();
            int myTurn                  = param.takeFirst().toInt();

            mainWindow->onP2pNotifyUploadQueueStandBy(remoteDeviceNo, uploadingCount, uploadStandByCount, myTurn);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE :
        {
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();
            int uploadingCount          = param.takeFirst().toInt();
            int uploadStandByCount      = param.takeFirst().toInt();
            int myTurn                  = param.takeFirst().toInt();

            mainWindow->onP2pNotifyUploadQueueRelease(remoteDeviceNo, uploadingCount, uploadStandByCount, myTurn);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_CONNECTION_CLOSED :
        {
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();

            // p2p 재접속이 이루어진다. 기존 전송을 Stop 시킨다.
            mainWindow->onP2pNotifyConnectionClosed(remoteDeviceNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_START_NEXT_TRANSFER :
        {
            qint64 remoteDeviceNo       = param.takeFirst().toLongLong();

            mainWindow->onP2pNotifyStartNextTransfer(remoteDeviceNo);
        }
        break;
    case saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_STATUS_MESSAGE :
        {
            QString text                = param.takeFirst();

            mainWindow->onP2pNotifyStatusMessage(text);
        }
        break;
    }
}
