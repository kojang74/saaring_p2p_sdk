
QT       += core gui network sql concurrent networkauth webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

## must check common.h curr_version
win32:VERSION = 1.0.0   # major.minor.patch.build
else:VERSION = 1.0.0    # major.minor.patch
DEFINES += "SAARING_VERSION=\"\\\"1.0.0\\\"\""

linux {
    DEFINES += _GLIBCXX_USE_CXX11_ABI=0
}

!win32 {
    QMAKE_CXXFLAGS += -std=gnu++11
}

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    mainwindowCallback.cpp \

HEADERS += \
    mainwindow.h \
    saaringglobal.h \

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += $$PWD/../../../../library/include

win32 {
    QMAKE_TARGET_COMPANY = "Mirinesoft Co., Ltd."
    QMAKE_TARGET_DESCRIPTION = "Free file sharing service, Saaring."
    QMAKE_TARGET_COPYRIGHT = "Copyright (C) 2012 Mirinesoft Co., Ltd. All rights reserved."
}

win32 {
    LIBS += winmm.lib Shell32.lib Iphlpapi.lib
    LIBS += -luser32 -lOle32 -lWs2_32

    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += _AMD64_
        message("_AMD64_");
    } else {
        DEFINES += _X86_
        message("_X86_");
    }

    # for firebase
    DEFINES += FIREBASE_CPP_SDK_DIR=D:/firebase/firebase_cpp_sdk
    FIREBASE_HOME = D:/firebase/firebase_cpp_sdk
    message($${FIREBASE_HOME}/libs/windows/VS2019/MD/x64/debug/firebase_app.lib)

    INCLUDEPATH += $${FIREBASE_HOME}/include

    CONFIG(release, debug|release) {
        contains(QMAKE_TARGET.arch, x86_64) {
            LIBS += -L$${FIREBASE_HOME}/libs/windows/VS2019/MD/x64/release/
        } else {
            LIBS += -L$${FIREBASE_HOME}/libs/windows/VS2019/MD/x86/release/
        }
    }
    CONFIG(debug, debug|release) {
        contains(QMAKE_TARGET.arch, x86_64) {
            LIBS += -L$${FIREBASE_HOME}/libs/windows/VS2019/MD/x64/debug/
        } else {
            LIBS += -L$${FIREBASE_HOME}/libs/windows/VS2019/MD/x86/debug/
        }
    }
    LIBS += -lfirebase_app -lfirebase_auth
    LIBS += -ladvapi32 -lws2_32 -lcrypt32
}


linux {
    FIREBASE_HOME = /home/kojang/work/firebase_cpp_sdk
    message($${FIREBASE_HOME}/libs/linux/x86_64/libfirebase_dynamic_links.a)
    message(TARGET:$$QMAKE_TARGET)
    message(TARGET:$$QMAKE_TARGET.arch)

    INCLUDEPATH += $${FIREBASE_HOME}/include

    CONFIG(release, debug|release) {
        contains(QMAKE_TARGET.arch, x86_64) {
            LIBS += -L$${FIREBASE_HOME}/libs/linux/x86_64/

        } else {
            # LIBS += -L$${FIREBASE_HOME}/libs/linux/i386/
            LIBS += -L$${FIREBASE_HOME}/libs/linux/x86_64/
            message("DEBUG 1")
        }
    }
    CONFIG(debug, debug|release) {
        contains(QMAKE_TARGET.arch, x86_64) {
            LIBS += -L$${FIREBASE_HOME}/libs/linux/x86_64/
        } else {
            # LIBS += -L$${FIREBASE_HOME}/libs/linux/i386/
            LIBS += -L$${FIREBASE_HOME}/libs/linux/x86_64/
            message("DEBUG 2")
        }
    }

    LIBS += -lfirebase_dynamic_links
    LIBS += -lfirebase_auth
    LIBS += -lfirebase_app
    LIBS += -lpthread

}

macx {

    FIREBASE_HOME = $$PWD/../firebase_cpp_sdk
    message($${FIREBASE_HOME}/libs/darwin/x86_64/libfirebase_dynamic_links.a)

    INCLUDEPATH += $${FIREBASE_HOME}/include
    LIBS += -lpthread

    CONFIG(release, debug|release) {
        contains(QMAKE_HOST.arch, x86_64) {
            LIBS += -L$${FIREBASE_HOME}/libs/darwin/x86_64/
            QMAKE_LFLAGS += -F$${FIREBASE_HOME}/frameworks/darwin/x86_64/
        } else {
            LIBS += -L$${FIREBASE_HOME}/libs/darwin/universal/
            QMAKE_LFLAGS += -F$${FIREBASE_HOME}/frameworks/darwin/universal/
        }
    }
    CONFIG(debug, debug|release) {
        contains(QMAKE_HOST.arch, x86_64) {
            LIBS += -L$${FIREBASE_HOME}/libs/darwin/x86_64/
            QMAKE_LFLAGS += -F$${FIREBASE_HOME}/frameworks/darwin/x86_64/
        } else {
            LIBS += -L$${FIREBASE_HOME}/libs/darwin/universal/
            QMAKE_LFLAGS += -F$${FIREBASE_HOME}/frameworks/darwin/universal/
        }
    }
    # LIBS += -lfirebase_dynamic_links
    # LIBS += -lfirebase_auth
    # LIBS += -lfirebase_app

    LIBS += -framework CoreFoundation
    LIBS += -framework Foundation
    LIBS += -framework Security

    LIBS += -framework firebase
    LIBS += -framework firebase_auth
    LIBS += -framework firebase_dynamic_links
}

message(TARGET=$$QMAKE_HOST.arch)
message(TARGET=$$QMAKE_HOST)
message("DEBUG = $${LIBS}")
