#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QJsonObject>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void MainWindow::on_buttonConnectSignaling_clicked()
{
    saaring::SaaringMemberInfo saaringMember;

    // TODO
    // Firebase 로그인을 통해 firebase token을 받아야 합니다. firebase token은 매번 변경되기 때문에, 매번 정상적으로 로그인해서 얻어와야 합니다.
    // Firebase Auth.를 통한 로그인 방법은 다음 블로그의 내용을 참고하세요.
    // https://blog.naver.com/kojang74
    // firebase email/password 로그인시, email 인증여부는 꼭 체크 해야 합니다.
    saaringMember.firebaseToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjJjZGFiZDIwNzVjODQxNDI0NDY3MTNlM2U0NGU5ZDcxOGU3YzJkYjQiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoi7J6l6rec7JikIiwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL3NhYXJpbmctZDc1ODgiLCJhdWQiOiJzYWFyaW5nLWQ3NTg4IiwiYXV0aF90aW1lIjoxNjMwNjU4Mjc0LCJ1c2VyX2lkIjoiY200TEt3RmtaRFlQTmR5eTNYenZHTEdQVWtIMyIsInN1YiI6ImNtNExLd0ZrWkRZUE5keXkzWHp2R0xHUFVrSDMiLCJpYXQiOjE2MzA2NTgyNzQsImV4cCI6MTYzMDY2MTg3NCwiZW1haWwiOiJrb2phbmc3NEBuYXZlci5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJrb2phbmc3NEBuYXZlci5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.DJtWYc8_Nft23FWWI40qjvImljY1hYfHhwLtbjHm9K9SPNwrVmUAXB0pxf3aNTeZmXH_vXsq6oVJ1BNHR0cmWlxDuE9vrfiL_hhhx9Fm0pbUV1OnHb93B5splgNGfvrRgnOKctDQOcMUf41qg-uNZ1taIVQcZnE69vhpPJGZPHCZ8B_2hs12RbrER_MGEflEwDBUKH8pgRcUrLNwzebTqf8e93Df4WcrgxA3KVtyiAK-DK8eN8j1XM_Tcl-UQ68s_7VYAjrKv6LUvWlX6Xnwm3pg1PFZuY16txE8ZIUwMFLgTm25T7ySmw-2YC3I4ixDsRmkDBmBCvIqv-XSu5TmDg";
    saaringMember.email         = "kojang74@naver.com";
    saaringMember.password      = "1234!@#$";
    saaringMember.userName      = "kojang74";
    saaringMember.idType        = (int)saaring::SaaringIdType::PASSWORD;
    saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_WINDOWS;

    QString resultCode;
    QString resultMessage;
    QJsonObject resultData;
    bool re = saaring::g_web->loginMember(saaringMember, resultCode, resultMessage, resultData);

    if(re == true && resultCode == saaring::service_resultCode_success)
    {
        // TODO
        // saaring::g_common->checkUpgrade 통해서 버전 체크 해야 한다.
        // 버전업이 필요 없으면, 정상적인 로그인 진행한다.
        saaring::g_member.firebaseToken = saaringMember.firebaseToken;
        saaring::g_member.authKey       = saaringMember.authKey;
        saaring::g_member.userNo        = saaringMember.userNo;
        saaring::g_member.joinStatus    = saaringMember.joinStatus;

        saaring::g_member.deviceNo      = saaringMember.deviceNo;
        saaring::g_member.deviceName    = saaringMember.deviceName;
        saaring::g_member.deviceType    = saaringMember.deviceType;

        saaring::g_member.userId        = saaringMember.userId;
        saaring::g_member.userName      = saaringMember.userName;

        saaring::g_member.email         = saaringMember.email;
        saaring::g_member.phoneNumber   = saaringMember.phoneNumber;
        saaring::g_member.password      = saaringMember.password;

        saaring::g_member.firstName     = saaringMember.firstName;
        saaring::g_member.lastName      = saaringMember.lastName;

        saaring::g_member.profilePic    = saaringMember.profilePic;
        saaring::g_member.message       = saaringMember.message;

        saaring::g_member.idType        = (int)saaring::SaaringIdType::PASSWORD;
        saaring::g_member.providerId    = saaringMember.providerId;
        saaring::g_member.idToken       = saaringMember.idToken;
        saaring::g_member.accessToken   = saaringMember.accessToken;
        saaring::g_member.uid           = saaringMember.uid;
        saaring::g_member.providerYN    = saaringMember.providerYN;
        saaring::g_member.numProvider   = saaringMember.numProvider;

        qDebug() << "============================================================================";
        qDebug() << "saaring::g_member.authKey=" << saaring::g_member.authKey;
        qDebug() << "saaring::g_member.language=" << saaring::g_member.language;

        // p2p 엔진 초기화
        saaring::g_p2p->initP2p(this, p2pCallback, saaring::g_member);

        // signaling server 접속 & 로그인 시도.
        // 로그인 성공시 onSignalingConnected()에서 후속 작업 처리
        QString authKey     = saaring::g_member.authKey;
        bool re = saaring::g_p2p->doSignalingConnect(authKey, false);
        qDebug() << "re=" << re;
    }
    else
    {
        if(resultCode == saaring::service_resultCode_error)
        {
            saaring::g_common->showMessageBox(resultMessage);
        }
        else
        {
            saaring::g_common->showMessageBox(QWEBSERVER_ERROR_MESSAGE);
        }
    }

}

void MainWindow::on_buttonGetShareFolderList_clicked()
{
    // 내 전체 공유폴더 목록 불러와서 세팅
    QString resultCode;
    QString resultMessage;
    QJsonObject resultData;

    QList<saaring::SaaringShareFolderInfo>      listFolder;
    bool re = saaring::g_web->listFolderAll(saaring::g_member.authKey, resultCode, resultMessage, resultData, listFolder);
    qDebug() << "re=" << re;
}

void MainWindow::on_buttonConnectToP2p_clicked()
{
    // 공유폴더에 P2P 접속
    QString myName = "Me";

    // TODO
    // 샤링 Desktop에서 공유폴더를 추가 하고,
    // listFolderAll 에서 읽어온 공유폴더중 한곳으로 접속한다.
    qint64 toUserNo                             = 47;
    qint64 toDeviceNo                           = 180;
    saaring::SaaringDeviceType toDeviceType     = saaring::SaaringDeviceType::SDT_WINDOWS;
    QString toDeviceName                        = "My Device";
    qint64 toServerFolderNo                     = 424;
    QString folderName                          = "My Folder";
    bool mustWakeUp                             = false;

    saaring::g_p2p->doP2pConnectShareFolder(toUserNo, myName,
                                            toDeviceNo, toDeviceType, toDeviceName,
                                            toServerFolderNo, folderName,
                                            mustWakeUp);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

