#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "saaring.h"
#include "saaringapi.h"
#include "saaringglobal.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_buttonConnectSignaling_clicked();
    void on_buttonGetShareFolderList_clicked();
    void on_buttonConnectToP2p_clicked();

    // for shared folder callback
    void onSharedFolderChanged();

    // for signaling & p2p callback
    // Signaling Server 접속 처리 관련
    void onSignalingConnected();
    void onSignalingClosed();
    void onSignalingLoginFailed();

    void onSignalingNotifyDeviceLiveYN(qint64 fromUserNo, qint64 fromDeviceNo, QString liveYN);
    void onSignalingNotifyRequestFriend(QString friendName);
    void onSignalingNotifyAddFriend(QString friendName, qint64 friendUserNo);

    void onPortmapFinished();
    void onP2pLoginFinished(qint64 friendDeviceNo, qint64 toRemoteShareNo);
    void onP2pWakeUpFinished(bool isSuccess);

    void onP2pFinishDownload(qint64 transNo, qint64 fileSize, qint64 receivedSize);
    void onP2pFinishUpload(qint64 transNo, qint64 fileSize, qint64 sentSize);
    void onP2pProgressDownload(qint64 transNo, qint64 fileSize, qint64 receivedSize);
    void onP2pProgressUpload(qint64 transNo, qint64 fileSize, qint64 sentSize);
    void onP2pAbortDownload(qint64 transNo);
    void onP2pAbortUpload(qint64 transNo);

    void onP2pFinishDownloadOther(qint64 jobId, qint64 remoteDeviceNo);
    void onP2pFinishUploadOther(qint64 jobId, qint64 remoteDeviceNo);
    void onP2pProgressDownloadOther(qint64 jobId, qint64 remoteDeviceNo, qint64 fileSize, qint64 currSize, QString filePath, QString serverName, QString deviceName);
    void onP2pProgressUploadOther(qint64 jobId, qint64 remoteDeviceNo, qint64 fileSize, qint64 currSize, QString filePath, QString serverName, QString deviceName);

    void onP2pProcessedRequest(int processType, qint64 jobId, qint64 remoteDeviceNo, int resultCode);
    void onP2pProcessedResponse(int processType, qint64 jobId, qint64 remoteDeviceNo, int resultCode);

    void onP2pProcessResponseFolderList(QString strJson);
    void onP2pProcessResponseFolderDetail(QString strJson);
    void onP2pProcessResponseFolderDetailFinished();
    void onP2pProcessResponseFolderDetailRecursive(QString strJson);
    void onP2pProcessResponseFileUploadQuery(qint64 transNo, qint64 fileStartPosition);
    void onP2pProcessResponseFileDownloadInviteSendFilesQuery(QString strJson);
    void onP2pProcessResponseSaveSendFilesInviteInfo(QString strJson);

    void onP2pNotifyDownloadQueueStandBy(qint64 remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn);
    void onP2pNotifyDownloadQueueRelease(qint64 remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn);
    void onP2pNotifyUploadQueueStandBy(qint64 remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn);
    void onP2pNotifyUploadQueueRelease(qint64 remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn);

    void onP2pNotifyConnectionClosed(qint64 remoteDeviceNo);
    void onP2pNotifyStartNextTransfer(qint64 remoteDeviceNo);
    void onP2pNotifyStatusMessage(QString text);

private:
    /** for file monitor & share dir thread callback */
    static void threadShareDirCallback(void *context, int command, QStringList &param);
    /** for signaling & p2p callback */
    static void p2pCallback(void *context, int command, QStringList &param);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
