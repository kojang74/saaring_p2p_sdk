﻿#ifndef __SAARING_GLOBAL_H__
#define __SAARING_GLOBAL_H__

#include "saaring.h"
#include "api/saaringdomain.h"

// Global static 변수
namespace saaring {

extern saaring::SaaringMemberInfo g_member;
extern QString g_saaringCurrVersion;

extern QString g_saaringApiDomain;
extern QString g_saaringImageDomain;
extern QString g_saaringServiceDomain;

} // namespace ssaring

#endif // __SAARING_GLOBAL_H__
