#include "mainwindow.h"

#include "saaring.h"
#include "saaringapi.h"

#include <QApplication>
#include <QLibrary>
#include <QDebug>

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

namespace saaring {

SaaringCommonInterface* g_common            = Q_NULLPTR;
SaaringDbInterface* g_db                    = Q_NULLPTR;
SaaringP2pInterface* g_p2p                  = Q_NULLPTR;
SaaringShareFolderInterface* g_shareFolder  = Q_NULLPTR;
SaaringWebInterface* g_web                  = Q_NULLPTR;

// Global static 변수
SaaringMemberInfo g_member;

QString g_saaringCurrVersion                = SAARING_VERSION;

QString g_saaringApiDomain                  = "";
QString g_saaringImageDomain                = "";
QString g_saaringServiceDomain              = "";

} // namespace ssaring

enum SAARING_RUNNING_MODE {
    PRODUCTION = 0,
    REAL=99999999,
    TEST=99999998,
    DEV=99999997
};
int g_runningMode = SAARING_RUNNING_MODE::PRODUCTION;

QLibrary *g_library = Q_NULLPTR;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void initLibrary()
{
    qDebug("");

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // TODO : DLL 파일이 있는 폴더 path를 맞춰주세요.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef Q_OS_WIN32
    QString pwd = QDir::currentPath() + "/../../../../library/lib/debug/x64";
    qDebug() << "pwd=" << pwd;

    QString filePath = pwd+"/sanet.dll";
    qDebug() << "filePath=" << filePath;

    QFile file(filePath);
    qDebug() << "file.exists()=" << file.exists();
    qDebug() << "QLibrary::isLibrary()=" << QLibrary::isLibrary(filePath);

    qDebug() << "";
    QLibrary library(filePath);

#elif defined(Q_OS_OSX)
    QString pwd = QDir::currentPath() + "/../../../../library/lib/debug/osx";
    qDebug() << "pwd=" << pwd;

    QLibrary library("libsanet.dylib");
#else
    QString pwd = QDir::currentPath() + "/../../../../library/lib/debug/linux";
    qDebug() << "pwd=" << pwd;
    QLibrary library(pwd+"/libsanet.so");
#endif

    if(!library.load())
    {
        qDebug() << library.errorString();
        QApplication::exit(0);
    }
    else
    {
        qDebug() << "library loaded...";
    }
    g_library = &library;

    // create classes
    typedef saaring::SaaringCommonInterface *(*CreateCommon)(int type);
    typedef saaring::SaaringDbInterface *(*CreateDb)(int type);
    typedef saaring::SaaringP2pInterface *(*CreateP2p)(int type);
    typedef saaring::SaaringShareFolderInterface *(*CreateShareFolder)();
    typedef saaring::SaaringWebInterface *(*CreateWeb)(QString apiDomain);

    qDebug() << "g_runningMode=" << g_runningMode;

    CreateCommon createCommon = (CreateCommon)library.resolve("Common");
    if(createCommon != Q_NULLPTR)
    {
        qDebug("createCommon Good...");
        saaring::g_common = createCommon(g_runningMode);
        if(saaring::g_common == Q_NULLPTR)
        {
            qDebug("g_common Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createCommon Bad...");
        QApplication::exit(0);
    }

    // domain 정보 세팅
    // 중요함.
    saaring::g_saaringApiDomain         = saaring::g_common->getSaaringApiDomain();
    saaring::g_saaringImageDomain       = saaring::g_common->getSaaringImageDomain();
    saaring::g_saaringServiceDomain     = saaring::g_common->getSaaringServiceDomain();
    qDebug() << "saaring::g_saaringApiDomain=" << saaring::g_saaringApiDomain;
    qDebug() << "saaring::g_saaringImageDomain=" << saaring::g_saaringImageDomain;
    qDebug() << "saaring::g_saaringApiDomain=" << saaring::g_saaringServiceDomain;

    CreateDb createDb = (CreateDb)library.resolve("Db");
    if(createDb != Q_NULLPTR)
    {
        qDebug("createDb Good...");
        saaring::g_db = createDb(g_runningMode);
        if(saaring::g_db == Q_NULLPTR)
        {
            qDebug("g_db Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createDb Bad...");
        QApplication::exit(0);
    }

    CreateP2p createP2p = (CreateP2p)library.resolve("P2p");
    if(createP2p != Q_NULLPTR)
    {
        qDebug("createP2p Good...");
        saaring::g_p2p = createP2p(g_runningMode);
        if(saaring::g_p2p == Q_NULLPTR)
        {
            qDebug("g_P2p Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createP2p Bad...");
        QApplication::exit(0);
    }

    CreateShareFolder createShareFolder = (CreateShareFolder)library.resolve("ShareFolder");
    if(createShareFolder != Q_NULLPTR)
    {
        qDebug("createShareFolder Good...");
        saaring::g_shareFolder = createShareFolder();
        if(saaring::g_shareFolder == Q_NULLPTR)
        {
            qDebug("g_ShareFolder Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createShareFolder Bad...");
        QApplication::exit(0);
    }

    CreateWeb createWeb = (CreateWeb)library.resolve("Web");
    if(createWeb != Q_NULLPTR)
    {
        qDebug("createWeb Good...");
        saaring::g_web = createWeb(saaring::g_saaringApiDomain);
        if(saaring::g_web == Q_NULLPTR)
        {
            qDebug("g_Web Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createWeb Bad...");
        QApplication::exit(0);
    }
}

void releaseLibrary()
{
    if(saaring::g_common != Q_NULLPTR)
    {
        delete saaring::g_common;
        *(&saaring::g_common) = Q_NULLPTR;
    }
    if(saaring::g_db != Q_NULLPTR)
    {
        delete saaring::g_db;
        *(&saaring::g_db) = Q_NULLPTR;
    }
    if(saaring::g_p2p != Q_NULLPTR)
    {
        delete saaring::g_p2p;
        *(&saaring::g_p2p) = Q_NULLPTR;
    }
    if(saaring::g_shareFolder != Q_NULLPTR)
    {
        delete saaring::g_shareFolder;
        *(&saaring::g_shareFolder) = Q_NULLPTR;
    }
    if(saaring::g_web != Q_NULLPTR)
    {
        delete saaring::g_web;
        *(&saaring::g_web) = Q_NULLPTR;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////


int main(int argc, char *argv[])
{
    // for log
    qSetMessagePattern("[%{time hh:mm:ss.zzz}:%{function}:%{line}] - %{message}");

    QApplication a(argc, argv);

    // init library
    initLibrary();

    MainWindow w;
    w.show();
    int result = a.exec();

    // release library
    releaseLibrary();

    return result;
}
