//
//  SaaringService.hpp
//  saaring
//
//  Created by kojang on 28/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//

#ifndef SaaringService_hpp
#define SaaringService_hpp

#include <QApplication>

#include "saaring.h"
#include "saaringapi.h"

#include <objc/objc.h>
#include <dispatch/dispatch.h>

class SaaringService : public QApplication
{
private:
    static void initLibrary();
    static void releaseLibrary();
    
    /** for file monitor & share dir thread callback */
    static void threadShareDirCallback(void *context, int command, QStringList &param);
    /** for signaling & p2p callback */
    static void p2pCallback(void *context, int command, QStringList &param);
    /** for test callback */
    static void testCallback(void *context, int command, QStringList &param);

    void (^blockForThreadShareDirCallback)(int, QStringList &);
    void (^blockForP2pCallback)(int, QStringList &);
    void (^blockForTestCallback)(int, QStringList &);
    
public:
    SaaringService();
    ~SaaringService();
    
    // for share folder
    void initThreadShareDir(void (^block)(int, QStringList &));
    // for p2p
    void initP2p(void (^block)(int, QStringList &));
    // for test
    void initTest(void (^block)(int, QStringList &));
    void testCallback();

    saaring::SaaringCommonInterface* getCommon();
    saaring::SaaringDbInterface* getDb();
    saaring::SaaringP2pInterface* getP2p();
    saaring::SaaringShareFolderInterface* getShareFolder();
    saaring::SaaringWebInterface* getWeb();

    saaring::SaaringMemberInfo* getMember();
};

#endif /* SaaringService_hpp */
