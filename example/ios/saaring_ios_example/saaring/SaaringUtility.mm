//
//  SaaringUtility.m
//  saaring
//
//  Created by kojang on 31/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <QJsonObject>
#include <QJsonDocument>

#include "SaaringUtility.hpp"

//////////////////////////////////////////////////////////////////////
// Saaring Utility
//////////////////////////////////////////////////////////////////////
@implementation SaaringUtility

+ (QDateTime) convertLongLongToQDateTime : (long long)timestamp {
    
    return QDateTime::fromMSecsSinceEpoch((qint64)timestamp);
}
+ (long long) convertQDateTimeToLongLong : (QDateTime)dateTime {
    
    return dateTime.toMSecsSinceEpoch();
}

+ (QString) convertNSStringToQString : (NSString *)string {
    
    return QString::fromUtf8([string UTF8String]);
}
+ (NSString *) convertQStringToNSString : (QString)string {
    
    return [NSString stringWithUTF8String:string.toStdString().c_str()];
}
+ (NSString *) convertQJsonObjectToNSString : (QJsonObject)json {

    QJsonDocument doc(json);
    QString strSource(doc.toJson(QJsonDocument::Compact));

    return [SaaringUtility convertQStringToNSString:strSource];
}

+ (void) convertQListToNSMutableArrayLongLong : (QList<qint64> *)source dest:(NSMutableArray *)dest {
    
    [dest removeAllObjects];
    for(int i=0 ; i<source->size() ; i++) {
        [dest addObject:[NSNumber numberWithLongLong:source->at(i)]];
    }
}
+ (void) convertNSMutableArrayToQListLongLong : (NSMutableArray *)source dest:(QList<qint64> *)dest {
    
    dest->clear();
    for(NSNumber *item in source) {
        dest->append([item longLongValue]);
    }
}

+ (void) convertQListToNSMutableArrayString : (QStringList *)source dest:(NSMutableArray *)dest {
    
    [dest removeAllObjects];
    for(int i=0 ; i<source->size() ; i++) {
        [dest addObject:[SaaringUtility convertQStringToNSString:source->at(i)]];
    }
}
+ (void) convertNSMutableArrayToQListString : (NSMutableArray *)source dest:(QStringList *)dest {
 
    dest->clear();
    for(NSString *item in source) {
        dest->append([SaaringUtility convertNSStringToQString:item]);
    }
}


@end
