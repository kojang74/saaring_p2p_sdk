//
//  SaaringWrapper.mm
//  qttest
//
//  Created by kojang on 23/01/2019.
//  Copyright © 2019 kojang. All rights reserved.
//
#import "SaaringWrapper.h"

#include "api/saaringcommoninterface.h"
#include "api/saaringdbinterface.h"
#include "api/saaringdomain.h"
#include "api/saaringp2pinterface.h"
#include "api/saaringsharefolderinterface.h"
#include "api/saaringwebinterface.h"

#include "SaaringService.hpp"
#include "SaaringUtility.hpp"

#include <QVariant>
#include <QStringList>
#include <QJsonObject>

#import <Foundation/Foundation.h>

//////////////////////////////////////////////////////////////////////
// for private utility
//////////////////////////////////////////////////////////////////////

@interface SaaringWrapper()

//////////////////////////////////////////////////////////////////////
// for callback
//////////////////////////////////////////////////////////////////////
- (void) threadShareDirCallback : (int)command withParam:(QStringList &)param;
- (void) p2pCallback : (int)command withParam:(QStringList &)param;
- (void) testCallback : (int)command withParam:(QStringList &)param;

@property void (^threadShareDirCallbackBlock)(int, QStringList &);
@property void (^p2pCallbackBlock)(int, QStringList &);
@property void (^testCallbackBlock)(int, QStringList &);

@property id threadShareDirCallbackSwiftContext;
@property id p2pCallbackSwiftContext;
@property id testCallbackSwiftContext;

//////////////////////////////////////////////////////////////////////
// for saaring interface
//////////////////////////////////////////////////////////////////////
@property SaaringService *saaringService;

@property saaring::SaaringCommonInterface *common;
@property saaring::SaaringDbInterface *db;
@property saaring::SaaringP2pInterface *p2p;
@property saaring::SaaringShareFolderInterface *shareFolder;
@property saaring::SaaringWebInterface *web;

@property saaring::SaaringMemberInfo *member;

@end

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

@implementation SaaringWrapper

//////////////////////////////////////////////////////////////////////
// for init
//////////////////////////////////////////////////////////////////////

static SaaringWrapper *instance = nil;

+ (SaaringWrapper *) getInstance {
    @synchronized (self) {
        if(instance == nil) {
            instance = [[SaaringWrapper alloc] initSaaring];
        }
    }
    return instance;
}

- (id)initSaaring {
    
    self = [super init];
    
    if(self) {
        self.saaringService         = new SaaringService();
        
        self.threadShareDirCallbackBlock = ^(int command, QStringList &param){
            [self threadShareDirCallback:command withParam:param];
        };
        self.p2pCallbackBlock = ^(int command, QStringList &param){
            [self p2pCallback:command withParam:param];
        };
        self.testCallbackBlock = ^(int command, QStringList &param){
            [self testCallback:command withParam:param];
        };

        self.common                 = self.saaringService->getCommon();
        self.db                     = self.saaringService->getDb();
        self.p2p                    = self.saaringService->getP2p();
        self.shareFolder            = self.saaringService->getShareFolder();
        self.web                    = self.saaringService->getWeb();
        self.member                 = self.saaringService->getMember();
        
    }

    return self;
}

- (void)exec {
    
    self.saaringService->exec();
}

- (void)dealloc {
    
    delete self.saaringService;
}

//////////////////////////////////////////////////////////////////////
// for common interface
//////////////////////////////////////////////////////////////////////
- (double)calSpeed:(long long)oldTime
          currTime:(long long)currTime
           oldSize:(long long)oldSize
          currSize:(long long)currSize
{
    QDateTime paramOldTime      = [SaaringUtility convertLongLongToQDateTime:oldTime];
    QDateTime paramCurrTime     = [SaaringUtility convertLongLongToQDateTime:currTime];
    
    return self.common->calSpeed(paramOldTime, paramCurrTime, (qint64)oldSize, (qint64)currSize);
}

- (NSString *)leftTime:(long long)oldTime
              currTime:(long long)currTime
               oldSize:(long long)oldSize
              currSize:(long long)currSize
             totalSize:(long long)totalSize
                 speed:(double)speed
{
    QDateTime paramOldTime      = [SaaringUtility convertLongLongToQDateTime:oldTime];
    QDateTime paramCurrTime     = [SaaringUtility convertLongLongToQDateTime:currTime];

    QString result = self.common->leftTime(paramOldTime, paramCurrTime, (qint64)oldSize, (qint64)currSize, (qint64)totalSize, (double)speed);
    return [SaaringUtility convertQStringToNSString:result];
}

- (bool)checkEmail:(NSString *)email
{
    QString paramEmail = QString::fromUtf8([email UTF8String]);
    
    return self.common->checkEmail(paramEmail);
}

- (NSString *)getDuplicationFilename:(NSString *)filePath
                   lastLocalFilePath:(NSString *)lastLocalFilePath
{
    QString paramFilePath           = [SaaringUtility convertNSStringToQString:filePath];
    QString paramLastLocalFilePath  = [SaaringUtility convertNSStringToQString:lastLocalFilePath];
    
    QString result = self.common->getDuplicationFilename(paramFilePath, paramLastLocalFilePath);
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)numberFormatWithComma:(unsigned long long)number
{
    QString result = self.common->numberFormatWithComma((quint64)number, ",");
    return [SaaringUtility convertQStringToNSString:result];
}

- (int)checkUpgrade:(NSString *)lastVer
         runningVer:(NSString *)runningVer
         currentVer:(NSString *)currentVer
{
    QString paramLastVer            = [SaaringUtility convertNSStringToQString:lastVer];
    QString paramRunningVer         = [SaaringUtility convertNSStringToQString:runningVer];
    QString paramCurrentVer         = [SaaringUtility convertNSStringToQString:currentVer];
    
    return self.common->checkUpgrade(paramLastVer, paramRunningVer, paramCurrentVer);
}

- (NSString *)getSizeWithUnit:(long long)size
{
    QString result = self.common->getSizeWithUnit((qint64)size);
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)changeProfileImage:(NSString *)authKey
                            path:(NSString *)path
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramPath               = [SaaringUtility convertNSStringToQString:path];
    
    QString result = self.common->changeProfileImage(paramAuthKey, paramPath);
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)getShareTopFolder
{
    QString result = self.common->getShareTopFolder();
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)getDownloadFolder
{
    QString result = self.common->getDownloadFolder();
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)removeFirstPath:(NSString *)path
{
    QString paramPath               = [SaaringUtility convertNSStringToQString:path];
    
    QString result = self.common->removeFirstPath(paramPath);
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)getProviderId:(int)idType
{
    QString result = self.common->getProviderId((saaring::SaaringIdType)idType);
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)getProviderName:(int)idType
{
    QString result = self.common->getProviderName((saaring::SaaringIdType)idType);
    return [SaaringUtility convertQStringToNSString:result];
}

- (NSString *)getProviderNameByProviderId:(NSString *)providerId:(NSString *)providerId
{
    QString paramProviderId         = [SaaringUtility convertNSStringToQString:providerId];
    
    QString result = self.common->getProviderNameByProviderId(paramProviderId);
    return [SaaringUtility convertQStringToNSString:result];
}

//////////////////////////////////////////////////////////////////////
// for db interface
//////////////////////////////////////////////////////////////////////

// for all
- (bool) isOpen
{
    return self.db->isOpen();
}

// for login
- (void) doAfterLogin:(int)idType
               email:(NSString *)email
         phoneNumber:(NSString *)phoneNumber
            password:(NSString *)password
          providerId:(NSString *)providerId
             idToken:(NSString *)idToken
         accessToken:(NSString *)accessToken
                 uid:(NSString *)uid
         isSaveEmail:(NSString *)isSaveEmail
         isAutoLogin:(NSString *)isAutoLogin
{
    QString paramEmail          = [SaaringUtility convertNSStringToQString:email];
    QString paramPhoneNumber    = [SaaringUtility convertNSStringToQString:phoneNumber];
    QString paramPassword       = [SaaringUtility convertNSStringToQString:password];
    QString paramProviderId     = [SaaringUtility convertNSStringToQString:providerId];
    QString paramIdToken        = [SaaringUtility convertNSStringToQString:idToken];
    QString paramAccessToken    = [SaaringUtility convertNSStringToQString:accessToken];
    QString paramUid            = [SaaringUtility convertNSStringToQString:uid];
    QString paramIsSaveEmail    = [SaaringUtility convertNSStringToQString:isSaveEmail];
    QString paramIsAutoLogin    = [SaaringUtility convertNSStringToQString:isAutoLogin];
    
    return self.db->doAfterLogin(idType, paramEmail, paramPhoneNumber, paramPassword,
                                 paramProviderId, paramIdToken, paramAccessToken, paramUid,
                                 paramIsSaveEmail, paramIsAutoLogin);
}

- (bool) updateLoginTb:(NSString *)idType
                email:(NSString *)email
          phoneNumber:(NSString *)phoneNumber
             password:(NSString *)password
          isSaveEmail:(NSString *)isSaveEmail
          isAutoLogin:(NSString *)isAutoLogin
     isLastLoginEmail:(NSString *)isLastLoginEmail
{
    QString paramIdType             = [SaaringUtility convertNSStringToQString:idType];
    QString paramEmail              = [SaaringUtility convertNSStringToQString:email];
    QString paramPhoneNumber        = [SaaringUtility convertNSStringToQString:phoneNumber];
    QString paramPassword           = [SaaringUtility convertNSStringToQString:password];
    QString paramIsSaveEmail        = [SaaringUtility convertNSStringToQString:isSaveEmail];
    QString paramIsAutoLogin        = [SaaringUtility convertNSStringToQString:isAutoLogin];
    QString paramIsLastLoginEmail   = [SaaringUtility convertNSStringToQString:isLastLoginEmail];
    
    return self.db->updateLoginTb(paramIdType, paramEmail, paramPhoneNumber, paramPassword, paramIsSaveEmail, paramIsAutoLogin, paramIsLastLoginEmail);
}
- (bool) updateLoginAutoStart:(NSString *)idType
                        email:(NSString *)email
                  phoneNumber:(NSString *)phoneNumber
                  isAutoStart:(NSString *)isAutoStart
{
    QString paramIdType             = [SaaringUtility convertNSStringToQString:idType];
    QString paramEmail              = [SaaringUtility convertNSStringToQString:email];
    QString paramPhoneNumber        = [SaaringUtility convertNSStringToQString:phoneNumber];
    QString paramIsAutoStart        = [SaaringUtility convertNSStringToQString:isAutoStart];
    
    return self.db->updateLoginAutoStart(paramIdType, paramEmail, paramPhoneNumber, paramIsAutoStart);
}

- (bool)updateSetSend:(SaaringSetupInfo *)saaringSetupInfo
{
    saaring::SaaringSetupInfo paramSaaringSetupInfo;
    
    [SaaringSetupInfo convertObjCSaaringSetupInfoToQt:saaringSetupInfo qt:&paramSaaringSetupInfo];
    bool result = self.db->updateSetSend(paramSaaringSetupInfo);
    [SaaringSetupInfo convertQtSaaringSetupInfoToObjC:&paramSaaringSetupInfo objc:saaringSetupInfo];
    
    return result;
}
- (bool)updateSetNetwork:(SaaringSetupInfo *)saaringSetupInfo
{
    saaring::SaaringSetupInfo paramSaaringSetupInfo;
    
    [SaaringSetupInfo convertObjCSaaringSetupInfoToQt:saaringSetupInfo qt:&paramSaaringSetupInfo];
    bool result = self.db->updateSetNetwork(paramSaaringSetupInfo);
    [SaaringSetupInfo convertQtSaaringSetupInfoToObjC:&paramSaaringSetupInfo objc:saaringSetupInfo];
    
    return result;
}

- (bool)readDbLastLogin:(SaaringSetupInfo *)saaringSetupInfo
{
    saaring::SaaringSetupInfo paramSaaringSetupInfo;
    
    [SaaringSetupInfo convertObjCSaaringSetupInfoToQt:saaringSetupInfo qt:&paramSaaringSetupInfo];
    bool result = self.db->readDbLastLogin(paramSaaringSetupInfo);
    [SaaringSetupInfo convertQtSaaringSetupInfoToObjC:&paramSaaringSetupInfo objc:saaringSetupInfo];
    
    return result;
}
- (bool) readDbByEmailOrPhoneNumber:(NSString *)idType
                              email:(NSString *)email
                        phoneNumber:(NSString *)phoneNumber
                   saaringSetupInfo:(SaaringSetupInfo *)saaringSetupInfo
{
    saaring::SaaringSetupInfo paramSaaringSetupInfo;
    QString paramIdType             = [SaaringUtility convertNSStringToQString:idType];
    QString paramEmail              = [SaaringUtility convertNSStringToQString:email];
    QString paramPhoneNumber        = [SaaringUtility convertNSStringToQString:phoneNumber];

    [SaaringSetupInfo convertObjCSaaringSetupInfoToQt:saaringSetupInfo qt:&paramSaaringSetupInfo];
    bool result = self.db->readDbByEmailOrPhoneNumber(paramIdType, paramEmail, paramPhoneNumber, paramSaaringSetupInfo);
    [SaaringSetupInfo convertQtSaaringSetupInfoToObjC:&paramSaaringSetupInfo objc:saaringSetupInfo];
    
    return result;
}

//// for transfer
- (long long)insertMyTransTbOne:(SaaringTransferBaseInfo *)saaringTransfer
{
    saaring::SaaringTransferBaseInfo paramSaaringTransfer;
    
    [SaaringTransferBaseInfo convertObjCSaaringTransferBaseInfoToQt:saaringTransfer qt:&paramSaaringTransfer];
    return self.db->insertMyTransTbOne(paramSaaringTransfer);
}
- (bool)insertMyTransTbMulti:(NSMutableArray *)listSaaringTransfer
{
    QList<saaring::SaaringTransferBaseInfo> paramListSaaringTransferBaseInfo;
    for(SaaringTransferBaseInfo *item in listSaaringTransfer) {
        saaring::SaaringTransferBaseInfo paramSaaringTransfer;
        
        [SaaringTransferBaseInfo convertObjCSaaringTransferBaseInfoToQt:item qt:&paramSaaringTransfer];
        paramListSaaringTransferBaseInfo.append(paramSaaringTransfer);
    }
    
    return self.db->insertMyTransTbMulti(paramListSaaringTransferBaseInfo);
}

- (bool)updateMyTransTb:(SaaringTransferBaseInfo *)saaringTransfer
{
    saaring::SaaringTransferBaseInfo paramSaaringTransfer;
    
    [SaaringTransferBaseInfo convertObjCSaaringTransferBaseInfoToQt:saaringTransfer qt:&paramSaaringTransfer];
    return self.db->updateMyTransTb(paramSaaringTransfer);
}
- (bool)updateMyTransTbStatus:(long long)transNo
                  transStatus:(int)transStatus
                   updateTime:(long long)updateTime
{
    QDateTime paramUpdateTime      = [SaaringUtility convertLongLongToQDateTime:updateTime];

    return self.db->updateMyTransTbStatus(transNo, transStatus, paramUpdateTime);
}

- (bool)updateMyTransTbSortNoMulti:(NSMutableArray *)listTransNo
                        updateTime:(long long)updateTime
{
    QDateTime paramUpdateTime      = [SaaringUtility convertLongLongToQDateTime:updateTime];
    
    QList<qint64> paramListTransNo;
    [SaaringUtility convertNSMutableArrayToQListLongLong:listTransNo dest:&paramListTransNo];
    
    return self.db->updateMyTransTbSortNoMulti(paramListTransNo, paramUpdateTime);
}

- (bool)updateMyTransTbProgress:(long long)transNo
                       fileSize:(long long)fileSize
                      transSize:(long long)transSize
                     updateTime:(long long)updateTime
{
    QDateTime paramUpdateTime      = [SaaringUtility convertLongLongToQDateTime:updateTime];
    
    return self.db->updateMyTransTbProgress(transNo, fileSize, transSize, paramUpdateTime);
}
- (bool)updateMyTransTbInit:(long long)myDeviceNo
{
    
    return self.db->updateMyTransTbInit(myDeviceNo);
}

- (bool)deleteMyTransTbOne:(long long)transNo
{
    return self.db->deleteMyTransTbOne(transNo);
}
- (bool)deleteMyTransTbMulti:(NSMutableArray *)listTransNo
{
    QList<qint64> paramListTransNo;
    [SaaringUtility convertNSMutableArrayToQListLongLong:listTransNo dest:&paramListTransNo];
    
    return self.db->deleteMyTransTbMulti(paramListTransNo);
}
- (bool)deleteMyTransTbAll:(long long)deviceNo
{
    return self.db->deleteMyTransTbAll(deviceNo);
}

- (bool)readMyTransTbByTransNo:(NSMutableArray *)listSaaringTransfer
                       transNo:(long long)transNo
{
    QList<saaring::SaaringTransferBaseInfo> paramListSaaringTransferBaseInfo;

    bool result = self.db->readMyTransTbByTransNo(paramListSaaringTransferBaseInfo, transNo);

    for(saaring::SaaringTransferBaseInfo item : paramListSaaringTransferBaseInfo) {
        SaaringTransferBaseInfo *paramSaaringTransfer = [[SaaringTransferBaseInfo alloc] init];
        
        [SaaringTransferBaseInfo convertQtSaaringTransferBaseInfoToObjC:&item objc:paramSaaringTransfer];
        
        [listSaaringTransfer addObject:paramSaaringTransfer];
    }
    
    return result;
}
- (bool)readMyTransTbTransferring:(NSMutableArray *)listSaaringTransfer
                       myDeviceNo:(long long)myDeviceNo
                         deleteYN:(NSString *)deleteYN
                         deviceNo:(long long)deviceNo
{
    QString paramDeleteYN            = [SaaringUtility convertNSStringToQString:deleteYN];

    QList<saaring::SaaringTransferBaseInfo> paramListSaaringTransferBaseInfo;

    bool result = self.db->readMyTransTbTransferring(paramListSaaringTransferBaseInfo, myDeviceNo, paramDeleteYN, deviceNo);

    for(saaring::SaaringTransferBaseInfo item : paramListSaaringTransferBaseInfo) {
        SaaringTransferBaseInfo *paramSaaringTransfer = [[SaaringTransferBaseInfo alloc] init];
        
        [SaaringTransferBaseInfo convertQtSaaringTransferBaseInfoToObjC:&item objc:paramSaaringTransfer];
        
        [listSaaringTransfer addObject:paramSaaringTransfer];
    }
    
    return result;
}
- (bool)readMyTransTbNotTransferringLastOne:(NSMutableArray *)listSaaringTransfer
                                 myDeviceNo:(long long)myDeviceNo
                                   deleteYN:(NSString *)deleteYN
                                   deviceNo:(long long)deviceNo
{
    QString paramDeleteYN            = [SaaringUtility convertNSStringToQString:deleteYN];

    QList<saaring::SaaringTransferBaseInfo> paramListSaaringTransferBaseInfo;

    bool result = self.db->readMyTransTbNotTransferringLastOne(paramListSaaringTransferBaseInfo, myDeviceNo, paramDeleteYN, deviceNo);

    for(saaring::SaaringTransferBaseInfo item : paramListSaaringTransferBaseInfo) {
        SaaringTransferBaseInfo *paramSaaringTransfer = [[SaaringTransferBaseInfo alloc] init];
        
        [SaaringTransferBaseInfo convertQtSaaringTransferBaseInfoToObjC:&item objc:paramSaaringTransfer];
        
        [listSaaringTransfer addObject:paramSaaringTransfer];
    }
    
    return result;
}
- (bool)readMyTransTbInit:(NSMutableArray *)listSaaringTransfer
               myDeviceNo:(long long)myDeviceNo
{
    QList<saaring::SaaringTransferBaseInfo> paramListSaaringTransferBaseInfo;

    bool result = self.db->readMyTransTbInit(paramListSaaringTransferBaseInfo, myDeviceNo);

    for(saaring::SaaringTransferBaseInfo item : paramListSaaringTransferBaseInfo) {
        SaaringTransferBaseInfo *paramSaaringTransfer = [[SaaringTransferBaseInfo alloc] init];
        
        [SaaringTransferBaseInfo convertQtSaaringTransferBaseInfoToObjC:&item objc:paramSaaringTransfer];
        
        [listSaaringTransfer addObject:paramSaaringTransfer];
    }
    
    return result;
}

//// for share
- (bool)insertShareTb:(SaaringShareInfo *)saaringShare
{
    saaring::SaaringShareInfo paramSaaringShare;
    
    [SaaringShareInfo convertObjCSaaringShareInfoToQt:saaringShare qt:&paramSaaringShare];
    return self.db->insertShareTb(paramSaaringShare);
}
- (bool)updateShareTb:(SaaringShareInfo *)saaringShare
{
    saaring::SaaringShareInfo paramSaaringShare;
    
    [SaaringShareInfo convertObjCSaaringShareInfoToQt:saaringShare qt:&paramSaaringShare];
    return self.db->updateShareTb(paramSaaringShare);
}
- (bool)deleteShareTb:(long long)shareNo
{
    return self.db->deleteShareTb(shareNo);
}

- (bool)readShareTb:(long long)deviceNo
    listSaaringShare:(NSMutableArray *)listSaaringShare
{
    QList<saaring::SaaringShareInfo> paramListSaaringShare;
    
    bool result = self.db->readShareTb(deviceNo, paramListSaaringShare);
    for(saaring::SaaringShareInfo item : paramListSaaringShare) {
        SaaringShareInfo *paramSaaringShare = [[SaaringShareInfo alloc] init];
        
        [SaaringShareInfo convertQtSaaringShareInfoToObjC:&item objc:paramSaaringShare];
        
        [listSaaringShare addObject:paramSaaringShare];
    }

    return result;
}

- (long long)getShareNoByServerFolderNo:(long long)serverFolderNo
{
    return self.db->getShareNoByServerFolderNo(serverFolderNo);
}
- (long long)getServerFolderNoByShareNo:(long long)shareNo
{
    return self.db->getServerFolderNoByShareNo(shareNo);
}
- (NSString *)getShareFolderPathByServerFolderNo:(long long)serverFolderNo
{
    QString result = self.db->getShareFolderPathByServerFolderNo(serverFolderNo);
    return [SaaringUtility convertQStringToNSString:result];
}

- (int)getShareFileCntWithShareNo:(long long)deviceNo shareNo:(long long)shareNo
{
    return self.db->getShareFileCntWithShareNo(deviceNo, shareNo);
}
- (long long)getShareFileSize:(long long)deviceNo
{
    return self.db->getShareFileSize(deviceNo);
}
- (long long)getShareFileSizeWithShareNo:(long long)deviceNo shareNo:(long long)shareNo
{
    return self.db->getShareFileSizeWithShareNo(deviceNo, shareNo);
}

//// for share detail
- (bool)updateShareDetailShortPath:(long long)shareNo
                         shareName:(NSString *)shareName
               shareFolderFullPath:(NSString *)shareFolderFullPath
{
    QString paramShareName              = [SaaringUtility convertNSStringToQString:shareName];
    QString paramShareFolderFullPath    = [SaaringUtility convertNSStringToQString:shareFolderFullPath];
    
    return self.db->updateShareDetailShortPath(shareNo, paramShareName, paramShareFolderFullPath);
}

- (bool)deleteShareDetailTb:(long long)shareNo
{
    return self.db->deleteShareDetailTb(shareNo);
}

- (void)makeMemoryShareDetailTb
{
    self.db->makeMemoryShareDetailTb();
}
- (void)backupMemoryShareDetailTb
{
    self.db->backupMemoryShareDetailTb();
}

// for invite files
- (bool)insertInviteFiles:(SaaringInviteFilesInfo *)saaringInviteFilesInfo
{
    saaring::SaaringInviteFilesInfo paramSaaringInviteFilesInfo;
    
    [SaaringInviteFilesInfo convertObjCSaaringInviteFilesInfoToQt:saaringInviteFilesInfo qt:&paramSaaringInviteFilesInfo];
    return self.db->insertInviteFiles(paramSaaringInviteFilesInfo);
}
- (bool)insertListInviteFiles:(NSMutableArray *)listSaaringInviteFilesInfo
{
    QList<saaring::SaaringInviteFilesInfo> paramListSaaringInviteFilesInfo;
    for(SaaringInviteFilesInfo *item in listSaaringInviteFilesInfo) {
        saaring::SaaringInviteFilesInfo paramSaaringInviteFilesInfo;
        
        [SaaringInviteFilesInfo convertObjCSaaringInviteFilesInfoToQt:item qt:&paramSaaringInviteFilesInfo];
        paramListSaaringInviteFilesInfo.append(paramSaaringInviteFilesInfo);
    }
    
    return self.db->insertListInviteFiles(paramListSaaringInviteFilesInfo);
}

- (bool)deleteExpired
{
    return self.db->deleteExpired();
}
- (bool)deleteInviteFilesByInviteNo:(long long)inviteNo
{
    return self.db->deleteInviteFilesByInviteNo(inviteNo);
}

- (bool)listInviteFiles:(long long)inviteNo
    listSaaringInviteFilesInfo:(NSMutableArray *)listSaaringInviteFilesInfo
{
    QList<saaring::SaaringInviteFilesInfo> paramListSaaringInviteFilesInfo;

    bool result = self.db->listInviteFiles(inviteNo, paramListSaaringInviteFilesInfo);

    for(saaring::SaaringInviteFilesInfo item : paramListSaaringInviteFilesInfo) {
        SaaringInviteFilesInfo *paramSaaringInviteFilesInfo = [[SaaringInviteFilesInfo alloc] init];
        
        [SaaringInviteFilesInfo convertQtSaaringInviteFilesInfoToObjC:&item objc:paramSaaringInviteFilesInfo];
        
        [listSaaringInviteFilesInfo addObject:paramSaaringInviteFilesInfo];
    }
    
    return result;
}
- (bool)listInviteFilesWithCertKey:(long long)inviteNo
                           certKey:(NSString *)certKey
        listSaaringInviteFilesInfo:(NSMutableArray *)listSaaringInviteFilesInfo
{
    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];

    QList<saaring::SaaringInviteFilesInfo> paramListSaaringInviteFilesInfo;

    bool result = self.db->listInviteFilesWithCertKey(inviteNo, paramCertKey, paramListSaaringInviteFilesInfo);

    for(saaring::SaaringInviteFilesInfo item : paramListSaaringInviteFilesInfo) {
        SaaringInviteFilesInfo *paramSaaringInviteFilesInfo = [[SaaringInviteFilesInfo alloc] init];
        
        [SaaringInviteFilesInfo convertQtSaaringInviteFilesInfoToObjC:&item objc:paramSaaringInviteFilesInfo];
        
        [listSaaringInviteFilesInfo addObject:paramSaaringInviteFilesInfo];
    }
    
    return result;
}

//////////////////////////////////////////////////////////////////////
// for share folder interface
//////////////////////////////////////////////////////////////////////
- (void)threadShareDirCallback:(int)command withParam:(QStringList &)param
{
    NSNumber *commandObject = [NSNumber numberWithInt:command];

    NSMutableArray *paramObject = [NSMutableArray arrayWithCapacity:param.size()];
    [SaaringUtility convertQListToNSMutableArrayString:&param dest:paramObject];
    
    [self.threadShareDirCallbackSwiftContext performSelector:@selector(threadShareDirSwiftCallback:withParam:) withObject:commandObject withObject:paramObject];
}

- (void)initThreadShareDir:(id)context
{
    self.threadShareDirCallbackSwiftContext = context;
    
    self.saaringService->initThreadShareDir(self.threadShareDirCallbackBlock);
}

- (void)startThreadShareFolder
{
    self.shareFolder->startThreadShareFolder();
}
- (void)stopThreadShareFolder
{
    self.shareFolder->stopThreadShareFolder();
}

- (void)shareFolderInit
{
    self.shareFolder->shareFolderInit();
}
- (void)shareFolderInsert:(long long)shareNo
                sharePath:(NSString *)sharePath
                shareName:(NSString *)shareName
{
    QString paramSharePath            = [SaaringUtility convertNSStringToQString:sharePath];
    QString paramShareName            = [SaaringUtility convertNSStringToQString:shareName];
    
    self.shareFolder->shareFolderInsert(shareNo, paramSharePath, paramShareName);
}
- (void)shareFolderDelete:(long long)shareNo
{
    self.shareFolder->shareFolderDelete(shareNo);
}
- (void)shareFolderUpdate:(NSString *)folderPath
{
    QString paramFolderPath            = [SaaringUtility convertNSStringToQString:folderPath];
    
    self.shareFolder->shareFolderUpdate(paramFolderPath);
}
- (void)serverFolderDelete:(long long)serverFolderNo
{
    self.shareFolder->serverFolderDelete(serverFolderNo);
}

- (int)getShareFolderCount
{
    return self.shareFolder->getShareFolderCount();
}
- (int)getShareFileCount
{
    return self.shareFolder->getShareFileCount();
}

- (void)startFileMonitor
{
    self.shareFolder->startFileMonitor();
}
- (void)stopFileMonitor
{
    self.shareFolder->stopFileMonitor();
}

- (bool)doesNeedDbBackup
{
    return self.shareFolder->doesNeedDbBackup();
}

//////////////////////////////////////////////////////////////////////
// for p2p interface
//////////////////////////////////////////////////////////////////////
- (void) p2pCallback : (int)command withParam:(QStringList &)param {
    
    NSNumber *commandObject = [NSNumber numberWithInt:command];

    NSMutableArray *paramObject = [NSMutableArray arrayWithCapacity:param.size()];
    [SaaringUtility convertQListToNSMutableArrayString:&param dest:paramObject];
    
    [self.p2pCallbackSwiftContext performSelector:@selector(p2pSwiftCallback:withParam:) withObject:commandObject withObject:paramObject];
}

- (void) initP2p : (id)context {
    
    self.p2pCallbackSwiftContext = context;
    
    self.saaringService->initP2p(self.p2pCallbackBlock);
}

- (void) closeP2p {

    self.p2p->closeP2p();
}
- (bool) isP2p {

    return self.p2p->isP2p();
}

- (SaaringServerInfo *) getServerInfo {

    saaring::SaaringServerInfo paramSaaringServerInfo = self.p2p->getServerInfo();

    SaaringServerInfo *saaringServerInfo = [[SaaringServerInfo alloc] init];
    [SaaringServerInfo convertQtSaaringServerInfoToObjC:&paramSaaringServerInfo objc:saaringServerInfo];

    return saaringServerInfo;
}

- (bool) isSignalingConnected {

    return self.p2p->isSignalingConnected();
}
- (bool) isSignalingLogin {

    return self.p2p->isSignalingLogin();
}
- (bool) doSignalingConnect : (NSString *)authKey isRefreshServerInfo:(bool)isRefreshServerInfo {

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    return self.p2p->doSignalingConnect(paramAuthKey, isRefreshServerInfo);
}
- (void) doSignalingClose {

    self.p2p->doSignalingClose();
}
- (void) reqHeartBeat {

    self.p2p->reqHeartBeat();
}

- (bool) isP2pLived:(long long)deviceNo {

    return self.p2p->isP2pLived(deviceNo);
}

- (void) updateShareFolders {

    self.p2p->updateShareFolders();
}
- (void) updateNetworkSendSettings {

    self.p2p->updateNetworkSendSettings();
}

- (void) doP2pConnect:(long long)toUserNo toUserName:(NSString *)toUserName toDeviceNo:(long long)toDeviceNo toDeviceType:(int)toDeviceType toDeviceName:(NSString *)toDeviceName mustWakeUp:(bool)mustWakeUp
{

    QString paramToUserName             = [SaaringUtility convertNSStringToQString:toUserName];
    QString paramToDeviceName           = [SaaringUtility convertNSStringToQString:toDeviceName];

    self.p2p->doP2pConnect((qint64)toUserNo, paramToUserName, (qint64)toDeviceNo, (saaring::SaaringDeviceType)toDeviceType, paramToDeviceName, mustWakeUp);
}
- (void) doP2pConnectShareFolder : (long long)toUserNo toUserName:(NSString *)toUserName toDeviceNo:(long long)toDeviceNo toDeviceType:(int)toDeviceType toDeviceName:(NSString *)toDeviceName toServerFolderNo:(long long)toServerFolderNo toFolderName:(NSString *)toFolderName mustWakeUp:(bool)mustWakeUp {

    QString paramToUserName             = [SaaringUtility convertNSStringToQString:toUserName];
    QString paramToDeviceName           = [SaaringUtility convertNSStringToQString:toDeviceName];
    QString paramToFolderName           = [SaaringUtility convertNSStringToQString:toFolderName];

    self.p2p->doP2pConnectShareFolder((qint64)toUserNo, paramToUserName, (qint64)toDeviceNo, (saaring::SaaringDeviceType)toDeviceType, paramToDeviceName, (qint64)toServerFolderNo, paramToFolderName, mustWakeUp);
}
- (void) doP2pConnectCancel : (long long)toUserNo toUserName:(NSString *)toUserName toDeviceNo:(long long)toDeviceNo toDeviceType:(int)toDeviceType toDeviceName:(NSString *)toDeviceName {

    QString paramToUserName             = [SaaringUtility convertNSStringToQString:toUserName];
    QString paramToDeviceName           = [SaaringUtility convertNSStringToQString:toDeviceName];

    self.p2p->doP2pConnectCancel((qint64)toUserNo, paramToUserName, (qint64)toDeviceNo, (saaring::SaaringDeviceType)toDeviceType, paramToDeviceName);
}

- (long long) doP2pFolderList : (long long)toDeviceNo {

    return self.p2p->doP2pFolderList((qint64)toDeviceNo);
}
- (long long) doP2pFolderDetail : (long long)toDeviceNo shareNo:(long long)shareNo serverFolderNo:(long long)serverFolderNo remotePath:(NSString *)remotePath {

    QString paramRemotePath             = [SaaringUtility convertNSStringToQString:remotePath];

    return self.p2p->doP2pFolderDetail((qint64)toDeviceNo, (qint64)shareNo, (qint64)serverFolderNo, paramRemotePath);
}
- (long long) doP2pFolderDetailRecursive : (long long)toDeviceNo shareNo:(long long)shareNo remotePath:(NSString *)remotePath {

    QString paramRemotePath             = [SaaringUtility convertNSStringToQString:remotePath];

    return self.p2p->doP2pFolderDetailRecursive((qint64)toDeviceNo, (qint64)shareNo, paramRemotePath);
}

- (long long) doP2pUploadQuery : (long long)toDeviceNo remoteFilePath:(NSString *)remoteFilePath fileSize:(long long)fileSize myTransNo:(long long)myTransNo {

    QString paramRemoteFilePath         = [SaaringUtility convertNSStringToQString:remoteFilePath];

    return self.p2p->doP2pUploadQuery((qint64)toDeviceNo, paramRemoteFilePath, (qint64)fileSize, (qint64)myTransNo);
}
- (long long) doP2pUpload : (long long)toDeviceNo remoteFilePath:(NSString *)remoteFilePath localFilePath:(NSString *)localFilePath fileStartPosition:(long long)fileStartPosition myTransNo:(long long)myTransNo {

    QString paramRemoteFilePath         = [SaaringUtility convertNSStringToQString:remoteFilePath];
    QString paramLocalFilePath          = [SaaringUtility convertNSStringToQString:localFilePath];

    return self.p2p->doP2pUpload((qint64)toDeviceNo, paramRemoteFilePath, paramLocalFilePath, (qint64)fileStartPosition, (qint64)myTransNo);
}
- (long long) doP2pDownload:(long long)toDeviceNo
            remoteFilePath:(NSString *)remoteFilePath
             localFilePath:(NSString *)localFilePath
                  fileSize:(long long)fileSize
         fileStartPosition:(long long)fileStartPosition
                 myTransNo:(long long)myTransNo
               folderDepth:(int)folderDepth
                  inviteNo:(long long)inviteNo
              remoteFileId:(long long)remoteFileId
{

    QString paramRemoteFilePath         = [SaaringUtility convertNSStringToQString:remoteFilePath];
    QString paramLocalFilePath          = [SaaringUtility convertNSStringToQString:localFilePath];

    return self.p2p->doP2pDownload((qint64)toDeviceNo,
                                   paramRemoteFilePath, paramLocalFilePath,
                                   (qint64)fileSize, (qint64)fileStartPosition, (qint64)myTransNo, folderDepth,
                                   inviteNo, remoteFileId);
}
- (long long) doP2pDownloadInviteSendFilesQuery:(long long)toDeviceNo
                                      inviteNo:(long long)inviteNo
                                       certKey:(NSString *)certKey
{

    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];

    return self.p2p->doP2pDownloadInviteSendFilesQuery((qint64)toDeviceNo, (qint64)inviteNo, paramCertKey);
}
- (long long) doP2pSaveSendFilesInviteInfo:(long long)toDeviceNo
                                 inviteNo:(long long)inviteNo
                               inviteCode:(NSString *)inviteCode
                              dynamicLink:(NSString *)dynamicLink
                        listShortPathName:(NSMutableArray *)listShortPathName
                             listFileName:(NSMutableArray *)listFileName
                             listFileSize:(NSMutableArray *)listFileSize
                                  certKey:(NSString *)certKey
                            inviteMessage:(NSString *)inviteMessage
{

    QString paramInviteCode         = [SaaringUtility convertNSStringToQString:inviteCode];
    QString paramDynamicLink        = [SaaringUtility convertNSStringToQString:dynamicLink];
    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];
    QString paramInviteMessage      = [SaaringUtility convertNSStringToQString:inviteMessage];

    QStringList paramListShortPathName;
    QStringList paramListFileName;
    QList<qint64> paramListFileSize;

    [SaaringUtility convertNSMutableArrayToQListString:listShortPathName dest:&paramListShortPathName];
    [SaaringUtility convertNSMutableArrayToQListString:listFileName dest:&paramListFileName];
    [SaaringUtility convertNSMutableArrayToQListLongLong:listFileSize dest:&paramListFileSize];

    return self.p2p->doP2pSaveSendFilesInviteInfo((qint64)toDeviceNo, (qint64)inviteNo,
                                                  paramInviteCode, paramDynamicLink,
                                                  paramListShortPathName, paramListFileName, paramListFileSize,
                                                  paramCertKey, paramInviteMessage);
}

- (void) doP2pProcessDownloadComplete : (long long)toDeviceNo {

    self.p2p->doP2pProcessDownloadComplete((qint64)toDeviceNo);
}
- (void) doP2pProcessUploadComplete : (long long)toDeviceNo {

    self.p2p->doP2pProcessUploadComplete((qint64)toDeviceNo);
}

- (void) doP2pProcessDownloadQueueCheck : (long long)toDeviceNo {

    self.p2p->doP2pProcessDownloadQueueCheck((qint64)toDeviceNo);
}
- (void) doP2pProcessUploadQueueCheck : (long long)toDeviceNo {

    self.p2p->doP2pProcessUploadQueueCheck((qint64)toDeviceNo);
}

- (void) doP2pOpenAllChannel : (NSMutableArray *)listDeviceNo {

    QList<qint64> paramListDeviceNo;
    [SaaringUtility convertNSMutableArrayToQListLongLong:listDeviceNo dest:&paramListDeviceNo];
    
    self.p2p->doP2pOpenAllChannel(paramListDeviceNo);
}
- (void) doP2pCloseChannel : (long long)remoteDeviceNo {

    self.p2p->doP2pCloseChannel((qint64)remoteDeviceNo);
}
- (long long) doP2pStopAllTransfer : (NSMutableArray *)listDeviceNo {

    QList<qint64> paramListDeviceNo;
    [SaaringUtility convertNSMutableArrayToQListLongLong:listDeviceNo dest:&paramListDeviceNo];
    
    return self.p2p->doP2pStopAllTransfer(paramListDeviceNo);
}
- (long long) doP2pCancelAllTransfer : (NSMutableArray *)listDeviceNo {

    QList<qint64> paramListDeviceNo;
    [SaaringUtility convertNSMutableArrayToQListLongLong:listDeviceNo dest:&paramListDeviceNo];
    
    return self.p2p->doP2pCancelAllTransfer(paramListDeviceNo);
}

- (void) listAllChannelDeviceNo : (NSMutableArray *)listDeviceNo {

    QList<qint64> paramListDeviceNo;
    
    self.p2p->listAllChannelDeviceNo(paramListDeviceNo);

    [SaaringUtility convertQListToNSMutableArrayLongLong:&paramListDeviceNo dest:listDeviceNo];
}
- (void) updateNumLiveTransfer : (int)numLiveTransfer {

    self.p2p->updateNumLiveTransfer(numLiveTransfer);
}

- (void) startRelayServer {
    
    self.p2p->startRelayServer();
}
- (void) stopRelayServer {

    self.p2p->stopRelayServer();
}

//////////////////////////////////////////////////////////////////////
// for web interface
//////////////////////////////////////////////////////////////////////

// connect
- (bool) listFolderOfOther : (NSString *)authKey friendUserNo:(long long)friendUserNo resultCode:(NSMutableString *)resultCode resultMessage:(NSMutableString *)resultMessage resultData:(NSMutableString *)resultData listSaaringShareFolderInfo:(NSMutableArray *)listSaaringShareFolderInfo {

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringShareFolderInfo> paramListShareFolderInfo;

    bool result = self.web->listFolderOfOther(paramAuthKey, (qint64)friendUserNo,
                                              paramResultCode, paramResultMessage, paramResultData,
                                              paramListShareFolderInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringShareFolderInfo item : paramListShareFolderInfo)
    {
        SaaringShareFolderInfo *paramSaaringShareFolderInfo = [[SaaringShareFolderInfo alloc] init];

        [SaaringShareFolderInfo convertQtSaaringShareFolderInfoToObjC:&item objc:paramSaaringShareFolderInfo];

        // append to list
        [listSaaringShareFolderInfo addObject:paramSaaringShareFolderInfo];
    }

    return result;
}
- (bool) listLiveDevice : (NSString *)authKey deviceNos:(NSString *)deviceNos resultCode:(NSMutableString *)resultCode resultMessage:(NSMutableString *)resultMessage resultData:(NSMutableString *)resultData listLiveDeviceNo:(NSMutableArray *)listLiveDeviceNo {

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramDeviceNos          = [SaaringUtility convertNSStringToQString:deviceNos];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<qint64> paramListLiveDeviceNo;

    bool result = self.web->listLiveDevice(paramAuthKey, paramDeviceNos,
                                           paramResultCode, paramResultMessage, paramResultData,
                                           paramListLiveDeviceNo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringUtility convertQListToNSMutableArrayLongLong:&paramListLiveDeviceNo dest:listLiveDeviceNo];

    return result;
}
- (bool) getBadgeInfo : (NSString *)authKey resultCode:(NSMutableString *)resultCode resultMessage:(NSMutableString *)resultMessage resultData:(NSMutableString *)resultData saaringBadgeInfo:(SaaringBadgeInfo *)saaringBadgeInfo {

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringBadgeInfo paramSaaringBadgeInfo;

    bool result = self.web->getBadgeInfo(paramAuthKey,
                                         paramResultCode, paramResultMessage, paramResultData,
                                         paramSaaringBadgeInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringBadgeInfo convertQtSaaringBadgeInfoToObjC:&paramSaaringBadgeInfo objc:saaringBadgeInfo];

    return result;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// friend
- (bool) listFriend:(NSString *)authKey
               resultCode:(NSMutableString *)resultCode
            resultMessage:(NSMutableString *)resultMessage
               resultData:(NSMutableString *)resultData
    listSaaringFriendInfo:(NSMutableArray *)listSaaringFriendInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringFriendInfo> paramListSaaringFriendInfo;

    bool result = self.web->listFriend(paramAuthKey,
                                       paramResultCode, paramResultMessage, paramResultData,
                                       paramListSaaringFriendInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringFriendInfo item : paramListSaaringFriendInfo)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];

        // append to list
        [listSaaringFriendInfo addObject:saaringFriendInfo];
    }

    return result;
}
- (bool) requestFriendByUserNo:(NSString *)authKey
                     toUserNo:(long long)toUserNo
                  resultCode:(NSMutableString *)resultCode
               resultMessage:(NSMutableString *)resultMessage
                  resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->requestFriendByUserNo(paramAuthKey, toUserNo,
                                                 paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

- (bool) listRequestingFriend:(NSString *)authKey
                   resultCode:(NSMutableString *)resultCode
                resultMessage:(NSMutableString *)resultMessage
                   resultData:(NSMutableString *)resultData
listSaaringIncomingRequestFriend:(NSMutableArray *)listSaaringIncomingRequestFriend
listSaaringRequestingFriendMember:(NSMutableArray *)listSaaringRequestingFriendMember
listSaaringRequestingFriendNonMember:(NSMutableArray *)listSaaringRequestingFriendNonMember
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringFriendInfo> paramListSaaringIncomingRequestFriend,
                                      paramListSaaringRequestingFriendMember,
                                      paramListSaaringRequestingFriendNonMember;

    bool result = self.web->listRequestingFriend(paramAuthKey,
                                                 paramResultCode, paramResultMessage, paramResultData,
                                                 paramListSaaringIncomingRequestFriend,
                                                 paramListSaaringRequestingFriendMember,
                                                 paramListSaaringRequestingFriendNonMember);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringFriendInfo item : paramListSaaringIncomingRequestFriend)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];

        // append to list
        [listSaaringIncomingRequestFriend addObject:saaringFriendInfo];
    }

    for(saaring::SaaringFriendInfo item : paramListSaaringRequestingFriendMember)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];

        // append to list
        [listSaaringRequestingFriendMember addObject:saaringFriendInfo];
    }

    for(saaring::SaaringFriendInfo item : paramListSaaringRequestingFriendNonMember)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];

        // append to list
        [listSaaringRequestingFriendNonMember addObject:saaringFriendInfo];
    }

    return result;
}
- (bool)cancelRequestFriend:(NSString *)authKey
                   toUserNo:(long long)toUserNo
                    toEmail:(NSString *)toEmail
              toPhoneNumber:(NSString *)toPhoneNumber
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramToEmail            = [SaaringUtility convertNSStringToQString:toEmail];
    QString paramToPhoneNumber      = [SaaringUtility convertNSStringToQString:toPhoneNumber];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->cancelRequestFriend(paramAuthKey, toUserNo, paramToEmail, paramToPhoneNumber,
                                                 paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)rejectRequestFriend:(NSString *)authKey
                   toUserNo:(long long)toUserNo
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->rejectRequestFriend(paramAuthKey, toUserNo,
                                                 paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)recoverRequestFriend:(NSString *)authKey
                    toUserNo:(long long)toUserNo
                  resultCode:(NSMutableString *)resultCode
               resultMessage:(NSMutableString *)resultMessage
                  resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->recoverRequestFriend(paramAuthKey, toUserNo,
                                                 paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)addFriend:(NSString *)authKey
     friendUserNo:(long long)friendUserNo
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->addFriend(paramAuthKey, (qint64)friendUserNo,
                                      paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

- (bool)blockFriend:(NSString *)authKey
      friendUserNos:(NSString *)friendUserNos
            blockYN:(NSString *)blockYN
         resultCode:(NSMutableString *)resultCode
      resultMessage:(NSMutableString *)resultMessage
         resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramFriendUserNos      = [SaaringUtility convertNSStringToQString:friendUserNos];
    QString paramBlockYN            = [SaaringUtility convertNSStringToQString:blockYN];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->blockFriend(paramAuthKey, paramFriendUserNos, paramBlockYN,
                                        paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)listBlock:(NSString *)authKey
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData
      listBlocked:(NSMutableArray *)listBlocked
     listRejected:(NSMutableArray *)listRejected
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringBlockInfo> paramListBlocked,
                                     paramListRejected;

    bool result = self.web->listBlock(paramAuthKey,
                                      paramResultCode, paramResultMessage, paramResultData,
                                      paramListBlocked, paramListRejected);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringBlockInfo item : paramListBlocked)
    {
        SaaringBlockInfo *saaringBlockInfo = [[SaaringBlockInfo alloc] init];
        [SaaringBlockInfo convertQtSaaringBlockInfoToObjC:&item objc:saaringBlockInfo];
        
        // append to list
        [listBlocked addObject:saaringBlockInfo];
    }
    
    for(saaring::SaaringBlockInfo item : paramListRejected)
    {
        SaaringBlockInfo *saaringBlockInfo = [[SaaringBlockInfo alloc] init];
        [SaaringBlockInfo convertQtSaaringBlockInfoToObjC:&item objc:saaringBlockInfo];
        
        // append to list
        [listRejected addObject:saaringBlockInfo];
    }

    return result;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
- (bool)inviteFriendSimple:(NSString *)authKey
                   certKey:(NSString *)certKey
             inviteMessage:(NSString *)inviteMessage
                resultCode:(NSMutableString *)resultCode
             resultMessage:(NSMutableString *)resultMessage
                resultData:(NSMutableString *)resultData
                  inviteNo:(NSMutableString *)inviteNo
                inviteCode:(NSMutableString *)inviteCode
               dynamicLink:(NSMutableString *)dynamicLink
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];
    QString paramInviteMessage      = [SaaringUtility convertNSStringToQString:inviteMessage];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;
    
    qint64 paramInviteNo;
    QString paramInviteCode;
    QString paramDynamicLink;

    bool result = self.web->inviteFriendSimple(paramAuthKey, paramCertKey, paramInviteMessage,
                                                 paramResultCode, paramResultMessage, paramResultData,
                                                 paramInviteNo, paramInviteCode, paramDynamicLink);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    QString paramStrInviteNo = QString::number(paramInviteNo);
    [inviteNo setString:[SaaringUtility convertQStringToNSString:paramStrInviteNo]];
    [inviteCode setString:[SaaringUtility convertQStringToNSString:paramInviteCode]];
    [dynamicLink setString:[SaaringUtility convertQStringToNSString:paramDynamicLink]];

    return result;
}

- (bool)inviteFriend:(NSString *)authKey
                     listToUserNo:(NSMutableArray *)listToUserNo
                   listToUserName:(NSMutableArray *)listToUserName
                      listToEmail:(NSMutableArray *)listToEmail
                listToPhoneNumber:(NSMutableArray *)listToPhoneNumber
                          certKey:(NSString *)certKey
                    inviteMessage:(NSString *)inviteMessage
                       resultCode:(NSMutableString *)resultCode
                    resultMessage:(NSMutableString *)resultMessage
                       resultData:(NSMutableString *)resultData
                         inviteNo:(NSMutableString *)inviteNo
                       inviteCode:(NSMutableString *)inviteCode
                      dynamicLink:(NSMutableString *)dynamicLink
                   listFailUserNo:(NSMutableArray *)listFailUserNo
                 listSuccessEmail:(NSMutableArray *)listSuccessEmail
                  listAddedFriend:(NSMutableArray *)listAddedFriend
        listIncomingRequestFriend:(NSMutableArray *)listIncomingRequestFriend
       listRequestingFriendMember:(NSMutableArray *)listRequestingFriendMember
    listRequestingFriendNonMember:(NSMutableArray *)listRequestingFriendNonMember
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];
    QString paramInviteMessage      = [SaaringUtility convertNSStringToQString:inviteMessage];

    QList<qint64> paramListToUserNo;
    QStringList paramListToUserName;
    QStringList paramListToEmail;
    QStringList paramListToPhoneNumber;

    [SaaringUtility convertNSMutableArrayToQListLongLong:listToUserNo dest:&paramListToUserNo];
    [SaaringUtility convertNSMutableArrayToQListString:listToUserName dest:&paramListToUserName];
    [SaaringUtility convertNSMutableArrayToQListString:listToEmail dest:&paramListToEmail];
    [SaaringUtility convertNSMutableArrayToQListString:listToPhoneNumber dest:&paramListToPhoneNumber];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    qint64 paramInviteNo;
    QString paramInviteCode;
    QString paramDynamicLink;

    QList<qint64>   paramListFailUserNo;
    QStringList     paramListSuccessEmail;

    QList<saaring::SaaringFriendInfo> paramListAddedFriend,
                                      paramListIncomingRequestFriend,
                                      paramListRequestingFriendMember,
                                      paramListRequestingFriendNonMember;

    bool result = self.web->inviteFriend(paramAuthKey,
                                         paramListToUserNo, paramListToUserName, paramListToEmail, paramListToPhoneNumber,
                                         paramCertKey, paramInviteMessage,
                                         paramResultCode, paramResultMessage, paramResultData,
                                         paramInviteNo, paramInviteCode, paramDynamicLink,
                                         paramListFailUserNo, paramListSuccessEmail,
                                         paramListAddedFriend,
                                         paramListIncomingRequestFriend,
                                         paramListRequestingFriendMember,
                                         paramListRequestingFriendNonMember);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    QString paramStrInviteNo = QString::number(paramInviteNo);
    [inviteNo setString:[SaaringUtility convertQStringToNSString:paramStrInviteNo]];
    [inviteCode setString:[SaaringUtility convertQStringToNSString:paramInviteCode]];
    [dynamicLink setString:[SaaringUtility convertQStringToNSString:paramDynamicLink]];

    [SaaringUtility convertQListToNSMutableArrayLongLong:&paramListFailUserNo dest:listFailUserNo];
    [SaaringUtility convertQListToNSMutableArrayString:&paramListSuccessEmail dest:listSuccessEmail];
    
    for(saaring::SaaringFriendInfo item : paramListAddedFriend)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];
        
        // append to list
        [listAddedFriend addObject:saaringFriendInfo];
    }
    
    for(saaring::SaaringFriendInfo item : paramListIncomingRequestFriend)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];
        
        // append to list
        [listIncomingRequestFriend addObject:saaringFriendInfo];
    }
    
    for(saaring::SaaringFriendInfo item : paramListRequestingFriendMember)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];
        
        // append to list
        [listRequestingFriendMember addObject:saaringFriendInfo];
    }
    
    for(saaring::SaaringFriendInfo item : paramListRequestingFriendNonMember)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];
        
        // append to list
        [listRequestingFriendNonMember addObject:saaringFriendInfo];
    }

    return result;
}

- (bool)cancelInviteFriend:(NSString *)authKey
                  inviteNo:(long long)inviteNo
                resultCode:(NSMutableString *)resultCode
             resultMessage:(NSMutableString *)resultMessage
                resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->cancelInviteFriend(paramAuthKey, (qint64)inviteNo,
                                                 paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

- (bool)completeInviteFriend:(NSString *)authKey
                    inviteNo:(long long)inviteNo
                  resultCode:(NSMutableString *)resultCode
               resultMessage:(NSMutableString *)resultMessage
                  resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->completeInviteFriend(paramAuthKey, (qint64)inviteNo,
                                                 paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

- (bool)extendInviteExpiredt:(NSString *)authKey
                  inviteNo:(long long)inviteNo
                resultCode:(NSMutableString *)resultCode
             resultMessage:(NSMutableString *)resultMessage
                resultData:(NSMutableString *)resultData
         saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;
    
    saaring::SaaringInviteInfo paramSaaringInviteInfo;
    [SaaringInviteInfo convertObjCSaaringInviteInfoToQt:saaringInviteInfo qt:&paramSaaringInviteInfo];

    bool result = self.web->extendInviteExpiredt(paramAuthKey, (qint64)inviteNo,
                                                 paramResultCode, paramResultMessage, paramResultData,
                                                 paramSaaringInviteInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];
    
    [SaaringInviteInfo convertQtSaaringInviteInfoToObjC:&paramSaaringInviteInfo objc:saaringInviteInfo];

    return result;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)getInvite:(NSString *)authKey
             inviteNo:(long long)inviteNo
           resultCode:(NSMutableString *)resultCode
        resultMessage:(NSMutableString *)resultMessage
           resultData:(NSMutableString *)resultData
    saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringInviteInfo paramSaaringInviteInfo;
    [SaaringInviteInfo convertObjCSaaringInviteInfoToQt:saaringInviteInfo qt:&paramSaaringInviteInfo];

    bool result = self.web->getInvite(paramAuthKey, (qint64)inviteNo,
                                      paramResultCode, paramResultMessage, paramResultData,
                                      paramSaaringInviteInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringInviteInfo convertQtSaaringInviteInfoToObjC:&paramSaaringInviteInfo objc:saaringInviteInfo];

    return result;
}
- (bool)getInviteByDynamicLink:(NSString *)dynamicLink
                       certKey:(NSString *)certKey
                    resultCode:(NSMutableString *)resultCode
                 resultMessage:(NSMutableString *)resultMessage
                    resultData:(NSMutableString *)resultData
             saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo
{

    QString paramDynamicLink        = [SaaringUtility convertNSStringToQString:dynamicLink];
    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringInviteInfo paramSaaringInviteInfo;
    [SaaringInviteInfo convertObjCSaaringInviteInfoToQt:saaringInviteInfo qt:&paramSaaringInviteInfo];

    bool result = self.web->getInviteByDynamicLink(paramDynamicLink, paramCertKey,
                                      paramResultCode, paramResultMessage, paramResultData,
                                      paramSaaringInviteInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringInviteInfo convertQtSaaringInviteInfoToObjC:&paramSaaringInviteInfo objc:saaringInviteInfo];

    return result;
}

- (bool)listInviteAll:(NSString *)authKey
               resultCode:(NSMutableString *)resultCode
            resultMessage:(NSMutableString *)resultMessage
               resultData:(NSMutableString *)resultData
    listSaaringInviteInfo:(NSMutableArray *)listSaaringInviteInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringInviteInfo> paramListSaaringInviteInfo;

    bool result = self.web->listInviteAll(paramAuthKey,
                                      paramResultCode, paramResultMessage, paramResultData,
                                      paramListSaaringInviteInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringInviteInfo item : paramListSaaringInviteInfo)
    {
        SaaringInviteInfo *saaringInviteInfo = [[SaaringInviteInfo alloc] init];
        [SaaringInviteInfo convertQtSaaringInviteInfoToObjC:&item objc:saaringInviteInfo];
        
        // append to list
        [listSaaringInviteInfo addObject:saaringInviteInfo];
    }

    return result;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
- (bool)inviteShareFolderSimple:(NSString *)authKey
    inviteShareFolderServerFolderNo:(long long)inviteShareFolderServerFolderNo
              inviteShareFolderPath:(NSString *)inviteShareFolderPath
                            certKey:(NSString *)certKey
                      inviteMessage:(NSString *)inviteMessage
                         resultCode:(NSMutableString *)resultCode
                      resultMessage:(NSMutableString *)resultMessage
                         resultData:(NSMutableString *)resultData
                           inviteNo:(NSMutableString *)inviteNo
                         inviteCode:(NSMutableString *)inviteCode
                        dynamicLink:(NSMutableString *)dynamicLink
{

    QString paramAuthKey                = [SaaringUtility convertNSStringToQString:authKey];
    QString paramInviteShareFolderPath  = [SaaringUtility convertNSStringToQString:inviteShareFolderPath];
    QString paramCertKey                = [SaaringUtility convertNSStringToQString:certKey];
    QString paramInviteMessage          = [SaaringUtility convertNSStringToQString:inviteMessage];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;
    
    qint64 paramInviteNo;
    QString paramInviteCode;
    QString paramDynamicLink;

    bool result = self.web->inviteShareFolderSimple(paramAuthKey,
                                                    (qint64)inviteShareFolderServerFolderNo,
                                                    paramInviteShareFolderPath,
                                                    paramCertKey, paramInviteMessage,
                                                    paramResultCode, paramResultMessage, paramResultData,
                                                    paramInviteNo, paramInviteCode, paramDynamicLink);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    QString paramStrInviteNo = QString::number(paramInviteNo);
    [inviteNo setString:[SaaringUtility convertQStringToNSString:paramStrInviteNo]];
    [inviteCode setString:[SaaringUtility convertQStringToNSString:paramInviteCode]];
    [dynamicLink setString:[SaaringUtility convertQStringToNSString:paramDynamicLink]];

    return result;
}
- (bool)inviteSendFilesSimple:(NSString *)authKey
                      numFile:(long long)numFile
                totalFileSize:(long long)totalFileSize
                      certKey:(NSString *)certKey
                inviteMessage:(NSString *)inviteMessage
                   resultCode:(NSMutableString *)resultCode
                resultMessage:(NSMutableString *)resultMessage
                   resultData:(NSMutableString *)resultData
                     inviteNo:(NSMutableString *)inviteNo
                   inviteCode:(NSMutableString *)inviteCode
                  dynamicLink:(NSMutableString *)dynamicLink
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];
    QString paramInviteMessage      = [SaaringUtility convertNSStringToQString:inviteMessage];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;
    
    qint64 paramInviteNo;
    QString paramInviteCode;
    QString paramDynamicLink;

    bool result = self.web->inviteSendFilesSimple(paramAuthKey, (qint64)numFile, (qint64)totalFileSize,
                                                 paramCertKey, paramInviteMessage,
                                                 paramResultCode, paramResultMessage, paramResultData,
                                                 paramInviteNo, paramInviteCode, paramDynamicLink);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    QString paramStrInviteNo = QString::number(paramInviteNo);
    [inviteNo setString:[SaaringUtility convertQStringToNSString:paramStrInviteNo]];
    [inviteCode setString:[SaaringUtility convertQStringToNSString:paramInviteCode]];
    [dynamicLink setString:[SaaringUtility convertQStringToNSString:paramDynamicLink]];

    return result;
}
- (bool)inviteSendFilesForRemoteDeviceSimple:(NSString *)authKey
                                     numFile:(long long)numFile
                               totalFileSize:(long long)totalFileSize
             inviteShareFolderServerFolderNo:(long long)inviteShareFolderServerFolderNo
                       inviteShareFolderPath:(NSString *)inviteShareFolderPath
                                     certKey:(NSString *)certKey
                               inviteMessage:(NSString *)inviteMessage
                                  resultCode:(NSMutableString *)resultCode
                               resultMessage:(NSMutableString *)resultMessage
                                  resultData:(NSMutableString *)resultData
                                    inviteNo:(NSMutableString *)inviteNo
                                  inviteCode:(NSMutableString *)inviteCode
                                 dynamicLink:(NSMutableString *)dynamicLink
{

    QString paramAuthKey                    = [SaaringUtility convertNSStringToQString:authKey];
    QString paramInviteShareFolderPath      = [SaaringUtility convertNSStringToQString:inviteShareFolderPath];
    QString paramCertKey                    = [SaaringUtility convertNSStringToQString:certKey];
    QString paramInviteMessage              = [SaaringUtility convertNSStringToQString:inviteMessage];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    qint64 paramInviteNo;
    QString paramInviteCode;
    QString paramDynamicLink;

    bool result = self.web->inviteSendFilesForRemoteDeviceSimple(paramAuthKey,
                                         (qint64)numFile, (qint64)totalFileSize,
                                         (qint64)inviteShareFolderServerFolderNo, paramInviteShareFolderPath,
                                         paramCertKey, paramInviteMessage,
                                         paramResultCode, paramResultMessage, paramResultData,
                                         paramInviteNo, paramInviteCode, paramDynamicLink);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    QString paramStrInviteNo = QString::number(paramInviteNo);
    [inviteNo setString:[SaaringUtility convertQStringToNSString:paramStrInviteNo]];
    [inviteCode setString:[SaaringUtility convertQStringToNSString:paramInviteCode]];
    [dynamicLink setString:[SaaringUtility convertQStringToNSString:paramDynamicLink]];

    return result;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)registerInviteByDynamicLink:(NSString *)authKey
                        dynamicLink:(NSString *)dynamicLink
                            certKey:(NSString *)certKey
                         resultCode:(NSMutableString *)resultCode
                      resultMessage:(NSMutableString *)resultMessage
                         resultData:(NSMutableString *)resultData
        saaringInviteRegisteredInfo:(SaaringInviteRegisteredInfo *)saaringInviteRegisteredInfo
                  saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramDynamicLink        = [SaaringUtility convertNSStringToQString:dynamicLink];
    QString paramCertKey            = [SaaringUtility convertNSStringToQString:certKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringInviteRegisteredInfo paramSaaringInviteRegisteredInfo;
    saaring::SaaringInviteInfo paramSaaringInviteInfo;

    [SaaringInviteRegisteredInfo convertObjCSaaringInviteRegisteredInfoToQt:saaringInviteRegisteredInfo qt:&paramSaaringInviteRegisteredInfo];
    [SaaringInviteInfo convertObjCSaaringInviteInfoToQt:saaringInviteInfo qt:&paramSaaringInviteInfo];

    bool result = self.web->registerInviteByDynamicLink(paramAuthKey, paramDynamicLink, paramCertKey,
                                      paramResultCode, paramResultMessage, paramResultData,
                                      paramSaaringInviteRegisteredInfo, paramSaaringInviteInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringInviteRegisteredInfo convertQtSaaringInviteRegisteredInfoToObjC:&paramSaaringInviteRegisteredInfo objc:saaringInviteRegisteredInfo];
    [SaaringInviteInfo convertQtSaaringInviteInfoToObjC:&paramSaaringInviteInfo objc:saaringInviteInfo];

    return result;
}

- (bool)deleteInviteRegistered:(NSString *)authKey
                      inviteNo:(long long)inviteNo
                    resultCode:(NSMutableString *)resultCode
                 resultMessage:(NSMutableString *)resultMessage
                    resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->deleteInviteRegistered(paramAuthKey, (qint64)inviteNo,
                                      paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

- (bool)getInviteRegistered:(NSString *)authKey
                       inviteNo:(long long)inviteNo
                     resultCode:(NSMutableString *)resultCode
                  resultMessage:(NSMutableString *)resultMessage
                     resultData:(NSMutableString *)resultData
    saaringInviteRegisteredInfo:(SaaringInviteRegisteredInfo *)saaringInviteRegisteredInfo
              saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringInviteRegisteredInfo paramSaaringInviteRegisteredInfo;
    saaring::SaaringInviteInfo paramSaaringInviteInfo;

    [SaaringInviteRegisteredInfo convertObjCSaaringInviteRegisteredInfoToQt:saaringInviteRegisteredInfo qt:&paramSaaringInviteRegisteredInfo];
    [SaaringInviteInfo convertObjCSaaringInviteInfoToQt:saaringInviteInfo qt:&paramSaaringInviteInfo];

    bool result = self.web->getInviteRegistered(paramAuthKey, (qint64)inviteNo,
                                      paramResultCode, paramResultMessage, paramResultData,
                                      paramSaaringInviteRegisteredInfo, paramSaaringInviteInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringInviteRegisteredInfo convertQtSaaringInviteRegisteredInfoToObjC:&paramSaaringInviteRegisteredInfo objc:saaringInviteRegisteredInfo];
    [SaaringInviteInfo convertQtSaaringInviteInfoToObjC:&paramSaaringInviteInfo objc:saaringInviteInfo];

    return result;
}

- (bool)listInviteRegisteredAll:(NSString *)authKey
                         resultCode:(NSMutableString *)resultCode
                      resultMessage:(NSMutableString *)resultMessage
                         resultData:(NSMutableString *)resultData
    listSaaringInviteRegisteredInfo:(NSMutableArray *)listSaaringInviteRegisteredInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringInviteRegisteredInfo> paramListSaaringInviteRegisteredInfo;

    bool result = self.web->listInviteRegisteredAll(paramAuthKey,
                                      paramResultCode, paramResultMessage, paramResultData,
                                      paramListSaaringInviteRegisteredInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringInviteRegisteredInfo item : paramListSaaringInviteRegisteredInfo)
    {
        SaaringInviteRegisteredInfo *saaringInviteRegisteredInfo = [[SaaringInviteRegisteredInfo alloc] init];
        [SaaringInviteRegisteredInfo convertQtSaaringInviteRegisteredInfoToObjC:&item objc:saaringInviteRegisteredInfo];
        
        // append to list
        [listSaaringInviteRegisteredInfo addObject:saaringInviteRegisteredInfo];
    }

    return result;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// member
- (bool)verifyFirebaseToken:(NSString *)firebaseToken
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData
{

    QString paramFirebaseToken      = [SaaringUtility convertNSStringToQString:firebaseToken];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->verifyFirebaseToken(paramFirebaseToken,
                                       paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)joinMember:(SaaringMemberInfo *)saaringMember
        resultCode:(NSMutableString *)resultCode
     resultMessage:(NSMutableString *)resultMessage
        resultData:(NSMutableString *)resultData
{

    saaring::SaaringMemberInfo paramSaaringMemberInfo;
    [SaaringMemberInfo convertObjCSaaringMemberInfoToQt:saaringMember qt:&paramSaaringMemberInfo];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->joinMember(paramSaaringMemberInfo,
                                       paramResultCode, paramResultMessage, paramResultData);

    if(result == true) {
        *(self.member) = paramSaaringMemberInfo;
    }

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringMemberInfo convertQtSaaringMemberInfoToObjC:&paramSaaringMemberInfo objc:saaringMember];

    return result;
}
- (bool)loginMember:(SaaringMemberInfo *)saaringMember
         resultCode:(NSMutableString *)resultCode
      resultMessage:(NSMutableString *)resultMessage
         resultData:(NSMutableString *)resultData
 {

    saaring::SaaringMemberInfo paramSaaringMemberInfo;
    [SaaringMemberInfo convertObjCSaaringMemberInfoToQt:saaringMember qt:&paramSaaringMemberInfo];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->loginMember(paramSaaringMemberInfo,
                                        paramResultCode, paramResultMessage, paramResultData);

    if(result == true) {
        *(self.member) = paramSaaringMemberInfo;
    }

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringMemberInfo convertQtSaaringMemberInfoToObjC:&paramSaaringMemberInfo objc:saaringMember];

    return result;
}
- (bool)doMe:(NSString *)authKey
                       deviceType:(int)deviceType
                       resultCode:(NSMutableString *)resultCode
                    resultMessage:(NSMutableString *)resultMessage
                       resultData:(NSMutableString *)resultData
                saaringMemberInfo:(SaaringMemberInfo *)saaringMemberInfo
    listSaaringMemberProviderInfo:(NSMutableArray *)listSaaringMemberProviderInfo
 {

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringMemberInfo paramSaaringMemberInfo;
    [SaaringMemberInfo convertObjCSaaringMemberInfoToQt:saaringMemberInfo qt:&paramSaaringMemberInfo];

    QList<saaring::SaaringMemberProviderInfo> paramListSaaringMemberProviderInfo;

    bool result = self.web->doMe(paramAuthKey, deviceType,
                                 paramResultCode, paramResultMessage, paramResultData,
                                 paramSaaringMemberInfo, paramListSaaringMemberProviderInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringMemberInfo convertQtSaaringMemberInfoToObjC:&paramSaaringMemberInfo objc:saaringMemberInfo];

    for(saaring::SaaringMemberProviderInfo item : paramListSaaringMemberProviderInfo)
    {
        SaaringMemberProviderInfo *saaringMemberProviderInfo = [[SaaringMemberProviderInfo alloc] init];
        [SaaringMemberProviderInfo convertQtSaaringMemberProviderInfoToObjC:&item objc:saaringMemberProviderInfo];

        // append to list
        [listSaaringMemberProviderInfo addObject:saaringMemberProviderInfo];
    }

    return result;
}
- (bool)isEmailExist:(NSString *)email
          resultCode:(NSMutableString *)resultCode
       resultMessage:(NSMutableString *)resultMessage
          resultData:(NSMutableString *)resultData
        isEmailExist:(NSMutableString *)isEmailExist
{

    QString paramEmail              = [SaaringUtility convertNSStringToQString:email];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;
    QVariant paramIsEmailExist;

    bool result = self.web->isEmailExist(paramEmail,
                                         paramResultCode, paramResultMessage, paramResultData,
                                         paramIsEmailExist);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [isEmailExist setString:[SaaringUtility convertQStringToNSString:paramIsEmailExist.toString()]];

    return result;
}

- (bool)doUnlink:(NSString *)authKey
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->doUnlink(paramAuthKey,
                                     paramResultCode, paramResultMessage, paramResultData);

    if(result == true) {
        self.member->userId         = "";
        self.member->email          = "";
        self.member->phoneNumber    = "";
        self.member->password       = "";
        self.member->userNo         = 0;
        self.member->deviceNo       = 0;
        self.member->authKey        = "";
        self.member->idType         = (int)saaring::SaaringIdType::PASSWORD;
        self.member->providerId     = saaring::PROVIDERID_PASSWORD;
        self.member->userName       = "";
        self.member->firstName      = "";
        self.member->lastName       = "";
        self.member->deviceName     = "";
        self.member->profilePic     = "";
        self.member->message        = "";
        self.member->idToken        = "";
        self.member->accessToken    = "";
        self.member->uid            = "";
        self.member->providerYN     = "Y";
    }

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)doLogout:(NSString *)authKey
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->doLogout(paramAuthKey,
                                     paramResultCode, paramResultMessage, paramResultData);

    if(result == true) {
        self.member->userId         = "";
        self.member->email          = "";
        self.member->phoneNumber    = "";
        self.member->password       = "";
        self.member->userNo         = 0;
        self.member->deviceNo       = 0;
        self.member->authKey        = "";
        self.member->idType         = (int)saaring::SaaringIdType::PASSWORD;
        self.member->providerId     = saaring::PROVIDERID_PASSWORD;
        self.member->userName       = "";
        self.member->firstName      = "";
        self.member->lastName       = "";
        self.member->deviceName     = "";
        self.member->profilePic     = "";
        self.member->message        = "";
        self.member->idToken        = "";
        self.member->accessToken    = "";
        self.member->uid            = "";
        self.member->providerYN     = "Y";
    }

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)searchUser:(NSString *)authKey
                  keyword:(NSString *)keyword
               resultCode:(NSMutableString *)resultCode
            resultMessage:(NSMutableString *)resultMessage
               resultData:(NSMutableString *)resultData
    listSaaringFriendInfo:(NSMutableArray *)listSaaringFriendInfo
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramKeyword            = [SaaringUtility convertNSStringToQString:keyword];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringFriendInfo> paramListSaaringFriendInfo;

    bool result = self.web->searchUser(paramAuthKey, paramKeyword,
                                       paramResultCode, paramResultMessage, paramResultData,
                                       paramListSaaringFriendInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringFriendInfo item : paramListSaaringFriendInfo)
    {
        SaaringFriendInfo *saaringFriendInfo = [[SaaringFriendInfo alloc] init];
        [SaaringFriendInfo convertQtSaaringFriendInfoToObjC:&item objc:saaringFriendInfo];

        // append to list
        [listSaaringFriendInfo addObject:saaringFriendInfo];
    }

    return result;
}

- (bool)changeProfilePic:(NSString *)authKey
              profilePic:(NSString *)profilePic
              resultCode:(NSMutableString *)resultCode
           resultMessage:(NSMutableString *)resultMessage
              resultData:(NSMutableString *)resultData
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramProfilePic         = [SaaringUtility convertNSStringToQString:profilePic];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->changeProfilePic(paramAuthKey, paramProfilePic,
                                             paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)changeEmail:(NSString *)authKey
              email:(NSString *)email
         resultCode:(NSMutableString *)resultCode
      resultMessage:(NSMutableString *)resultMessage
         resultData:(NSMutableString *)resultData
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramEmail              = [SaaringUtility convertNSStringToQString:email];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->changeEmail(paramAuthKey, paramEmail,
                                        paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)changeUserName:(NSString *)authKey
              userName:(NSString *)userName
            resultCode:(NSMutableString *)resultCode
         resultMessage:(NSMutableString *)resultMessage
            resultData:(NSMutableString *)resultData
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramUserName           = [SaaringUtility convertNSStringToQString:userName];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->changeUserName(paramAuthKey, paramUserName,
                                           paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

// setup
- (bool)changeDeviceName:(NSString *)authKey
              deviceName:(NSString *)deviceName
              resultCode:(NSMutableString *)resultCode
           resultMessage:(NSMutableString *)resultMessage
              resultData:(NSMutableString *)resultData
{

    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramDeviceName         = [SaaringUtility convertNSStringToQString:deviceName];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->changeDeviceName(paramAuthKey, paramDeviceName,
                                             paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

// folder
- (bool)deleteFolder:(NSString *)authKey
      serverFolderNo:(long long)serverFolderNo
          resultCode:(NSMutableString *)resultCode
       resultMessage:(NSMutableString *)resultMessage
          resultData:(NSMutableString *)resultData
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    bool result = self.web->deleteFolder(paramAuthKey, (qint64)serverFolderNo,
                                         paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}
- (bool)addOrUpdateFolder:(NSString *)authKey
    saaringShareFolderInfo:(SaaringShareFolderInfo *)saaringShareFolderInfo
                resultCode:(NSMutableString *)resultCode
             resultMessage:(NSMutableString *)resultMessage
                resultData:(NSMutableString *)resultData
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringShareFolderInfo paramSaaringShareFolderInfo;
    [SaaringShareFolderInfo convertObjCSaaringShareFolderInfoToQt:saaringShareFolderInfo qt:&paramSaaringShareFolderInfo];

    bool result = self.web->addOrUpdateFolder(paramAuthKey, paramSaaringShareFolderInfo,
                                              paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    [SaaringShareFolderInfo convertQtSaaringShareFolderInfoToObjC:&paramSaaringShareFolderInfo objc:saaringShareFolderInfo];

    return result;
}
- (bool)listFolderAll:(NSString *)authKey
                    resultCode:(NSMutableString *)resultCode
                 resultMessage:(NSMutableString *)resultMessage
                    resultData:(NSMutableString *)resultData
    listSaaringShareFolderInfo:(NSMutableArray *)listSaaringShareFolderInfo
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringShareFolderInfo> paramListSaaringShareFolderInfo;

    bool result = self.web->listFolderAll(paramAuthKey,
                                          paramResultCode, paramResultMessage, paramResultData,
                                          paramListSaaringShareFolderInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringShareFolderInfo item : paramListSaaringShareFolderInfo)
    {
        SaaringShareFolderInfo *paramSaaringShareFolderInfo = [[SaaringShareFolderInfo alloc] init];

        [SaaringShareFolderInfo convertQtSaaringShareFolderInfoToObjC:&item objc:paramSaaringShareFolderInfo];

        [listSaaringShareFolderInfo addObject:paramSaaringShareFolderInfo];
    }

    return result;
}
- (bool)checkFolderPassword:(NSString *)authKey
             serverFolderNo:(long long)serverFolderNo
                   password:(NSString *)password
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];
    QString paramPassword           = [SaaringUtility convertNSStringToQString:password];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    saaring::SaaringShareFolderInfo paramSaaringShareFolderInfo;

    bool result = self.web->checkFolderPassword(paramAuthKey, (qint64)serverFolderNo, paramPassword,
                                                  paramResultCode, paramResultMessage, paramResultData);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    return result;
}

// phone address
- (bool)addPhoneAddress:(NSString *)authKey
        listPhoneNumber:(NSMutableArray *)listPhoneNumber
             resultCode:(NSMutableString *)resultCode
          resultMessage:(NSMutableString *)resultMessage
             resultData:(NSMutableString *)resultData
listSaaringPhoneAddressInfo:(NSMutableArray *)listSaaringPhoneAddressInfo
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QStringList paramListPhoneNumber;
    [SaaringUtility convertNSMutableArrayToQListString:listPhoneNumber dest:&paramListPhoneNumber];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringPhoneAddressInfo> paramListSaaringPhoneAddressInfo;

    bool result = self.web->addPhoneAddress(paramAuthKey, paramListPhoneNumber,
                                            paramResultCode, paramResultMessage, paramResultData,
                                            paramListSaaringPhoneAddressInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringPhoneAddressInfo item : paramListSaaringPhoneAddressInfo)
    {
        SaaringPhoneAddressInfo *paramSaaringPhoneAddressInfo = [[SaaringPhoneAddressInfo alloc] init];

        [SaaringPhoneAddressInfo convertQtSaaringPhoneAddressInfoToObjC:&item objc:paramSaaringPhoneAddressInfo];

        [listSaaringPhoneAddressInfo addObject:paramSaaringPhoneAddressInfo];
    }

    return result;
}

- (bool)listPhoneAddress:(NSString *)authKey
              resultCode:(NSMutableString *)resultCode
           resultMessage:(NSMutableString *)resultMessage
              resultData:(NSMutableString *)resultData
listSaaringPhoneAddressInfo:(NSMutableArray *)listSaaringPhoneAddressInfo
{
    QString paramAuthKey            = [SaaringUtility convertNSStringToQString:authKey];

    QString paramResultCode;
    QString paramResultMessage;
    QJsonObject paramResultData;

    QList<saaring::SaaringPhoneAddressInfo> paramListSaaringPhoneAddressInfo;

    bool result = self.web->listPhoneAddress(paramAuthKey,
                                             paramResultCode, paramResultMessage, paramResultData,
                                             paramListSaaringPhoneAddressInfo);

    [resultCode setString:[SaaringUtility convertQStringToNSString:paramResultCode]];
    [resultMessage setString:[SaaringUtility convertQStringToNSString:paramResultMessage]];
    [resultData setString:[SaaringUtility convertQJsonObjectToNSString:paramResultData]];

    for(saaring::SaaringPhoneAddressInfo item : paramListSaaringPhoneAddressInfo)
    {
        SaaringPhoneAddressInfo *paramSaaringPhoneAddressInfo = [[SaaringPhoneAddressInfo alloc] init];

        [SaaringPhoneAddressInfo convertQtSaaringPhoneAddressInfoToObjC:&item objc:paramSaaringPhoneAddressInfo];

        [listSaaringPhoneAddressInfo addObject:paramSaaringPhoneAddressInfo];
    }

    return result;
}

//////////////////////////////////////////////////////////////////////
// for test interface
//////////////////////////////////////////////////////////////////////
- (void) testCallback : (int)command withParam:(QStringList &)param {
    
    NSNumber *commandObject = [NSNumber numberWithInt:command];

    NSMutableArray *paramObject = [NSMutableArray arrayWithCapacity:param.size()];
    [SaaringUtility convertQListToNSMutableArrayString:&param dest:paramObject];
    
    NSLog(@"commandObject = %@", commandObject);
    NSLog(@"paramObject = %@", paramObject);
    
    [self.testCallbackSwiftContext performSelector:@selector(testSwiftCallback:withParam:) withObject:commandObject withObject:paramObject];
}

- (void) initTest : (id)context {
    
    self.testCallbackSwiftContext = context;
    
    NSLog(@"initTest...");
    self.saaringService->initTest(self.testCallbackBlock);
}

- (void) testCallback {
    
    self.saaringService->testCallback();
}

- (void) testSwiftCall : (id)context {
    
    self.testCallbackSwiftContext = context;
    
    NSNumber *commandObject = [NSNumber numberWithInt:1111];
    NSMutableArray *paramObject = [NSMutableArray arrayWithCapacity:1];
    [paramObject addObject:@"Eezy"];
    [paramObject addObject:@"Tutorials"];
    [self.testCallbackSwiftContext performSelector:@selector(testSwiftCallback:withParam:) withObject:commandObject withObject:paramObject];
}

@end
