//
//  SaaringWrapper.h
//  qttest
//
//  Created by kojang on 23/01/2019.
//  Copyright © 2019 kojang. All rights reserved.
//

#ifndef SaaringWrapper_h
#define SaaringWrapper_h

#import <Foundation/Foundation.h>
#import "SaaringDomainWrapper.h"

@interface SaaringWrapper : NSObject

//////////////////////////////////////////////////////////////////////
// for init
//////////////////////////////////////////////////////////////////////
+ (SaaringWrapper *) getInstance;
- (id) initSaaring;
- (void) exec;

//////////////////////////////////////////////////////////////////////
// for common interface
//////////////////////////////////////////////////////////////////////
- (double)calSpeed:(long long)oldTime
          currTime:(long long)currTime
           oldSize:(long long)oldSize
          currSize:(long long)currSize;
- (NSString *)leftTime:(long long)oldTime
              currTime:(long long)currTime
               oldSize:(long long)oldSize
              currSize:(long long)currSize
             totalSize:(long long)totalSize
                 speed:(double)speed;
- (bool)checkEmail:(NSString *)email;
- (NSString *)getDuplicationFilename:(NSString *)filePath
                   lastLocalFilePath:(NSString *)lastLocalFilePath;
- (NSString *)numberFormatWithComma:(unsigned long long)number;
- (int)checkUpgrade:(NSString *)lastVer
         runningVer:(NSString *)runningVer
         currentVer:(NSString *)currentVer;
- (NSString *)getSizeWithUnit:(long long)size;
- (NSString *)changeProfileImage:(NSString *)authKey
                            path:(NSString *)path;
- (NSString *)getShareTopFolder;
- (NSString *)getDownloadFolder;
- (NSString *)removeFirstPath:(NSString *)path;
- (NSString *)getProviderId:(int)idType;
- (NSString *)getProviderName:(int)idType;
- (NSString *)getProviderNameByProviderId:(NSString *)providerId;

//////////////////////////////////////////////////////////////////////
// for db interface
//////////////////////////////////////////////////////////////////////

// for all
- (bool) isOpen;

// for login
- (void)doAfterLogin:(int)idType
               email:(NSString *)email
         phoneNumber:(NSString *)phoneNumber
            password:(NSString *)password
          providerId:(NSString *)providerId
             idToken:(NSString *)idToken
         accessToken:(NSString *)accessToken
                 uid:(NSString *)uid
         isSaveEmail:(NSString *)isSaveEmail
         isAutoLogin:(NSString *)isAutoLogin;

- (bool)updateLoginTb:(int)idType
                email:(NSString *)email
          phoneNumber:(NSString *)phoneNumber
             password:(NSString *)password
          isSaveEmail:(NSString *)isSaveEmail
          isAutoLogin:(NSString *)isAutoLogin
     isLastLoginEmail:(NSString *)isLastLoginEmail;
- (bool)updateLoginAutoStart:(int)idType
                       email:(NSString *)email
                 phoneNumber:(NSString *)phoneNumber
                 isAutoStart:(NSString *)isAutoStart;

- (bool)updateSetSend:(SaaringSetupInfo *)saaringSetupInfo;
- (bool)updateSetNetwork:(SaaringSetupInfo *)saaringSetupInfo;

- (bool)readDbLastLogin:(SaaringSetupInfo *)saaringSetupInfo;
- (bool)readDbByEmailOrPhoneNumber:(int)idType
                             email:(NSString *)email
                       phoneNumber:(NSString *)phoneNumber
                  saaringSetupInfo:(SaaringSetupInfo *)saaringSetupInfo;

// for transfer
- (long long)insertMyTransTbOne:(SaaringTransferBaseInfo *)saaringTransfer;
- (bool)insertMyTransTbMulti:(NSMutableArray *)listSaaringTransfer;

- (bool)updateMyTransTb:(SaaringTransferBaseInfo *)saaringTransfer;
- (bool)updateMyTransTbStatus:(long long)transNo
                  transStatus:(int)transStatus
                   updateTime:(long long)updateTime;

- (bool)updateMyTransTbSortNoMulti:(NSMutableArray *)listTransNo
                        updateTime:(long long)updateTime;

- (bool)updateMyTransTbProgress:(long long)transNo
                       fileSize:(long long)fileSize
                      transSize:(long long)transSize
                     updateTime:(long long)updateTime;
- (bool)updateMyTransTbInit:(long long)myDeviceNo;

- (bool)deleteMyTransTbOne:(long long)transNo;
- (bool)deleteMyTransTbMulti:(NSMutableArray *)listTransNo;
- (bool)deleteMyTransTbAll:(long long)deviceNo;

- (bool)readMyTransTbByTransNo:(NSMutableArray *)listSaaringTransfer
                       transNo:(long long)transNo;
- (bool)readMyTransTbTransferring:(NSMutableArray *)listSaaringTransfer
                       myDeviceNo:(long long)myDeviceNo
                         deleteYN:(NSString *)deleteYN
                         deviceNo:(long long)deviceNo;
- (bool)readMyTransTbNotTransferringLastOne:(NSMutableArray *)listSaaringTransfer
                                 myDeviceNo:(long long)myDeviceNo
                                   deleteYN:(NSString *)deleteYN
                                   deviceNo:(long long)deviceNo;
- (bool)readMyTransTbInit:(NSMutableArray *)listSaaringTransfer
               myDeviceNo:(long long)myDeviceNo;

// for share
- (bool)insertShareTb:(SaaringShareInfo *)saaringShare;
- (bool)updateShareTb:(SaaringShareInfo *)saaringShare;
- (bool)deleteShareTb:(long long)shareNo;

- (bool)readShareTb:(long long)deviceNo
   listSaaringShare:(NSMutableArray *)listSaaringShare;

- (long long)getShareNoByServerFolderNo:(long long)serverFolderNo;
- (long long)getServerFolderNoByShareNo:(long long)shareNo;
- (NSString *)getShareFolderPathByServerFolderNo:(long long)serverFolderNo;

- (int)getShareFileCntWithShareNo:(long long)deviceNo
                          shareNo:(long long)shareNo;
- (long long)getShareFileSize:(long long)deviceNo;
- (long long)getShareFileSizeWithShareNo:(long long)deviceNo
                                 shareNo:(long long)shareNo;

// for share detail
- (bool)updateShareDetailShortPath:(long long)shareNo
                         shareName:(NSString *)shareName
               shareFolderFullPath:(NSString *)shareFolderFullPath;

- (bool)deleteShareDetailTb:(long long)shareNo;

- (void)makeMemoryShareDetailTb;
- (void)backupMemoryShareDetailTb;

// for invite files
- (bool)insertInviteFiles:(SaaringInviteFilesInfo *)saaringInviteFilesInfo;
- (bool)insertListInviteFiles:(NSMutableArray *)listSaaringInviteFilesInfo;

- (bool)deleteExpired;
- (bool)deleteInviteFilesByInviteNo:(long long)inviteNo;

- (bool)listInviteFiles:(long long)inviteNo
    listSaaringInviteFilesInfo:(NSMutableArray *)listSaaringInviteFilesInfo;
- (bool)listInviteFilesWithCertKey:(long long)inviteNo
                           certKey:(NSString *)certKey
        listSaaringInviteFilesInfo:(NSMutableArray *)listSaaringInviteFilesInfo;

//////////////////////////////////////////////////////////////////////
// for share folder interface
//////////////////////////////////////////////////////////////////////
- (void)initThreadShareDir:(id)context;

- (void)startThreadShareFolder;
- (void)stopThreadShareFolder;

- (void)shareFolderInit;
- (void)shareFolderInsert:(long long)shareNo
                sharePath:(NSString *)sharePath
                shareName:(NSString *)shareName;
- (void)shareFolderDelete:(long long)shareNo;
- (void)shareFolderUpdate:(NSString *)folderPath;
- (void)serverFolderDelete:(long long)serverFolderNo;

- (int)getShareFolderCount;
- (int)getShareFileCount;

- (void)startFileMonitor;
- (void)stopFileMonitor;

- (bool)doesNeedDbBackup;

//////////////////////////////////////////////////////////////////////
// for p2p interface
//////////////////////////////////////////////////////////////////////
- (void)initP2p : (id)context;
- (void)closeP2p;
- (bool)isP2p;

- (SaaringServerInfo *)getServerInfo;

- (bool)isSignalingConnected;
- (bool)isSignalingLogin;
- (bool)doSignalingConnect:(NSString *)authKey
       isRefreshServerInfo:(bool)isRefreshServerInfo;
- (void)doSignalingClose;
- (void)reqHeartBeat;

- (bool)isP2pLived:(long long)deviceNo;

- (void) updateShareFolders;
- (void) updateNetworkSendSettings;

- (void)doP2pConnect:(long long)toUserNo
          toUserName:(NSString *)toUserName
          toDeviceNo:(long long)toDeviceNo
        toDeviceType:(int)toDeviceType
        toDeviceName:(NSString *)toDeviceName
          mustWakeUp:(bool)mustWakeUp;
- (void)doP2pConnectShareFolder:(long long)toUserNo
                     toUserName:(NSString *)toUserName
                     toDeviceNo:(long long)toDeviceNo
                   toDeviceType:(int)toDeviceType
                   toDeviceName:(NSString *)toDeviceName
               toServerFolderNo:(long long)toServerFolderNo
                   toFolderName:(NSString *)toFolderName
                     mustWakeUp:(bool)mustWakeUp;
- (void)doP2pConnectCancel:(long long)toUserNo
                toUserName:(NSString *)toUserName
                toDeviceNo:(long long)toDeviceNo
              toDeviceType:(int)toDeviceType
              toDeviceName:(NSString *)toDeviceName;

- (long long)doP2pFolderList:(long long)toDeviceNo;
- (long long)doP2pFolderDetail:(long long)toDeviceNo
                       shareNo:(long long)shareNo
                serverFolderNo:(long long)serverFolderNo
                    remotePath:(NSString *)remotePath;
- (long long)doP2pFolderDetailRecursive:(long long)toDeviceNo
                                shareNo:(long long)shareNo
                             remotePath:(NSString *)remotePath;

- (long long)doP2pUploadQuery:(long long)toDeviceNo
               remoteFilePath:(NSString *)remoteFilePath
                     fileSize:(long long)fileSize
                    myTransNo:(long long)myTransNo;
- (long long)doP2pUpload:(long long)toDeviceNo
          remoteFilePath:(NSString *)remoteFilePath
           localFilePath:(NSString *)localFilePath
       fileStartPosition:(long long)fileStartPosition
               myTransNo:(long long)myTransNo;
- (long long)doP2pDownload:(long long)toDeviceNo
            remoteFilePath:(NSString *)remoteFilePath
             localFilePath:(NSString *)localFilePath
                  fileSize:(long long)fileSize
         fileStartPosition:(long long)fileStartPosition
                 myTransNo:(long long)myTransNo
               folderDepth:(int)folderDepth
                  inviteNo:(long long)inviteNo
              remoteFileId:(long long)remoteFileId;
- (long long)doP2pDownloadInviteSendFilesQuery:(long long)toDeviceNo
                                      inviteNo:(long long)inviteNo
                                       certKey:(NSString *)certKey;
- (long long)doP2pSaveSendFilesInviteInfo:(long long)toDeviceNo
                                 inviteNo:(long long)inviteNo
                               inviteCode:(NSString *)inviteCode
                              dynamicLink:(NSString *)dynamicLink
                        listShortPathName:(NSMutableArray *)listShortPathName
                             listFileName:(NSMutableArray *)listFileName
                             listFileSize:(NSMutableArray *)listFileSize
                                  certKey:(NSString *)certKey
                            inviteMessage:(NSString *)inviteMessage;

- (void)doP2pProcessDownloadComplete:(long long)toDeviceNo;
- (void)doP2pProcessUploadComplete:(long long)toDeviceNo;

- (void)doP2pProcessDownloadQueueCheck:(long long)toDeviceNo;
- (void)doP2pProcessUploadQueueCheck:(long long)toDeviceNo;

- (void)doP2pOpenAllChannel:(NSMutableArray *)listDeviceNo;
- (void)doP2pCloseChannel:(long long)deviceNo
           remoteDeviceNo:(long long)remoteDeviceNo;
- (long long)doP2pStopAllTransfer:(NSMutableArray *)listDeviceNo;
- (long long)doP2pCancelAllTransfer:(NSMutableArray *)listDeviceNo;

- (void)listAllChannelDeviceNo:(NSMutableArray *)listDeviceNo;
- (void)updateNumLiveTransfer:(int)numLiveTransfer;

- (void) startRelayServer;
- (void) stopRelayServer;

//////////////////////////////////////////////////////////////////////
// for web interface
//////////////////////////////////////////////////////////////////////

// connect
- (bool)listFolderOfOther:(NSString *)authKey
                  friendUserNo:(long long)friendUserNo
                    resultCode:(NSMutableString *)resultCode
                 resultMessage:(NSMutableString *)resultMessage
                    resultData:(NSMutableString *)resultData
    listSaaringShareFolderInfo:(NSMutableArray *)listSaaringShareFolderInfo;
- (bool)listLiveDevice:(NSString *)authKey
             deviceNos:(NSString *)deviceNos
            resultCode:(NSMutableString *)resultCode
         resultMessage:(NSMutableString *)resultMessage
            resultData:(NSMutableString *)resultData
      listLiveDeviceNo:(NSMutableArray *)listLiveDeviceNo;
- (bool)getBadgeInfo:(NSString *)authKey
          resultCode:(NSMutableString *)resultCode
       resultMessage:(NSMutableString *)resultMessage
          resultData:(NSMutableString *)resultData
    saaringBadgeInfo:(SaaringBadgeInfo *)saaringBadgeInfo;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// friend
- (bool)listFriend:(NSString *)authKey
               resultCode:(NSMutableString *)resultCode
            resultMessage:(NSMutableString *)resultMessage
               resultData:(NSMutableString *)resultData
    listSaaringFriendInfo:(NSMutableArray *)listSaaringFriendInfo;

- (bool)requestFriendByUserNo:(NSString *)authKey
                     toUserNo:(long long)toUserNo
                   resultCode:(NSMutableString *)resultCode
                resultMessage:(NSMutableString *)resultMessage
                   resultData:(NSMutableString *)resultData;
- (bool)listRequestingFriend:(NSString *)authKey
                              resultCode:(NSMutableString *)resultCode
                           resultMessage:(NSMutableString *)resultMessage
                              resultData:(NSMutableString *)resultData
        listSaaringIncomingRequestFriend:(NSMutableArray *)listSaaringIncomingRequestFriend
       listSaaringRequestingFriendMember:(NSMutableArray *)listSaaringRequestingFriendMember
    listSaaringRequestingFriendNonMember:(NSMutableArray *)listSaaringRequestingFriendNonMember;
- (bool)cancelRequestFriend:(NSString *)authKey
                   toUserNo:(long long)toUserNo
                    toEmail:(NSString *)toEmail
              toPhoneNumber:(NSString *)toPhoneNumber
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData;
- (bool)rejectRequestFriend:(NSString *)authKey
                   toUserNo:(long long)toUserNo
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData;
- (bool)recoverRequestFriend:(NSString *)authKey
                    toUserNo:(long long)toUserNo
                  resultCode:(NSMutableString *)resultCode
               resultMessage:(NSMutableString *)resultMessage
                  resultData:(NSMutableString *)resultData;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)addFriend:(NSString *)authKey
     friendUserNo:(long long)friendUserNo
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData;

- (bool)blockFriend:(NSString *)authKey
      friendUserNos:(NSString *)friendUserNos
            blockYN:(NSString *)blockYN
         resultCode:(NSMutableString *)resultCode
      resultMessage:(NSMutableString *)resultMessage
         resultData:(NSMutableString *)resultData;
- (bool)listBlock:(NSString *)authKey
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData
      listBlocked:(NSMutableArray *)listBlocked
     listRejected:(NSMutableArray *)listRejected;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)inviteFriendSimple:(NSString *)authKey
                   certKey:(NSString *)certKey
             inviteMessage:(NSString *)inviteMessage
                resultCode:(NSMutableString *)resultCode
             resultMessage:(NSMutableString *)resultMessage
                resultData:(NSMutableString *)resultData
                  inviteNo:(NSMutableString *)inviteNo
                inviteCode:(NSMutableString *)inviteCode
               dynamicLink:(NSMutableString *)dynamicLink;
- (bool)inviteFriend:(NSString *)authKey
                     listToUserNo:(NSMutableArray *)listToUserNo
                   listToUserName:(NSMutableArray *)listToUserName
                      listToEmail:(NSMutableArray *)listToEmail
                listToPhoneNumber:(NSMutableArray *)listToPhoneNumber
                          certKey:(NSString *)certKey
                    inviteMessage:(NSString *)inviteMessage
                       resultCode:(NSMutableString *)resultCode
                    resultMessage:(NSMutableString *)resultMessage
                       resultData:(NSMutableString *)resultData
                         inviteNo:(NSMutableString *)inviteNo
                       inviteCode:(NSMutableString *)inviteCode
                      dynamicLink:(NSMutableString *)dynamicLink
                   listFailUserNo:(NSMutableArray *)listFailUserNo
                 listSuccessEmail:(NSMutableArray *)listSuccessEmail
                  listAddedFriend:(NSMutableArray *)listAddedFriend
        listIncomingRequestFriend:(NSMutableArray *)listIncomingRequestFriend
       listRequestingFriendMember:(NSMutableArray *)listRequestingFriendMember
    listRequestingFriendNonMember:(NSMutableArray *)listRequestingFriendNonMember;

- (bool)cancelInviteFriend:(NSString *)authKey
                  inviteNo:(long long)inviteNo
                resultCode:(NSMutableString *)resultCode
             resultMessage:(NSMutableString *)resultMessage
                resultData:(NSMutableString *)resultData;
- (bool)completeInviteFriend:(NSString *)authKey
                    inviteNo:(long long)inviteNo
                  resultCode:(NSMutableString *)resultCode
               resultMessage:(NSMutableString *)resultMessage
                  resultData:(NSMutableString *)resultData;
- (bool)extendInviteExpiredt:(NSString *)authKey
                    inviteNo:(long long)inviteNo
                  resultCode:(NSMutableString *)resultCode
               resultMessage:(NSMutableString *)resultMessage
                  resultData:(NSMutableString *)resultData
           saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)getInvite:(NSString *)authKey
             inviteNo:(long long)inviteNo
           resultCode:(NSMutableString *)resultCode
        resultMessage:(NSMutableString *)resultMessage
           resultData:(NSMutableString *)resultData
    saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo;
- (bool)getInviteByDynamicLink:(NSString *)dynamicLink
                       certKey:(NSString *)certKey
                    resultCode:(NSMutableString *)resultCode
                 resultMessage:(NSMutableString *)resultMessage
                    resultData:(NSMutableString *)resultData
             saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo;

- (bool)listInviteAll:(NSString *)authKey
               resultCode:(NSMutableString *)resultCode
            resultMessage:(NSMutableString *)resultMessage
               resultData:(NSMutableString *)resultData
    listSaaringInviteInfo:(NSMutableArray *)listSaaringInviteInfo;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)inviteShareFolderSimple:(NSString *)authKey
    inviteShareFolderServerFolderNo:(long long)inviteShareFolderServerFolderNo
              inviteShareFolderPath:(NSString *)inviteShareFolderPath
                            certKey:(NSString *)certKey
                      inviteMessage:(NSString *)inviteMessage
                         resultCode:(NSMutableString *)resultCode
                      resultMessage:(NSMutableString *)resultMessage
                         resultData:(NSMutableString *)resultData
                           inviteNo:(NSMutableString *)inviteNo
                         inviteCode:(NSMutableString *)inviteCode
                        dynamicLink:(NSMutableString *)dynamicLink;
- (bool)inviteSendFilesSimple:(NSString *)authKey
                      numFile:(long long)numFile
                totalFileSize:(long long)totalFileSize
                      certKey:(NSString *)certKey
                inviteMessage:(NSString *)inviteMessage
                   resultCode:(NSMutableString *)resultCode
                resultMessage:(NSMutableString *)resultMessage
                   resultData:(NSMutableString *)resultData
                     inviteNo:(NSMutableString *)inviteNo
                   inviteCode:(NSMutableString *)inviteCode
                  dynamicLink:(NSMutableString *)dynamicLink;
- (bool)inviteSendFilesForRemoteDeviceSimple:(NSString *)authKey
                                     numFile:(long long)numFile
                               totalFileSize:(long long)totalFileSize
             inviteShareFolderServerFolderNo:(long long)inviteShareFolderServerFolderNo
                       inviteShareFolderPath:(NSString *)inviteShareFolderPath
                                     certKey:(NSString *)certKey
                               inviteMessage:(NSString *)inviteMessage
                                  resultCode:(NSMutableString *)resultCode
                               resultMessage:(NSMutableString *)resultMessage
                                  resultData:(NSMutableString *)resultData
                                    inviteNo:(NSMutableString *)inviteNo
                                  inviteCode:(NSMutableString *)inviteCode
                                 dynamicLink:(NSMutableString *)dynamicLink;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

- (bool)registerInviteByDynamicLink:(NSString *)authKey
                        dynamicLink:(NSString *)dynamicLink
                            certKey:(NSString *)certKey
                         resultCode:(NSMutableString *)resultCode
                      resultMessage:(NSMutableString *)resultMessage
                         resultData:(NSMutableString *)resultData
        saaringInviteRegisteredInfo:(SaaringInviteRegisteredInfo *)saaringInviteRegisteredInfo
                  saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo;
- (bool)deleteInviteRegistered:(NSString *)authKey
                      inviteNo:(long long)inviteNo
                    resultCode:(NSMutableString *)resultCode
                 resultMessage:(NSMutableString *)resultMessage
                    resultData:(NSMutableString *)resultData;
- (bool)getInviteRegistered:(NSString *)authKey
                       inviteNo:(long long)inviteNo
                     resultCode:(NSMutableString *)resultCode
                  resultMessage:(NSMutableString *)resultMessage
                     resultData:(NSMutableString *)resultData
    saaringInviteRegisteredInfo:(SaaringInviteRegisteredInfo *)saaringInviteRegisteredInfo
              saaringInviteInfo:(SaaringInviteInfo *)saaringInviteInfo;
- (bool)listInviteRegisteredAll:(NSString *)authKey
                         resultCode:(NSMutableString *)resultCode
                      resultMessage:(NSMutableString *)resultMessage
                         resultData:(NSMutableString *)resultData
    listSaaringInviteRegisteredInfo:(NSMutableArray *)listSaaringInviteRegisteredInfo;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// member
- (bool)verifyFirebaseToken:(NSString *)firebaseToken
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData;
- (bool)joinMember:(SaaringMemberInfo *)saaringMember
        resultCode:(NSMutableString *)resultCode
     resultMessage:(NSMutableString *)resultMessage
        resultData:(NSMutableString *)resultData;
- (bool)loginMember:(SaaringMemberInfo *)saaringMember
         resultCode:(NSMutableString *)resultCode
      resultMessage:(NSMutableString *)resultMessage
         resultData:(NSMutableString *)resultData;
- (bool)doMe:(NSString *)authKey
                       deviceType:(int)deviceType
                       resultCode:(NSMutableString *)resultCode
                    resultMessage:(NSMutableString *)resultMessage
                       resultData:(NSMutableString *)resultData
                saaringMemberInfo:(SaaringMemberInfo *)saaringMemberInfo
    listSaaringMemberProviderInfo:(NSMutableArray *)listSaaringMemberProviderInfo;

- (bool)isEmailExist:(NSString *)email
          resultCode:(NSMutableString *)resultCode
       resultMessage:(NSMutableString *)resultMessage
          resultData:(NSMutableString *)resultData
        isEmailExist:(NSMutableString *)isEmailExist;

- (bool)doUnlink:(NSString *)authKey
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData;
- (bool)doLogout:(NSString *)authKey
       resultCode:(NSMutableString *)resultCode
    resultMessage:(NSMutableString *)resultMessage
       resultData:(NSMutableString *)resultData;
- (bool)searchUser:(NSString *)authKey
                  keyword:(NSString *)keyword
               resultCode:(NSMutableString *)resultCode
            resultMessage:(NSMutableString *)resultMessage
               resultData:(NSMutableString *)resultData
    listSaaringFriendInfo:(NSMutableArray *)listSaaringFriendInfo;

- (bool)changeProfilePic:(NSString *)authKey
              profilePic:(NSString *)profilePic
              resultCode:(NSMutableString *)resultCode
           resultMessage:(NSMutableString *)resultMessage
              resultData:(NSMutableString *)resultData;
- (bool)changeEmail:(NSString *)authKey
              email:(NSString *)email
         resultCode:(NSMutableString *)resultCode
      resultMessage:(NSMutableString *)resultMessage
         resultData:(NSMutableString *)resultData;
- (bool)changeUserName:(NSString *)authKey
              userName:(NSString *)userName
             firstName:(NSString *)firstName
              lastName:(NSString *)lastName
            resultCode:(NSMutableString *)resultCode
         resultMessage:(NSMutableString *)resultMessage
            resultData:(NSMutableString *)resultData;

// setup
- (bool)changeDeviceName:(NSString *)authKey
              deviceName:(NSString *)deviceName
              resultCode:(NSMutableString *)resultCode
           resultMessage:(NSMutableString *)resultMessage
              resultData:(NSMutableString *)resultData;

// folder
- (bool)deleteFolder:(NSString *)authKey
      serverFolderNo:(long long)serverFolderNo
          resultCode:(NSMutableString *)resultCode
       resultMessage:(NSMutableString *)resultMessage
          resultData:(NSMutableString *)resultData;
- (bool)addOrUpdateFolder:(NSString *)authKey
    saaringShareFolderInfo:(SaaringShareFolderInfo *)saaringShareFolderInfo
                resultCode:(NSMutableString *)resultCode
             resultMessage:(NSMutableString *)resultMessage
                resultData:(NSMutableString *)resultData;
- (bool)listFolderAll:(NSString *)authKey
                    resultCode:(NSMutableString *)resultCode
                 resultMessage:(NSMutableString *)resultMessage
                    resultData:(NSMutableString *)resultData
    listSaaringShareFolderInfo:(NSMutableArray *)listSaaringShareFolderInfo;
- (bool)checkFolderPassword:(NSString *)authKey
             serverFolderNo:(long long)serverFolderNo
                   password:(NSString *)password
                 resultCode:(NSMutableString *)resultCode
              resultMessage:(NSMutableString *)resultMessage
                 resultData:(NSMutableString *)resultData;

// phone address
- (bool)addPhoneAddress:(NSString *)authKey
        listPhoneNumber:(NSMutableArray *)listPhoneNumber
             resultCode:(NSMutableString *)resultCode
          resultMessage:(NSMutableString *)resultMessage
             resultData:(NSMutableString *)resultData
listSaaringPhoneAddressInfo:(NSMutableArray *)listSaaringPhoneAddressInfo;
- (bool)listPhoneAddress:(NSString *)authKey
              resultCode:(NSMutableString *)resultCode
           resultMessage:(NSMutableString *)resultMessage
              resultData:(NSMutableString *)resultData
listSaaringPhoneAddressInfo:(NSMutableArray *)listSaaringPhoneAddressInfo;


//////////////////////////////////////////////////////////////////////
// for test interface
//////////////////////////////////////////////////////////////////////
// - (void)testCallback:(int)command
//            withParam:(NSMutableArray *)param;
- (void)initTest:(id)context;
- (void)testCallback;
- (void)testSwiftCall:(id)context ;

@end

#endif /* SaaringWrapper_h */
