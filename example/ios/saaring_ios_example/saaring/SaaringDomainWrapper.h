//
//  SaaringDomainWrapper.h
//  saaring
//
//  Created by kojang on 30/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//

#ifndef SaaringDomainWrapper_h
#define SaaringDomainWrapper_h

#import <Foundation/Foundation.h>

//////////////////////////////////////////////////////////////////////
// SaaringSetupInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringSetupInfo : NSObject

@property NSString *idType;                         // 0 email, 1 phone
@property NSString *email;                          // 로그인 ID(email)
@property NSString *phoneNumber;                    // phoneNumber
@property NSString *password;                       // encrypt
@property NSString *providerId;                     // providerId
@property NSString *idToken;                        // idToken
@property NSString *accessToken;                    // accessToken
@property NSString *uid;                            // uid
@property NSString *isUseMobileNetwork;             // 3G/4G 모바일 네트웍 사용여부
@property NSString *isSaveEmail;                    // 로그인시, 아이디 저장여부.       0 unchecked, 1 checkecd
@property NSString *isAutoLogin;                    // 로그인시, 자동로그인 여부.       0 unchecked, 1 checkecd
@property NSString *isLastLoginEmail;               // 최종 로그인한 아이디인지 여부     0 nomal, 1 last login (최종 로그인한 ID만 1로 세팅되어 있음)
@property NSString *isAutoStart;                    // 컴퓨터 부팅시, 자동실행 여부
@property NSString *isTransferListAutoResend;       // 전송목록에서 자동 재전송 여부
@property NSString *isTransferListAutoDelete;       // 전송목록에서 전송완료 목록 자동 삭제 여부
@property NSString *transferListResendMinute;       // 전송목록에서 자동 재전송 체크 시간 간격
@property NSString *isDisplayTraySendStatus;        // Tray에 전송상태 표시여부
@property NSString *isContinueReceiveFile;          // 파일 전송시, 이어받기 여부
@property NSString *isRelayServer;                  // 중계서버로 사용할지 여부(미사용
@property int relayServerPort;                      // 중계서버로 사용될때, 중계서버의 port
@property int otherDownloadCount;                   // 최대 다운로드 허용 수 (나에게 다운로드 해갈 수 있는 peer의 최대 갯수)
@property int otherDownloadSpeed;                   // 최대 다운로드 속도 (나에게 다운로드 해갈 수 있는 peer의 최대 전송 속도; 단위=KB/sec)
@property int myDownloadSpeed;                      // 내가 다운로드 받을때의 최대 다운로드 속도 (다운로드 갯수는 제한하지 않는다. 느리면 중단하면 되기 때문에; 단위=KB/sec)
@property int otherUploadCount;                     // 최대 업로드 허용 수 (나에게 업로드 할 수 있는 peer의 최대 갯수)
@property int otherUploadSpeed;                     // 최대 업로드 속도 (나에게 업로드 할 수 있는 peer의 최대 전송 속도; 단위=KB/sec)
@property int myUploadSpeed;                        // 내가 업로드 할때의 최대 업로드 속도 (업로드 갯수는 제한하지 않는다. 느리면 중단하면 되기 때문에; 단위=KB/sec)
@property NSString *deviceOpenFriendUserNo;         // 디바이스 공개 수준에서, 특정 친구에게 오픈할경우, 회원번호 리스트 (미사용)
@property NSString *deviceOpenFriendUserName;       // 디바이스 공개 수준에서, 특정 친구에게 오픈할경우, 이름 리스트 (미사용)
@property int portmapWanCtrlPort;                   // 포트매핑 정보. Control 용
@property int portmapLanCtrlPort;                   // 포트매핑 정보. Control 용
@property int portmapWanFilePort;                   // 포트매핑 정보. Filer 용
@property int portmapLanFilePort;                   // 포트매핑 정보. Filer 용
@property int portmapWanRelayPort;                  // 포트매핑 정보. Filer 용
@property int portmapLanRelayPort;                  // 포트매핑 정보. Filer 용
@property NSString *localIpAddress;                 // 포트매핑 정보. 로컬 IP

+ (void) convertQtSaaringSetupInfoToObjC : (void *)qt objc:(SaaringSetupInfo *)objc;
+ (void) convertObjCSaaringSetupInfoToQt : (SaaringSetupInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringMemberInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringMemberInfo : NSObject

@property NSString *authKey;

@property NSString *runningVersion;
@property NSString *lastVersion;

@property long long deviceNo;
@property int deviceType;
@property NSString *deviceModel;
@property NSString *deviceName;

@property NSString *language;

@property NSString *osName;
@property NSString *osVersion;

@property long long userNo;
@property NSString *userId;
@property NSString *userName;

@property NSString *password;

@property NSString *email;
@property NSString *phoneNumber;

@property NSString *firstName;
@property NSString *lastName;

@property NSString *profilePic;
@property NSString *message;

@property int idType;
@property NSString *providerId;
@property NSString *idToken;
@property NSString *accessToken;
@property NSString *uid;
@property NSString *providerYN;
@property int numProvider;

@property long long defaultServerFolderNo;

@property int joinStatus;
@property NSString *pushKey;
@property NSString *loginKey;

@property NSString *firebaseToken;

// id/pw확인
@property long long certNo;
@property long long certType;
@property NSString *certKey;

// additial member
@property bool isNewUser;

+ (void) convertQtSaaringMemberInfoToObjC : (void *)qt objc:(SaaringMemberInfo *)objc;
+ (void) convertObjCSaaringMemberInfoToQt : (SaaringMemberInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringMemberProviderInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringMemberProviderInfo : NSObject

@property long long userNo;

@property int idType;
@property NSString *providerId;
@property NSString *idToken;
@property NSString *accessToken;
@property NSString *uid;
@property NSString *providerYN;

+ (void)convertQtSaaringMemberProviderInfoToObjC:(void *)qt objc:(SaaringMemberProviderInfo *)objc;
+ (void)convertObjCSaaringMemberProviderInfoToQt:(SaaringMemberProviderInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringFriendInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringFriendInfo : SaaringMemberInfo

@property NSString *favoriteYN;
@property NSString *regdt;

@property int numDevice;
@property int numLiveDevice;
@property int numOfflineMobileDevice;
@property int numFile;
@property int numFolder;
@property long long totalFileSize;

@property int requestStatus;
@property long long requestdt;

+ (void) convertQtSaaringFriendInfoToObjC : (void *)qt objc:(SaaringFriendInfo *)objc;
+ (void) convertObjCSaaringFriendInfoToQt : (SaaringFriendInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteUsersInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringInviteUsersInfo : NSObject

@property long long inviteNo;
@property long long userNo;
@property long long toUserNo;

@property NSString *toEmail;
@property NSString *toPhoneNumber;
@property long long regdt;

+ (void)convertQtSaaringInviteUsersInfoToObjC:(void *)qt objc:(SaaringInviteUsersInfo *)objc;
+ (void)convertObjCSaaringInviteUsersInfoToQt:(SaaringInviteUsersInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteFilesInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringInviteFilesInfo : NSObject

@property long long fileId;
@property long long inviteNo;

@property NSString *certKey;
@property NSString *localPath;
@property NSString *indirectPath;
@property NSString *fileName;
@property long long fileSize;

@property long long expiredt;
@property long long regdt;

+ (void)convertQtSaaringInviteFilesInfoToObjC:(void *)qt objc:(SaaringInviteFilesInfo *)objc;
+ (void)convertObjCSaaringInviteFilesInfoToQt:(SaaringInviteFilesInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteSecretInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringInviteSecretInfo : NSObject

@property long long userNo;
@property NSString *secretCode;
@property long long expiredt;
@property long long regdt;

+ (void)convertQtSaaringInviteSecretInfoToObjC:(void *)qt objc:(SaaringInviteSecretInfo *)objc;
+ (void)convertObjCSaaringInviteSecretInfoToQt:(SaaringInviteSecretInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringInviteInfo : NSObject

@property long long inviteNo;
@property NSString *inviteCode;

@property long long userNo;
@property NSString *userName;

@property long long inviteDeviceNo;
@property int inviteDeviceType;
@property NSString *inviteDeviceName;
@property int inviteType;

@property NSString *certKey;
@property long long numFile;
@property long long totalFileSize;

@property long long inviteShareFolderServerFolderNo;
@property NSString *inviteShareFolderName;
@property NSString *inviteShareFolderPath;
@property NSString *inviteShareFolderPasswordYN;

@property NSString *inviteMessage;

@property NSString *dynamicLink;
@property NSString *rejectYN;

@property long long expiredt;
@property long long regdt;

+ (void)convertQtSaaringInviteInfoToObjC:(void *)qt objc:(SaaringInviteInfo *)objc;
+ (void)convertObjCSaaringInviteInfoToQt:(SaaringInviteInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteRegisteredInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringInviteRegisteredInfo : NSObject

@property long long userNo;
@property long long inviteNo;
@property NSString *inviteCode;

@property int inviteType;
@property NSString *certKey;

@property long long fromUserNo;
@property NSString *fromUserName;

@property NSString *inviteMessage;
@property NSString *dynamicLink;

@property long long expiredt;
@property long long regdt;

+ (void)convertQtSaaringInviteRegisteredInfoToObjC:(void *)qt objc:(SaaringInviteRegisteredInfo *)objc;
+ (void)convertObjCSaaringInviteRegisteredInfoToQt:(SaaringInviteRegisteredInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringBlockInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringBlockInfo : NSObject

@property long long userNo;
@property NSString *email;
@property NSString *phoneNumber;
@property int idType;
@property NSString *userId;
@property NSString *userName;
@property NSString *profilePic;

+ (void) convertQtSaaringBlockInfoToObjC : (void *)qt objc:(SaaringBlockInfo *)objc;
+ (void) convertObjCSaaringBlockInfoToQt : (SaaringBlockInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringTransferBaseInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringTransferBaseInfo : NSObject

@property long long transNo;
@property long long userNo;
@property NSString *userName;
@property long long deviceNo;
@property int deviceType;
@property NSString *deviceName;
@property NSString *upDown;
@property int transStatus;
@property NSString *localPath;
@property NSString *remotePath;
@property long long fileSize;
@property long long fileStartPosition;
@property long long transSize;
@property NSString *deleteYN;
@property int sortNo;
@property long long updateTime;
@property long long myDeviceNo;
@property long long inviteNo;
@property long long fileId;

+ (void) convertQtSaaringTransferBaseInfoToObjC : (void *)qt objc:(SaaringTransferBaseInfo *)objc;
+ (void) convertObjCSaaringTransferBaseInfoToQt : (SaaringTransferBaseInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringTransferInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringTransferInfo : NSObject

@property NSString *fileName;       // 파일이름
@property int status;               // 상태

@property long long fileSize;       // 파일사이즈
@property NSString *sendSpeed;      // 전송속도
@property NSString *leftTime;       // 남은시간
@property long long transNo;

@property long long currSize;       // 현재까지 전송된 사이즈
@property NSString *currTime;       // 현재까지 마지막으로 전송한 시간. YYYYMMddHHmmsszzz
@property long long userNo;         // toUserNo
@property NSString *userName;       // 서버

@property long long deviceNo;
@property int deviceType;
@property NSString *deviceName;     // 디바이스

@property NSString *upDown;         // U or D
@property bool isMyAction;          // 내가 down/up 이면 true, 상대방이 down/up 이면 false
@property NSString *folderName;     // 공유폴더명
@property NSString *remotePath;
@property long long inviteNo;
@property long long fileId;

+ (void) convertQtSaaringTransferInfoToObjC : (void *)qt objc:(SaaringTransferInfo *)objc;
+ (void) convertObjCSaaringTransferInfoToQt : (SaaringTransferInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringShareInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringShareInfo : NSObject

@property long long shareNo;
@property long long deviceNo;
@property NSString *shareFolderPath;
@property NSString *shareName;

// 서버에서발행하는 folderno
@property long long serverFolderNo;
@property int fileCount;
@property long long fileSize;

@property int downKind;
@property NSMutableArray *listDownFriendUserNo;         // long long
@property NSMutableArray *listDownFriendUserName;       // String
@property int upKind;
@property NSMutableArray *listUpFriendUserNo;           // long long
@property NSMutableArray *listUpFriendUserName;         // String

@property NSString *downYN;
@property NSString *upYN;
@property NSString *password;
@property NSString *passwordYN;

+ (void) convertQtSaaringShareInfoToObjC : (void *)qt objc:(SaaringShareInfo *)objc;
+ (void) convertObjCSaaringShareInfoToQt : (SaaringShareInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringShareDetailInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringShareDetailInfo : NSObject

@property long long shareDetailNo;
@property long long deviceNo;
@property long long shareNo;
@property NSString *fullPathName;
@property NSString *shortPathName;
@property NSString *fileName;
@property NSString *fileExtension;
@property long long fileSize;
@property long long updateTime;
@property NSString *folderYN;       // Y(Yes) or N(No)

+ (void) convertQtSaaringShareDetailInfoToObjC : (void *)qt objc:(SaaringShareDetailInfo *)objc;
+ (void) convertObjCSaaringShareDetailInfoToQt : (SaaringShareDetailInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringShareFolderInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringShareFolderInfo : NSObject

@property long long deviceNo;
@property int deviceType;
@property NSString *deviceName;
@property NSString *liveYN;

@property long long serverFolderNo;

@property NSString *folderName;
@property long long fileSize;
@property int fileCount;

@property int shareType;
@property int downShareType;
@property int upShareType;
@property int shareFoaf;

@property int numFolder;
@property int numFile;
@property long long totalFileSize;

@property NSString *password;
@property NSString *passwordYN;
@property NSString *downloadYN;
@property NSString *uploadYN;

@property NSMutableArray *listDownFriendUserNo;     // long long
@property NSMutableArray *listUpFriendUserNo;       // long long

+ (void) convertQtSaaringShareFolderInfoToObjC : (void *)qt objc:(SaaringShareFolderInfo *)objc;
+ (void) convertObjCSaaringShareFolderInfoToQt : (SaaringShareFolderInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringBadgeInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringBadgeInfo : NSObject

@property int numNewRequestFriend;
@property int numNewNotice;

+ (void) convertQtSaaringBadgeInfoToObjC : (void *)qt objc:(SaaringBadgeInfo *)objc;
+ (void) convertObjCSaaringBadgeInfoToQt : (SaaringBadgeInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringServerInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringServerInfo : NSObject

@property NSString *signalingIp;
@property int signalingPort;
@property NSString *rendezvousIp;
@property int rendezvousPort;
@property NSString *relayIp;
@property int relayPort;
@property NSString *runningVersion;
@property NSString *lastVersion;

+ (void) convertQtSaaringServerInfoToObjC : (void *)qt objc:(SaaringServerInfo *)objc;
+ (void) convertObjCSaaringServerInfoToQt : (SaaringServerInfo *)objc qt:(void *)qt;

@end

//////////////////////////////////////////////////////////////////////
// SaaringPhoneAddressInfo
//////////////////////////////////////////////////////////////////////

@interface SaaringPhoneAddressInfo : NSObject

@property long long userNo;
@property NSString *phoneNumber;
@property int countryCode;
@property long long nationalNumber;
@property long long friendUserNo;
@property NSString *friendYN;
@property NSString *blockYN;
@property long long matchingdt;
@property long long frienddt;
@property long long regdt;
@property NSString *userName;
@property NSString *email;
@property NSString *profilePic;
@property NSString *message;

+ (void) convertQtSaaringPhoneAddressInfoToObjC : (void *)qt objc:(SaaringPhoneAddressInfo *)objc;
+ (void) convertObjCSaaringPhoneAddressInfoToQt : (SaaringPhoneAddressInfo *)objc qt:(void *)qt;

@end

#endif /* SaaringDomainWrapper_h */
