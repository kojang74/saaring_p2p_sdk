//
//  SaaringDomainWrapper.m
//  saaring
//
//  Created by kojang on 30/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//
#import "SaaringDomainWrapper.h"
#import <Foundation/Foundation.h>

#include "api/saaringcommoninterface.h"
#include "api/saaringdbinterface.h"
#include "api/saaringdomain.h"
#include "api/saaringp2pinterface.h"
#include "api/saaringsharefolderinterface.h"
#include "api/saaringwebinterface.h"

#include "SaaringUtility.hpp"

//////////////////////////////////////////////////////////////////////
// SaaringSetupInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringSetupInfo

+ (void) convertQtSaaringSetupInfoToObjC : (saaring::SaaringSetupInfo *)qt objc:(SaaringSetupInfo *)objc {
    
    objc.idType                     = [SaaringUtility convertQStringToNSString:qt->idType];
    objc.email                      = [SaaringUtility convertQStringToNSString:qt->email];
    objc.phoneNumber                = [SaaringUtility convertQStringToNSString:qt->phoneNumber];
    objc.password                   = [SaaringUtility convertQStringToNSString:qt->password];
    objc.providerId                 = [SaaringUtility convertQStringToNSString:qt->providerId];
    objc.idToken                    = [SaaringUtility convertQStringToNSString:qt->idToken];
    objc.accessToken                = [SaaringUtility convertQStringToNSString:qt->accessToken];
    objc.uid                        = [SaaringUtility convertQStringToNSString:qt->uid];
    objc.isUseMobileNetwork         = [SaaringUtility convertQStringToNSString:qt->isUseMobileNetwork];
    objc.isSaveEmail                = [SaaringUtility convertQStringToNSString:qt->isSaveEmail];
    objc.isAutoLogin                = [SaaringUtility convertQStringToNSString:qt->isAutoLogin];
    objc.isLastLoginEmail           = [SaaringUtility convertQStringToNSString:qt->isLastLoginEmail];
    objc.isAutoStart                = [SaaringUtility convertQStringToNSString:qt->isAutoStart];
    objc.isTransferListAutoResend   = [SaaringUtility convertQStringToNSString:qt->isTransferListAutoResend];
    objc.isTransferListAutoDelete   = [SaaringUtility convertQStringToNSString:qt->isTransferListAutoDelete];
    objc.transferListResendMinute   = [SaaringUtility convertQStringToNSString:qt->transferListResendMinute];
    objc.isDisplayTraySendStatus    = [SaaringUtility convertQStringToNSString:qt->isDisplayTraySendStatus];
    objc.isContinueReceiveFile      = [SaaringUtility convertQStringToNSString:qt->isContinueReceiveFile];
    objc.isRelayServer              = [SaaringUtility convertQStringToNSString:qt->isRelayServer];
    objc.relayServerPort            = qt->relayServerPort;
    objc.otherDownloadCount         = qt->otherDownloadCount;
    objc.otherDownloadSpeed         = qt->otherDownloadSpeed;
    objc.myDownloadSpeed            = qt->myDownloadSpeed;
    objc.otherUploadCount           = qt->otherUploadCount;
    objc.otherUploadSpeed           = qt->otherUploadSpeed;
    objc.myUploadSpeed              = qt->myUploadSpeed;
    objc.deviceOpenFriendUserNo     = [SaaringUtility convertQStringToNSString:qt->deviceOpenFriendUserNo];
    objc.deviceOpenFriendUserName   = [SaaringUtility convertQStringToNSString:qt->deviceOpenFriendUserName];
    objc.portmapWanCtrlPort         = qt->portmapWanCtrlPort;
    objc.portmapLanCtrlPort         = qt->portmapLanCtrlPort;
    objc.portmapWanFilePort         = qt->portmapWanFilePort;
    objc.portmapLanFilePort         = qt->portmapLanFilePort;
    objc.portmapWanRelayPort        = qt->portmapWanRelayPort;
    objc.portmapLanRelayPort        = qt->portmapLanRelayPort;
    objc.localIpAddress             = [SaaringUtility convertQStringToNSString:qt->localIpAddress];
}
+ (void) convertObjCSaaringSetupInfoToQt : (SaaringSetupInfo *)objc qt:(saaring::SaaringSetupInfo *)qt {
    
    qt->idType                      = [SaaringUtility convertNSStringToQString:objc.idType];
    qt->email                       = [SaaringUtility convertNSStringToQString:objc.email];
    qt->phoneNumber                 = [SaaringUtility convertNSStringToQString:objc.phoneNumber];
    qt->password                    = [SaaringUtility convertNSStringToQString:objc.password];
    qt->providerId                  = [SaaringUtility convertNSStringToQString:objc.providerId];
    qt->idToken                     = [SaaringUtility convertNSStringToQString:objc.idToken];
    qt->accessToken                 = [SaaringUtility convertNSStringToQString:objc.accessToken];
    qt->uid                         = [SaaringUtility convertNSStringToQString:objc.uid];
    qt->isUseMobileNetwork          = [SaaringUtility convertNSStringToQString:objc.isUseMobileNetwork];
    qt->isSaveEmail                 = [SaaringUtility convertNSStringToQString:objc.isSaveEmail];
    qt->isAutoLogin                 = [SaaringUtility convertNSStringToQString:objc.isAutoLogin];
    qt->isLastLoginEmail            = [SaaringUtility convertNSStringToQString:objc.isLastLoginEmail];
    qt->isAutoStart                 = [SaaringUtility convertNSStringToQString:objc.isAutoStart];
    qt->isTransferListAutoResend    = [SaaringUtility convertNSStringToQString:objc.isTransferListAutoResend];
    qt->isTransferListAutoDelete    = [SaaringUtility convertNSStringToQString:objc.isTransferListAutoDelete];
    qt->transferListResendMinute    = [SaaringUtility convertNSStringToQString:objc.transferListResendMinute];
    qt->isDisplayTraySendStatus     = [SaaringUtility convertNSStringToQString:objc.isDisplayTraySendStatus];
    qt->isContinueReceiveFile       = [SaaringUtility convertNSStringToQString:objc.isContinueReceiveFile];
    qt->isRelayServer               = [SaaringUtility convertNSStringToQString:objc.isRelayServer];
    qt->relayServerPort             = objc.relayServerPort;
    qt->otherDownloadCount          = objc.otherDownloadCount;
    qt->otherDownloadSpeed          = objc.otherDownloadSpeed;
    qt->myDownloadSpeed             = objc.myDownloadSpeed;
    qt->otherUploadCount            = objc.otherUploadCount;
    qt->otherUploadSpeed            = objc.otherUploadSpeed;
    qt->myUploadSpeed               = objc.myUploadSpeed;
    qt->deviceOpenFriendUserNo      = [SaaringUtility convertNSStringToQString:objc.deviceOpenFriendUserNo];
    qt->deviceOpenFriendUserName    = [SaaringUtility convertNSStringToQString:objc.deviceOpenFriendUserName];
    qt->portmapWanCtrlPort          = objc.portmapWanCtrlPort;
    qt->portmapLanCtrlPort          = objc.portmapLanCtrlPort;
    qt->portmapWanFilePort          = objc.portmapWanFilePort;
    qt->portmapLanFilePort          = objc.portmapLanFilePort;
    qt->portmapWanRelayPort         = objc.portmapWanRelayPort;
    qt->portmapLanRelayPort         = objc.portmapLanRelayPort;
    qt->localIpAddress              = [SaaringUtility convertNSStringToQString:objc.localIpAddress];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringMemberInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringMemberInfo

- (id) init {
    
    self = [super init];
    if(self) {
        self.isNewUser = false;
    }
    
    return self;
}

+ (void) convertQtSaaringMemberInfoToObjC : (saaring::SaaringMemberInfo *)qt objc:(SaaringMemberInfo *)objc {
    
    objc.authKey                    = [SaaringUtility convertQStringToNSString:qt->authKey];
    
    objc.runningVersion             = [SaaringUtility convertQStringToNSString:qt->runningVersion];
    objc.lastVersion                = [SaaringUtility convertQStringToNSString:qt->lastVersion];
    
    objc.deviceNo                   = qt->deviceNo;
    objc.deviceType                 = qt->deviceType;
    objc.deviceModel                = [SaaringUtility convertQStringToNSString:qt->deviceModel];
    objc.deviceName                 = [SaaringUtility convertQStringToNSString:qt->deviceName];
    
    objc.language                   = [SaaringUtility convertQStringToNSString:qt->language];
    
    objc.osName                     = [SaaringUtility convertQStringToNSString:qt->osName];
    objc.osVersion                  = [SaaringUtility convertQStringToNSString:qt->osVersion];
    
    objc.userNo                     = qt->userNo;
    objc.userId                     = [SaaringUtility convertQStringToNSString:qt->userId];
    objc.userName                   = [SaaringUtility convertQStringToNSString:qt->userName];
    
    objc.password                   = [SaaringUtility convertQStringToNSString:qt->password];
    
    objc.email                      = [SaaringUtility convertQStringToNSString:qt->email];
    objc.phoneNumber                = [SaaringUtility convertQStringToNSString:qt->phoneNumber];
    
    objc.firstName                  = [SaaringUtility convertQStringToNSString:qt->firstName];
    objc.lastName                   = [SaaringUtility convertQStringToNSString:qt->lastName];
    
    objc.profilePic                 = [SaaringUtility convertQStringToNSString:qt->profilePic];
    objc.message                    = [SaaringUtility convertQStringToNSString:qt->message];

    objc.idType                     = qt->idType;
    objc.providerId                 = [SaaringUtility convertQStringToNSString:qt->providerId];
    objc.idToken                    = [SaaringUtility convertQStringToNSString:qt->idToken];
    objc.accessToken                = [SaaringUtility convertQStringToNSString:qt->accessToken];
    objc.uid                        = [SaaringUtility convertQStringToNSString:qt->uid];
    objc.providerYN                 = [SaaringUtility convertQStringToNSString:qt->providerYN];
    objc.numProvider                = qt->numProvider;

    objc.defaultServerFolderNo      = qt->defaultServerFolderNo;
    
    objc.joinStatus                 = qt->joinStatus;
    objc.pushKey                    = [SaaringUtility convertQStringToNSString:qt->pushKey];
    objc.loginKey                   = [SaaringUtility convertQStringToNSString:qt->loginKey];
    
    objc.firebaseToken              = [SaaringUtility convertQStringToNSString:qt->firebaseToken];
    
    objc.certNo                     = qt->certNo;
    objc.certType                   = qt->certType;
    objc.certKey                    = [SaaringUtility convertQStringToNSString:qt->certKey];
    
    objc.isNewUser                  = qt->isNewUser;
}
+ (void) convertObjCSaaringMemberInfoToQt : (SaaringMemberInfo *)objc qt:(saaring::SaaringMemberInfo *)qt {
 
    qt->authKey                     = [SaaringUtility convertNSStringToQString:objc.authKey];
    
    qt->runningVersion              = [SaaringUtility convertNSStringToQString:objc.runningVersion];
    qt->lastVersion                 = [SaaringUtility convertNSStringToQString:objc.lastVersion];
    
    qt->deviceNo                    = objc.deviceNo;
    qt->deviceType                  = objc.deviceType;
    qt->deviceModel                 = [SaaringUtility convertNSStringToQString:objc.deviceModel];
    qt->deviceName                  = [SaaringUtility convertNSStringToQString:objc.deviceName];
    
    qt->language                    = [SaaringUtility convertNSStringToQString:objc.language];
    
    qt->osName                      = [SaaringUtility convertNSStringToQString:objc.osName];
    qt->osVersion                   = [SaaringUtility convertNSStringToQString:objc.osVersion];
    
    qt->userNo                      = objc.userNo;
    qt->userId                      = [SaaringUtility convertNSStringToQString:objc.userId];
    qt->userName                    = [SaaringUtility convertNSStringToQString:objc.userName];
    
    qt->password                    = [SaaringUtility convertNSStringToQString:objc.password];
    
    qt->email                       = [SaaringUtility convertNSStringToQString:objc.email];
    qt->phoneNumber                 = [SaaringUtility convertNSStringToQString:objc.phoneNumber];
    
    qt->firstName                   = [SaaringUtility convertNSStringToQString:objc.firstName];
    qt->lastName                    = [SaaringUtility convertNSStringToQString:objc.lastName];
    
    qt->profilePic                  = [SaaringUtility convertNSStringToQString:objc.profilePic];
    qt->message                     = [SaaringUtility convertNSStringToQString:objc.message];
    
    qt->idType                      = objc.idType;
    qt->providerId                  = [SaaringUtility convertNSStringToQString:objc.providerId];
    qt->idToken                     = [SaaringUtility convertNSStringToQString:objc.idToken];
    qt->accessToken                 = [SaaringUtility convertNSStringToQString:objc.accessToken];
    qt->uid                         = [SaaringUtility convertNSStringToQString:objc.uid];
    qt->providerYN                  = [SaaringUtility convertNSStringToQString:objc.providerYN];
    qt->numProvider                 = objc.numProvider;
    
    qt->defaultServerFolderNo       = objc.defaultServerFolderNo;

    qt->joinStatus                  = objc.joinStatus;
    qt->pushKey                     = [SaaringUtility convertNSStringToQString:objc.pushKey];
    qt->loginKey                    = [SaaringUtility convertNSStringToQString:objc.loginKey];
    
    qt->firebaseToken               = [SaaringUtility convertNSStringToQString:objc.firebaseToken];
    
    qt->certNo                      = objc.certNo;
    qt->certType                    = objc.certType;
    qt->certKey                     = [SaaringUtility convertNSStringToQString:objc.certKey];
    
    qt->isNewUser                   = objc.isNewUser;
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringMemberProviderInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringMemberProviderInfo

+ (void) convertQtSaaringMemberProviderInfoToObjC : (saaring::SaaringMemberProviderInfo *)qt objc:(SaaringMemberProviderInfo *)objc {
    
    objc.userNo                     = qt->userNo;
    
    objc.idType                     = qt->idType;
    objc.providerId                 = [SaaringUtility convertQStringToNSString:qt->providerId];
    objc.idToken                    = [SaaringUtility convertQStringToNSString:qt->idToken];
    objc.accessToken                = [SaaringUtility convertQStringToNSString:qt->accessToken];
    objc.uid                        = [SaaringUtility convertQStringToNSString:qt->uid];
    objc.providerYN                 = [SaaringUtility convertQStringToNSString:qt->providerYN];
}
- (void) convertObjCSaaringMemberProviderInfoToQt : (SaaringMemberProviderInfo *)objc qt:(saaring::SaaringMemberProviderInfo *)qt {
    
    qt->userNo                      = objc.userNo;

    qt->idType                      = objc.idType;
    qt->providerId                  = [SaaringUtility convertNSStringToQString:objc.providerId];
    qt->idToken                     = [SaaringUtility convertNSStringToQString:objc.idToken];
    qt->accessToken                 = [SaaringUtility convertNSStringToQString:objc.accessToken];
    qt->uid                         = [SaaringUtility convertNSStringToQString:objc.uid];
    qt->providerYN                  = [SaaringUtility convertNSStringToQString:objc.providerYN];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringFriendInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringFriendInfo

+ (void) convertQtSaaringFriendInfoToObjC : (saaring::SaaringFriendInfo *)qt objc:(SaaringFriendInfo *)objc {
    
    objc.favoriteYN                 = [SaaringUtility convertQStringToNSString:qt->favoriteYN];
    objc.regdt                      = [SaaringUtility convertQStringToNSString:qt->regdt];
    
    objc.numDevice                  = qt->numDevice;
    objc.numLiveDevice              = qt->numLiveDevice;
    objc.numOfflineMobileDevice     = qt->numOfflineMobileDevice;
    objc.numFile                    = qt->numFile;
    objc.numFolder                  = qt->numFolder;
    objc.totalFileSize              = qt->totalFileSize;
    
    objc.requestStatus              = qt->requestStatus;
    objc.requestdt                  = [SaaringUtility convertQDateTimeToLongLong:qt->requestdt];
    
    [SaaringMemberInfo convertQtSaaringMemberInfoToObjC:qt objc:objc];
}
- (void) convertObjCSaaringFriendInfoToQt : (SaaringFriendInfo *)objc qt:(saaring::SaaringFriendInfo *)qt {
    
    qt->favoriteYN                  = [SaaringUtility convertNSStringToQString:objc.favoriteYN];
    qt->regdt                       = [SaaringUtility convertNSStringToQString:objc.regdt];
    
    qt->numDevice                   = objc.numDevice;
    qt->numLiveDevice               = objc.numLiveDevice;
    qt->numOfflineMobileDevice      = objc.numOfflineMobileDevice;
    qt->numFile                     = objc.numFile;
    qt->numFolder                   = objc.numFolder;
    qt->totalFileSize               = objc.totalFileSize;
    
    qt->requestStatus               = objc.requestStatus;
    qt->requestdt                   = [SaaringUtility convertLongLongToQDateTime:objc.requestdt];
    
    [SaaringMemberInfo convertObjCSaaringMemberInfoToQt:objc qt:qt];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteUsersInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringInviteUsersInfo

+ (void) convertQtSaaringInviteUsersInfoToObjC : (saaring::SaaringInviteUsersInfo *)qt objc:(SaaringInviteUsersInfo *)objc {
    
    objc.inviteNo                   = qt->inviteNo;
    objc.userNo                     = qt->userNo;
    objc.toUserNo                   = qt->toUserNo;

    objc.toEmail                    = [SaaringUtility convertQStringToNSString:qt->toEmail];
    objc.toPhoneNumber              = [SaaringUtility convertQStringToNSString:qt->toPhoneNumber];
    objc.regdt                      = [SaaringUtility convertQDateTimeToLongLong:qt->regdt];
}
- (void) convertObjCSaaringInviteUsersInfoToQt : (SaaringInviteUsersInfo *)objc qt:(saaring::SaaringInviteUsersInfo *)qt {
    
    qt->inviteNo                    = objc.inviteNo;
    qt->userNo                      = objc.userNo;
    qt->toUserNo                    = objc.toUserNo;

    qt->toEmail                     = [SaaringUtility convertNSStringToQString:objc.toEmail];
    qt->toPhoneNumber               = [SaaringUtility convertNSStringToQString:objc.toPhoneNumber];
    qt->regdt                       = [SaaringUtility convertLongLongToQDateTime:objc.regdt];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteFilesInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringInviteFilesInfo

+ (void) convertQtSaaringInviteFilesInfoToObjC : (saaring::SaaringInviteFilesInfo *)qt objc:(SaaringInviteFilesInfo *)objc {
    
    objc.fileId                     = qt->fileId;
    objc.inviteNo                   = qt->inviteNo;

    objc.certKey                    = [SaaringUtility convertQStringToNSString:qt->certKey];
    objc.localPath                  = [SaaringUtility convertQStringToNSString:qt->localPath];
    objc.indirectPath               = [SaaringUtility convertQStringToNSString:qt->indirectPath];
    objc.fileName                   = [SaaringUtility convertQStringToNSString:qt->fileName];
    objc.fileSize                   = qt->fileSize;

    objc.expiredt                   = [SaaringUtility convertQDateTimeToLongLong:qt->expiredt];
    objc.regdt                      = [SaaringUtility convertQDateTimeToLongLong:qt->regdt];
}
- (void) convertObjCSaaringInviteFilesInfoToQt : (SaaringInviteFilesInfo *)objc qt:(saaring::SaaringInviteFilesInfo *)qt {
    
    qt->fileId                      = objc.fileId;
    qt->inviteNo                    = objc.inviteNo;

    qt->certKey                     = [SaaringUtility convertNSStringToQString:objc.certKey];
    qt->localPath                   = [SaaringUtility convertNSStringToQString:objc.localPath];
    qt->indirectPath                = [SaaringUtility convertNSStringToQString:objc.indirectPath];
    qt->fileName                    = [SaaringUtility convertNSStringToQString:objc.fileName];
    qt->fileSize                    = objc.fileSize;
    
    qt->expiredt                    = [SaaringUtility convertLongLongToQDateTime:objc.expiredt];
    qt->regdt                       = [SaaringUtility convertLongLongToQDateTime:objc.regdt];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteSecretInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringInviteSecretInfo

+ (void) convertQtSaaringInviteSecretInfoToObjC : (saaring::SaaringInviteSecretInfo *)qt objc:(SaaringInviteSecretInfo *)objc {
    
    objc.userNo                     = qt->userNo;
    objc.secretCode                 = [SaaringUtility convertQStringToNSString:qt->secretCode];
    
    objc.expiredt                   = [SaaringUtility convertQDateTimeToLongLong:qt->expiredt];
    objc.regdt                      = [SaaringUtility convertQDateTimeToLongLong:qt->regdt];
}
- (void) convertObjCSaaringInviteSecretInfoToQt : (SaaringInviteSecretInfo *)objc qt:(saaring::SaaringInviteSecretInfo *)qt {
    
    qt->userNo                      = objc.userNo;
    qt->secretCode                  = [SaaringUtility convertNSStringToQString:objc.secretCode];
    
    qt->expiredt                    = [SaaringUtility convertLongLongToQDateTime:objc.expiredt];
    qt->regdt                       = [SaaringUtility convertLongLongToQDateTime:objc.regdt];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringInviteInfo

+ (void) convertQtSaaringInviteInfoToObjC : (saaring::SaaringInviteInfo *)qt objc:(SaaringInviteInfo *)objc {
    
    objc.inviteNo                   = qt->inviteNo;
    objc.inviteCode                 = [SaaringUtility convertQStringToNSString:qt->inviteCode];

    objc.userNo                     = qt->userNo;
    objc.userName                   = [SaaringUtility convertQStringToNSString:qt->userName];

    objc.inviteDeviceNo             = qt->inviteDeviceNo;
    objc.inviteDeviceType           = qt->inviteDeviceType;
    objc.inviteDeviceName           = [SaaringUtility convertQStringToNSString:qt->inviteDeviceName];
    objc.inviteType                 = qt->inviteType;

    objc.certKey                    = [SaaringUtility convertQStringToNSString:qt->certKey];
    objc.numFile                    = qt->numFile;
    objc.totalFileSize              = qt->totalFileSize;

    objc.inviteShareFolderServerFolderNo = qt->inviteShareFolderServerFolderNo;
    objc.inviteShareFolderName      = [SaaringUtility convertQStringToNSString:qt->inviteShareFolderName];
    objc.inviteShareFolderPath      = [SaaringUtility convertQStringToNSString:qt->inviteShareFolderPath];
    objc.inviteShareFolderPasswordYN = [SaaringUtility convertQStringToNSString:qt->inviteShareFolderPasswordYN];

    objc.inviteMessage              = [SaaringUtility convertQStringToNSString:qt->inviteMessage];

    objc.dynamicLink                = [SaaringUtility convertQStringToNSString:qt->dynamicLink];
    objc.rejectYN                   = [SaaringUtility convertQStringToNSString:qt->rejectYN];

    objc.expiredt                   = [SaaringUtility convertQDateTimeToLongLong:qt->expiredt];
    objc.regdt                      = [SaaringUtility convertQDateTimeToLongLong:qt->regdt];
}
- (void) convertObjCSaaringInviteInfoToQt : (SaaringInviteInfo *)objc qt:(saaring::SaaringInviteInfo *)qt {
    
    qt->inviteNo                    = objc.inviteNo;
    qt->inviteCode                  = [SaaringUtility convertNSStringToQString:objc.inviteCode];

    qt->userNo                      = objc.userNo;
    qt->userName                    = [SaaringUtility convertNSStringToQString:objc.userName];

    qt->inviteDeviceNo              = objc.inviteDeviceNo;
    qt->inviteDeviceType            = objc.inviteDeviceType;
    qt->inviteDeviceName            = [SaaringUtility convertNSStringToQString:objc.inviteDeviceName];
    qt->inviteType                  = objc.inviteType;

    qt->certKey                     = [SaaringUtility convertNSStringToQString:objc.certKey];
    qt->numFile                     = objc.numFile;
    qt->totalFileSize               = objc.totalFileSize;

    qt->inviteShareFolderServerFolderNo = objc.inviteShareFolderServerFolderNo;
    qt->inviteShareFolderName       = [SaaringUtility convertNSStringToQString:objc.inviteShareFolderName];
    qt->inviteShareFolderPath       = [SaaringUtility convertNSStringToQString:objc.inviteShareFolderPath];
    qt->inviteShareFolderPasswordYN = [SaaringUtility convertNSStringToQString:objc.inviteShareFolderPasswordYN];

    qt->inviteMessage               = [SaaringUtility convertNSStringToQString:objc.inviteMessage];

    qt->dynamicLink                 = [SaaringUtility convertNSStringToQString:objc.dynamicLink];
    qt->rejectYN                    = [SaaringUtility convertNSStringToQString:objc.rejectYN];

    qt->expiredt                    = [SaaringUtility convertLongLongToQDateTime:objc.expiredt];
    qt->regdt                       = [SaaringUtility convertLongLongToQDateTime:objc.regdt];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringInviteRegisteredInfo
//////////////////////////////////////////////////////////////////////
@implementation SaaringInviteRegisteredInfo

+ (void) convertQtSaaringInviteRegisteredInfoToObjC : (saaring::SaaringInviteRegisteredInfo *)qt objc:(SaaringInviteRegisteredInfo *)objc {
    
    objc.userNo                     = qt->userNo;
    objc.inviteNo                   = qt->inviteNo;
    objc.inviteCode                 = [SaaringUtility convertQStringToNSString:qt->inviteCode];

    objc.inviteType                 = qt->inviteType;
    objc.certKey                    = [SaaringUtility convertQStringToNSString:qt->certKey];

    objc.fromUserNo                 = qt->fromUserNo;
    objc.fromUserName               = [SaaringUtility convertQStringToNSString:qt->fromUserName];

    objc.inviteMessage              = [SaaringUtility convertQStringToNSString:qt->inviteMessage];
    objc.dynamicLink                = [SaaringUtility convertQStringToNSString:qt->dynamicLink];

    objc.expiredt                   = [SaaringUtility convertQDateTimeToLongLong:qt->expiredt];
    objc.regdt                      = [SaaringUtility convertQDateTimeToLongLong:qt->regdt];
}
- (void) convertObjCSaaringInviteRegisteredInfoToQt : (SaaringInviteRegisteredInfo *)objc qt:(saaring::SaaringInviteRegisteredInfo *)qt {
    
    qt->userNo                      = objc.userNo;
    qt->inviteNo                    = objc.inviteNo;
    qt->inviteCode                  = [SaaringUtility convertNSStringToQString:objc.inviteCode];

    qt->inviteType                  = objc.inviteType;
    qt->certKey                     = [SaaringUtility convertNSStringToQString:objc.certKey];

    qt->fromUserNo                  = objc.fromUserNo;
    qt->fromUserName                = [SaaringUtility convertNSStringToQString:objc.fromUserName];

    qt->inviteMessage               = [SaaringUtility convertNSStringToQString:objc.inviteMessage];
    qt->dynamicLink                 = [SaaringUtility convertNSStringToQString:objc.dynamicLink];

    qt->expiredt                    = [SaaringUtility convertLongLongToQDateTime:objc.expiredt];
    qt->regdt                       = [SaaringUtility convertLongLongToQDateTime:objc.regdt];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringBlockInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringBlockInfo

+ (void) convertQtSaaringBlockInfoToObjC : (saaring::SaaringBlockInfo *)qt objc:(SaaringBlockInfo *)objc {
    
    objc.userNo                     = qt->userNo;
    objc.email                      = [SaaringUtility convertQStringToNSString:qt->email];
    objc.phoneNumber                = [SaaringUtility convertQStringToNSString:qt->phoneNumber];
    objc.idType                     = qt->idType;
    objc.userId                     = [SaaringUtility convertQStringToNSString:qt->userId];
    objc.userName                   = [SaaringUtility convertQStringToNSString:qt->userName];
    objc.profilePic                 = [SaaringUtility convertQStringToNSString:qt->profilePic];
}
+ (void) convertObjCSaaringBlockInfoToQt : (SaaringBlockInfo *)objc qt:(saaring::SaaringBlockInfo *)qt {
    
    qt->userNo                      = objc.userNo;
    qt->email                       = [SaaringUtility convertNSStringToQString:objc.email];
    qt->phoneNumber                 = [SaaringUtility convertNSStringToQString:objc.phoneNumber];
    qt->idType                      = objc.idType;
    qt->userId                      = [SaaringUtility convertNSStringToQString:objc.userId];
    qt->userName                    = [SaaringUtility convertNSStringToQString:objc.userName];
    qt->profilePic                  = [SaaringUtility convertNSStringToQString:objc.profilePic];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringTransferBaseInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringTransferBaseInfo

+ (void) convertQtSaaringTransferBaseInfoToObjC : (saaring::SaaringTransferBaseInfo *)qt objc:(SaaringTransferBaseInfo *)objc {
    
    objc.transNo                    = qt->transNo;
    objc.userNo                     = qt->userNo;
    objc.userName                   = [SaaringUtility convertQStringToNSString:qt->userName];
    objc.deviceNo                   = qt->deviceNo;
    objc.deviceType                 = qt->deviceType;
    objc.deviceName                 = [SaaringUtility convertQStringToNSString:qt->deviceName];
    objc.upDown                     = [SaaringUtility convertQStringToNSString:qt->upDown];
    objc.transStatus                = qt->transStatus;
    objc.localPath                  = [SaaringUtility convertQStringToNSString:qt->localPath];
    objc.remotePath                 = [SaaringUtility convertQStringToNSString:qt->remotePath];
    objc.fileSize                   = qt->fileSize;
    objc.fileStartPosition          = qt->fileStartPosition;
    objc.transSize                  = qt->transSize;
    objc.deleteYN                   = [SaaringUtility convertQStringToNSString:qt->deleteYN];
    objc.sortNo                     = qt->sortNo;
    objc.updateTime                 = [SaaringUtility convertQDateTimeToLongLong:qt->updateTime];
    objc.myDeviceNo                 = qt->myDeviceNo;
    objc.inviteNo                   = qt->inviteNo;
    objc.fileId                     = qt->fileId;
}
+ (void) convertObjCSaaringTransferBaseInfoToQt : (SaaringTransferBaseInfo *)objc qt:(saaring::SaaringTransferBaseInfo *)qt {
    
    qt->transNo                     = objc.transNo;
    qt->userNo                      = objc.userNo;
    qt->userName                    = [SaaringUtility convertNSStringToQString:objc.userName];
    qt->deviceNo                    = objc.deviceNo;
    qt->deviceType                  = objc.deviceType;
    qt->deviceName                  = [SaaringUtility convertNSStringToQString:objc.deviceName];
    qt->upDown                      = [SaaringUtility convertNSStringToQString:objc.upDown];
    qt->transStatus                 = objc.transStatus;
    qt->localPath                   = [SaaringUtility convertNSStringToQString:objc.localPath];
    qt->remotePath                  = [SaaringUtility convertNSStringToQString:objc.remotePath];
    qt->fileSize                    = objc.fileSize;
    qt->fileStartPosition           = objc.fileStartPosition;
    qt->transSize                   = objc.transSize;
    qt->deleteYN                    = [SaaringUtility convertNSStringToQString:objc.deleteYN];
    qt->sortNo                      = objc.sortNo;
    qt->updateTime                  = [SaaringUtility convertLongLongToQDateTime:objc.updateTime];
    qt->myDeviceNo                  = objc.myDeviceNo;
    qt->inviteNo                    = objc.inviteNo;
    qt->fileId                      = objc.fileId;
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringTransferInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringTransferInfo

+ (void) convertQtSaaringTransferInfoToObjC : (saaring::SaaringTransferInfo *)qt objc:(SaaringTransferInfo *)objc {
    
    objc.fileName                   = [SaaringUtility convertQStringToNSString:qt->fileName];
    objc.status                     = (int)qt->status;

    objc.fileSize                   = qt->fileSize;
    objc.sendSpeed                  = [SaaringUtility convertQStringToNSString:qt->sendSpeed];
    objc.leftTime                   = [SaaringUtility convertQStringToNSString:qt->leftTime];
    objc.transNo                    = qt->transNo;

    objc.currSize                   = qt->currSize;
    objc.currTime                   = [SaaringUtility convertQStringToNSString:qt->currTime];
    objc.userNo                     = qt->userNo;
    objc.userName                   = [SaaringUtility convertQStringToNSString:qt->userName];

    objc.deviceNo                   = qt->deviceNo;
    objc.deviceName                 = [SaaringUtility convertQStringToNSString:qt->deviceName];
    objc.deviceType                 = qt->deviceType;

    objc.upDown                     = [SaaringUtility convertQStringToNSString:qt->upDown];
    objc.isMyAction                 = qt->isMyAction;
    objc.folderName                 = [SaaringUtility convertQStringToNSString:qt->folderName];
    objc.remotePath                 = [SaaringUtility convertQStringToNSString:qt->remotePath];
    objc.inviteNo                   = qt->inviteNo;
    objc.fileId                     = qt->fileId;
}
+ (void) convertObjCSaaringTransferInfoToQt : (SaaringTransferInfo *)objc qt:(saaring::SaaringTransferInfo *)qt {
    
    qt->fileName                    = [SaaringUtility convertNSStringToQString:objc.fileName];
    qt->status                      = (saaring::SaaringTransferStatusType)objc.status;

    qt->fileSize                    = objc.fileSize;
    qt->sendSpeed                   = [SaaringUtility convertNSStringToQString:objc.sendSpeed];
    qt->leftTime                    = [SaaringUtility convertNSStringToQString:objc.leftTime];
    qt->transNo                     = objc.transNo;

    qt->currSize                    = objc.currSize;
    qt->currTime                    = [SaaringUtility convertNSStringToQString:objc.currTime];
    qt->userNo                      = objc.userNo;
    qt->userName                    = [SaaringUtility convertNSStringToQString:objc.userName];

    qt->deviceNo                    = objc.deviceNo;
    qt->deviceName                  = [SaaringUtility convertNSStringToQString:objc.deviceName];
    qt->deviceType                  = objc.deviceType;

    qt->upDown                      = [SaaringUtility convertNSStringToQString:objc.upDown];
    qt->isMyAction                  = objc.isMyAction;
    qt->folderName                  = [SaaringUtility convertNSStringToQString:objc.folderName];
    qt->remotePath                  = [SaaringUtility convertNSStringToQString:objc.remotePath];
    qt->inviteNo                    = objc.inviteNo;
    qt->fileId                      = objc.fileId;
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringShareInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringShareInfo

+ (void) convertQtSaaringShareInfoToObjC : (saaring::SaaringShareInfo *)qt objc:(SaaringShareInfo *)objc {
    
    objc.shareNo                    = qt->shareNo;
    objc.deviceNo                   = qt->deviceNo;
    objc.shareFolderPath            = [SaaringUtility convertQStringToNSString:qt->shareFolderPath];
    objc.shareName                  = [SaaringUtility convertQStringToNSString:qt->shareName];
    
    objc.serverFolderNo             = qt->serverFolderNo;
    objc.fileCount                  = qt->fileCount;
    objc.fileSize                   = qt->fileSize;
    
    objc.downKind                   = qt->downKind;
    [SaaringUtility convertQListToNSMutableArrayLongLong:&qt->listDownFriendUserNo dest:objc.listDownFriendUserNo];
    [SaaringUtility convertQListToNSMutableArrayString:&qt->listDownFriendUserName dest:objc.listDownFriendUserName];
    objc.upKind                     = qt->upKind;
    [SaaringUtility convertQListToNSMutableArrayLongLong:&qt->listUpFriendUserNo dest:objc.listUpFriendUserNo];
    [SaaringUtility convertQListToNSMutableArrayString:&qt->listUpFriendUserName dest:objc.listUpFriendUserName];
    
    objc.downYN                     = [SaaringUtility convertQStringToNSString:qt->downYN];
    objc.upYN                       = [SaaringUtility convertQStringToNSString:qt->upYN];
    objc.password                   = [SaaringUtility convertQStringToNSString:qt->password];
    objc.passwordYN                 = [SaaringUtility convertQStringToNSString:qt->passwordYN];
}
+ (void) convertObjCSaaringShareInfoToQt : (SaaringShareInfo *)objc qt:(saaring::SaaringShareInfo *)qt {
    
    qt->shareNo                     = objc.shareNo;
    qt->deviceNo                    = objc.deviceNo;
    qt->shareFolderPath             = [SaaringUtility convertNSStringToQString:objc.shareFolderPath];
    qt->shareName                   = [SaaringUtility convertNSStringToQString:objc.shareName];
    
    qt->serverFolderNo              = objc.serverFolderNo;
    qt->fileCount                   = objc.fileCount;
    qt->fileSize                    = objc.fileSize;
    
    qt->downKind                    = objc.downKind;
    [SaaringUtility convertNSMutableArrayToQListLongLong:objc.listDownFriendUserNo dest:&qt->listDownFriendUserNo];
    [SaaringUtility convertNSMutableArrayToQListString:objc.listDownFriendUserName dest:&qt->listDownFriendUserName];
    qt->upKind                      = objc.upKind;
    [SaaringUtility convertNSMutableArrayToQListLongLong:objc.listUpFriendUserNo dest:&qt->listUpFriendUserNo];
    [SaaringUtility convertNSMutableArrayToQListString:objc.listUpFriendUserName dest:&qt->listUpFriendUserName];
    
    qt->downYN                      = [SaaringUtility convertNSStringToQString:objc.downYN];
    qt->upYN                        = [SaaringUtility convertNSStringToQString:objc.upYN];
    qt->password                    = [SaaringUtility convertNSStringToQString:objc.password];
    qt->passwordYN                  = [SaaringUtility convertNSStringToQString:objc.passwordYN];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringShareDetailInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringShareDetailInfo

+ (void) convertQtSaaringShareDetailInfoToObjC : (saaring::SaaringShareDetailInfo *)qt objc:(SaaringShareDetailInfo *)objc {
    
    objc.shareDetailNo              = qt->shareDetailNo;
    objc.deviceNo                   = qt->deviceNo;
    objc.shareNo                    = qt->shareNo;
    objc.fullPathName               = [SaaringUtility convertQStringToNSString:qt->fullPathName];
    objc.shortPathName              = [SaaringUtility convertQStringToNSString:qt->shortPathName];
    objc.fileName                   = [SaaringUtility convertQStringToNSString:qt->fileName];
    objc.fileExtension              = [SaaringUtility convertQStringToNSString:qt->fileExtension];
    objc.fileSize                   = qt->fileSize;
    objc.updateTime                 = [SaaringUtility convertQDateTimeToLongLong:qt->updateTime];
    objc.folderYN                   = [SaaringUtility convertQStringToNSString:qt->folderYN];

}
+ (void) convertObjCSaaringShareDetailInfoToQt : (SaaringShareDetailInfo *)objc qt:(saaring::SaaringShareDetailInfo *)qt {
    
    qt->shareDetailNo               = objc.shareDetailNo;
    qt->deviceNo                    = objc.deviceNo;
    qt->shareNo                     = objc.shareNo;
    qt->fullPathName                = [SaaringUtility convertNSStringToQString:objc.fullPathName];
    qt->shortPathName               = [SaaringUtility convertNSStringToQString:objc.shortPathName];
    qt->fileName                    = [SaaringUtility convertNSStringToQString:objc.fileName];
    qt->fileExtension               = [SaaringUtility convertNSStringToQString:objc.fileExtension];
    qt->fileSize                    = objc.fileSize;
    qt->updateTime                  = [SaaringUtility convertLongLongToQDateTime:objc.updateTime];
    qt->folderYN                    = [SaaringUtility convertNSStringToQString:objc.folderYN];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringShareFolderInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringShareFolderInfo

+ (void) convertQtSaaringShareFolderInfoToObjC : (saaring::SaaringShareFolderInfo *)qt objc:(SaaringShareFolderInfo *)objc {
    
    objc.deviceNo                   = qt->deviceNo;
    objc.deviceType                 = qt->deviceType;
    objc.deviceName                 = [SaaringUtility convertQStringToNSString:qt->deviceName];
    objc.liveYN                     = [SaaringUtility convertQStringToNSString:qt->liveYN];
    
    objc.serverFolderNo             = qt->serverFolderNo;
    
    objc.folderName                 = [SaaringUtility convertQStringToNSString:qt->folderName];
    objc.fileSize                   = qt->fileSize;
    objc.fileCount                  = qt->fileCount;
    
    objc.shareType                  = qt->shareType;
    objc.downShareType              = qt->downShareType;
    objc.upShareType                = qt->upShareType;
    objc.shareFoaf                  = qt->shareFoaf;
    
    objc.numFolder                  = qt->numFolder;
    objc.numFile                    = qt->numFile;
    objc.totalFileSize              = qt->totalFileSize;
    
    objc.password                   = [SaaringUtility convertQStringToNSString:qt->password];
    objc.passwordYN                 = [SaaringUtility convertQStringToNSString:qt->passwordYN];
    objc.downloadYN                 = [SaaringUtility convertQStringToNSString:qt->downloadYN];
    objc.uploadYN                   = [SaaringUtility convertQStringToNSString:qt->uploadYN];
    
    [SaaringUtility convertQListToNSMutableArrayLongLong:&qt->listDownFriendUserNo dest:objc.listDownFriendUserNo];
    [SaaringUtility convertQListToNSMutableArrayLongLong:&qt->listUpFriendUserNo dest:objc.listUpFriendUserNo];
}
+ (void) convertObjCSaaringShareFolderInfoToQt : (SaaringShareFolderInfo *)objc qt:(saaring::SaaringShareFolderInfo *)qt {
    
    qt->deviceNo                    = objc.deviceNo;
    qt->deviceType                  = objc.deviceType;
    qt->deviceName                  = [SaaringUtility convertNSStringToQString:objc.deviceName];
    qt->liveYN                      = [SaaringUtility convertNSStringToQString:objc.liveYN];
    
    qt->serverFolderNo              = objc.serverFolderNo;
    
    qt->folderName                  = [SaaringUtility convertNSStringToQString:objc.folderName];
    qt->fileSize                    = objc.fileSize;
    qt->fileCount                   = objc.fileCount;
    
    qt->shareType                   = objc.shareType;
    qt->downShareType               = objc.downShareType;
    qt->upShareType                 = objc.upShareType;
    qt->shareFoaf                   = objc.shareFoaf;
    
    qt->numFolder                   = objc.numFolder;
    qt->numFile                     = objc.numFile;
    qt->totalFileSize               = objc.totalFileSize;
    
    qt->password                    = [SaaringUtility convertNSStringToQString:objc.password];
    qt->passwordYN                  = [SaaringUtility convertNSStringToQString:objc.passwordYN];
    qt->downloadYN                  = [SaaringUtility convertNSStringToQString:objc.downloadYN];
    qt->uploadYN                    = [SaaringUtility convertNSStringToQString:objc.uploadYN];
    
    [SaaringUtility convertNSMutableArrayToQListLongLong:objc.listDownFriendUserNo dest:&qt->listDownFriendUserNo];
    [SaaringUtility convertNSMutableArrayToQListLongLong:objc.listUpFriendUserNo dest:&qt->listUpFriendUserNo];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringBadgeInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringBadgeInfo

+ (void) convertQtSaaringBadgeInfoToObjC : (saaring::SaaringBadgeInfo *)qt objc:(SaaringBadgeInfo *)objc {

    objc.numNewRequestFriend        = qt->numNewRequestFriend;
    objc.numNewNotice               = qt->numNewNotice;
}
+ (void) convertObjCSaaringBadgeInfoToQt : (SaaringBadgeInfo *)objc qt:(saaring::SaaringBadgeInfo *)qt {
    
    qt->numNewRequestFriend         = objc.numNewRequestFriend;
    qt->numNewNotice                = objc.numNewNotice;
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringServerInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringServerInfo

+ (void) convertQtSaaringServerInfoToObjC : (saaring::SaaringServerInfo *)qt objc:(SaaringServerInfo *)objc {
    
    objc.signalingIp                = [SaaringUtility convertQStringToNSString:qt->signalingIp];
    objc.signalingPort              = qt->signalingPort;
    objc.rendezvousIp               = [SaaringUtility convertQStringToNSString:qt->rendezvousIp];
    objc.rendezvousPort             = qt->rendezvousPort;
    objc.relayIp                    = [SaaringUtility convertQStringToNSString:qt->relayIp];
    objc.relayPort                  = qt->relayPort;
    objc.runningVersion             = [SaaringUtility convertQStringToNSString:qt->runningVersion];
    objc.lastVersion                = [SaaringUtility convertQStringToNSString:qt->lastVersion];
}
+ (void) convertObjCSaaringServerInfoToQt : (SaaringServerInfo *)objc qt:(saaring::SaaringServerInfo *)qt {
    
    qt->signalingIp                 = [SaaringUtility convertNSStringToQString:objc.signalingIp];
    qt->signalingPort               = objc.signalingPort;
    qt->rendezvousIp                = [SaaringUtility convertNSStringToQString:objc.rendezvousIp];
    qt->rendezvousPort              = objc.rendezvousPort;
    qt->relayIp                     = [SaaringUtility convertNSStringToQString:objc.relayIp];
    qt->relayPort                   = objc.relayPort;
    qt->runningVersion              = [SaaringUtility convertNSStringToQString:objc.runningVersion];
    qt->lastVersion                 = [SaaringUtility convertNSStringToQString:objc.lastVersion];
}

@end

//////////////////////////////////////////////////////////////////////
// SaaringPhoneAddressInfo
//////////////////////////////////////////////////////////////////////

@implementation SaaringPhoneAddressInfo

+ (void) convertQtSaaringPhoneAddressInfoToObjC : (saaring::SaaringPhoneAddressInfo *)qt objc:(SaaringPhoneAddressInfo *)objc {
    
    objc.userNo                     = qt->userNo;
    objc.phoneNumber                = [SaaringUtility convertQStringToNSString:qt->phoneNumber];
    objc.countryCode                = qt->countryCode;
    objc.nationalNumber             = qt->nationalNumber;
    objc.friendUserNo               = qt->friendUserNo;
    objc.friendYN                   = [SaaringUtility convertQStringToNSString:qt->friendYN];
    objc.blockYN                    = [SaaringUtility convertQStringToNSString:qt->blockYN];
    objc.matchingdt                 = qt->matchingdt;
    objc.frienddt                   = qt->frienddt;
    objc.regdt                      = qt->regdt;
    objc.userName                   = [SaaringUtility convertQStringToNSString:qt->userName];
    objc.email                      = [SaaringUtility convertQStringToNSString:qt->email];
    objc.profilePic                 = [SaaringUtility convertQStringToNSString:qt->profilePic];
    objc.message                    = [SaaringUtility convertQStringToNSString:qt->message];
}
+ (void) convertObjCSaaringPhoneAddressInfoToQt : (SaaringPhoneAddressInfo *)objc qt:(saaring::SaaringPhoneAddressInfo *)qt {
    
    qt->userNo                      = objc.userNo;
    qt->phoneNumber                 = [SaaringUtility convertNSStringToQString:objc.phoneNumber];
    qt->countryCode                 = objc.countryCode;
    qt->nationalNumber              = objc.nationalNumber;
    qt->friendUserNo                = objc.friendUserNo;
    qt->friendYN                    = [SaaringUtility convertNSStringToQString:objc.friendYN];
    qt->blockYN                     = [SaaringUtility convertNSStringToQString:objc.blockYN];
    qt->matchingdt                  = objc.matchingdt;
    qt->frienddt                    = objc.frienddt;
    qt->regdt                       = objc.regdt;
    qt->userName                    = [SaaringUtility convertNSStringToQString:objc.userName];
    qt->email                       = [SaaringUtility convertNSStringToQString:objc.email];
    qt->profilePic                  = [SaaringUtility convertNSStringToQString:objc.profilePic];
    qt->message                     = [SaaringUtility convertNSStringToQString:objc.message];
}

@end
