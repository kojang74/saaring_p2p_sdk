//
//  ViewController.swift
//  saaring
//
//  Created by kojang on 23/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

//    let saaring = SaaringSwiftContext();
    var saaring : SaaringSwiftContext? = nil;
    var authKey : String? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("start...");
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            print("init start...")
            self.saaring = SaaringSwiftContext();
            print("init end...")
            print("exec start...")
            self.saaring!.exec();
            print("exec end...")
        }
        print("end...");
    }

    @IBAction func buttonConnectSignaling(_ sender: Any) {
        print("buttonConnectSignaling clicked...")
        
//        let saaring = SaaringWrapper.getInstance();
//        let string: String? = saaring?.getSizeWithUnit(12345678);
//        print("saaring?.getSizeWithUnit = \(string)")
        
//        print("testSwiftCall")
//        saaring.testSwiftCall();

//        print("initTest")
//        saaring.initTest();

//        print("Parameter test");
//        saaring.testStruct();
        
        let saaringMemberInfo : SaaringMemberInfo = SaaringMemberInfo();

        let resultCode : NSMutableString = NSMutableString();
        let resultMessage : NSMutableString = NSMutableString();
        let resultData : NSMutableString = NSMutableString();

        saaringMemberInfo.userId       = "kojang74@naver.com";
        saaringMemberInfo.email        = "kojang74@naver.com";
        saaringMemberInfo.password     = "1234!@#$";
        saaringMemberInfo.idType       = 0;
        saaringMemberInfo.firebaseToken="eyJhbGciOiJSUzI1NiIsImtpZCI6IjkwMDk1YmM2ZGM2ZDY3NzkxZDdkYTFlZWIxYTU1OWEzZDViMmM0ODYiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoi7J6l6rec7JikIiwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL3NhYXJpbmctZDc1ODgiLCJhdWQiOiJzYWFyaW5nLWQ3NTg4IiwiYXV0aF90aW1lIjoxNjI5ODQ2ODI4LCJ1c2VyX2lkIjoiY200TEt3RmtaRFlQTmR5eTNYenZHTEdQVWtIMyIsInN1YiI6ImNtNExLd0ZrWkRZUE5keXkzWHp2R0xHUFVrSDMiLCJpYXQiOjE2Mjk4NDY4MzAsImV4cCI6MTYyOTg1MDQzMCwiZW1haWwiOiJrb2phbmc3NEBuYXZlci5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJrb2phbmc3NEBuYXZlci5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.GdYRQGSzCHFMiY_wQExiuNUsNodU4jgtIP2QHAeAkv_eLCMQSOdCUoHwP92dPcLqv-EEfS0y0Vt1qXWpgKLcpbpsjt5ty9KaAluNQ821qh-RzBfnW-26PatqYgpERV5g_vshmaYesnma9RqsqdoxK3X1zOp2a8ONcQtZjqUaaHAB0B0oy9K6KbjPK7rFx69tgEA4Q6Izwh1CSAEUMpqMlY8qwgp1M1xBlYBkLVLclIYp5LM8N3R1JJF0YPj9pVdHD4YdRiFeVw3Yka4G08ypKOqn8iYLKu2WvTyDX-Tv_jdMOV7z0Xq4WuW29YBzsa6Yu8FDhrgwLPHteskG4WOz7Q";

        let result : Bool = self.saaring!.loginMember(saaringMemberInfo:saaringMemberInfo, resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);

        print("result of result = \(result)")
        
        if result == true {
            
            let strResultData = resultData as String;
            if let dataResultData = strResultData.data(using: .utf8) {
            
                let jsonResultData = try! JSONSerialization.jsonObject(with: dataResultData, options: .allowFragments) as! [String : AnyObject];
                
                let userId = jsonResultData["userId"] as! NSString;
                let authKey = jsonResultData["authKey"] as! NSString;
                let userNo = jsonResultData["userNo"] as! NSNumber;
                
                print("userId=\(userId)");
                print("authKey=\(authKey)");
                print("userNo=\(userNo.int64Value)");

                self.authKey = authKey as String;
                
                self.saaring!.initP2p();

                self.saaring!.doSignalingConnect(authKey:authKey as String, isRefreshServerInfo: false);
            }
            
        }
        
    }

    @IBAction func buttonGetShareFolder(_ sender: Any) {
        print("buttonGetShareFolder clicked...")
        
//        print("testCallback")
//        saaring.testCallback();
        
        let resultCode : NSMutableString = NSMutableString();
        let resultMessage : NSMutableString = NSMutableString();
        let resultData : NSMutableString = NSMutableString();

        let listSaaringShareFolderInfo : NSMutableArray = NSMutableArray();

        self.saaring!.listFolderAll(authKey:self.authKey ?? "", resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                    listSaaringShareFolderInfo: listSaaringShareFolderInfo);
        
    }
    
    @IBAction func buttonConnectP2p(_ sender: Any) {
        print("buttonConnectP2p clicked...")
        
        self.saaring!.doP2pConnectShareFolder(toUserNo: 47, toUserName: "HOHOHO", toDeviceNo: 180, toDeviceType: 4, toDeviceName: "HAHAHA", toServerFolderNo: 424, toFolderName: "hoho folder", mustWakeUp: false);
    }
    
    @IBAction func buttonOpenNewScene(_ sender: Any) {
        print("buttonOpenNewScene clicked...")
    }
    
    @IBAction func buttonExit(_ sender: Any) {
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
    }
}

