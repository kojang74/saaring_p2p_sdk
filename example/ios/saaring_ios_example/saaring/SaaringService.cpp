//
//  SaaringService.cpp
//  saaring
//
//  Created by kojang on 28/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//

#include "SaaringService.hpp"

#include <QtPlugin>
#include <QDebug>

Q_IMPORT_PLUGIN(QIOSIntegrationPlugin)
Q_IMPORT_PLUGIN(QGifPlugin)
Q_IMPORT_PLUGIN(QICNSPlugin)
Q_IMPORT_PLUGIN(QICOPlugin)
Q_IMPORT_PLUGIN(QJpegPlugin)
Q_IMPORT_PLUGIN(QMacHeifPlugin)
Q_IMPORT_PLUGIN(QMacJp2Plugin)
Q_IMPORT_PLUGIN(QTgaPlugin)
Q_IMPORT_PLUGIN(QTiffPlugin)
Q_IMPORT_PLUGIN(QWbmpPlugin)
Q_IMPORT_PLUGIN(QWebpPlugin)
Q_IMPORT_PLUGIN(QGenericEnginePlugin)
Q_IMPORT_PLUGIN(QSQLiteDriverPlugin)

namespace saaring {
    
    SaaringCommonInterface* g_common            = Q_NULLPTR;
    SaaringDbInterface* g_db                    = Q_NULLPTR;
    SaaringP2pInterface* g_p2p                  = Q_NULLPTR;
    SaaringShareFolderInterface* g_shareFolder  = Q_NULLPTR;
    SaaringWebInterface* g_web                  = Q_NULLPTR;
    
    // Global static 변수
    SaaringMemberInfo g_member;

    QString g_saaringApiDomain = "";
    QString g_saaringImageDomain = "";
    QString g_saaringServiceDomain = "";

} // namespace ssaring

enum SAARING_RUNNING_MODE
{
    PRODUCTION = 0,
    REAL = 99999999,
    TEST = 99999998,
    DEV = 99999997,
    KOJANG = 99999996
};
int g_runningMode = SAARING_RUNNING_MODE::PRODUCTION;

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int argc = 1;
char *argv[] = {{"saaring"}};

SaaringService::SaaringService()
    : QApplication(argc, argv)
{
    SaaringService::initLibrary();
}

SaaringService::~SaaringService()
{
    SaaringService::releaseLibrary();
}

void SaaringService::initLibrary()
{
    if(saaring::g_common == Q_NULLPTR)
    {
        saaring::g_common = saaring::Common(g_runningMode);
    }

    // domain 정보 세팅
    // 중요함.
    saaring::g_saaringApiDomain = saaring::g_common->getSaaringApiDomain();
    saaring::g_saaringImageDomain = saaring::g_common->getSaaringImageDomain();
    saaring::g_saaringServiceDomain = saaring::g_common->getSaaringServiceDomain();
    qDebug() << "saaring::g_saaringApiDomain=" << saaring::g_saaringApiDomain;
    qDebug() << "saaring::g_saaringImageDomain=" << saaring::g_saaringImageDomain;
    qDebug() << "saaring::g_saaringApiDomain=" << saaring::g_saaringServiceDomain;

    if(saaring::g_db == Q_NULLPTR)
    {
        saaring::g_db = saaring::Db(g_runningMode);
    }
    if(saaring::g_p2p == Q_NULLPTR)
    {
        saaring::g_p2p = saaring::P2p(g_runningMode);
    }
    if(saaring::g_shareFolder == Q_NULLPTR)
    {
        saaring::g_shareFolder = saaring::ShareFolder();
    }
    if(saaring::g_web == Q_NULLPTR)
    {
        saaring::g_web = saaring::Web(saaring::g_saaringApiDomain);
    }
}

void SaaringService::releaseLibrary()
{
    if(saaring::g_common != Q_NULLPTR)
    {
        delete saaring::g_common;
        *(&saaring::g_common) = Q_NULLPTR;
    }
    if(saaring::g_db != Q_NULLPTR)
    {
        delete saaring::g_db;
        *(&saaring::g_db) = Q_NULLPTR;
    }
    if(saaring::g_p2p != Q_NULLPTR)
    {
        delete saaring::g_p2p;
        *(&saaring::g_p2p) = Q_NULLPTR;
    }
    if(saaring::g_shareFolder != Q_NULLPTR)
    {
        delete saaring::g_shareFolder;
        *(&saaring::g_shareFolder) = Q_NULLPTR;
    }
    if(saaring::g_web != Q_NULLPTR)
    {
        delete saaring::g_web;
        *(&saaring::g_web) = Q_NULLPTR;
    }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void SaaringService::threadShareDirCallback(void *context, int command, QStringList &param)
{
    qDebug() << "command=" << command;
    qDebug() << "param=" << param;
    
    SaaringService *saaringService = (SaaringService*)context;
    
    saaringService->blockForThreadShareDirCallback(command, param);
}

void SaaringService::p2pCallback(void *context, int command, QStringList &param)
{
    qDebug() << "command=" << command;
    qDebug() << "param=" << param;
    
    SaaringService *saaringService = (SaaringService*)context;
    
    saaringService->blockForP2pCallback(command, param);
}

void SaaringService::testCallback(void *context, int command, QStringList &param)
{
    qDebug() << "command=" << command;
    qDebug() << "param=" << param;
    
    SaaringService *saaringService = (SaaringService*)context;
    
    saaringService->blockForTestCallback(command, param);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void SaaringService::initThreadShareDir(void (^block)(int, QStringList &))
{
    this->blockForThreadShareDirCallback = block;
    
    saaring::g_shareFolder->initThreadShareDir(this, SaaringService::threadShareDirCallback, saaring::g_member);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void SaaringService::initP2p(void (^block)(int, QStringList &))
{
    this->blockForP2pCallback = block;
    
    saaring::g_p2p->initP2p(this, SaaringService::p2pCallback, saaring::g_member);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void SaaringService::initTest(void (^block)(int, QStringList &))
{
    this->blockForTestCallback = block;
}

void SaaringService::testCallback()
{
    QStringList list;
    list.append("11111");
    list.append("22222");
    list.append("33333");
    SaaringService::testCallback(this, 111, list);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

saaring::SaaringCommonInterface* SaaringService::getCommon()
{
    return saaring::g_common;
}
saaring::SaaringDbInterface* SaaringService::getDb()
{
    return saaring::g_db;
}
saaring::SaaringP2pInterface* SaaringService::getP2p()
{
    return saaring::g_p2p;
}
saaring::SaaringShareFolderInterface* SaaringService::getShareFolder()
{
    return saaring::g_shareFolder;
}
saaring::SaaringWebInterface* SaaringService::getWeb()
{
    return saaring::g_web;
}
saaring::SaaringMemberInfo* SaaringService::getMember()
{
    return &saaring::g_member;
}
