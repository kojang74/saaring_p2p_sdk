//
//  SaaringUtility.hh
//  saaring
//
//  Created by kojang on 31/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//

#ifndef SaaringUtility_h
#define SaaringUtility_h

#import <Foundation/Foundation.h>

#include <QString>
#include <QStringList>
#include <QDateTime>
#include <QList>

//////////////////////////////////////////////////////////////////////
// Utility
//////////////////////////////////////////////////////////////////////
@interface SaaringUtility : NSObject

//////////////////////////////////////////////////////////////////////
// for private utility
//////////////////////////////////////////////////////////////////////
+ (QDateTime) convertLongLongToQDateTime : (long long)timestamp;
+ (long long) convertQDateTimeToLongLong : (QDateTime)dateTime;

+ (QString) convertNSStringToQString : (NSString *)string;
+ (NSString *) convertQStringToNSString : (QString)string;
+ (NSString *) convertQJsonObjectToNSString : (QJsonObject)json;

+ (void) convertQListToNSMutableArrayLongLong : (QList<qint64> *)source dest:(NSMutableArray *)dest;
+ (void) convertNSMutableArrayToQListLongLong : (NSMutableArray *)source dest:(QList<qint64> *)dest;

+ (void) convertQListToNSMutableArrayString : (QStringList *)source dest:(NSMutableArray *)dest;
+ (void) convertNSMutableArrayToQListString : (NSMutableArray *)source dest:(QStringList *)dest;

@end

#endif /* SaaringUtility_h */
