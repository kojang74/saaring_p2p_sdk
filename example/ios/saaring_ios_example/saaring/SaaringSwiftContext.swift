//
//  SaaringCallback.swift
//  saaring
//
//  Created by kojang on 28/01/2019.
//  Copyright © 2019 mirinesoft. All rights reserved.
//

import Foundation
import Darwin

var g_member : SaaringMemberInfo = SaaringMemberInfo();

enum ShareDirCallbackCommand : Int {
    case SDCC_SHARE_DIR_REFRESH                                  = 1
};

enum P2pCallbackCommand : Int {
    case PCC_SIGNALING_CONNECTED                                 = 10000
    case PCC_SIGNALING_CLOSED                                    = 10001
    case PCC_SIGNALING_LOGIN_FAILED                              = 10002
    
    case PCC_SIGNALING_NOTIFY_DEVICE_LIVEYN                      = 11000
    case PCC_SIGNALING_NOTIFY_REQUEST_FRIEND                     = 11001
    case PCC_SIGNALING_NOTIFY_ADD_FRIEND                         = 11002
    
    case PCC_P2P_PORTMAP_FINISHED                                = 20000
    case PCC_P2P_LOGIN_FINISHED                                  = 20001
    case PCC_P2P_WAKEUP_FINISHED                                 = 20002
    
    case PCC_P2P_FINISH_DOWNLOAD                                 = 21000
    case PCC_P2P_FINISH_UPLOAD                                   = 21001
    case PCC_P2P_PROGRESS_DOWNLOAD                               = 21002
    case PCC_P2P_PROGRESS_UPLOAD                                 = 21003
    case PCC_P2P_ABORT_DOWNLOAD                                  = 21004
    case PCC_P2P_ABORT_UPLOAD                                    = 21005
    case PCC_P2P_FINISH_DOWNLOAD_OTHER                           = 21006
    case PCC_P2P_FINISH_UPLOAD_OTHER                             = 21007
    case PCC_P2P_PROGRESS_DOWNLOAD_OTHER                         = 21008
    case PCC_P2P_PROGRESS_UPLOAD_OTHER                           = 21009
    
    case PCC_P2P_PROCESSED_REQUEST                               = 22000
    case PCC_P2P_PROCESSED_RESPONSE                              = 22001
    case PCC_P2P_PROCESS_FOLDER_LIST                             = 22002
    case PCC_P2P_PROCESS_FOLDER_DETAIL                           = 22003
    case PCC_P2P_PROCESS_FOLDER_DETAIL_FINISHED                  = 22004
    case PCC_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE                 = 22005
    case PCC_P2P_PROCESS_FILE_UPLOAD_QUERY                       = 22006
    case PCC_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY   = 22007
    case PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO             = 22008
    
    case PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY                  = 30000
    case PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                   = 30001
    case PCC_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY                    = 30002
    case PCC_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                     = 30003
    
    case PCC_P2P_NOTIFY_CONNECTION_CLOSED                        = 40000
    case PCC_P2P_NOTIFY_START_NEXT_TRANSFER                      = 40001
    case PCC_P2P_NOTIFY_STATUS_MESSAGE                           = 40002
};

@objc class SaaringSwiftContext : NSObject
{
    let saaringWrapper = SaaringWrapper.getInstance();
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for init
    //
    //////////////////////////////////////////////////////////////////////////////////
    func exec()
    {
        saaringWrapper?.exec();
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for callback
    //
    //////////////////////////////////////////////////////////////////////////////////
    @objc(threadShareDirSwiftCallback:withParam:)
    func threadShareDirSwiftCallback(command : NSNumber, param : NSMutableArray)
    {
        print("threadShareDirSwiftCallback : command = \(command)")
        let commandType = ShareDirCallbackCommand(rawValue: command.intValue)
        print("threadShareDirSwiftCallback : commandType = \(commandType)")
        
        var items = param as NSArray? as? [String]
        print("threadShareDirSwiftCallback : items = \(String(describing: items))")
        
        if let items = param as NSArray? as? [String] {
            
            switch commandType {
            case .SDCC_SHARE_DIR_REFRESH? :
                onShareDirRefresh(path:String(items[0]))
                
            default :
                // do nothing...
                onCallbackError()
            }
            
        } else {
            onCallbackError()
        }
    }
    
    @objc(p2pSwiftCallback:withParam:)
    func p2pSwiftCallback(command : NSNumber, param : NSMutableArray)
    {
        print("p2pSwiftCallback : command = \(command)")
        let commandType = P2pCallbackCommand(rawValue: command.intValue)
        print("p2pSwiftCallback : commandType = \(commandType)")
        
        var items = param as NSArray? as? [String]
        print("p2pSwiftCallback : items = \(items)")
        
        if let items = param as NSArray? as? [String] {
            
            switch commandType {
            case .PCC_SIGNALING_CONNECTED? :
                onSignalingConnected()
                
            case .PCC_SIGNALING_CLOSED? :
                onSignalingClosed()
                
            case .PCC_SIGNALING_LOGIN_FAILED? :
                onSignalingLoginFailed()
                
            case .PCC_SIGNALING_NOTIFY_DEVICE_LIVEYN? :
                onSignalingNotifyDeviceLiveYN(fromUserNo:(items[0] as NSString).longLongValue,
                                              fromDeviceNo:(items[1] as NSString).longLongValue,
                                              liveYN:String(items[2]))
                
            case .PCC_SIGNALING_NOTIFY_REQUEST_FRIEND? :
                onSignalingNotifyRequestFriend(friendName:String(items[0]))
                
            case .PCC_SIGNALING_NOTIFY_ADD_FRIEND? :
                onSignalingNotifyAddFriend(friendName:String(items[0]),
                                           friendUserNo:(items[1] as NSString).longLongValue)
                
            case .PCC_P2P_PORTMAP_FINISHED? :
                onPortmapFinished()
                
            case .PCC_P2P_LOGIN_FINISHED? :
                onP2pLoginFinish(friendDeviceNo:(items[0] as NSString).longLongValue,
                                 toRemoteShareNo:(items[1] as NSString).longLongValue)
                
            case .PCC_P2P_WAKEUP_FINISHED? :
                let isSuccess = items[0] == "0" ? false : true;
                onP2pWakeUpFinish(isSuccess:isSuccess)
                
            case .PCC_P2P_FINISH_DOWNLOAD? :
                onP2pFinishDownload(transNo:(items[0] as NSString).longLongValue,
                                    fileSize:(items[1] as NSString).longLongValue,
                                    receivedSize:(items[2] as NSString).longLongValue)
                
            case .PCC_P2P_FINISH_UPLOAD? :
                onP2pFinishUpload(transNo:(items[0] as NSString).longLongValue,
                                  fileSize:(items[1] as NSString).longLongValue,
                                  sentSize:(items[2] as NSString).longLongValue)
                
            case .PCC_P2P_PROGRESS_DOWNLOAD? :
                onP2pProgressDownload(transNo:(items[0] as NSString).longLongValue,
                                      fileSize:(items[1] as NSString).longLongValue,
                                      receivedSize:(items[2] as NSString).longLongValue)
                
            case .PCC_P2P_PROGRESS_UPLOAD? :
                onP2pProgressUpload(transNo:(items[0] as NSString).longLongValue,
                                    fileSize:(items[1] as NSString).longLongValue,
                                    sentSize:(items[2] as NSString).longLongValue)
                
            case .PCC_P2P_ABORT_DOWNLOAD? :
                onP2pAbortDownload(transNo:(items[0] as NSString).longLongValue)
                
            case .PCC_P2P_ABORT_UPLOAD? :
                onP2pAbortUpload(transNo:(items[0] as NSString).longLongValue)
                
            case .PCC_P2P_FINISH_DOWNLOAD_OTHER? :
                onP2pFinishDownloadOther(jobId:(items[0] as NSString).longLongValue,
                                         remoteDeviceNo:(items[1] as NSString).longLongValue)
                
            case .PCC_P2P_FINISH_UPLOAD_OTHER? :
                onP2pFinishUploadOther(jobId:(items[0] as NSString).longLongValue,
                                       remoteDeviceNo:(items[1] as NSString).longLongValue)
                
            case .PCC_P2P_PROGRESS_DOWNLOAD_OTHER? :
                onP2pProgressDownloadOther(jobId:(items[0] as NSString).longLongValue,
                                           remoteDeviceNo:(items[1] as NSString).longLongValue,
                                           fileSize:(items[2] as NSString).longLongValue,
                                           currSize:(items[3] as NSString).longLongValue,
                                           filePath:String(items[4]),
                                           serverName:String(items[5]),
                                           deviceName:String(items[6]))
                
            case .PCC_P2P_PROGRESS_UPLOAD_OTHER? :
                onP2pProgressUploadOther(jobId:(items[0] as NSString).longLongValue,
                                         remoteDeviceNo:(items[1] as NSString).longLongValue,
                                         fileSize:(items[2] as NSString).longLongValue,
                                         currSize:(items[3] as NSString).longLongValue,
                                         filePath:String(items[4]),
                                         serverName:String(items[5]),
                                         deviceName:String(items[6]))
                
            case .PCC_P2P_PROCESSED_REQUEST? :
                onProgressedRequest(processType:(items[0] as NSString).integerValue,
                                    jobId:(items[1] as NSString).longLongValue,
                                    remoteDeviceNo:(items[2] as NSString).longLongValue,
                                    resultCode:(items[3] as NSString).integerValue)
                
            case .PCC_P2P_PROCESSED_RESPONSE? :
                onProgressedResponse(processType:(items[0] as NSString).integerValue,
                                     jobId:(items[1] as NSString).longLongValue,
                                     remoteDeviceNo:(items[2] as NSString).longLongValue,
                                     resultCode:(items[3] as NSString).integerValue)
                
            case .PCC_P2P_PROCESS_FOLDER_LIST? :
                onP2pProcessResponseFolderList(strJson:String(items[0]))
                
            case .PCC_P2P_PROCESS_FOLDER_DETAIL? :
                onP2pProcessResponseFolderDetail(strJson:String(items[0]))
                
            case .PCC_P2P_PROCESS_FOLDER_DETAIL_FINISHED? :
                onP2pProcessResponseFolderDetailFinished()
                
            case .PCC_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE? :
                onP2pProcessResponseFolderDetailRecursive(strJson:String(items[0]))
                
            case .PCC_P2P_PROCESS_FILE_UPLOAD_QUERY? :
                onP2pProcessResponseFileUploadQuery(transNo:(items[0] as NSString).longLongValue, fileStartPosition:(items[1] as NSString).longLongValue)
                
            case .PCC_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY? :
                onP2pProcessResponseFileDownloadInviteSendFilesQuery(strJson:String(items[0]))
                
            case .PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO? :
                onP2pProcessResponseSaveSendFilesInviteInfo(strJson:String(items[0]))
                
            case .PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY? :
                onP2pNotifyDownloadQueueStandBy(remoteDeviceNo:(items[0] as NSString).longLongValue,
                                                downloadingCount:(items[1] as NSString).integerValue,
                                                downloadStandByCount:(items[2] as NSString).integerValue,
                                                myTurn:(items[3] as NSString).integerValue)
                
            case .PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE? :
                onP2pNotifyDownloadQueueRelease(remoteDeviceNo:(items[0] as NSString).longLongValue,
                                                downloadingCount:(items[1] as NSString).integerValue,
                                                downloadStandByCount:(items[2] as NSString).integerValue,
                                                myTurn:(items[3] as NSString).integerValue)
                
            case .PCC_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY? :
                onP2pNotifyUploadQueueStandBy(remoteDeviceNo:(items[0] as NSString).longLongValue,
                                              uploadingCount:(items[1] as NSString).integerValue,
                                              uploadStandByCount:(items[2] as NSString).integerValue,
                                              myTurn:(items[3] as NSString).integerValue)
                
            case .PCC_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE? :
                onP2pNotifyUploadQueueRelease(remoteDeviceNo:(items[0] as NSString).longLongValue,
                                              uploadingCount:(items[1] as NSString).integerValue,
                                              uploadStandByCount:(items[2] as NSString).integerValue,
                                              myTurn:(items[3] as NSString).integerValue)
                
            case .PCC_P2P_NOTIFY_CONNECTION_CLOSED? :
                onTransmitListStop(remoteDeviceNo:(items[0] as NSString).longLongValue)
                
            case .PCC_P2P_NOTIFY_START_NEXT_TRANSFER? :
                startNextTransfer(remoteDeviceNo:(items[0] as NSString).longLongValue)
                
            case .PCC_P2P_NOTIFY_STATUS_MESSAGE? :
                onP2pNotifyStatusMessage(text:String(items[0]))
                
            default :
                // do nothing...
                onCallbackError()
            }
            
        } else {
            onCallbackError()
        }
    }
    
    ///////////////////////////////////////////////////////////////////////
    //
    // for test
    //
    ///////////////////////////////////////////////////////////////////////
    
    @objc(testSwiftCallback:withParam:)
    func testSwiftCallback(command : NSNumber, param : NSMutableArray)
    {
        print("testSwiftCallback : command = \(command)")
        
        var items = param as NSArray? as? [String]
        print("testSwiftCallback : items = \(items)")
    }
    
    @objc(testSwiftCallback2)
    func testSwiftCallback2()
    {
        print("testSwiftCallback2....")
    }
    
    func initTest()
    {
        saaringWrapper?.initTest(self);
    }
    
    func testCallback()
    {
        saaringWrapper?.testCallback();
    }
    
    func testSwiftCall()
    {
        saaringWrapper?.testSwiftCall(self);
    }
    
    func testStruct()
    {
        var saaringMemberInfo : SaaringMemberInfo = SaaringMemberInfo();
        
        saaringMemberInfo.authKey = "1234";
        print("testStruct.... saaringMemberInfo.certKey=\(saaringMemberInfo.authKey)")
        
        saaringMemberInfo.certNo = 7890;
        print("testStruct.... saaringMemberInfo.certNo=\(saaringMemberInfo.certNo)")
        
    }
    
    ///////////////////////////////////////////////////////////////////////
    //
    // for callback handler
    //
    ///////////////////////////////////////////////////////////////////////
    
    // for error
    func onCallbackError()
    {
        // do nothing
    }
    
    ///////////////////////////////////////////////////////////////////////
    //
    // for Share folder thread callback handler
    //
    ///////////////////////////////////////////////////////////////////////
    
    // for share dir
    func onShareDirRefresh(path:String)
    {
        // do something
    }
    func onShareDirJobFinished()
    {
        // do nothing
    }
    func onShareDirJobError()
    {
        // do nothing
    }
    
    ///////////////////////////////////////////////////////////////////////
    //
    // for p2p callback handler
    //
    ///////////////////////////////////////////////////////////////////////
    
    // for p2p
    func onSignalingConnected()
    {
        // do something
    }
    func onSignalingClosed()
    {
        // do something
    }
    func onSignalingLoginFailed()
    {
        // do something
    }
    func onSignalingNotifyDeviceLiveYN(fromUserNo:Int64, fromDeviceNo:Int64, liveYN:String)
    {
        // do something
    }
    func onSignalingNotifyRequestFriend(friendName:String)
    {
        // do something
    }
    func onSignalingNotifyAddFriend(friendName:String, friendUserNo:Int64)
    {
        // do something
    }
    func onPortmapFinished()
    {
        // do something
    }
    func onP2pLoginFinish(friendDeviceNo:Int64, toRemoteShareNo:Int64)
    {
        // do something
    }
    func onP2pWakeUpFinish(isSuccess:Bool)
    {
        // do something
    }
    func onP2pFinishDownload(transNo:Int64, fileSize:Int64, receivedSize:Int64)
    {
        // do something
    }
    func onP2pFinishUpload(transNo:Int64, fileSize:Int64, sentSize:Int64)
    {
        // do something
    }
    func onP2pProgressDownload(transNo:Int64, fileSize:Int64, receivedSize:Int64)
    {
        // do something
    }
    func onP2pProgressUpload(transNo:Int64, fileSize:Int64, sentSize:Int64)
    {
        // do something
    }
    func onP2pAbortDownload(transNo:Int64)
    {
        // do something
    }
    func onP2pAbortUpload(transNo:Int64)
    {
        // do something
    }
    func onP2pFinishDownloadOther(jobId:Int64, remoteDeviceNo:Int64)
    {
        // do something
    }
    func onP2pFinishUploadOther(jobId:Int64, remoteDeviceNo:Int64)
    {
        // do something
    }
    func onP2pProgressDownloadOther(jobId:Int64, remoteDeviceNo:Int64, fileSize:Int64, currSize:Int64,
                                    filePath:String, serverName:String, deviceName:String)
    {
        // do something
    }
    func onP2pProgressUploadOther(jobId:Int64, remoteDeviceNo:Int64, fileSize:Int64, currSize:Int64,
                                  filePath:String, serverName:String, deviceName:String)
    {
        // do something
    }
    func onProgressedRequest(processType:Int, jobId:Int64, remoteDeviceNo:Int64, resultCode:Int)
    {
        // do something
    }
    func onProgressedResponse(processType:Int, jobId:Int64, remoteDeviceNo:Int64, resultCode:Int)
    {
        // do something
    }
    func onP2pProcessResponseFolderList(strJson:String)
    {
        // do something
    }
    func onP2pProcessResponseFolderDetail(strJson:String)
    {
        // do something
    }
    func onP2pProcessResponseFolderDetailFinished()
    {
        // do something
    }
    func onP2pProcessResponseFolderDetailRecursive(strJson:String)
    {
        // do something
    }
    func onP2pProcessResponseFileUploadQuery(transNo:Int64, fileStartPosition:Int64)
    {
        // do something
    }
    func onP2pProcessResponseFileDownloadInviteSendFilesQuery(strJson:String)
    {
        // do something
    }
    func onP2pProcessResponseSaveSendFilesInviteInfo(strJson:String)
    {
        // do something
    }
    func onP2pNotifyDownloadQueueStandBy(remoteDeviceNo:Int64, downloadingCount:Int, downloadStandByCount:Int, myTurn:Int)
    {
        // do something
    }
    func onP2pNotifyDownloadQueueRelease(remoteDeviceNo:Int64, downloadingCount:Int, downloadStandByCount:Int, myTurn:Int)
    {
        // do something
    }
    func onP2pNotifyUploadQueueStandBy(remoteDeviceNo:Int64, uploadingCount:Int, uploadStandByCount:Int, myTurn:Int)
    {
        // do something
    }
    func onP2pNotifyUploadQueueRelease(remoteDeviceNo:Int64, uploadingCount:Int, uploadStandByCount:Int, myTurn:Int)
    {
        // do something
    }
    func onTransmitListStop(remoteDeviceNo:Int64)
    {
        // do something
    }
    func startNextTransfer(remoteDeviceNo:Int64)
    {
        // do something
    }
    func onP2pNotifyStatusMessage(text:String)
    {
        // do something
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for common
    //
    //////////////////////////////////////////////////////////////////////////////////
    func calSpeed(oldTime:Int64, currTime:Int64, oldSize:Int64, currSize:Int64) -> Double
    {
        let result : Double? = saaringWrapper?.calSpeed(oldTime, currTime:currTime, oldSize:oldSize, currSize:currSize);
        return result!;
    }
    
    func leftTime(oldTime:Int64, currTime:Int64, oldSize:Int64, currSize:Int64, totalSize:Int64, speed:Double) -> String
    {
        let result : String? = saaringWrapper?.leftTime(oldTime, currTime:currTime, oldSize:oldSize, currSize:currSize, totalSize:totalSize, speed:speed);
        return result!;
    }
    
    func getSizeWithUnit(size:Int64) -> String
    {
        let result : String? = saaringWrapper?.getSizeWithUnit(size);
        return result!;
    }
    
    func checkEmail(email:String) -> Bool
    {
        let result : Bool? = saaringWrapper?.checkEmail(email);
        return result == true;
    }
    
    func getDuplicationFilename(filePath:String, lastLocalFilePath:String) -> String
    {
        let result : String? = saaringWrapper?.getDuplicationFilename(filePath, lastLocalFilePath:lastLocalFilePath);
        return result!;
    }
    
    func numberFormatContainComma(number:UInt64) -> String
    {
        let result : String? = saaringWrapper?.numberFormat(withComma: number);
        return result!;
    }
    
    func checkUpgrade(lastVer:String, runningVer:String, currentVer:String) -> Int32
    {
        let result : Int32? = saaringWrapper?.checkUpgrade(lastVer, runningVer:runningVer, currentVer:currentVer);
        return result!;
    }
    
    func changeProfileImage(authKey:String, path:String) -> String
    {
        let result : String? = saaringWrapper?.changeProfileImage(authKey, path:path);
        return result!;
    }
    
    func getDownloadFolder() -> String
    {
        let result : String? = saaringWrapper?.getDownloadFolder();
        return result!;
    }
    
    func getShareTopFolder() -> String
    {
        let result : String? = saaringWrapper?.getShareTopFolder();
        return result!;
    }
    
    func removeFirstPath(path:String) -> String
    {
        let result : String? = saaringWrapper?.removeFirstPath(path);
        return result!;
    }
    
    func getProviderId(idType:Int32) -> String
    {
        let result : String? = saaringWrapper?.getProviderId(idType);
        return result!;
    }
    
    func getProviderName(idType:Int32) -> String
    {
        let result : String? = saaringWrapper?.getProviderName(idType);
        return result!;
    }
    
    func getProviderNameByProviderId(providerId:String) -> String
    {
        let result : String? = saaringWrapper?.getProviderName(byProviderId: providerId);
        return result!;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for db
    //
    //////////////////////////////////////////////////////////////////////////////////
    
    // for all
    func isOpen() -> Bool
    {
        let result : Bool? = saaringWrapper?.isOpen();
        return result == true;
    }
    
    // for login
    func doLoginSecondAction(idType:Int32, email:String, phoneNumber:String, password:String,
                             providerId:String, idToken:String, accessToken:String, uid:String,
                             isSaveEmail:String, isAutoLogin:String)
    {
        saaringWrapper?.do(afterLogin: idType, email:email, phoneNumber:phoneNumber, password:password,
                           providerId:providerId, idToken:idToken, accessToken:accessToken, uid:uid,
                           isSaveEmail:isSaveEmail, isAutoLogin:isAutoLogin);
    }

    func updateLoginTb(idType:Int32, email:String, phoneNumber:String, password:String, isSaveEmail:String, isAutoLogin:String, isLastLoginEmail:String) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateLoginTb(idType, email:email, phoneNumber:phoneNumber, password:password, isSaveEmail:isSaveEmail, isAutoLogin:isAutoLogin, isLastLoginEmail:isLastLoginEmail);
        return result == true;
    }
    func updateLoginAutoStart(idType:Int32, email:String, phoneNumber:String, password:String, isAutoStart:String) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateLoginAutoStart(idType, email:email, phoneNumber:phoneNumber, isAutoStart:isAutoStart);
        return result == true;
    }
    
    func updateSetSend(saaringSetupInfo:SaaringSetupInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateSetSend(saaringSetupInfo);
        return result == true;
    }
    func updateSetNetwork(saaringSetupInfo:SaaringSetupInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateSetNetwork(saaringSetupInfo);
        return result == true;
    }
    
    func readDbLastLogin(saaringSetupInfo:SaaringSetupInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.readDbLastLogin(saaringSetupInfo);
        return result == true;
    }
    func readDbByEmailOrPhoneNumber(idType:Int32, email:String, phoneNumber:String, saaringSetupInfo:SaaringSetupInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.readDb(byEmailOrPhoneNumber: idType, email:email, phoneNumber:phoneNumber, saaringSetupInfo:saaringSetupInfo);
        return result == true;
    }
    
    // for transfer
    func insertMyTransTbOne(saaringTransfer:SaaringTransferBaseInfo) -> Int64
    {
        let result : Int64? = saaringWrapper?.insertMyTransTbOne(saaringTransfer);
        return result!;
    }
    func insertMyTransTbMulti(listSaaringTransfer:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.insertMyTransTbMulti(listSaaringTransfer);
        return result == true;
    }
    
    func updateMyTransTb(saaringTransfer:SaaringTransferBaseInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateMyTransTb(saaringTransfer);
        return result == true;
    }
    func updateMyTransTbStatus(transNo:Int64, transStatus:Int32, updateTime:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateMyTransTbStatus(transNo, transStatus:transStatus, updateTime:updateTime);
        return result == true;
    }
    
    func updateMyTransTbSortNoMulti(listTransNo:NSMutableArray, updateTime:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateMyTransTbSortNoMulti(listTransNo, updateTime:updateTime);
        return result == true;
    }
    
    func updateMyTransTbProgress(transNo:Int64, fileSize:Int64, transSize:Int64, updateTime:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateMyTransTbProgress(transNo, fileSize:fileSize, transSize:transSize, updateTime:updateTime);
        return result == true;
    }
    func updateMyTransTbInit(myDeviceNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateMyTransTbInit(myDeviceNo);
        return result == true;
    }
    
    func deleteMyTransTbOne(transNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteMyTransTbOne(transNo);
        return result == true;
    }
    func deleteMyTransTbMulti(listTransNo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteMyTransTbMulti(listTransNo);
        return result == true;
    }
    func deleteMyTransTbAll(deviceNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteMyTransTbAll(deviceNo);
        return result == true;
    }
    
    func readMyTransTbByTransNo(listSaaringTransfer:NSMutableArray, transNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.readMyTransTb(byTransNo: listSaaringTransfer, transNo:transNo);
        return result == true;
    }
    func readMyTransTbTransferring(listSaaringTransfer:NSMutableArray, myDeviceNo:Int64, deleteYN:String, deviceNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.readMyTransTbTransferring(listSaaringTransfer, myDeviceNo:myDeviceNo, deleteYN:deleteYN, deviceNo:deviceNo);
        return result == true;
    }
    func readMyTransTbNotTransferringLastOne(listSaaringTransfer:NSMutableArray, myDeviceNo:Int64, deleteYN:String, deviceNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.readMyTransTbNotTransferringLastOne(listSaaringTransfer, myDeviceNo:myDeviceNo, deleteYN:deleteYN, deviceNo:deviceNo);
        return result == true;
    }
    func readMyTransTbInit(listSaaringTransfer:NSMutableArray, myDeviceNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.readMyTransTbInit(listSaaringTransfer, myDeviceNo:myDeviceNo);
        return result == true;
    }
    
    // for share
    func insertShareTb(saaringShare:SaaringShareInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.insertShareTb(saaringShare);
        return result == true;
    }
    func updateShareTb(saaringShare:SaaringShareInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateShareTb(saaringShare);
        return result == true;
    }
    func deleteShareTb(shareNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteShareTb(shareNo);
        return result == true;
    }
    
    func readShareTb(deviceNo:Int64, listSaaringShare:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.readShareTb(deviceNo, listSaaringShare:listSaaringShare);
        return result == true;
    }
    
    func getShareNoByServerFolderNo(serverFolderNo:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.getShareNo(byServerFolderNo: serverFolderNo);
        return result!;
    }
    func getServerFolderNoWhereShareNo(shareNo:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.getServerFolderNo(byShareNo: shareNo);
        return result!;
    }
    func getShareFolderPathWhereServerFolderNo(serverFolderNo:Int64) -> String
    {
        let result : String? = saaringWrapper?.getShareFolderPath(byServerFolderNo: serverFolderNo);
        return result!;
    }
    
    func getShareFileCntWhereShareNo(deviceNo:Int64, shareNo:Int64) -> Int32
    {
        let result : Int32? = saaringWrapper?.getShareFileCnt(withShareNo: deviceNo, shareNo:shareNo);
        return result!;
    }
    func getShareFileSize(deviceNo:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.getShareFileSize(deviceNo);
        return result!;
    }
    func getShareFileSizeWhereShareNo(deviceNo:Int64, shareNo:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.getShareFileSize(withShareNo: deviceNo, shareNo:shareNo);
        return result!;
    }
    
    // for share detail
    func updateShareDetailShortPath(shareNo:Int64, shareName:String, shareFolderFullPath:String) -> Bool
    {
        let result : Bool? = saaringWrapper?.updateShareDetailShortPath(shareNo, shareName:shareName, shareFolderFullPath:shareFolderFullPath);
        return result == true;
    }
    
    func deleteShareDetailTb(shareNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteShareDetailTb(shareNo);
        return result == true;
    }
    
    func makeMemoryShareDetailTb()
    {
        saaringWrapper?.makeMemoryShareDetailTb();
    }
    func backupMemoryShareDetailTb()
    {
        saaringWrapper?.backupMemoryShareDetailTb();
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for invite files
    //
    //////////////////////////////////////////////////////////////////////////////////
    func insertInviteFiles(saaringInviteFilesInfo:SaaringInviteFilesInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.insertInviteFiles(saaringInviteFilesInfo);
        return result == true;
    }
    func insertInviteFiles(listSaaringInviteFilesInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.insertListInviteFiles(listSaaringInviteFilesInfo);
        return result == true;
    }
    
    func deleteExpired() -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteExpired();
        return result == true;
    }
    func deleteInviteFilesByInviteNo(inviteNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteInviteFiles(byInviteNo: inviteNo);
        return result == true;
    }
    
    func listInviteFiles(inviteNo:Int64, listSaaringInviteFilesInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listInviteFiles(inviteNo, listSaaringInviteFilesInfo: listSaaringInviteFilesInfo);
        return result == true;
    }
    func listInviteFilesWithCertKey(inviteNo:Int64, certKey:String, listSaaringInviteFilesInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listInviteFiles(withCertKey: inviteNo, certKey: certKey, listSaaringInviteFilesInfo: listSaaringInviteFilesInfo);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for share dir
    //
    //////////////////////////////////////////////////////////////////////////////////
    func initThreadShareDirCallback()
    {
        saaringWrapper?.initThreadShareDir(self);
    }
    
    func startThreadShareFolder()
    {
        saaringWrapper?.startThreadShareFolder();
    }
    func stopThreadShareFolder()
    {
        saaringWrapper?.stopThreadShareFolder();
    }
    
    func shareFolderInit()
    {
        saaringWrapper?.shareFolderInit();
    }
    func shareFolderInsert(shareNo:Int64, sharePath:String, shareName:String)
    {
        saaringWrapper?.shareFolderInsert(shareNo, sharePath:sharePath, shareName:shareName);
    }
    func shareFolderDelete(shareNo:Int64)
    {
        saaringWrapper?.shareFolderDelete(shareNo);
    }
    func shareFolderUpdate(folderPath:String)
    {
        saaringWrapper?.shareFolderUpdate(folderPath);
    }
    func serverFolderDelete(serverFolderNo:Int64)
    {
        saaringWrapper?.serverFolderDelete(serverFolderNo);
    }
    
    func getShareFolderCount() -> Int32
    {
        let result : Int32? = saaringWrapper?.getShareFolderCount();
        return result!;
    }
    func getShareFileCount() -> Int32
    {
        let result : Int32? = saaringWrapper?.getShareFileCount();
        return result!;
    }
    
    func startFileMonitor()
    {
        saaringWrapper?.startFileMonitor();
    }
    func stopFileMonitor()
    {
        saaringWrapper?.stopFileMonitor();
    }

    func doesNeedDbBackup() -> Bool
    {
        let result : Bool? = saaringWrapper?.doesNeedDbBackup();
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for p2p
    //
    //////////////////////////////////////////////////////////////////////////////////
    func initP2p()
    {
        saaringWrapper?.initP2p(self);
    }
    func closeP2p()
    {
        saaringWrapper?.closeP2p();
    }
    func isP2p() -> Bool
    {
        let result : Bool? = saaringWrapper?.isP2p();
        return result == true;
    }
    
    func getServerInfo() -> SaaringServerInfo
    {
        let result : SaaringServerInfo? = saaringWrapper?.getServerInfo();
        return result!;
    }
    
    func isSignalingConnected() -> Bool
    {
        let result : Bool? = saaringWrapper?.isSignalingConnected();
        return result == true;
    }
    func isSignalingLogin() -> Bool
    {
        let result : Bool? = saaringWrapper?.isSignalingLogin();
        return result == true;
    }
    func doSignalingConnect(authKey:String, isRefreshServerInfo:Bool) -> Bool
    {
        
        let result : Bool? = saaringWrapper!.doSignalingConnect(authKey, isRefreshServerInfo:isRefreshServerInfo);
        print("result = \(result)");
        
        return result == true;        
    }
    func doSignalingClose()
    {
        saaringWrapper?.doSignalingClose();
    }
    func reqHeartBeat()
    {
        saaringWrapper?.reqHeartBeat();
    }
    
    func isP2pLived(deviceNo:Int64) -> Bool
    {
        let result : Bool? = saaringWrapper?.isP2pLived(deviceNo);
        return result!;
    }
    
    func updateShareFolders()
    {
        saaringWrapper?.updateShareFolders();
    }
    func updateNetworkSendSettings()
    {
        saaringWrapper?.updateNetworkSendSettings();
    }
    
    func doP2pConnect(toUserNo:Int64, toUserName:String, toDeviceNo:Int64, toDeviceType:Int32, toDeviceName:String, mustWakeUp:Bool)
    {
        saaringWrapper?.doP2pConnect(toUserNo, toUserName:toUserName, toDeviceNo:toDeviceNo, toDeviceType:toDeviceType, toDeviceName:toDeviceName, mustWakeUp:mustWakeUp);
    }
    func doP2pConnectShareFolder(toUserNo:Int64, toUserName:String, toDeviceNo:Int64, toDeviceType:Int32, toDeviceName:String, toServerFolderNo:Int64, toFolderName:String, mustWakeUp:Bool)
    {
        saaringWrapper?.doP2pConnectShareFolder(toUserNo, toUserName:toUserName, toDeviceNo:toDeviceNo, toDeviceType:toDeviceType, toDeviceName:toDeviceName, toServerFolderNo:toServerFolderNo, toFolderName:toFolderName, mustWakeUp:mustWakeUp);
    }
    func doP2pConnectCancel(toUserNo:Int64, toUserName:String, toDeviceNo:Int64, toDeviceType:Int32, toDeviceName:String)
    {
        saaringWrapper?.doP2pConnectCancel(toUserNo, toUserName:toUserName, toDeviceNo:toDeviceNo, toDeviceType:toDeviceType, toDeviceName:toDeviceName);
    }
    
    func doP2pFolderList(toDeviceNo:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pFolderList(toDeviceNo);
        return result!;
    }
    func doP2pFolderDetail(toDeviceNo:Int64, shareNo:Int64, serverFolderNo:Int64, remotePath:String) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pFolderDetail(toDeviceNo, shareNo:shareNo, serverFolderNo:serverFolderNo, remotePath:remotePath);
        return result!;
    }
    func doP2pFolderDetailRecursive(toDeviceNo:Int64, shareNo:Int64, remotePath:String) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pFolderDetailRecursive(toDeviceNo, shareNo:shareNo, remotePath:remotePath);
        return result!;
    }
    
    func doP2pUploadQuery(toDeviceNo:Int64, remoteFilePath:String, fileSize:Int64, myTransNo:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pUploadQuery(toDeviceNo, remoteFilePath:remoteFilePath, fileSize:fileSize, myTransNo:myTransNo);
        return result!;
    }
    func doP2pUpload(toDeviceNo:Int64, remoteFilePath:String, localFilePath:String, fileStartPosition:Int64, myTransNo:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pUpload(toDeviceNo, remoteFilePath:remoteFilePath, localFilePath:localFilePath, fileStartPosition:fileStartPosition, myTransNo:myTransNo);
        return result!;
    }
    func doP2pDownload(toDeviceNo:Int64, remoteFilePath:String, localFilePath:String,
                       fileSize:Int64, fileStartPosition:Int64, myTransNo:Int64, folderDepth:Int32,
                       inviteNo:Int64, remoteFileId:Int64) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pDownload(toDeviceNo, remoteFilePath:remoteFilePath, localFilePath:localFilePath,
                                                            fileSize:fileSize, fileStartPosition:fileStartPosition, myTransNo:myTransNo, folderDepth:folderDepth,
                                                            inviteNo:inviteNo, remoteFileId:remoteFileId);
        return result!;
    }
    func doP2pDownloadInviteSendFilesQuery(toDeviceNo:Int64, inviteNo:Int64, certKey:String) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pDownloadInviteSendFilesQuery(toDeviceNo, inviteNo:inviteNo, certKey:certKey);
        return result!;
    }
    func doP2pSaveSendFilesInviteInfo(toDeviceNo:Int64, inviteNo:Int64,
                                      inviteCode:String, dynamicLink:String,
                                      listShortPathName:NSMutableArray,
                                      listFileName:NSMutableArray,
                                      listFileSize:NSMutableArray,
                                      certKey:String, inviteMessage:String) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pSaveSendFilesInviteInfo(toDeviceNo, inviteNo:inviteNo,
                                                                           inviteCode:inviteCode, dynamicLink:dynamicLink,
                                                                           listShortPathName:listShortPathName,
                                                                           listFileName:listFileName,
                                                                           listFileSize:listFileSize,
                                                                           certKey:certKey, inviteMessage:inviteMessage);
        return result!;
    }
    
    func doP2pProcessDownloadComplete(toDeviceNo:Int64)
    {
        saaringWrapper?.doP2pProcessDownloadComplete(toDeviceNo);
    }
    func doP2pProcessUploadComplete(toDeviceNo:Int64)
    {
        saaringWrapper?.doP2pProcessUploadComplete(toDeviceNo);
    }
    
    func doP2pProcessDownloadQueueCheck(toDeviceNo:Int64)
    {
        saaringWrapper?.doP2pProcessDownloadQueueCheck(toDeviceNo);
    }
    func doP2pProcessUploadQueueCheck(toDeviceNo:Int64)
    {
        saaringWrapper?.doP2pProcessUploadQueueCheck(toDeviceNo);
    }
    
    func doP2pOpenAllChannel(listDeviceNo:NSMutableArray)
    {
        saaringWrapper?.doP2pOpenAllChannel(listDeviceNo);
    }
    func doP2pCloseChannel(deviceNo:Int64, remoteDeviceNo:Int64)
    {
        saaringWrapper?.doP2pCloseChannel(deviceNo, remoteDeviceNo:remoteDeviceNo);
    }
    func doP2pStopAllTransfer(listDeviceNo:NSMutableArray) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pStopAllTransfer(listDeviceNo);
        return result!;
    }
    func doP2pCancelAllTransfer(listDeviceNo:NSMutableArray) -> Int64
    {
        let result : Int64? = saaringWrapper?.doP2pCancelAllTransfer(listDeviceNo);
        return result!;
    }
    
    func listAllChannelDeviceNo(listDeviceNo:NSMutableArray)
    {
        saaringWrapper?.listAllChannelDeviceNo(listDeviceNo);
    }
    func updateNumLiveTransfer(numLiveTransfer:Int32)
    {
        saaringWrapper?.updateNumLiveTransfer(numLiveTransfer);
    }
    
    func startRelayServer()
    {
        saaringWrapper?.startRelayServer();
    }
    func stopRelayServer()
    {
        saaringWrapper?.stopRelayServer();
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for web
    //
    //////////////////////////////////////////////////////////////////////////////////
    
    // for connect
    func listFolderOfOther(authKey:String, friendUserNo:Int64,
                           resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                           listSaaringShareFolderInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listFolder(ofOther: authKey, friendUserNo:friendUserNo,
                                                        resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                        listSaaringShareFolderInfo:listSaaringShareFolderInfo);
        return result == true;
    }
    func listLiveDevice(authKey:String, deviceNos:String,
                        resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                        listLiveDeviceNo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listLiveDevice(authKey, deviceNos:deviceNos,
                                                            resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                            listLiveDeviceNo:listLiveDeviceNo);
        return result == true;
    }
    func getBadgeInfo(authKey:String,
                      resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                      saaringBadgeInfo:SaaringBadgeInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.getBadgeInfo(authKey,
                                                          resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                          saaringBadgeInfo:saaringBadgeInfo);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    // for friend
    func listFriend(authKey:String,
                    resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                    listSaaringFriendInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listFriend(authKey,
                                                        resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                        listSaaringFriendInfo:listSaaringFriendInfo);
        return result == true;
    }
    func requestFriendByUserNo(authKey:String, toUserNo:Int64,
                               resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.requestFriend(byUserNo: authKey, toUserNo:toUserNo,
                                                           resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func listRequestingFriend(authKey:String,
                              resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                              listSaaringIncomingRequestFriend:NSMutableArray,
                              listSaaringRequestingFriendMember:NSMutableArray,
                              listSaaringRequestingFriendNonMember:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listRequestingFriend(authKey,
                                                                  resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                  listSaaringIncomingRequestFriend:listSaaringIncomingRequestFriend,
                                                                  listSaaringRequestingFriendMember:listSaaringRequestingFriendMember,
                                                                  listSaaringRequestingFriendNonMember:listSaaringRequestingFriendNonMember);
        return result == true;
    }
    
    func cancelRequestFriend(authKey:String, toUserNo:Int64, toEmail:String, toPhoneNumber:String,
                             resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.cancelRequestFriend(authKey, toUserNo:toUserNo, toEmail:toEmail, toPhoneNumber:toPhoneNumber,
                                                                 resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func rejectRequestFriend(authKey:String, toUserNo:Int64,
                             resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.rejectRequestFriend(authKey, toUserNo:toUserNo,
                                                                 resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func recoverRequestFriend(authKey:String, toUserNo:Int64,
                              resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.recoverRequestFriend(authKey, toUserNo:toUserNo,
                                                                  resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    
    func addFriend(authKey:String, friendUserNo:Int64,
                   resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.addFriend(authKey, friendUserNo:friendUserNo,
                                                       resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    
    func blockFriend(authKey:String, friendUserNos:String, blockYN:String,
                     resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.blockFriend(authKey, friendUserNos:friendUserNos, blockYN:blockYN,
                                                         resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func listBlock(authKey:String,
                   resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                   listBlocked:NSMutableArray, listRejected:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listBlock(authKey,
                                                       resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                       listBlocked:listBlocked, listRejected:listRejected);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    
    func inviteFriendSimple(authKey:String, certKey:String, inviteMessage:String,
                            resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                            inviteNo:NSMutableString, inviteCode:NSMutableString, dynamicLink:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.inviteFriendSimple(authKey, certKey:certKey, inviteMessage:inviteMessage,
                                                                resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                inviteNo:inviteNo, inviteCode:inviteCode, dynamicLink:dynamicLink);
        return result == true;
    }
    func inviteFriend(authKey:String,
                      listToUserNo:NSMutableArray, listToUserName:NSMutableArray,
                      listToEmail:NSMutableArray, listToPhoneNumber:NSMutableArray,
                      certKey:String, inviteMessage:String,
                      resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                      inviteNo:NSMutableString, inviteCode:NSMutableString, dynamicLink:NSMutableString,
                      listFailUserNo:NSMutableArray, listSuccessEmail:NSMutableArray,
                      listSaaringAddedFriend:NSMutableArray,
                      listSaaringIncomingRequestFriend:NSMutableArray,
                      listSaaringRequestingFriendMember:NSMutableArray,
                      listSaaringRequestingFriendNonMember:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.inviteFriend(authKey,
                                                          listToUserNo:listToUserNo, listToUserName:listToUserName,
                                                          listToEmail:listToEmail, listToPhoneNumber:listToPhoneNumber,
                                                          certKey:certKey, inviteMessage:inviteMessage,
                                                          resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                          inviteNo:inviteNo, inviteCode:inviteCode, dynamicLink:dynamicLink,
                                                          listFailUserNo:listFailUserNo, listSuccessEmail:listSuccessEmail,
                                                          listAddedFriend:listSaaringAddedFriend,
                                                          listIncomingRequestFriend:listSaaringIncomingRequestFriend,
                                                          listRequestingFriendMember:listSaaringRequestingFriendMember,
                                                          listRequestingFriendNonMember:listSaaringRequestingFriendNonMember);
        return result == true;
    }
    func cancelInviteFriend(authKey:String, inviteNo:Int64,
                            resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.cancelInviteFriend(authKey, inviteNo:inviteNo,
                                                                resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func completeInviteFriend(authKey:String, inviteNo:Int64,
                              resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.completeInviteFriend(authKey, inviteNo:inviteNo,
                                                                  resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func extendInviteExpiredt(authKey:String, inviteNo:Int64,
                              resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                              saaringInviteInfo:SaaringInviteInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.extendInviteExpiredt(authKey, inviteNo:inviteNo,
                                                                  resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                  saaringInviteInfo:saaringInviteInfo);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    
    func getInvite(authKey:String, inviteNo:Int64,
                   resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                   saaringInviteInfo:SaaringInviteInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.getInvite(authKey, inviteNo:inviteNo,
                                                       resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                       saaringInviteInfo:saaringInviteInfo);
        return result == true;
    }
    func getInviteByDynamicLink(dynamicLink:String, certKey:String,
                                resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                                saaringInviteInfo:SaaringInviteInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.getInviteByDynamicLink(dynamicLink, certKey:certKey,
                                                                    resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                    saaringInviteInfo:saaringInviteInfo);
        return result == true;
    }
    
    func listInviteAll(authKey:String,
                       resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                       listSaaringInviteInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listInviteAll(authKey,
                                                           resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                           listSaaringInviteInfo:listSaaringInviteInfo);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    
    func inviteShareFolderSimple(authKey:String,
                                 inviteShareFolderServerFolderNo:Int64,
                                 inviteShareFolderPath:String,
                                 certKey:String, inviteMessage:String,
                                 resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                                 inviteNo:NSMutableString, inviteCode:NSMutableString, dynamicLink:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.inviteShareFolderSimple(authKey,
                                                                     inviteShareFolderServerFolderNo:inviteShareFolderServerFolderNo,
                                                                     inviteShareFolderPath:inviteShareFolderPath,
                                                                     certKey:certKey, inviteMessage:inviteMessage,
                                                                     resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                     inviteNo:inviteNo, inviteCode:inviteCode, dynamicLink:dynamicLink);
        return result == true;
    }
    func inviteSendFilesSimple(authKey:String,
                               numFile:Int64, totalFileSize:Int64,
                               certKey:String, inviteMessage:String,
                               resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                               inviteNo:NSMutableString, inviteCode:NSMutableString, dynamicLink:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.inviteSendFilesSimple(authKey,
                                                                   numFile:numFile, totalFileSize:totalFileSize,
                                                                   certKey:certKey, inviteMessage:inviteMessage,
                                                                   resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                   inviteNo:inviteNo, inviteCode:inviteCode, dynamicLink:dynamicLink);
        return result == true;
    }
    func inviteSendFilesForRemoteDeviceSimple(authKey:String,
                                              numFile:Int64, totalFileSize:Int64,
                                              inviteShareFolderServerFolderNo:Int64, inviteShareFolderPath:String,
                                              certKey:String, inviteMessage:String,
                                              resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                                              inviteNo:NSMutableString, inviteCode:NSMutableString, dynamicLink:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.inviteSendFiles(forRemoteDeviceSimple: authKey,
                                                             numFile:numFile, totalFileSize:totalFileSize,
                                                             inviteShareFolderServerFolderNo:inviteShareFolderServerFolderNo,
                                                             inviteShareFolderPath:inviteShareFolderPath,
                                                             certKey:certKey, inviteMessage:inviteMessage,
                                                             resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                             inviteNo:inviteNo, inviteCode:inviteCode, dynamicLink:dynamicLink);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    
    func registerInviteByDynamicLink(authKey:String,
                                     dynamicLink:String, certKey:String,
                                     resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                                     saaringInviteRegisteredInfo:SaaringInviteRegisteredInfo,
                                     saaringInviteInfo:SaaringInviteInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.registerInvite(byDynamicLink: authKey,
                                                            dynamicLink:dynamicLink, certKey:certKey,
                                                            resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                            saaringInviteRegisteredInfo:saaringInviteRegisteredInfo,
                                                            saaringInviteInfo:saaringInviteInfo);
        return result == true;
    }
    func deleteInviteRegistered(authKey:String, inviteNo:Int64,
                                resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteInviteRegistered(authKey, inviteNo:inviteNo,
                                                                    resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func getInviteRegistered(authKey:String, inviteNo:Int64,
                             resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                             saaringInviteRegisteredInfo:SaaringInviteRegisteredInfo,
                             saaringInviteInfo:SaaringInviteInfo) -> Bool
    {
        let result : Bool? = saaringWrapper?.getInviteRegistered(authKey, inviteNo:inviteNo,
                                                                 resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                 saaringInviteRegisteredInfo:saaringInviteRegisteredInfo,
                                                                 saaringInviteInfo:saaringInviteInfo);
        return result == true;
    }
    func listInviteRegisteredAll(authKey:String,
                                 resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                                 listSaaringInviteRegisteredInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listInviteRegisteredAll(authKey,
                                                                     resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                                     listSaaringInviteRegisteredInfo:listSaaringInviteRegisteredInfo);
        return result == true;
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    
    // for member
    func verifyFirebaseToken(firebaseToken:String,
                             resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.verifyFirebaseToken(firebaseToken,
                                                                 resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func joinMember(saaringMemberInfo:SaaringMemberInfo,
                    resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.joinMember(saaringMemberInfo,
                                                        resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func loginMember(saaringMemberInfo:SaaringMemberInfo,
                     resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        
        let result : Bool? = saaringWrapper?.loginMember(saaringMemberInfo, resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        print("result = \(result)")
        
        return result == true;
    }
    func doMe(authKey:String, deviceType:Int32,
              resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
              saaringMemberInfo:SaaringMemberInfo, listSaaringMemberProviderInfo:NSMutableArray) -> Bool
    {
        
        let result : Bool? = saaringWrapper?.doMe(authKey, deviceType:deviceType,
                                                  resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                  saaringMemberInfo:saaringMemberInfo,
                                                  listSaaringMemberProviderInfo:listSaaringMemberProviderInfo);
        print("result = \(result)")
        
        return result == true;
    }
    func isEmailExist(email:String,
                      resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                      isEmailExist:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.isEmailExist(email,
                                                          resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                          isEmailExist:isEmailExist);
        return result == true;
    }
    
    func doUnlink(authKey:String,
                  resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.doUnlink(authKey, resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func doLogout(authKey:String,
                  resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.doLogout(authKey, resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func searchUser(authKey:String, keyword:String,
                    resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                    listSaaringFriendInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.searchUser(authKey, keyword:keyword, resultCode:resultCode, resultMessage:resultMessage, resultData:resultData, listSaaringFriendInfo:listSaaringFriendInfo);
        return result == true;
    }
    
    func changeProfilePic(authKey:String, profilePic:String,
                          resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.changeProfilePic(authKey, profilePic:profilePic, resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func changeEmail(authKey:String, email:String,
                     resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.changeEmail(authKey, email:email,
                                                         resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func changeUserName(authKey:String, userName:String, firstName:String, lastName:String,
                        resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.changeUserName(authKey, userName:userName, firstName:firstName, lastName:lastName,
                                                            resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    
    // for setup
    func changeDeviceName(authKey:String, deviceName:String,
                          resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.changeDeviceName(authKey, deviceName:deviceName,
                                                              resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    
    // for folder
    func deleteFolder(authKey:String, serverFolderNo:Int64,
                      resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.deleteFolder(authKey, serverFolderNo:serverFolderNo,
                                                          resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func addOrUpdateFolder(authKey:String, saaringShareFolderInfo:SaaringShareFolderInfo,
                           resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.addOrUpdateFolder(authKey, saaringShareFolderInfo:saaringShareFolderInfo,
                                                               resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    func listFolderAll(authKey:String,
                       resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                       listSaaringShareFolderInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listFolderAll(authKey,
                                                           resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                           listSaaringShareFolderInfo:listSaaringShareFolderInfo);
        return result == true;
    }
    func checkFolderPassword(authKey:String, serverFolderNo:Int64, password:String,
                             resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString) -> Bool
    {
        let result : Bool? = saaringWrapper?.checkFolderPassword(authKey, serverFolderNo:serverFolderNo, password:password,
                                                                 resultCode:resultCode, resultMessage:resultMessage, resultData:resultData);
        return result == true;
    }
    
    // for phone address
    func addPhoneAddress(authKey:String, listPhoneNumber:NSMutableArray,
                         resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                         listSaaringPhoneAddressInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.addPhoneAddress(authKey, listPhoneNumber:listPhoneNumber,
                                                             resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                             listSaaringPhoneAddressInfo:listSaaringPhoneAddressInfo);
        return result == true;
    }
    
    func listPhoneAddress(authKey:String,
                          resultCode:NSMutableString, resultMessage:NSMutableString, resultData:NSMutableString,
                          listSaaringPhoneAddressInfo:NSMutableArray) -> Bool
    {
        let result : Bool? = saaringWrapper?.listPhoneAddress(authKey,
                                                              resultCode:resultCode, resultMessage:resultMessage, resultData:resultData,
                                                              listSaaringPhoneAddressInfo:listSaaringPhoneAddressInfo);
        return result == true;
    }
    
}
