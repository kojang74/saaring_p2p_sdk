package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class BadgeInfo implements Parcelable
{
    int                     numNewRequestFriend;
    int                     numNewNotice;

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<BadgeInfo> CREATOR = new Creator<BadgeInfo>() {
        @Override
        public BadgeInfo createFromParcel(Parcel source) {
            return new BadgeInfo(source);
        }

        @Override
        public BadgeInfo[] newArray(int size) {
            return new BadgeInfo[size];
        }
    };

    public BadgeInfo() {
    }

    protected BadgeInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(                  this.numNewRequestFriend);
        dest.writeInt(                  this.numNewNotice);
    }

    public void readFromParcel(Parcel in) {
        this.numNewRequestFriend        = in.readInt();
        this.numNewNotice               = in.readInt();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public int getNumNewRequestFriend() {
        return numNewRequestFriend;
    }
    public void setNumNewRequestFriend(int numNewRequestFriend) {
        this.numNewRequestFriend = numNewRequestFriend;
    }
    public int getNumNewNotice() {
        return numNewNotice;
    }
    public void setNumNewNotice(int numNewNotice) {
        this.numNewNotice = numNewNotice;
    }
}
