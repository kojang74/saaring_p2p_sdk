package com.mirinesoft.saaring;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

import com.mirinesoft.saaring.domain.*;

/**
 * call a Service -> Activity callback function
 */
public class SaaringServiceHandler extends Handler
{
    public static final int MSG_SR_ON_SHARE_DIR_REFRESH                                             = 1;

    public static final int MSG_SR_ON_SIGNALING_CONNECTED                                           = 10000;
    public static final int MSG_SR_ON_SIGNALING_CLOSED                                              = 10001;
    public static final int MSG_SR_ON_SIGNALING_LOGIN_FAILED                                        = 10002;

    public static final int MSG_SR_ON_SIGNALING_NOTIFY_DEVICE_LIVEYN                                = 11000;
    public static final int MSG_SR_ON_SIGNALING_NOTIFY_REQUEST_FRIEND                               = 11001;
    public static final int MSG_SR_ON_SIGNALING_NOTIFY_ADD_FRIEND                                   = 11002;

    public static final int MSG_SR_ON_P2P_PORTMAP_FINISHED                                          = 20000;
    public static final int MSG_SR_ON_P2P_LOGIN_FINISHED                                            = 20001;
    public static final int MSG_SR_ON_P2P_WAKEUP_FINISHED                                           = 20002;

    public static final int MSG_SR_ON_P2P_FINISH_DOWNLOAD                                           = 21000;
    public static final int MSG_SR_ON_P2P_FINISH_UPLOAD                                             = 21001;
    public static final int MSG_SR_ON_P2P_PROGRESS_DOWNLOAD                                         = 21002;
    public static final int MSG_SR_ON_P2P_PROGRESS_UPLOAD                                           = 21003;
    public static final int MSG_SR_ON_P2P_ABORT_DOWNLOAD                                            = 21004;
    public static final int MSG_SR_ON_P2P_ABORT_UPLOAD                                              = 21005;
    public static final int MSG_SR_ON_P2P_FINISH_DOWNLOAD_OTHER                                     = 21006;
    public static final int MSG_SR_ON_P2P_FINISH_UPLOAD_OTHER                                       = 21007;
    public static final int MSG_SR_ON_P2P_PROGRESS_DOWNLOAD_OTHER                                   = 21008;
    public static final int MSG_SR_ON_P2P_PROGRESS_UPLOAD_OTHER                                     = 21009;

    public static final int MSG_SR_ON_P2P_PROCESSED_REQUEST                                         = 22000;
    public static final int MSG_SR_ON_P2P_PROCESSED_RESPONSE                                        = 22001;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_LIST                                       = 22002;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL                                     = 22003;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED                            = 22004;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE                           = 22005;
    public static final int MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY                                 = 22006;
    public static final int MSG_SR_ON_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY             = 22007;
    public static final int MSG_SR_ON_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO                       = 22008;

    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STANDBY                             = 30000;
    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                             = 30001;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STANDBY                               = 30002;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                               = 30003;

    public static final int MSG_SR_ON_P2P_NOTIFY_CONNECTION_CLOSED                                  = 40000;
    public static final int MSG_SR_ON_P2P_NOTIFY_START_NEXT_TRANSFER                                = 40001;
    public static final int MSG_SR_ON_P2P_NOTIFY_STATUS_MESSAGE                                     = 40002;

    RemoteCallbackList<ISaaringServiceCallback> callbacks = null;

    SaaringServiceHandler(RemoteCallbackList<ISaaringServiceCallback> callbacks)
    {
        this.callbacks = callbacks;
    }

    @Override
    public void handleMessage(Message msg)
    {
        int numCallback = this.callbacks.beginBroadcast();
        Bundle data = msg.getData();

        for(int i=0 ; i<numCallback ; i++)
        {
            switch (msg.what)
            {
                case MSG_SR_ON_SHARE_DIR_REFRESH:
                    {
                        try {
                            StringData path = new StringData(data.getString("path"));
                            this.callbacks.getBroadcastItem(i).onShareDirRefresh(path);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_SIGNALING_CONNECTED:
                    {
                        try {
                            this.callbacks.getBroadcastItem(i).onSignalingConnected();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_SIGNALING_CLOSED:
                    {
                        try {
                            this.callbacks.getBroadcastItem(i).onSignalingClosed();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_SIGNALING_LOGIN_FAILED:
                    {
                        try {
                            this.callbacks.getBroadcastItem(i).onSignalingLoginFailed();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_SIGNALING_NOTIFY_DEVICE_LIVEYN:
                    {
                        try {
                            long fromUserNo = data.getLong("fromUserNo");
                            long fromDeviceNo = data.getLong("fromDeviceNo");
                            StringData liveYN = new StringData(data.getString("liveYN"));
                            this.callbacks.getBroadcastItem(i).onSignalingNotifyDeviceLiveYN(fromUserNo, fromDeviceNo, liveYN);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_SIGNALING_NOTIFY_REQUEST_FRIEND:
                    {
                        try {
                            StringData friendName = new StringData(data.getString("friendName"));
                            this.callbacks.getBroadcastItem(i).onSignalingNotifyRequestFriend(friendName);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_SIGNALING_NOTIFY_ADD_FRIEND:
                    {
                        try {
                            StringData friendName = new StringData(data.getString("friendName"));
                            long friendUserNo = data.getLong("friendUserNo");
                            this.callbacks.getBroadcastItem(i).onSignalingNotifyAddFriend(friendName, friendUserNo);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PORTMAP_FINISHED:
                    {
                        try {
                            this.callbacks.getBroadcastItem(i).onP2pPortmapFinished();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_LOGIN_FINISHED:
                    {
                        try {
                            long friendDeviceNo = data.getLong("friendDeviceNo");
                            long toRemoteShareNo = data.getLong("toRemoteShareNo");
                            this.callbacks.getBroadcastItem(i).onP2pLoginFinished(friendDeviceNo, toRemoteShareNo);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_WAKEUP_FINISHED:
                    {
                        try {
                            boolean isSuccess = data.getBoolean("isSuccess");
                            this.callbacks.getBroadcastItem(i).onP2pWakeUpFinished(isSuccess);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_FINISH_DOWNLOAD:
                    {
                        try {
                            long transNo = data.getLong("transNo");
                            long fileSize = data.getLong("fileSize");
                            long receivedSize = data.getLong("receivedSize");
                            this.callbacks.getBroadcastItem(i).onP2pFinishDownload(transNo, fileSize, receivedSize);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_FINISH_UPLOAD:
                    {
                        try {
                            long transNo = data.getLong("transNo");
                            long fileSize = data.getLong("fileSize");
                            long sentSize = data.getLong("sentSize");
                            this.callbacks.getBroadcastItem(i).onP2pFinishUpload(transNo, fileSize, sentSize);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROGRESS_DOWNLOAD:
                    {
                        try {
                            long transNo = data.getLong("transNo");
                            long fileSize = data.getLong("fileSize");
                            long receivedSize = data.getLong("receivedSize");
                            this.callbacks.getBroadcastItem(i).onP2pProgressDownload(transNo, fileSize, receivedSize);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROGRESS_UPLOAD:
                    {
                        try {
                            long transNo = data.getLong("transNo");
                            long fileSize = data.getLong("fileSize");
                            long sentSize = data.getLong("sentSize");
                            this.callbacks.getBroadcastItem(i).onP2pProgressUpload(transNo, fileSize, sentSize);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_ABORT_DOWNLOAD:
                    {
                        try {
                            long transNo = data.getLong("transNo");
                            this.callbacks.getBroadcastItem(i).onP2pAbortDownload(transNo);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_ABORT_UPLOAD:
                    {
                        try {
                            long transNo = data.getLong("transNo");
                            this.callbacks.getBroadcastItem(i).onP2pAbortUpload(transNo);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_FINISH_DOWNLOAD_OTHER:
                    {
                        try {
                            long jobId = data.getLong("jobId");
                            long remoteDeviceNo = data.getLong("remoteDeviceNo");
                            this.callbacks.getBroadcastItem(i).onP2pFinishDownloadOther(jobId, remoteDeviceNo);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_FINISH_UPLOAD_OTHER:
                    {
                        try {
                            long jobId = data.getLong("jobId");
                            long remoteDeviceNo = data.getLong("remoteDeviceNo");
                            this.callbacks.getBroadcastItem(i).onP2pFinishUploadOther(jobId, remoteDeviceNo);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROGRESS_DOWNLOAD_OTHER:
                    {
                        try {
                            long jobId = data.getLong("jobId");
                            long remoteDeviceNo = data.getLong("remoteDeviceNo");
                            long fileSize = data.getLong("fileSize");
                            long currSize = data.getLong("currSize");
                            StringData filePath = new StringData(data.getString("filePath"));
                            StringData serverName = new StringData(data.getString("serverName"));
                            StringData deviceName = new StringData(data.getString("deviceName"));
                            this.callbacks.getBroadcastItem(i).onP2pProgressDownloadOther(jobId, remoteDeviceNo, fileSize, currSize,
                                                                                          filePath, serverName, deviceName);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROGRESS_UPLOAD_OTHER:
                    {
                        try {
                            long jobId = data.getLong("jobId");
                            long remoteDeviceNo = data.getLong("remoteDeviceNo");
                            long fileSize = data.getLong("fileSize");
                            long currSize = data.getLong("currSize");
                            StringData filePath = new StringData(data.getString("filePath"));
                            StringData serverName = new StringData(data.getString("serverName"));
                            StringData deviceName = new StringData(data.getString("deviceName"));
                            this.callbacks.getBroadcastItem(i).onP2pProgressUploadOther(jobId, remoteDeviceNo, fileSize, currSize,
                                                                                        filePath, serverName, deviceName);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESSED_REQUEST:
                    {
                        try {
                            int processType = data.getInt("processType");
                            long jobId = data.getLong("jobId");
                            long remoteDeviceNo = data.getLong("remoteDeviceNo");
                            int resultCode = data.getInt("resultCode");
                            this.callbacks.getBroadcastItem(i).onP2pProcessedRequest(processType, jobId, remoteDeviceNo, resultCode);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESSED_RESPONSE:
                    {
                        try {
                            int processType = data.getInt("processType");
                            long jobId = data.getLong("jobId");
                            long remoteDeviceNo = data.getLong("remoteDeviceNo");
                            int resultCode = data.getInt("resultCode");
                            this.callbacks.getBroadcastItem(i).onP2pProcessedResponse(processType, jobId, remoteDeviceNo, resultCode);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESS_FOLDER_LIST:
                    {
                        try {
                            StringData strJson = new StringData(data.getString("strJson"));
                            this.callbacks.getBroadcastItem(i).onP2pProcessResponseFolderList(strJson);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL:
                    {
                        try {
                            StringData strJson = new StringData(data.getString("strJson"));
                            this.callbacks.getBroadcastItem(i).onP2pProcessResponseFolderDetail(strJson);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED:
                    {
                        try {
                            this.callbacks.getBroadcastItem(i).onP2pProcessResponseFolderDetailFinished();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE:
                    {
                        try {
                            StringData strJson = new StringData(data.getString("strJson"));
                            this.callbacks.getBroadcastItem(i).onP2pProcessResponseFolderDetailRecursive(strJson);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY:
                    {
                        try {
                            long transNo = data.getLong("transNo");
                            long fileStartPosition = data.getLong("fileStartPosition");
                            this.callbacks.getBroadcastItem(i).onP2pProcessResponseFileUploadQuery(transNo, fileStartPosition);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY:
                    {
                        try {
                            StringData strJson = new StringData(data.getString("strJson"));
                            this.callbacks.getBroadcastItem(i).onP2pProcessResponseFileDownloadInviteSendFilesQuery(strJson);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO:
                    {
                        try {
                            StringData strJson = new StringData(data.getString("strJson"));
                            this.callbacks.getBroadcastItem(i).onP2pProcessResponseSaveSendFilesInviteInfo(strJson);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STANDBY:
                {
                    try {
                        long remoteDeviceNo = data.getLong("remoteDeviceNo");
                        int downloadingCount = data.getInt("downloadingCount");
                        int downloadStandByCount = data.getInt("downloadStandByCount");
                        int myTurn = data.getInt("myTurn");
                        this.callbacks.getBroadcastItem(i).onP2pNotifyDownloadQueueStandBy(remoteDeviceNo, downloadingCount, downloadStandByCount, myTurn);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE:
                {
                    try {
                        long remoteDeviceNo = data.getLong("remoteDeviceNo");
                        int downloadingCount = data.getInt("downloadingCount");
                        int downloadStandByCount = data.getInt("downloadStandByCount");
                        int myTurn = data.getInt("myTurn");
                        this.callbacks.getBroadcastItem(i).onP2pNotifyDownloadQueueRelease(remoteDeviceNo, downloadingCount, downloadStandByCount, myTurn);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STANDBY:
                {
                    try {
                        long remoteDeviceNo = data.getLong("remoteDeviceNo");
                        int uploadingCount = data.getInt("uploadingCount");
                        int uploadStandByCount = data.getInt("uploadStandByCount");
                        int myTurn = data.getInt("myTurn");
                        this.callbacks.getBroadcastItem(i).onP2pNotifyUploadQueueStandBy(remoteDeviceNo, uploadingCount, uploadStandByCount, myTurn);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE:
                {
                    try {
                        long remoteDeviceNo = data.getLong("remoteDeviceNo");
                        int uploadingCount = data.getInt("uploadingCount");
                        int uploadStandByCount = data.getInt("uploadStandByCount");
                        int myTurn = data.getInt("myTurn");
                        this.callbacks.getBroadcastItem(i).onP2pNotifyUploadQueueRelease(remoteDeviceNo, uploadingCount, uploadStandByCount, myTurn);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;

                case MSG_SR_ON_P2P_NOTIFY_CONNECTION_CLOSED:
                    {
                        try {
                            long remoteDeviceNo = data.getLong("remoteDeviceNo");
                            this.callbacks.getBroadcastItem(i).onP2pNotifyConnectionClosed(remoteDeviceNo);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case MSG_SR_ON_P2P_NOTIFY_START_NEXT_TRANSFER:
	                {
	                    try {
	                        long remoteDeviceNo = data.getLong("remoteDeviceNo");
	                        this.callbacks.getBroadcastItem(i).onP2pNotifyStartNextTransfer(remoteDeviceNo);
	                    } catch (RemoteException e) {
	                        e.printStackTrace();
	                    }
	                }
	                break;
                case MSG_SR_ON_P2P_NOTIFY_STATUS_MESSAGE:
                    {
                        try {
                            StringData text = new StringData(data.getString("text"));
                            this.callbacks.getBroadcastItem(i).onP2pNotifyStatusMessage(text);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        Log.d("SaaringService", "Handler work.. :: callbacks clients count is " + numCallback);
        this.callbacks.finishBroadcast();
    }
}
