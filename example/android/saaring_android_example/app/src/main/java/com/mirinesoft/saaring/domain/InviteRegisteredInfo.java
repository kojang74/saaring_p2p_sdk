package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class InviteRegisteredInfo implements Parcelable
{
    long                    userNo;
    long                    inviteNo;
    String                  inviteCode              = "";

    int                     inviteType;
    String                  certKey                 = "";

    long                    fromUserNo;
    String                  fromUserName            = "";

    String                  inviteMessage           = "";
    String                  dynamicLink             = "";

    String                  expiredt                = "";
    String                  regdt                   = "";


    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<InviteRegisteredInfo> CREATOR = new Creator<InviteRegisteredInfo>() {
        @Override
        public InviteRegisteredInfo createFromParcel(Parcel source) {
            return new InviteRegisteredInfo(source);
        }

        @Override
        public InviteRegisteredInfo[] newArray(int size) {
            return new InviteRegisteredInfo[size];
        }
    };

    public InviteRegisteredInfo() {
    }

    protected InviteRegisteredInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(         this.userNo);
        dest.writeLong(         this.inviteNo);
        dest.writeString(       this.inviteCode);

        dest.writeInt(          this.inviteType);
        dest.writeString(       this.certKey);

        dest.writeLong(         this.fromUserNo);
        dest.writeString(       this.fromUserName);

        dest.writeString(       this.inviteMessage);
        dest.writeString(       this.dynamicLink);

        dest.writeString(       this.expiredt);
        dest.writeString(       this.regdt);
    }

    public void readFromParcel(Parcel in) {
        this.userNo                 = in.readLong();
        this.inviteNo               = in.readLong();
        this.inviteCode             = in.readString();

        this.inviteType             = in.readInt();
        this.certKey                = in.readString();

        this.fromUserNo             = in.readLong();
        this.fromUserName           = in.readString();

        this.inviteMessage          = in.readString();
        this.dynamicLink            = in.readString();

        this.expiredt               = in.readString();
        this.regdt                  = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getUserNo() {
        return userNo;
    }
    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }
    public long getInviteNo() {
        return inviteNo;
    }
    public void setInviteNo(long inviteNo) {
        this.inviteNo = inviteNo;
    }
    public String getInviteCode() {
        return inviteCode;
    }
    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }
    public int getInviteType() {
        return inviteType;
    }
    public void setInviteType(int inviteType) {
        this.inviteType = inviteType;
    }
    public String getCertKey() {
        return certKey;
    }
    public void setCertKey(String certKey) {
        this.certKey = certKey;
    }
    public long getFromUserNo() {
        return fromUserNo;
    }
    public void setFromUserNo(long fromUserNo) {
        this.fromUserNo = fromUserNo;
    }
    public String getFromUserName() {
        return fromUserName;
    }
    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }
    public String getInviteMessage() {
        return inviteMessage;
    }
    public void setInviteMessage(String inviteMessage) {
        this.inviteMessage = inviteMessage;
    }
    public String getDynamicLink() {
        return dynamicLink;
    }
    public void setDynamicLink(String dynamicLink) {
        this.dynamicLink = dynamicLink;
    }
    public String getExpiredt() {
        return expiredt;
    }
    public void setExpiredt(String expiredt) {
        this.expiredt = expiredt;
    }
    public String getRegdt() {
        return regdt;
    }
    public void setRegdt(String regdt) {
        this.regdt = regdt;
    }
}
