package com.mirinesoft.saaring;

import android.os.Bundle;
import android.os.Message;

import java.util.List;

import com.mirinesoft.saaring.domain.*;

public class SaaringNative {

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for Callback
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static void onShareDirRefresh(String path)
    {
        System.out.println("SaaringNative.onShareDirRefresh #######################################################");
        System.out.println("path="+path+" #######################################################");

        Bundle data = new Bundle();
        data.putString("path", path);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_SHARE_DIR_REFRESH);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onSignalingConnected()
    {
        System.out.println("SaaringNative.onSignalingConnected #######################################################");

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_SIGNALING_CONNECTED);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onSignalingClosed()
    {
        System.out.println("SaaringNative.onSignalingClosed #######################################################");

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_SIGNALING_CLOSED);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onSignalingLoginFailed()
    {
        System.out.println("SaaringNative.onSignalingLoginFailed #######################################################");

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_SIGNALING_LOGIN_FAILED);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onSignalingNotifyDeviceLiveYN(long fromUserNo, long fromDeviceNo, String liveYN)
    {
        System.out.println("SaaringNative.onSignalingNotifyDeviceLiveYN #######################################################");
        System.out.println("fromUserNo="+fromUserNo+" #######################################################");
        System.out.println("fromDeviceNo="+fromDeviceNo+" #######################################################");
        System.out.println("liveYN="+liveYN+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("fromUserNo", fromUserNo);
        data.putLong("fromDeviceNo", fromDeviceNo);
        data.putString("liveYN", liveYN);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_SIGNALING_NOTIFY_DEVICE_LIVEYN);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onSignalingNotifyRequestFriend(String friendName)
    {
        System.out.println("SaaringNative.onSignalingNotifyRequestFriend #######################################################");
        System.out.println("friendName="+friendName+" #######################################################");

        Bundle data = new Bundle();
        data.putString("friendName", friendName);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_SIGNALING_NOTIFY_REQUEST_FRIEND);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onSignalingNotifyAddFriend(String friendName, long friendUserNo)
    {
        System.out.println("SaaringNative.onSignalingNotifyAddFriend #######################################################");
        System.out.println("friendName="+friendName+" #######################################################");
        System.out.println("friendUserNo="+friendUserNo+" #######################################################");

        Bundle data = new Bundle();
        data.putString("friendName", friendName);
        data.putLong("friendUserNo", friendUserNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_SIGNALING_NOTIFY_ADD_FRIEND);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pPortmapFinished()
    {
        System.out.println("SaaringNative.onP2pPortmapFinished #######################################################");

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PORTMAP_FINISHED);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pLoginFinished(long friendDeviceNo, long toRemoteShareNo)
    {
        System.out.println("SaaringNative.onP2pLoginFinished #######################################################");
        System.out.println("friendDeviceNo="+friendDeviceNo+" #######################################################");
        System.out.println("toRemoteShareNo="+toRemoteShareNo+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("friendDeviceNo", friendDeviceNo);
        data.putLong("toRemoteShareNo", toRemoteShareNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_LOGIN_FINISHED);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pWakeUpFinished(boolean isSuccess)
    {
        System.out.println("SaaringNative.onP2pWakeUpFinished #######################################################");
        System.out.println("isSuccess="+isSuccess+" #######################################################");

        Bundle data = new Bundle();
        data.putBoolean("isSuccess", isSuccess);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_WAKEUP_FINISHED);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pFinishDownload(long transNo, long fileSize, long receivedSize)
    {
        System.out.println("SaaringNative.onP2pFinishDownload #######################################################");
        System.out.println("transNo="+transNo+" #######################################################");
        System.out.println("fileSize="+fileSize+" #######################################################");
        System.out.println("receivedSize="+receivedSize+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("transNo", transNo);
        data.putLong("fileSize", fileSize);
        data.putLong("receivedSize", receivedSize);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_FINISH_DOWNLOAD);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pFinishUpload(long transNo, long fileSize, long sentSize)
    {
        System.out.println("SaaringNative.onP2pFinishUpload #######################################################");
        System.out.println("transNo="+transNo+" #######################################################");
        System.out.println("fileSize="+fileSize+" #######################################################");
        System.out.println("sentSize="+sentSize+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("transNo", transNo);
        data.putLong("fileSize", fileSize);
        data.putLong("sentSize", sentSize);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_FINISH_UPLOAD);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProgressDownload(long transNo, long fileSize, long receivedSize)
    {
        // System.out.println("SaaringNative.onP2pProgressDownload #######################################################");
        // System.out.println("transNo="+transNo+" #######################################################");
        // System.out.println("fileSize="+fileSize+" #######################################################");
        // System.out.println("receivedSize="+receivedSize+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("transNo", transNo);
        data.putLong("fileSize", fileSize);
        data.putLong("receivedSize", receivedSize);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROGRESS_DOWNLOAD);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProgressUpload(long transNo, long fileSize, long sentSize)
    {
        // System.out.println("SaaringNative.onP2pProgressUpload #######################################################");
        // System.out.println("transNo="+transNo+" #######################################################");
        // System.out.println("fileSize="+fileSize+" #######################################################");
        // System.out.println("sentSize="+sentSize+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("transNo", transNo);
        data.putLong("fileSize", fileSize);
        data.putLong("sentSize", sentSize);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROGRESS_UPLOAD);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pAbortDownload(long transNo)
    {
        System.out.println("SaaringNative.onP2pAbortDownload #######################################################");
        System.out.println("transNo="+transNo+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("transNo", transNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_ABORT_DOWNLOAD);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pAbortUpload(long transNo)
    {
        System.out.println("SaaringNative.onP2pAbortUpload #######################################################");
        System.out.println("transNo="+transNo+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("transNo", transNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_ABORT_UPLOAD);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pFinishDownloadOther(long jobId, long remoteDeviceNo)
    {
        System.out.println("SaaringNative.onP2pFinishDownloadOther #######################################################");
        System.out.println("jobId="+jobId+" #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("jobId", jobId);
        data.putLong("remoteDeviceNo", remoteDeviceNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_FINISH_DOWNLOAD_OTHER);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pFinishUploadOther(long jobId, long remoteDeviceNo)
    {
        System.out.println("SaaringNative.onP2pFinishUploadOther #######################################################");
        System.out.println("jobId="+jobId+" #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("jobId", jobId);
        data.putLong("remoteDeviceNo", remoteDeviceNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_FINISH_UPLOAD_OTHER);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProgressDownloadOther(long jobId, long remoteDeviceNo, long fileSize, long currSize, String filePath, String serverName, String deviceName)
    {
        System.out.println("SaaringNative.onP2pProgressDownloadOther #######################################################");
        System.out.println("jobId="+jobId+" #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("fileSize="+fileSize+" #######################################################");
        System.out.println("currSize="+currSize+" #######################################################");
        System.out.println("filePath="+filePath+" #######################################################");
        System.out.println("serverName="+serverName+" #######################################################");
        System.out.println("deviceName="+deviceName+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("jobId", jobId);
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putLong("fileSize", fileSize);
        data.putLong("currSize", currSize);
        data.putString("filePath", filePath);
        data.putString("serverName", serverName);
        data.putString("deviceName", deviceName);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROGRESS_DOWNLOAD_OTHER);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProgressUploadOther(long jobId, long remoteDeviceNo, long fileSize, long currSize, String filePath, String serverName, String deviceName)
    {
        System.out.println("SaaringNative.onP2pProgressUploadOther #######################################################");
        System.out.println("jobId="+jobId+" #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("fileSize="+fileSize+" #######################################################");
        System.out.println("currSize="+currSize+" #######################################################");
        System.out.println("filePath="+filePath+" #######################################################");
        System.out.println("serverName="+serverName+" #######################################################");
        System.out.println("deviceName="+deviceName+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("jobId", jobId);
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putLong("fileSize", fileSize);
        data.putLong("currSize", currSize);
        data.putString("filePath", filePath);
        data.putString("serverName", serverName);
        data.putString("deviceName", deviceName);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROGRESS_UPLOAD_OTHER);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessedRequest(int processType, long jobId, long remoteDeviceNo, int resultCode)
    {
        System.out.println("SaaringNative.onP2pProcessedRequest #######################################################");
        System.out.println("processType="+processType+" #######################################################");
        System.out.println("jobId="+jobId+" #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("resultCode="+resultCode+" #######################################################");

        Bundle data = new Bundle();
        data.putInt("processType", processType);
        data.putLong("jobId", jobId);
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putInt("resultCode", resultCode);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESSED_REQUEST);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessedResponse(int processType, long jobId, long remoteDeviceNo, int resultCode)
    {
        System.out.println("SaaringNative.onP2pResponseReceived #######################################################");
        System.out.println("processType="+processType+" #######################################################");
        System.out.println("jobId="+jobId+" #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("resultCode="+resultCode+" #######################################################");

        Bundle data = new Bundle();
        data.putInt("processType", processType);
        data.putLong("jobId", jobId);
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putInt("resultCode", resultCode);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESSED_RESPONSE);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessResponseFolderList(String strJson)
    {
        System.out.println("SaaringNative.onP2pProcessResponseFolderList #######################################################");
        System.out.println("strJson="+strJson+" #######################################################");

        Bundle data = new Bundle();
        data.putString("strJson", strJson);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESS_FOLDER_LIST);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessResponseFolderDetail(String strJson)
    {
        System.out.println("SaaringNative.onP2pProcessResponseFolderDetail #######################################################");
        System.out.println("strJson="+strJson+" #######################################################");

        Bundle data = new Bundle();
        data.putString("strJson", strJson);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessResponseFolderDetailFinished()
    {
        System.out.println("SaaringNative.onP2pProcessResponseFolderDetailFinished #######################################################");

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessResponseFolderRecursive(String strJson)
    {
        System.out.println("SaaringNative.onP2pProcessResponseFolderRecursive #######################################################");
        System.out.println("strJson="+strJson+" #######################################################");

        Bundle data = new Bundle();
        data.putString("strJson", strJson);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessResponseFileUploadQuery(long transNo, long fileStartPosition)
    {
        System.out.println("SaaringNative.onP2pProcessResponseFileUploadQuery #######################################################");
        System.out.println("transNo="+transNo+" #######################################################");
        System.out.println("fileStartPosition="+fileStartPosition+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("transNo", transNo);
        data.putLong("fileStartPosition", fileStartPosition);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessResponseFileDownloadInviteSendFilesQuery(String strJson)
    {
        System.out.println("SaaringNative.onP2pProcessResponseFileDownloadInviteSendFilesQuery #######################################################");
        System.out.println("strJson="+strJson+" #######################################################");

        Bundle data = new Bundle();
        data.putString("strJson", strJson);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pProcessResponseSaveSendFilesInviteInfo(String strJson)
    {
        System.out.println("SaaringNative.onP2pProcessResponseSaveSendFilesInviteInfo #######################################################");
        System.out.println("strJson="+strJson+" #######################################################");

        Bundle data = new Bundle();
        data.putString("strJson", strJson);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }


    public static void onP2pNotifyDownloadQueueStandBy(long remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
    {
        System.out.println("SaaringNative.onP2pNotifyDownloadQueueStandBy #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("downloadingCount="+downloadingCount+" #######################################################");
        System.out.println("downloadStandByCount="+downloadStandByCount+" #######################################################");
        System.out.println("myTurn="+myTurn+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putInt("downloadingCount", downloadingCount);
        data.putInt("downloadStandByCount", downloadStandByCount);
        data.putInt("myTurn", myTurn);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STANDBY);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }
    public static void onP2pNotifyDownloadQueueRelease(long remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
    {
        System.out.println("SaaringNative.onP2pNotifyDownloadQueueRelease #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("downloadingCount="+downloadingCount+" #######################################################");
        System.out.println("downloadStandByCount="+downloadStandByCount+" #######################################################");
        System.out.println("myTurn="+myTurn+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putInt("downloadingCount", downloadingCount);
        data.putInt("downloadStandByCount", downloadStandByCount);
        data.putInt("myTurn", myTurn);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }
    public static void onP2pNotifyUploadQueueStandBy(long remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
    {
        System.out.println("SaaringNative.onP2pNotifyUploadQueueStandBy #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("uploadingCount="+uploadingCount+" #######################################################");
        System.out.println("uploadStandByCount="+uploadStandByCount+" #######################################################");
        System.out.println("myTurn="+myTurn+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putInt("uploadingCount", uploadingCount);
        data.putInt("uploadStandByCount", uploadStandByCount);
        data.putInt("myTurn", myTurn);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STANDBY);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }
    public static void onP2pNotifyUploadQueueRelease(long remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
    {
        System.out.println("SaaringNative.onP2pNotifyUploadQueueRelease #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");
        System.out.println("uploadingCount="+uploadingCount+" #######################################################");
        System.out.println("uploadStandByCount="+uploadStandByCount+" #######################################################");
        System.out.println("myTurn="+myTurn+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("remoteDeviceNo", remoteDeviceNo);
        data.putInt("uploadingCount", uploadingCount);
        data.putInt("uploadStandByCount", uploadStandByCount);
        data.putInt("myTurn", myTurn);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }


    public static void onP2pNotifyConnectionClosed(long remoteDeviceNo)
    {
        System.out.println("SaaringNative.onP2pNotifyConnectionClosed #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("remoteDeviceNo", remoteDeviceNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_NOTIFY_CONNECTION_CLOSED);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pNotifyStartNextTransfer(long remoteDeviceNo)
    {
        System.out.println("SaaringNative.onP2pNotifyStartNextTransfer #######################################################");
        System.out.println("remoteDeviceNo="+remoteDeviceNo+" #######################################################");

        Bundle data = new Bundle();
        data.putLong("remoteDeviceNo", remoteDeviceNo);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_NOTIFY_START_NEXT_TRANSFER);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    public static void onP2pNotifyStatusMessage(String text)
    {
        System.out.println("SaaringNative.onP2pNotifyStatusMessage #######################################################");
        System.out.println("text="+text+" #######################################################");

        Bundle data = new Bundle();
        data.putString("text", text);

        Message msg = Message.obtain(SaaringService.saaringServiceHandler, SaaringServiceHandler.MSG_SR_ON_P2P_NOTIFY_STATUS_MESSAGE);
        msg.setData(data);

        SaaringService.saaringServiceHandler.sendMessage(msg);
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for saaring common jni interface
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static native void test();

    public static native double calSpeed(long oldTime, long currTime, long oldSize, long currSize);
    public static native String leftTime(long oldTime, long currTime, long oldSize, long currSize, long totalSize, double speed);

    public static native String getSizeWithUnit(long size);

    public static native boolean checkEmail(String email);
    public static native String getDuplicationFilename(String filePath, StringBuffer lastLocalFilePath);
    public static native String numberFormatWithComma(long number, String separator);

    public static native int checkUpgrade(String lastVer, String runningVer, String currentVer);

    public static native String changeProfileImage(String authKey, String path);

    public static native String getDownloadFolder();
    public static native String getShareTopFolder();

    public static native String removeFirstPath(String path);

    public static native String getProviderId(int idType);
    public static native String getProviderName(int idType);
    public static native String getProviderNameByProviderId(String providerId);

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for saaring db jni interface
    //
    //////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////
    // for All
    //////////////////////////////////////////////////////////////////////////////////
    public static native boolean isOpen();

    //////////////////////////////////////////////////////////////////////////////////
    // for login
    //////////////////////////////////////////////////////////////////////////////////
    public static native void doAfterLogin(int idType, String email, String phoneNumber, String password,
                                           String providerId, String idToken, String accessToken, String uid,
                                           String isSaveEmail, String isAutoLogin);

    public static native boolean updateLoginTb(int idType, String email, String phoneNumber, String password, String isSaveEmail, String isAutoLogin, String isLastLoginEmail);
    public static native boolean updateLoginAutoStart(int idType, String email, String phoneNumber, String isAutoStart);

    public static native boolean updateSetSend(SetupInfo setupInfo);
    public static native boolean updateSetNetwork(SetupInfo setupInfo);

    public static native boolean readDbLastLogin(SetupInfo setupInfo);
    public static native boolean readDbByEmailOrPhoneNumber(int idType, String email, String phoneNumber, SetupInfo setupInfo);

    //////////////////////////////////////////////////////////////////////////////////
    // for transfer
    //////////////////////////////////////////////////////////////////////////////////
    public static native long insertMyTransTbOne(TransferBaseInfo transferBaseInfo);
    public static native boolean insertMyTransTbMulti(List<TransferBaseInfo> listTransferBaseInfo);

    public static native boolean updateMyTransTb(TransferBaseInfo transferBaseInfo);
    public static native boolean updateMyTransTbStatus(long transNo, int transStatus, long updateTime);

    public static native boolean updateMyTransTbSortNoMulti(List<Long> listTransNo, long updateTime);

    public static native boolean updateMyTransTbProgress(long transNo, long fileSize, long transSize, long updateTime);
    public static native boolean updateMyTransTbInit(long myDeviceNo);

    public static native boolean deleteMyTransTbOne(long transNo);
    public static native boolean deleteMyTransTbMulti(List<Long> listTransNo);
    public static native boolean deleteMyTransTbAll(long deviceNo);

    public static native boolean readMyTransTbByTransNo(List<TransferBaseInfo> listTransferBaseInfo, long transNo);
    public static native boolean readMyTransTbAll(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo);
    public static native boolean readMyTransTbTransferring(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo);
    public static native boolean readMyTransTbNotTransferringLastOne(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo);
    public static native boolean readMyTransTbByTransStatusLastOne(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo, int transStatus);
    public static native boolean readMyTransTbInit(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo);

    //////////////////////////////////////////////////////////////////////////////////
    // for share
    //////////////////////////////////////////////////////////////////////////////////
    public static native boolean insertShareTb(ShareInfo shareInfo);
    public static native boolean updateShareTb(ShareInfo shareInfo);
    public static native boolean deleteShareTb(long shareNo);
    public static native boolean readShareTb(long deviceNo, List<ShareInfo> listShareInfo);

    public static native long getShareNoByServerFolderNo(long serverFolderNo);
    public static native long getServerFolderNoByShareNo(long shareNo);
    public static native String getShareFolderPathByServerFolderNo(long serverFolderNo);

    public static native int getShareFileCntWithShareNo(long deviceNo, long shareNo);
    public static native long getShareFileSize(long deviceNo);
    public static native long getShareFileSizeWithShareNo(long deviceNo, long shareNo);

    //////////////////////////////////////////////////////////////////////////////////
    // for share detail
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean updateShareDetailShortPath(long shareNo, String shareName, String shareFolderFullPath);
    public static native boolean deleteShareDetailTb(long deviceNo);

    public static native void makeMemoryShareDetailTb();
    public static native void backupMemoryShareDetailTb();

    //////////////////////////////////////////////////////////////////////////////////
    // for invite files
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean insertInviteFiles(InviteFilesInfo saaringInviteFilesInfo);
    public static native boolean insertListInviteFiles(List<InviteFilesInfo> listSaaringInviteFilesInfo);

    public static native void deleteExpired();
    public static native void deleteInviteFilesByInviteNo(long inviteNo);

    public static native boolean listInviteFiles(long inviteNo, List<InviteFilesInfo> listSaaringInviteFilesInfo);
    public static native boolean listInviteFilesWithCertKey(long inviteNo, String certKey, List<InviteFilesInfo> listSaaringInviteFilesInfo);

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for saaring p2p jni interface
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static native void initP2p();
    public static native void closeP2p();
    public static native boolean isP2p();

    public static native boolean isSignalingConnected();
    public static native boolean isSignalingLogin();
    public static native boolean doSignalingConnect(String authKey, boolean isRefreshServerInfo);
    public static native void doSignalingClose();
    public static native void reqHeartBeat();

    public static native ServerInfo getServerInfo();
    public static native boolean isP2pLived(long deviceNo);

    public static native void updateShareFolders();
    public static native void updateNetworkSendSettings();

    public static native void doP2pConnect(long toUserNo, String toUserName,
                                           long toDeviceNo, int toDeviceType, String toDeviceName,
                                           boolean mustWakeUp);
    public static native void doP2pConnectShareFolder(long toUserNo, String toUserName,
                                                      long toDeviceNo, int toDeviceType, String toDeviceName,
                                                      long toServerFolderNo, String toFolderName, boolean mustWakeUp);
    public static native void doP2pConnectCancel(long toUserNo, String toUserName,
                                                 long toDeviceNo, int toDeviceType, String toDeviceName);

    public static native long doP2pFolderList(long toDeviceNo);
    public static native long doP2pFolderDetail(long toDeviceNo, long shareNo, long serverFolderNo, String remotePath);
    public static native long doP2pFolderDetailRecursive(long toDeviceNo, long shareNo, String remotePath);

    public static native long doP2pUploadQuery(long toDeviceNo, String remoteFilePath, long fileSize, long myTransNo);
    public static native long doP2pUpload(long toDeviceNo, String remoteFilePath, String localFilePath,
                                          long fileStartPosition, long myTransNo);
    public static native long doP2pDownload(long toDeviceNo, String remoteFilePath, String localFilePath,
                                            long fileSize, long fileStartPosition, long myTransNo, int folderDepth,
                                            long inviteNo, long remoteFileId);
    public static native long doP2pDownloadInviteSendFilesQuery(long toDeviceNo, long inviteNo, String certKey);
    public static native long doP2pSaveSendFilesInviteInfo(long toDeviceNo,
                                                           long inviteNo, String inviteCode, String dynamicLink,
                                                           List<String> listShortPathName, List<String> listFileName, List<Long> listFileSize,
                                                           String certKey, String inviteMessage);

    public static native long doP2pProcessDownloadComplete(long toDeviceNo);
    public static native long doP2pProcessUploadComplete(long toDeviceNo);

    public static native long doP2pProcessDownloadQueueCheck(long toDeviceNo);
    public static native long doP2pProcessUploadQueueCheck(long toDeviceNo);

    public static native void doP2pOpenAllChannel(List<Long> listDeviceNo);
    public static native void doP2pCloseChannel(long remoteDeviceNo);
    public static native long doP2pStopAllTransfer(List<Long> listDeviceNo);
    public static native long doP2pCancelAllTransfer(List<Long> listDeviceNo);

    public static native void listAllChannelDeviceNo(List<Long> listDeviceNo);
    public static native void updateNumLiveTransfer(int numLiveTransfer);

    public static native void startRelayServer();
    public static native void stopRelayServer();

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for saaring share folder jni interface
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static native void startThreadShareFolder();
    public static native void stopThreadShareFolder();

    public static native void shareFolderInit();
    public static native void shareFolderInsert(long shareNo, String sharePath, String shareName);
    public static native void shareFolderDelete(long shareNo);
    public static native void shareFolderUpdate(String folderPath);
    public static native void serverFolderDelete(long serverFolderNo);

    public static native int getShareFolderCount();
    public static native int getShareFileCount();

    public static native void startFileMonitor();
    public static native void stopFileMonitor();

    public static native void doesNeedDbBackup();

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for saaring web jni interface
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static native boolean listFolderOfOther(String authKey, long friendUserNo,
                                                   StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                   List<ShareFolderInfo> listShareFolderInfo);
    public static native boolean listLiveDevice(String authKey, String deviceNos,
                                                StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                List<Long> listLiveDeviceNo);
    public static native boolean getBadgeInfo(String authKey,
                                              StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                              BadgeInfo saaringBadgeInfo);

    public static native boolean listFriend(String authKey,
                                            StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                            List<FriendInfo> listFriendInfo);
    public static native boolean requestFriendByUserNo(String authKey, long toUserNo,
                                                       StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean listRequestingFriend(String authKey,
                                                      StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                      List<FriendInfo> listIncomingRequestFriend,
                                                      List<FriendInfo> listRequestingFriendMember,
                                                      List<FriendInfo> listRequestingFriendNonMember);
    public static native boolean cancelRequestFriend(String authKey, long toUserNo, String toEmail, String toPhoneNumber,
                                                      StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean rejectRequestFriend(String authKey, long toUserNo,
                                                     StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean recoverRequestFriend(String authKey, long toUserNo,
                                                     StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean addFriend(String authKey, long friendUserNo,
                                           StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean blockFriend(String authKey, String friendUserNos, String blockYN,
                                             StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean listBlock(String authKey,
                                           StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                           List<BlockInfo> listBlocked, List<BlockInfo> listRejected);

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean inviteFriendSimple(String authKey,
                                                    String certKey, String inviteMessage,
                                                    StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                    StringBuffer inviteNo, StringBuffer inviteCode, StringBuffer dynamicLink);
    public static native boolean inviteFriend(String authKey,
                                              List<Long> listToUserNo, List<String> listToUserName,
                                              List<String> listToEmail, List<String> listToPhoneNumber,
                                              String certKey, String inviteMessage,
                                              StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                              StringBuffer inviteNo, StringBuffer inviteCode, StringBuffer dynamicLink,
                                              List<Long> listFailUserNo,
                                              List<String> listSuccessEmail,
                                              List<FriendInfo> listAddedFriend,
                                              List<FriendInfo> listIncomingRequestFriend,
                                              List<FriendInfo> listRequestingFriendMember,
                                              List<FriendInfo> listRequestingFriendNonMember);
    public static native boolean cancelInviteFriend(String authKey, long inviteNo,
                                                    StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean completeInviteFriend(String authKey, long inviteNo,
                                                      StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean extendInviteExpiredt(String authKey, long inviteNo,
                                                      StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                      InviteInfo inviteInfo);

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean getInvite(String authKey, long inviteNo,
                                           StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                           InviteInfo inviteInfo);
    public static native boolean getInviteByDynamicLink(String dynamicLink, String certKey,
                                                        StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                        InviteInfo inviteInfo);

    public static native boolean listInviteAll(String authKey,
                                               StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                               List<InviteInfo> listInviteInfo);

    public static native boolean inviteShareFolderSimple(String authKey,
                                                         long inviteShareFolderServerFolderNo,
                                                         String certKey, String inviteMessage,
                                                         StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                         StringBuffer inviteNo, StringBuffer inviteCode, StringBuffer dynamicLink);
    public static native boolean inviteSendFilesSimple(String authKey,
                                                       long numFile, long totalFileSize,
                                                       String certKey, String inviteMessage,
                                                       StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                       StringBuffer inviteNo, StringBuffer inviteCode, StringBuffer dynamicLink);
    public static native boolean inviteSendFilesForRemoteDeviceSimple(String authKey,
                                                       long numFile, long totalFileSize,
                                                       long inviteShareFolderServerFolderNo, String inviteShareFolderPath,
                                                       String certKey, String inviteMessage,
                                                       StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                       StringBuffer inviteNo, StringBuffer inviteCode, StringBuffer dynamicLink);

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean registerInviteByDynamicLink(String authKey,
                                                             String dynamicLink, String certKey,
                                                             StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                             InviteRegisteredInfo inviteRegisteredInfo,
                                                             InviteInfo inviteInfo);
    public static native boolean deleteInviteRegistered(String authKey, long inviteNo,
                                                        StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean getInviteRegistered(String authKey, long inviteNo,
                                                     StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                     InviteRegisteredInfo inviteRegisteredInfo,
                                                     InviteInfo inviteInfo);
    public static native boolean listInviteRegisteredAll(String authKey,
                                                         StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                         List<InviteRegisteredInfo> listInviteRegisteredInfo);

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean verifyFirebaseToken(String firebaseToken,
                                            StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean joinMember(MemberInfo memberInfo,
                                          StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean loginMember(MemberInfo memberInfo,
                                            StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean doMe(String authKey, int deviceType,
                                      StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                      MemberInfo memberInfo, List<MemberProviderInfo> listMemberProviderInfo);

    // isEmailExist = "true" or "false"
    public static native boolean isEmailExist(String email,
                                              StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                              StringBuffer isEmailExist);
    public static native boolean doUnlink(String authKey,
                                          StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean doLogout(String authKey,
                                          StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);

    public static native boolean searchUser(String authKey, String keyword,
                                            StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                            List<FriendInfo> listFriendInfo);
    public static native boolean changeProfilePic(String authKey, String profilePic,
                                                  StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean changeEmail(String authKey, String email,
                                             StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean changeUserName(String authKey, String userName,
                                                StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);

    public static native boolean changeDeviceName(String authKey, String deviceName,
                                                  StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);

    public static native boolean deleteFolder(String authKey, long serverFolderNo,
                                              StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean addOrUpdateFolder(String authKey, ShareFolderInfo shareFolderInfo,
                                                   StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);
    public static native boolean listFolderAll(String authKey,
                                               StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                               List<ShareFolderInfo> listFolder);
    public static native boolean checkFolderPassword(String authKey, long serverFolderNo, String password,
                                                     StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData);

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public static native boolean addPhoneAddress(String authKey, List<String> listPhoneNumber,
                                                 StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                 List<PhoneAddressInfo> listPhoneAddress);

    public static native boolean listPhoneAddress(String authKey,
                                                  StringBuffer resultCode, StringBuffer resultMessage, StringBuffer resultData,
                                                  List<PhoneAddressInfo> listPhoneAddress);

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for saaring.cpp jni interface
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static native void test2();

}
