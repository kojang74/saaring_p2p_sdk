package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class MemberInfo implements Parcelable
{
    String                  authKey             = "";

    String                  runningVersion      = "";
    String                  lastVersion         = "";

    long                    deviceNo;
    int                     deviceType;
    String                  deviceModel         = "";
    String                  deviceName          = "";

    String                  language            = "";

    String                  osName              = "";
    String                  osVersion           = "";

    long                    userNo;
    String                  userId              = "";
    String                  userName            = "";

    String                  password            = "";

    String                  email               = "";
    String                  phoneNumber         = "";

    String                  firstName           = "";
    String                  lastName            = "";

    String                  profilePic          = "";
    String                  message             = "";

    int                     idType;
    String                  providerId          = "";
    String                  idToken             = "";
    String                  accessToken         = "";
    String                  uid                 = "";
    String                  providerYN          = "";
    int                     numProvider;
    
    long                    defaultServerFolderNo;

    int                     joinStatus;
    String                  pushKey             = "";
    String                  loginKey            = "";

    String                  firebaseToken       = "";

    // id/pw확인
    long                    certNo;
    long                    certType;
    String                  certKey             = "";

    // additial member
    boolean                 isNewUser           = false;

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////

    public static final Creator<MemberInfo> CREATOR = new Creator<MemberInfo>() {
        @Override
        public MemberInfo createFromParcel(Parcel source) {
            return new MemberInfo(source);
        }

        @Override
        public MemberInfo[] newArray(int size) {
            return new MemberInfo[size];
        }
    };

    public MemberInfo() {
    }

    protected MemberInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(               this.authKey);
        dest.writeString(               this.runningVersion);
        dest.writeString(               this.lastVersion);
        dest.writeLong(                 this.deviceNo);
        dest.writeInt(                  this.deviceType);
        dest.writeString(               this.deviceModel);
        dest.writeString(               this.deviceName);
        dest.writeString(               this.language);
        dest.writeString(               this.osName);
        dest.writeString(               this.osVersion);
        dest.writeLong(                 this.userNo);
        dest.writeString(               this.userId);
        dest.writeString(               this.userName);
        dest.writeString(               this.password);
        dest.writeString(               this.email);
        dest.writeString(               this.phoneNumber);
        dest.writeString(               this.firstName);
        dest.writeString(               this.lastName);
        dest.writeString(               this.profilePic);
        dest.writeString(               this.message);
        dest.writeInt(                  this.idType);
        dest.writeString(               this.providerId);
        dest.writeString(               this.accessToken);
        dest.writeString(               this.idToken);
        dest.writeString(               this.uid);
        dest.writeString(               this.providerYN);
        dest.writeInt(                  this.numProvider);
        dest.writeLong(                 this.defaultServerFolderNo);
        dest.writeInt(                  this.joinStatus);
        dest.writeString(               this.pushKey);
        dest.writeString(               this.loginKey);
        dest.writeString(               this.firebaseToken);
        dest.writeLong(                 this.certNo);
        dest.writeLong(                 this.certType);
        dest.writeString(               this.certKey);
        dest.writeByte(                 this.isNewUser ? (byte) 1 : (byte) 0);
    }

    public void readFromParcel(Parcel in) {
        this.authKey                    = in.readString();
        this.runningVersion             = in.readString();
        this.lastVersion                = in.readString();
        this.deviceNo                   = in.readLong();
        this.deviceType                 = in.readInt();
        this.deviceModel                = in.readString();
        this.deviceName                 = in.readString();
        this.language                   = in.readString();
        this.osName                     = in.readString();
        this.osVersion                  = in.readString();
        this.userNo                     = in.readLong();
        this.userId                     = in.readString();
        this.userName                   = in.readString();
        this.password                   = in.readString();
        this.email                      = in.readString();
        this.phoneNumber                = in.readString();
        this.firstName                  = in.readString();
        this.lastName                   = in.readString();
        this.profilePic                 = in.readString();
        this.message                    = in.readString();
        this.idType                     = in.readInt();
        this.providerId                 = in.readString();
        this.accessToken                = in.readString();
        this.idToken                    = in.readString();
        this.uid                        = in.readString();
        this.providerYN                 = in.readString();
        this.numProvider                = in.readInt();
        this.defaultServerFolderNo      = in.readLong();
        this.joinStatus                 = in.readInt();
        this.pushKey                    = in.readString();
        this.loginKey                   = in.readString();
        this.firebaseToken              = in.readString();
        this.certNo                     = in.readLong();
        this.certType                   = in.readLong();
        this.certKey                    = in.readString();
        this.isNewUser                  = in.readByte() != 0;
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getRunningVersion() {
        return runningVersion;
    }

    public void setRunningVersion(String runningVersion) {
        this.runningVersion = runningVersion;
    }

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

    public long getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(long deviceNo) {
        this.deviceNo = deviceNo;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public long getUserNo() {
        return userNo;
    }

    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getProviderYN() {
        return providerYN;
    }

    public void setProviderYN(String providerYN) {
        this.providerYN = providerYN;
    }

    public int getNumProvider() {
        return numProvider;
    }

    public void setNumProvider(int numProvider) {
        this.numProvider = numProvider;
    }

    public long getDefaultServerFolderNo() {
        return defaultServerFolderNo;
    }

    public void setDefaultServerFolderNo(long defaultServerFolderNo) {
        this.defaultServerFolderNo = defaultServerFolderNo;
    }

    public int getJoinStatus() {
        return joinStatus;
    }

    public void setJoinStatus(int joinStatus) {
        this.joinStatus = joinStatus;
    }

    public String getPushKey() {
        return pushKey;
    }

    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }

    public String getLoginKey() {
        return loginKey;
    }

    public void setLoginKey(String loginKey) {
        this.loginKey = loginKey;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public long getCertNo() {
        return certNo;
    }

    public void setCertNo(long certNo) {
        this.certNo = certNo;
    }

    public long getCertType() {
        return certType;
    }

    public void setCertType(long certType) {
        this.certType = certType;
    }

    public String getCertKey() {
        return certKey;
    }

    public void setCertKey(String certKey) {
        this.certKey = certKey;
    }

    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean isNewUser) {
        this.isNewUser = isNewUser;
    }
}
