package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class InviteFilesInfo implements Parcelable
{
    long                    fileId;
    long                    inviteNo;

    String                  certKey             = "";
    String                  localPath           = "";
    String                  indirectPath        = "";
    String                  fileName            = "";

    long                    fileSize;
    long                    expiredt;
    long                    regdt;

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<InviteFilesInfo> CREATOR = new Creator<InviteFilesInfo>() {
        @Override
        public InviteFilesInfo createFromParcel(Parcel source) {
            return new InviteFilesInfo(source);
        }

        @Override
        public InviteFilesInfo[] newArray(int size) {
            return new InviteFilesInfo[size];
        }
    };

    public InviteFilesInfo() {
    }

    protected InviteFilesInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(         this.fileId);
        dest.writeLong(         this.inviteNo);

        dest.writeString(       this.certKey);
        dest.writeString(       this.localPath);
        dest.writeString(       this.indirectPath);
        dest.writeString(       this.fileName);

        dest.writeLong(         this.fileSize);
        dest.writeLong(         this.expiredt);
        dest.writeLong(         this.regdt);
    }

    public void readFromParcel(Parcel in) {
        this.fileId             = in.readLong();
        this.inviteNo           = in.readLong();

        this.certKey            = in.readString();
        this.localPath          = in.readString();
        this.indirectPath       = in.readString();
        this.fileName           = in.readString();

        this.fileSize           = in.readLong();
        this.expiredt           = in.readLong();
        this.regdt              = in.readLong();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getFileId() {
        return fileId;
    }
    public void setFileId(long fileId) {
        this.fileId = fileId;
    }
    public long getInviteNo() {
        return inviteNo;
    }
    public void setInviteNo(long inviteNo) {
        this.inviteNo = inviteNo;
    }
    public String getCertKey() {
        return certKey;
    }
    public void setCertKey(String certKey) {
        this.certKey = certKey;
    }
    public String getLocalPath() {
        return localPath;
    }
    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }
    public String getIndirectPath() {
        return indirectPath;
    }
    public void setIndirectPath(String indirectPath) {
        this.indirectPath = indirectPath;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public long getFileSize() {
        return fileSize;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public long getExpiredt() {
        return expiredt;
    }
    public void setExpiredt(long expiredt) {
        this.expiredt = expiredt;
    }
    public long getRegdt() {
        return regdt;
    }
    public void setRegdt(long regdt) {
        this.regdt = regdt;
    }
}
