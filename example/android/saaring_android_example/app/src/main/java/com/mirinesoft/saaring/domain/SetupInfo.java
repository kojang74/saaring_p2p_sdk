package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class SetupInfo implements Parcelable
{
    String             idType                       = "";               // 0 email, 1 phone
    String             email                        = "";               // 로그인 ID(email)
    String             phoneNumber                  = "";               // 로그인 ID(phoneNumber)
    String             password                     = "";               // encrypt
    String             providerId                   = "";               // SNS 연결, providerId
    String             idToken                      = "";               // SNS 연결, idToken
    String             accessToken                  = "";               // SNS 연결, accessToken
    String             uid                          = "";               // SNS 연결, uid. encrypt
    String             _isUseMobileNetwork          = "";               // 3G/4G 모바일 네트웍 사용여부
    String             _isSaveEmail                 = "";               // 로그인시, 아이디 저장여부.       0 unchecked, 1 checkecd
    String             _isAutoLogin                 = "";               // 로그인시, 자동로그인 여부.       0 unchecked, 1 checkecd
    String             _isLastLoginEmail            = "";               // 최종 로그인한 아이디인지 여부     0 nomal, 1 last login (최종 로그인한 ID만 1로 세팅되어 있음)
    String             _isAutoStart                 = "";               // 컴퓨터 부팅시, 자동실행 여부
    String             _isTransferListAutoResend    = "";               // 전송목록에서 자동 재전송 여부
    String             _isTransferListAutoDelete    = "";               // 전송목록에서 전송완료 목록 자동 삭제 여부
    String             transferListResendMinute     = "";               // 전송목록에서 자동 재전송 체크 시간 간격
    String             _isDisplayTraySendStatus     = "";               // Tray에 전송상태 표시여부
    String             _isContinueReceiveFile       = "";               // 파일 전송시, 이어받기 여부
    String             _isRelayServer               = "";               // 중계서버로 사용할지 여부(미사용
    int                relayServerPort;                                 // 중계서버로 사용될때, 중계서버의 port
    int                otherDownloadCount;                              // 최대 다운로드 허용 수 (나에게 다운로드 해갈 수 있는 peer의 최대 갯수)
    int                otherDownloadSpeed;                              // 최대 다운로드 속도 (나에게 다운로드 해갈 수 있는 peer의 최대 전송 속도; 단위=KB/sec)
    int                myDownloadSpeed;                                 // 내가 다운로드 받을때의 최대 다운로드 속도 (다운로드 갯수는 제한하지 않는다. 느리면 중단하면 되기 때문에; 단위=KB/sec)
    int                otherUploadCount;                                // 최대 업로드 허용 수 (나에게 업로드 할 수 있는 peer의 최대 갯수)
    int                otherUploadSpeed;                                // 최대 업로드 속도 (나에게 업로드 할 수 있는 peer의 최대 전송 속도; 단위=KB/sec)
    int                myUploadSpeed;                                   // 내가 업로드 할때의 최대 업로드 속도 (업로드 갯수는 제한하지 않는다. 느리면 중단하면 되기 때문에; 단위=KB/sec)
    String             deviceOpenFriendUserNo       = "";               // 디바이스 공개 수준에서, 특정 친구에게 오픈할경우, 회원번호 리스트 (미사용)
    String             deviceOpenFriendUserName     = "";               // 디바이스 공개 수준에서, 특정 친구에게 오픈할경우, 이름 리스트 (미사용)
    int                portmapWanCtrlPort;                              // 포트매핑 정보. Control 용
    int                portmapLanCtrlPort;                              // 포트매핑 정보. Control 용
    int                portmapWanFilePort;                              // 포트매핑 정보. Filer 용
    int                portmapLanFilePort;                              // 포트매핑 정보. Filer 용
    int                portmapWanRelayPort;                             // 포트매핑 정보. Filer 용
    int                portmapLanRelayPort;                             // 포트매핑 정보. Filer 용
    String             localIpAddress               = "";               // 포트매핑 정보. 로컬 IP
	
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<SetupInfo> CREATOR = new Creator<SetupInfo>() {
        @Override
        public SetupInfo createFromParcel(Parcel source) {
            return new SetupInfo(source);
        }

        @Override
        public SetupInfo[] newArray(int size) {
            return new SetupInfo[size];
        }
    };

    public SetupInfo() {
    }

    protected SetupInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(                   this.idType);
        dest.writeString(                   this.email);
        dest.writeString(                   this.phoneNumber);
        dest.writeString(                   this.password);
        dest.writeString(                   this.providerId);
        dest.writeString(                   this.idToken);
        dest.writeString(                   this.accessToken);
        dest.writeString(                   this.uid);
        dest.writeString(                   this._isUseMobileNetwork);
        dest.writeString(                   this._isSaveEmail);
        dest.writeString(                   this._isAutoLogin);
        dest.writeString(                   this._isLastLoginEmail);
        dest.writeString(                   this._isAutoStart);
        dest.writeString(                   this._isTransferListAutoResend);
        dest.writeString(                   this._isTransferListAutoDelete);
        dest.writeString(                   this.transferListResendMinute);
        dest.writeString(                   this._isDisplayTraySendStatus);
        dest.writeString(                   this._isContinueReceiveFile);
        dest.writeString(                   this._isRelayServer);
        dest.writeInt(                      this.relayServerPort);
        dest.writeInt(                      this.otherDownloadCount);
        dest.writeInt(                      this.otherDownloadSpeed);
        dest.writeInt(                      this.myDownloadSpeed);
        dest.writeInt(                      this.otherUploadCount);
        dest.writeInt(                      this.otherUploadSpeed);
        dest.writeInt(                      this.myUploadSpeed);
        dest.writeString(                   this.deviceOpenFriendUserNo);
        dest.writeString(                   this.deviceOpenFriendUserName);
        dest.writeInt(                      this.portmapWanCtrlPort);
        dest.writeInt(                      this.portmapLanCtrlPort);
        dest.writeInt(                      this.portmapWanFilePort);
        dest.writeInt(                      this.portmapLanFilePort);
        dest.writeInt(                      this.portmapWanRelayPort);
        dest.writeInt(                      this.portmapLanRelayPort);
        dest.writeString(                   this.localIpAddress);
    }

    public void readFromParcel(Parcel in) {
        this.idType                         = in.readString();
        this.email                          = in.readString();
        this.phoneNumber                    = in.readString();
        this.password                       = in.readString();
        this.providerId                     = in.readString();
        this.idToken                        = in.readString();
        this.accessToken                    = in.readString();
        this.uid                            = in.readString();
        this._isUseMobileNetwork            = in.readString();
        this._isSaveEmail                   = in.readString();
        this._isAutoLogin                   = in.readString();
        this._isLastLoginEmail              = in.readString();
        this._isAutoStart                   = in.readString();
        this._isTransferListAutoResend      = in.readString();
        this._isTransferListAutoDelete      = in.readString();
        this.transferListResendMinute       = in.readString();
        this._isDisplayTraySendStatus       = in.readString();
        this._isContinueReceiveFile         = in.readString();
        this._isRelayServer                 = in.readString();
        this.relayServerPort                = in.readInt();
        this.otherDownloadCount             = in.readInt();
        this.otherDownloadSpeed             = in.readInt();
        this.myDownloadSpeed                = in.readInt();
        this.otherUploadCount               = in.readInt();
        this.otherUploadSpeed               = in.readInt();
        this.myUploadSpeed                  = in.readInt();
        this.deviceOpenFriendUserNo         = in.readString();
        this.deviceOpenFriendUserName       = in.readString();
        this.portmapWanCtrlPort             = in.readInt();
        this.portmapLanCtrlPort             = in.readInt();
        this.portmapWanFilePort             = in.readInt();
        this.portmapLanFilePort             = in.readInt();
        this.portmapWanRelayPort            = in.readInt();
        this.portmapLanRelayPort            = in.readInt();
        this.localIpAddress                 = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public String getIdType() {
        return idType;
    }
    public void setIdType(String idType) {
        this.idType = idType;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getProviderId() {
        return providerId;
    }
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
    public String getIdToken() {
        return idToken;
    }
    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }
    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public String getUid() {
        return uid;
    }
    public void setUid(String uid) {
        this.uid = uid;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getIsUseMobileNetwork() {
        return _isUseMobileNetwork;
    }
    public void setIsUseMobileNetwork(String isUseMobileNetwork) {
        this._isUseMobileNetwork = isUseMobileNetwork;
    }
    public String getIsSaveEmail() {
        return _isSaveEmail;
    }
    public void setIsSaveEmail(String isSaveEmail) {
        this._isSaveEmail = isSaveEmail;
    }
    public String getIsAutoLogin() {
        return _isAutoLogin;
    }
    public void setIsAutoLogin(String isAutoLogin) {
        this._isAutoLogin = isAutoLogin;
    }
    public String getIsLastLoginEmail() {
        return _isLastLoginEmail;
    }
    public void setIsLastLoginEmail(String isLastLoginEmail) {
        this._isLastLoginEmail = isLastLoginEmail;
    }
    public String getIsAutoStart() {
        return _isAutoStart;
    }
    public void setIsAutoStart(String isAutoStart) {
        this._isAutoStart = isAutoStart;
    }
    public String getIsTransferListAutoResend() {
        return _isTransferListAutoResend;
    }
    public void setIsTransferListAutoResend(String isTransferListAutoResend) {
        this._isTransferListAutoResend = isTransferListAutoResend;
    }
    public String getIsTransferListAutoDelete() {
        return _isTransferListAutoDelete;
    }
    public void setIsTransferListAutoDelete(String isTransferListAutoDelete) {
        this._isTransferListAutoDelete = isTransferListAutoDelete;
    }
    public String getTransferListResendMinute() {
        return transferListResendMinute;
    }
    public void setTransferListResendMinute(String transferListResendMinute) {
        this.transferListResendMinute = transferListResendMinute;
    }
    public String getIsDisplayTraySendStatus() {
        return _isDisplayTraySendStatus;
    }
    public void setIsDisplayTraySendStatus(String isDisplayTraySendStatus) {
        this._isDisplayTraySendStatus = isDisplayTraySendStatus;
    }
    public String getIsContinueReceiveFile() {
        return _isContinueReceiveFile;
    }
    public void setIsContinueReceiveFile(String isContinueReceiveFile) {
        this._isContinueReceiveFile = isContinueReceiveFile;
    }
    public String getIsRelayServer() {
        return _isRelayServer;
    }
    public void setIsRelayServer(String isRelayServer) {
        this._isRelayServer = isRelayServer;
    }
    public int getRelayServerPort() {
        return relayServerPort;
    }
    public void setRelayServerPort(int relayServerPort) {
        this.relayServerPort = relayServerPort;
    }
    public int getOtherDownloadCount() {
        return otherDownloadCount;
    }
    public void setOtherDownloadCount(int otherDownloadCount) {
        this.otherDownloadCount = otherDownloadCount;
    }
    public int getOtherDownloadSpeed() {
        return otherDownloadSpeed;
    }
    public void setOtherDownloadSpeed(int otherDownloadSpeed) {
        this.otherDownloadSpeed = otherDownloadSpeed;
    }
    public int getMyDownloadSpeed() {
        return myDownloadSpeed;
    }
    public void setMyDownloadSpeed(int myDownloadSpeed) {
        this.myDownloadSpeed = myDownloadSpeed;
    }
    public int getOtherUploadCount() {
        return otherUploadCount;
    }
    public void setOtherUploadCount(int otherUploadCount) {
        this.otherUploadCount = otherUploadCount;
    }
    public int getOtherUploadSpeed() {
        return otherUploadSpeed;
    }
    public void setOtherUploadSpeed(int otherUploadSpeed) {
        this.otherUploadSpeed = otherUploadSpeed;
    }
    public int getMyUploadSpeed() {
        return myUploadSpeed;
    }
    public void setMyUploadSpeed(int myUploadSpeed) {
        this.myUploadSpeed = myUploadSpeed;
    }
    public String getDeviceOpenFriendUserNo() {
        return deviceOpenFriendUserNo;
    }
    public void setDeviceOpenFriendUserNo(String deviceOpenFriendUserNo) {
        this.deviceOpenFriendUserNo = deviceOpenFriendUserNo;
    }
    public String getDeviceOpenFriendUserName() {
        return deviceOpenFriendUserName;
    }
    public void setDeviceOpenFriendUserName(String deviceOpenFriendUserName) {
        this.deviceOpenFriendUserName = deviceOpenFriendUserName;
    }
    public int getPortmapWanCtrlPort() {
        return portmapWanCtrlPort;
    }
    public void setPortmapWanCtrlPort(int portmapWanCtrlPort) {
        this.portmapWanCtrlPort = portmapWanCtrlPort;
    }
    public int getPortmapLanCtrlPort() {
        return portmapLanCtrlPort;
    }
    public void setPortmapLanCtrlPort(int portmapLanCtrlPort) {
        this.portmapLanCtrlPort = portmapLanCtrlPort;
    }
    public int getPortmapWanFilePort() {
        return portmapWanFilePort;
    }
    public void setPortmapWanFilePort(int portmapWanFilePort) {
        this.portmapWanFilePort = portmapWanFilePort;
    }
    public int getPortmapLanFilePort() {
        return portmapLanFilePort;
    }
    public void setPortmapLanFilePort(int portmapLanFilePort) {
        this.portmapLanFilePort = portmapLanFilePort;
    }
    public int getPortmapWanRelayPort() {
        return portmapWanRelayPort;
    }
    public void setPortmapWanRelayPort(int portmapWanRelayPort) {
        this.portmapWanRelayPort = portmapWanRelayPort;
    }
    public int getPortmapLanRelayPort() {
        return portmapLanRelayPort;
    }
    public void setPortmapLanRelayPort(int portmapLanRelayPort) {
        this.portmapLanRelayPort = portmapLanRelayPort;
    }
    public String getLocalIpAddress() {
        return localIpAddress;
    }
    public void setLocalIpAddress(String localIpAddress) {
        this.localIpAddress = localIpAddress;
    }
}
