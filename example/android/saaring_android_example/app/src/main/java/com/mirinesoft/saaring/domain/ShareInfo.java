package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ShareInfo implements Parcelable
{
    long                    shareNo;
    long                    deviceNo;
    String                  shareFolderPath                     = "";
    String                  shareName                           = "";

    // 서버에서발행하는 folderNo
    long                    serverFolderNo;
    int                     fileCount;
    long                    fileSize;

    int                     downKind;
    List<Long>              listDownFriendUserNo		= new ArrayList();;
    List<String>            listDownFriendUserName		= new ArrayList();;
    int                     upKind;
    List<Long>              listUpFriendUserNo			= new ArrayList();;
    List<String>            listUpFriendUserName		= new ArrayList();;

    String                  downYN                              = "";
    String                  upYN                                = "";
    String                  password                            = "";
    String                  passwordYN                          = "";
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<ShareInfo> CREATOR = new Creator<ShareInfo>() {
        @Override
        public ShareInfo createFromParcel(Parcel source) {
            return new ShareInfo(source);
        }

        @Override
        public ShareInfo[] newArray(int size) {
            return new ShareInfo[size];
        }
    };

    public ShareInfo() {
    }

    protected ShareInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(                 this.shareNo);
        dest.writeLong(                 this.deviceNo);
        dest.writeString(               this.shareFolderPath);
        dest.writeString(               this.shareName);
        dest.writeLong(                 this.serverFolderNo);
        dest.writeInt(                  this.fileCount);
        dest.writeLong(                 this.fileSize);
        dest.writeInt(                  this.downKind);
        dest.writeList(                 this.listDownFriendUserNo);
        dest.writeStringList(           this.listDownFriendUserName);
        dest.writeInt(                  this.upKind);
        dest.writeList(                 this.listUpFriendUserNo);
        dest.writeStringList(           this.listUpFriendUserName);
        dest.writeString(               this.downYN);
        dest.writeString(               this.upYN);
        dest.writeString(               this.password);
        dest.writeString(               this.passwordYN);
    }

    public void readFromParcel(Parcel in) {
        this.shareNo                    = in.readLong();
        this.deviceNo                   = in.readLong();
        this.shareFolderPath            = in.readString();
        this.shareName                  = in.readString();
        this.serverFolderNo             = in.readLong();
        this.fileCount                  = in.readInt();
        this.fileSize                   = in.readLong();
        this.downKind                   = in.readInt();
        this.listDownFriendUserNo       = new ArrayList<Long>();
        in.readList(this.listDownFriendUserNo, Long.class.getClassLoader());
        this.listDownFriendUserName     = in.createStringArrayList();
        this.upKind                     = in.readInt();
        this.listUpFriendUserNo         = new ArrayList<Long>();
        in.readList(this.listUpFriendUserNo, Long.class.getClassLoader());
        this.listUpFriendUserName       = in.createStringArrayList();
        this.downYN                     = in.readString();
        this.upYN                       = in.readString();
        this.password                   = in.readString();
        this.passwordYN                 = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getShareNo() {
        return shareNo;
    }
    public void setShareNo(long shareNo) {
        this.shareNo = shareNo;
    }
    public long getDeviceNo() {
        return deviceNo;
    }
    public void setDeviceNo(long deviceNo) {
        this.deviceNo = deviceNo;
    }
    public String getShareFolderPath() {
        return shareFolderPath;
    }
    public void setShareFolderPath(String shareFolderPath) {
        this.shareFolderPath = shareFolderPath;
    }
    public String getShareName() {
        return shareName;
    }
    public void setShareName(String shareName) {
        this.shareName = shareName;
    }
    public long getServerFolderNo() {
        return serverFolderNo;
    }
    public void setServerFolderNo(long serverFolderNo) {
        this.serverFolderNo = serverFolderNo;
    }
    public int getFileCount() {
        return fileCount;
    }
    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }
    public long getFileSize() {
        return fileSize;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public int getDownKind() {
        return downKind;
    }
    public void setDownKind(int downKind) {
        this.downKind = downKind;
    }
    public List<Long> getListDownFriendUserNo() {
        return listDownFriendUserNo;
    }
    public void setListDownFriendUserNo(List<Long> listDownFriendUserNo) {
        this.listDownFriendUserNo = listDownFriendUserNo;
    }
    public List<String> getListDownFriendUserName() {
        return listDownFriendUserName;
    }
    public void setListDownFriendUserName(List<String> listDownFriendUserName) {
        this.listDownFriendUserName = listDownFriendUserName;
    }
    public int getUpKind() {
        return upKind;
    }
    public void setUpKind(int upKind) {
        this.upKind = upKind;
    }
    public List<Long> getListUpFriendUserNo() {
        return listUpFriendUserNo;
    }
    public void setListUpFriendUserNo(List<Long> listUpFriendUserNo) {
        this.listUpFriendUserNo = listUpFriendUserNo;
    }
    public List<String> getListUpFriendUserName() {
        return listUpFriendUserName;
    }
    public void setListUpFriendUserName(List<String> listUpFriendUserName) {
        this.listUpFriendUserName = listUpFriendUserName;
    }
    public String getDownYN() {
        return downYN;
    }
    public void setDownYN(String downYN) {
        this.downYN = downYN;
    }
    public String getUpYN() {
        return upYN;
    }
    public void setUpYN(String upYN) {
        this.upYN = upYN;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPasswordYN() {
        return passwordYN;
    }
    public void setPasswordYN(String passwordYN) {
        this.passwordYN = passwordYN;
    }
}
