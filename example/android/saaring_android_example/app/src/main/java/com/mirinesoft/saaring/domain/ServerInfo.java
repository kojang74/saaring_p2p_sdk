package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class ServerInfo implements Parcelable
{
    String          signalingIp                  = "";
    int             signalingPort;
    String          rendezvousIp                 = "";
    int             rendezvousPort;
    String          relayIp                      = "";
    int             relayPort;
    String          runningVersion               = "";
    String          lastVersion                  = "";
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<ServerInfo> CREATOR = new Creator<ServerInfo>() {
        public ServerInfo createFromParcel(Parcel in) {
            return new ServerInfo(in);
        }

        public ServerInfo[] newArray( int size ) {
            return new ServerInfo[size];
        }
    };

    public ServerInfo() {
    }

    public ServerInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(       signalingIp);
        dest.writeInt(          signalingPort);
        dest.writeString(       rendezvousIp);
        dest.writeInt(          rendezvousPort);
        dest.writeString(       relayIp);
        dest.writeInt(          relayPort);
        dest.writeString(       signalingIp);
        dest.writeString(       signalingIp);
    }

    public void readFromParcel(Parcel in) {
        signalingIp             = in.readString();
        signalingPort           = in.readInt();
        rendezvousIp            = in.readString();
        rendezvousPort          = in.readInt();
        relayIp                 = in.readString();
        relayPort               = in.readInt();
        runningVersion          = in.readString();
        lastVersion             = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public String getSignalingIp()
    {
    	return this.signalingIp;
    }
    public void setSignalingIp(String signalingIp)
    {
    	this.signalingIp = signalingIp;
    }
    public int getSignalingPort()
    {
        return this.signalingPort;
    }
    public void setSignalingPort(int signalingPort)
    {
        this.signalingPort = signalingPort;
    }
    public String getRendezvousIp()
    {
        return this.rendezvousIp;
    }
    public void setRendezvousIp(String rendezvousIp)
    {
        this.rendezvousIp = rendezvousIp;
    }
    public int getRendezvousPort()
    {
        return this.rendezvousPort;
    }
    public void setRendezvousPort(int rendezvousPort)
    {
        this.rendezvousPort = rendezvousPort;
    }
    public String getRelayIp()
    {
        return this.relayIp;
    }
    public void setRelayIp(String relayIp)
    {
        this.relayIp = relayIp;
    }
    public int getRelayPort()
    {
        return this.relayPort;
    }
    public void setRelayPort(int relayPort)
    {
        this.relayPort = relayPort;
    }
    public String getRunningVersion()
    {
        return this.runningVersion;
    }
    public void setRunningVersion(String runningVersion)
    {
        this.runningVersion = runningVersion;
    }
    public String getLastVersion()
    {
        return this.lastVersion;
    }
    public void setLastVersion(String lastVersion)
    {
        this.lastVersion = lastVersion;
    }
}
