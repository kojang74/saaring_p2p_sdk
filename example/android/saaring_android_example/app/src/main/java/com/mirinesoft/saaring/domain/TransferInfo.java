package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class TransferInfo implements Parcelable
{
    String                             fileName  = "";   		// 파일이름
    int                                status;     				// SEND 상태
    long                               fileSize;   				// 파일사이즈
    String                             sendSpeed  = "";  		// 전송속도
    String                             leftTime  = "";   		// 남은시간
    long                               transNo;
    long                               currSize;   				// 현재까지 전송된 사이즈
    String                             currTime  = "";   		// 현재까지 마지막으로 전송한 시간. YYYYMMddHHmmsszzz
    long                               userNo;     				// toUserNo
    String                             userName  = "";     		// 서버
    long                               deviceNo;
    int                                deviceType;
    String                             deviceName  = "";     	// 디바이스
    String                             upDown  = "";     		// U or D
    boolean                            isMyAction; 				// 내가 down/up 이면 true, 상대방이 down/up 이면 false
    String                             folderName  = ""; 		// 공유폴더명
    String                             remotePath  = "";
    long                               inviteNo;
    long                               fileId;
    boolean                            isCheck = false; 		//화면상에서의 체크박스


    public TransferInfo(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<TransferInfo> CREATOR = new Creator<TransferInfo>() {
        @Override
        public TransferInfo createFromParcel(Parcel in) {
            return new TransferInfo(in);
        }

        @Override
        public TransferInfo[] newArray(int size) {
            return new TransferInfo[size];
        }
    };

    public TransferInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(               this.fileName);
        dest.writeInt(                  this.status);
        dest.writeLong(                 this.fileSize);
        dest.writeString(               this.sendSpeed);
        dest.writeString(               this.leftTime);
        dest.writeLong(                 this.transNo);
        dest.writeLong(                 this.currSize);
        dest.writeString(               this.currTime);
        dest.writeLong(                 this.userNo);
        dest.writeString(               this.userName);
        dest.writeLong(                 this.deviceNo);
        dest.writeInt(                  this.deviceType);
        dest.writeString(               this.deviceName);
        dest.writeString(               this.upDown);
        dest.writeByte(                 (byte)(this.isMyAction ? 1 : 0));
        dest.writeString(               this.folderName);
        dest.writeString(               this.remotePath);
        dest.writeLong(                 this.inviteNo);
        dest.writeLong(                 this.fileId);
    }

    public void readFromParcel(Parcel in) {
        fileName 		= in.readString();
        status 			= in.readInt();
        fileSize 		= in.readLong();
        sendSpeed 		= in.readString();
        leftTime 		= in.readString();
        transNo 		= in.readLong();
        currSize 		= in.readLong();
        currTime 		= in.readString();
        userNo 			= in.readLong();
        userName 		= in.readString();
        deviceNo 		= in.readLong();
        deviceType 		= in.readInt();
        deviceName 		= in.readString();
        upDown 			= in.readString();
        isMyAction 		= in.readByte() != 0;
        folderName 		= in.readString();
        remotePath 		= in.readString();
        inviteNo 		= in.readLong();
        fileId  		= in.readLong();
    }

    //getter
    public String getFileName() {
        return fileName;
    }

    public int getStatus() {
        return status;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getSendSpeed() {
        return sendSpeed;
    }

    public String getLeftTime() {
        return leftTime;
    }

    public long getTransNo() {
        return transNo;
    }

    public long getCurrSize() {
        return currSize;
    }

    public String getCurrTime() {
        return currTime;
    }

    public long getUserNo() {
        return userNo;
    }

    public String getUserName() {
        return userName;
    }

    public long getDeviceNo() {
        return deviceNo;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getUpDown() {
        return upDown;
    }

    public boolean isMyAction() {
        return isMyAction;
    }

    public String getFolderName() {
        return folderName;
    }

    public String getRemotePath() {
        return remotePath;
    }

    public long getInviteNo() {
        return inviteNo;
    }
    public long getFileId() {
        return fileId;
    }

    public boolean getIsCheck() { return  isCheck; }

    //setter
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public void setSendSpeed(String sendSpeed) {
        this.sendSpeed = sendSpeed;
    }

    public void setLeftTime(String leftTime) {
        this.leftTime = leftTime;
    }

    public void setTransNo(long transNo) {
        this.transNo = transNo;
    }

    public void setCurrSize(long currSize) {
        this.currSize = currSize;
    }

    public void setCurrTime(String currTime) {
        this.currTime = currTime;
    }

    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setDeviceNo(long deviceNo) {
        this.deviceNo = deviceNo;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public void setUpDown(String upDown) {
        this.upDown = upDown;
    }

    public void setMyAction(boolean myAction) {
        isMyAction = myAction;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    public void setInviteNo(long inviteNo) {
        this.inviteNo = inviteNo;
    }
    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public void setIsCheck(boolean isCheck) { this.isCheck = isCheck; }
}
