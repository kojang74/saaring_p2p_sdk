package com.mirinesoft.saaring;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;

import org.qtproject.qt5.android.bindings.QtService;

import java.util.ArrayList;
import java.util.List;

import com.mirinesoft.saaring.domain.*;

public class SaaringService extends QtService
{
    public static void startSaaringService(Context ctx)
    {
        System.out.println("===========================>>> startSaaringService...");
        ctx.startService(new Intent(ctx, SaaringService.class));
    }

    public static void stopSaaringService(Context ctx)
    {
        System.out.println("===========================>>> stopSaaringService...");
        ctx.stopService(new Intent(ctx, SaaringService.class));
    }

    /** Called when the service is being created. */
    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("===========================>>> onCreate...");
    }

    /** The service is starting, due to a call to startService() */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int ret = super.onStartCommand(intent, flags, startId);
        System.out.println("===========================>>> onStartCommand...");
        return START_NOT_STICKY;
    }

    /** A client is binding to the service with bindService() */
    @Override
    public IBinder onBind(Intent intent) {
        System.out.println("===========================>>> onBind...");
        return saaringServiceBinder;
    }

    /** Called when all clients have unbound with unbindService() */
    @Override
    public boolean onUnbind(Intent intent) {
        System.out.println("===========================>>> onUnbind...");
        return super.onUnbind(intent);
    }

    /** Called when a client is binding to the service with bindService()*/
    @Override
    public void onRebind(Intent intent) {
        System.out.println("===========================>>> onRebind...");
        super.onRebind(intent);
    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
        System.out.println("===========================>>> onDestroy...");
        super.onDestroy();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // Execute JNI function
    // and
    // Call a callback, if need.
    //
    //////////////////////////////////////////////////////////////////////////////////
    private static RemoteCallbackList<ISaaringServiceCallback> callbacks = new RemoteCallbackList<ISaaringServiceCallback>();
    public static SaaringServiceHandler saaringServiceHandler = new SaaringServiceHandler(callbacks);

    private final ISaaringServiceBinder.Stub saaringServiceBinder = new ISaaringServiceBinder.Stub() {

        @Override
        public boolean unregisterCallback(ISaaringServiceCallback callback) throws RemoteException
        {
            boolean flag = false;

            if(callback != null)
            {
                flag = callbacks.register(callback);
            }

            return flag;
        }

        @Override
        public boolean registerCallback(ISaaringServiceCallback callback) throws RemoteException
        {
            boolean flag = false;

            if(callback != null)
            {
                flag = unregisterCallback(callback);
            }

            return flag;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring common jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public void test()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService test...");
            SaaringNative.test();
        }

        @Override
        public double calSpeed(long oldTime, long currTime, long oldSize, long currSize)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService calSpeed...");
            return SaaringNative.calSpeed(oldTime, currTime, oldSize, currSize);
        }
        @Override
        public String leftTime(long oldTime, long currTime, long oldSize, long currSize, long totalSize, double speed)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService leftTime...");
            return SaaringNative.leftTime(oldTime, currTime, oldSize, currSize, totalSize, speed);
        }

        @Override
        public boolean checkEmail(String email)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService checkEmail...");
            return SaaringNative.checkEmail(email);
        }
        @Override
        public String getDuplicationFilename(String filePath, StringData lastLocalFilePath)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getDuplicationFilename...");

            StringBuffer paramLastLocalFilePath        = new StringBuffer(lastLocalFilePath.getData());

            String dupFilePath = SaaringNative.getDuplicationFilename(filePath, paramLastLocalFilePath);

            lastLocalFilePath.setData(         paramLastLocalFilePath.toString());

            return dupFilePath;
        }
        @Override
        public String numberFormatWithComma(long number, String separator)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService numberFormatWithComma...");
            return SaaringNative.numberFormatWithComma(number, separator);
        }

        @Override
        public int checkUpgrade(String lastVer, String runningVer, String currentVer)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService checkUpgrade...");
            return SaaringNative.checkUpgrade(lastVer, runningVer, currentVer);
        }

        @Override
        public String getSizeWithUnit(long size)
            throws RemoteException
        {
            //System.out.println("===========================>>> SaaringService getSizeWithUnit...");
            return SaaringNative.getSizeWithUnit(size);
        }

        @Override
        public String getDownloadFolder()
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getDownloadFolder...");
            return SaaringNative.getDownloadFolder();
        }

        @Override
        public String getShareTopFolder()
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareTopFolder...");
            return SaaringNative.getShareTopFolder();
        }

        @Override
        public String changeProfileImage(String authKey, String path)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService changeProfileImage...");
            return SaaringNative.changeProfileImage(authKey, path);
        }

        @Override
        public String removeFirstPath(String path)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService removeFirstPath...");
            return SaaringNative.removeFirstPath(path);
        }

        @Override
        public String getProviderId(int idType)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getProviderId...");
            return SaaringNative.getProviderId(idType);
        }

        @Override
        public String getProviderName(int idType)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getProviderName...");
            return SaaringNative.getProviderName(idType);
        }

        @Override
        public String getProviderNameByProviderId(String providerId)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getProviderNameByProviderId...");
            return SaaringNative.getProviderNameByProviderId(providerId);
        }

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring db jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////
        // for All
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public boolean isOpen()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService isOpen...");
            return SaaringNative.isOpen();
        }

        //////////////////////////////////////////////////////////////////////////////////
        // for login
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public boolean updateLoginTb(int idType, String email, String phoneNumber, String password, String isSaveEmail, String isAutoLogin, String isLastLoginEmail)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateLoginTb...");
            return SaaringNative.updateLoginTb(idType, email, phoneNumber, password, isSaveEmail, isAutoLogin, isLastLoginEmail);
        }
        @Override
        public boolean updateLoginAutoStart(int idType, String email, String phoneNumber, String isAutoStart)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateLoginAutoStart...");
            return SaaringNative.updateLoginAutoStart(idType, email, phoneNumber, isAutoStart);
        }

        @Override
        public boolean updateSetSend(SetupInfo setupInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateSetSend...");
            return SaaringNative.updateSetSend(setupInfo);
        }
        @Override
        public boolean updateSetNetwork(SetupInfo setupInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateSetNetwork...");
            return SaaringNative.updateSetNetwork(setupInfo);
        }

        @Override
        public boolean readDbLastLogin(SetupInfo setupInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readDbLastLogin...");
            return SaaringNative.readDbLastLogin(setupInfo);
        }
        @Override
        public boolean readDbByEmailOrPhoneNumber(int idType, String email, String phoneNumber, SetupInfo setupInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readDbByEmailOrPhoneNumber...");
            return SaaringNative.readDbByEmailOrPhoneNumber(idType, email, phoneNumber, setupInfo);
        }

        @Override
        public void doAfterLogin(int idType, String email, String phoneNumber, String password,
                                 String providerId, String idToken, String accessToken, String uid,
                                 String isSaveEmail, String isAutoLogin)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doAfterLoginSns...");
            SaaringNative.doAfterLogin(idType, email, phoneNumber, password,
                                        providerId, idToken, accessToken, uid,
                                        isSaveEmail, isAutoLogin);
        }

        //////////////////////////////////////////////////////////////////////////////////
        // for transfer
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public long insertMyTransTbOne(TransferBaseInfo transferBaseInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService insertMyTransTbOne...");
            return SaaringNative.insertMyTransTbOne(transferBaseInfo);
        }
        @Override
        public boolean insertMyTransTbMulti(List<TransferBaseInfo> listTransferBaseInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService insertMyTransTbMulti...");
            return SaaringNative.insertMyTransTbMulti(listTransferBaseInfo);
        }

        @Override
        public boolean updateMyTransTb(TransferBaseInfo transferBaseInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateMyTransTb...");
            return SaaringNative.updateMyTransTb(transferBaseInfo);
        }
        @Override
        public boolean updateMyTransTbStatus(long transNo, int transStatus, long updateTime)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateMyTransTbStatus...");
            return SaaringNative.updateMyTransTbStatus(transNo, transStatus, updateTime);
        }

        @Override
        public boolean updateMyTransTbSortNoMulti(long[] arrayTransNo, long updateTime)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateMyTransTbSortNoMulti...");

            List<Long> listTransNo = new ArrayList();
            for(long data : arrayTransNo)
            {
                listTransNo.add(data);
            }

            return SaaringNative.updateMyTransTbSortNoMulti(listTransNo, updateTime);
        }

        @Override
        public boolean updateMyTransTbProgress(long transNo, long fileSize, long transSize, long updateTime)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateMyTransTbProgress...");
            return SaaringNative.updateMyTransTbProgress(transNo, fileSize, transSize, updateTime);
        }
        @Override
        public boolean updateMyTransTbInit(long myDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateMyTransTbInit...");
            return SaaringNative.updateMyTransTbInit(myDeviceNo);
        }

        @Override
        public boolean deleteMyTransTbOne(long transNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteMyTransTbOne...");
            return SaaringNative.deleteMyTransTbOne(transNo);
        }
        @Override
        public boolean deleteMyTransTbMulti(long[] arrayTransNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteMyTransTbMulti...");

            List<Long> listTransNo = new ArrayList();
            for(long data : arrayTransNo)
            {
                listTransNo.add(data);
            }

            return SaaringNative.deleteMyTransTbMulti(listTransNo);
        }
        @Override
        public boolean deleteMyTransTbAll(long deviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteMyTransTbAll...");
            return SaaringNative.deleteMyTransTbAll(deviceNo);
        }

        @Override
        public boolean readMyTransTbByTransNo(List<TransferBaseInfo> listTransferBaseInfo, long transNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readMyTransTbByTransNo...");
            return SaaringNative.readMyTransTbByTransNo(listTransferBaseInfo, transNo);
        }
        @Override
        public boolean readMyTransTbAll(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readMyTransTbAll...");
            return SaaringNative.readMyTransTbAll(listTransferBaseInfo, myDeviceNo, deleteYN, deviceNo);
        }
        @Override
        public boolean readMyTransTbTransferring(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readMyTransTbTransferring...");
            return SaaringNative.readMyTransTbTransferring(listTransferBaseInfo, myDeviceNo, deleteYN, deviceNo);
        }
        @Override
        public boolean readMyTransTbNotTransferringLastOne(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readMyTransTbNotTransferringLastOne...");
            return SaaringNative.readMyTransTbNotTransferringLastOne(listTransferBaseInfo, myDeviceNo, deleteYN, deviceNo);
        }
        @Override
        public boolean readMyTransTbByTransStatusLastOne(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo, int transStatus)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readMyTransTbByTransStatusLastOne...");
            return SaaringNative.readMyTransTbByTransStatusLastOne(listTransferBaseInfo, myDeviceNo, deleteYN, deviceNo, transStatus);
        }
        @Override
        public boolean readMyTransTbInit(List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readMyTransTbInit...");
            return SaaringNative.readMyTransTbInit(listTransferBaseInfo, myDeviceNo);
        }

        //////////////////////////////////////////////////////////////////////////////////
        // for share
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public boolean insertShareTb(ShareInfo shareInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService insertShareTb...");
            return SaaringNative.insertShareTb(shareInfo);
        }
        @Override
        public boolean updateShareTb(ShareInfo shareInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateShareTb...");
            return SaaringNative.updateShareTb(shareInfo);
        }
        @Override
        public boolean deleteShareTb(long shareNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteShareTb...");
            return SaaringNative.deleteShareTb(shareNo);
        }
        @Override
        public boolean readShareTb(long deviceNo, List<ShareInfo> listShareInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService readShareTb...");
            return SaaringNative.readShareTb(deviceNo, listShareInfo);
        }

        @Override
        public long getShareNoByServerFolderNo(long serverFolderNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareNoByServerFolderNo...");
            return SaaringNative.getShareNoByServerFolderNo(serverFolderNo);
        }
        @Override
        public long getServerFolderNoByShareNo(long shareNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getServerFolderNoByShareNo...");
            return SaaringNative.getServerFolderNoByShareNo(shareNo);
        }
        @Override
        public String getShareFolderPathByServerFolderNo(long serverFolderNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareFolderPathByServerFolderNo...");
            return SaaringNative.getShareFolderPathByServerFolderNo(serverFolderNo);
        }

        @Override
        public int getShareFileCntWithShareNo(long deviceNo, long shareNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareFileCntWithShareNo...");
            return SaaringNative.getShareFileCntWithShareNo(deviceNo, shareNo);
        }
        @Override
        public long getShareFileSize(long deviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareFileSize...");
            return SaaringNative.getShareFileSize(deviceNo);
        }
        @Override
        public long getShareFileSizeWithShareNo(long deviceNo, long shareNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareFileSizeWithShareNo...");
            return SaaringNative.getShareFileSizeWithShareNo(deviceNo, shareNo);
        }

        //////////////////////////////////////////////////////////////////////////////////
        // for share detail
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean updateShareDetailShortPath(long shareNo, String shareName, String shareFolderFullPath)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateShareDetailShortPath...");
            return SaaringNative.updateShareDetailShortPath(shareNo, shareName, shareFolderFullPath);
        }
        @Override
        public boolean deleteShareDetailTb(long deviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteShareDetailTb...");
            return SaaringNative.deleteShareDetailTb(deviceNo);
        }

        @Override
        public void makeMemoryShareDetailTb()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService makeMemoryShareDetailTb...");
            SaaringNative.makeMemoryShareDetailTb();
        }
        @Override
        public void backupMemoryShareDetailTb()
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService backupMemoryShareDetailTb...");
            SaaringNative.backupMemoryShareDetailTb();
        }

        //////////////////////////////////////////////////////////////////////////////////
        // for invite files
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean insertInviteFiles(InviteFilesInfo saaringInviteFilesInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService insertInviteFiles...");
            return SaaringNative.insertInviteFiles(saaringInviteFilesInfo);
        }

        @Override
        public boolean insertListInviteFiles(List<InviteFilesInfo> listSaaringInviteFilesInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService insertListInviteFiles...");
            return SaaringNative.insertListInviteFiles(listSaaringInviteFilesInfo);
        }

        @Override
        public void deleteExpired()
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteExpired...");
            SaaringNative.deleteExpired();
        }

        @Override
        public void deleteInviteFilesByInviteNo(long inviteNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteInviteFilesByInviteNo...");
            SaaringNative.deleteInviteFilesByInviteNo(inviteNo);
        }

        @Override
        public boolean listInviteFiles(long inviteNo, List<InviteFilesInfo> listSaaringInviteFilesInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listInviteFiles...");
            return SaaringNative.listInviteFiles(inviteNo, listSaaringInviteFilesInfo);
        }

        @Override
        public boolean listInviteFilesWithCertKey(long inviteNo, String certKey, List<InviteFilesInfo> listSaaringInviteFilesInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listInviteFilesWithCertKey...");
            return SaaringNative.listInviteFilesWithCertKey(inviteNo, certKey, listSaaringInviteFilesInfo);
        }

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring p2p jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public void initP2p()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService initP2p...");
            SaaringNative.initP2p();
        }
        @Override
        public void closeP2p()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService closeP2p...");
            SaaringNative.closeP2p();
        }
        @Override
        public boolean isP2p()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService isP2p...");
            return SaaringNative.isP2p();
        }

        @Override
        public boolean isSignalingConnected()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService isSignalingConnected...");
            return SaaringNative.isSignalingConnected();
        }
        @Override
        public boolean isSignalingLogin()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService isSignalingLogin...");
            return SaaringNative.isSignalingLogin();
        }
        @Override
        public boolean doSignalingConnect(String authKey, boolean isRefreshServerInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doSignalingConnect...");
            return SaaringNative.doSignalingConnect(authKey, isRefreshServerInfo);
        }
        @Override
        public void doSignalingClose()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doSignalingClose...");
            SaaringNative.doSignalingClose();
        }
        @Override
        public void reqHeartBeat()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService reqHeartBeat...");
            SaaringNative.reqHeartBeat();
        }

        @Override
        public ServerInfo getServerInfo()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getServerInfo...");
            return SaaringNative.getServerInfo();
        }
        @Override
        public boolean isP2pLived(long deviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService isP2pLived...");
            return SaaringNative.isP2pLived(deviceNo);
        }

        @Override
        public void updateShareFolders()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateShareFolders...");
            SaaringNative.updateShareFolders();
        }
        @Override
        public void updateNetworkSendSettings()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateNetworkSendSettings...");
            SaaringNative.updateNetworkSendSettings();
        }

        @Override
        public void doP2pConnect(long toUserNo, String toUserName,
									long toDeviceNo, int toDeviceType, String toDeviceName,
									boolean mustWakeUp)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pConnect...");
            SaaringNative.doP2pConnect(toUserNo, toUserName,
            							toDeviceNo, toDeviceType, toDeviceName,
            							mustWakeUp);
        }
        @Override
        public void doP2pConnectShareFolder(long toUserNo, String toUserName,
												long toDeviceNo, int toDeviceType, String toDeviceName,
												long toServerFolderNo, String toFolderName, boolean mustWakeUp)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pConnectShareFolder...");
            SaaringNative.doP2pConnectShareFolder(toUserNo, toUserName,
                                                  toDeviceNo, toDeviceType, toDeviceName,
                                                  toServerFolderNo, toFolderName, mustWakeUp);
        }
        @Override
        public void doP2pConnectCancel(long toUserNo, String toUserName,
												long toDeviceNo, int toDeviceType, String toDeviceName)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pConnectCancel...");
            SaaringNative.doP2pConnectCancel(toUserNo, toUserName,
            									toDeviceNo, toDeviceType, toDeviceName);
        }

        @Override
        public long doP2pFolderList(long toDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pFolderList...");
            return SaaringNative.doP2pFolderList(toDeviceNo);
        }
        @Override
        public long doP2pFolderDetail(long toDeviceNo, long shareNo, long serverFolderNo, String remotePath)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pFolderDetail...");
            return SaaringNative.doP2pFolderDetail(toDeviceNo, shareNo, serverFolderNo, remotePath);
        }
        @Override
        public long doP2pFolderDetailRecursive(long toDeviceNo, long shareNo, String remotePath)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pFolderDetailRecursive...");
            return SaaringNative.doP2pFolderDetailRecursive(toDeviceNo, shareNo, remotePath);
        }

        @Override
        public long doP2pUploadQuery(long toDeviceNo, String remoteFilePath, long fileSize, long myTransNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pUploadQuery..." + "toDeviceNo:" + toDeviceNo + "remoteFilePath:" + remoteFilePath +  "fileSize:" + fileSize + "myTransNo:" + myTransNo);
            return SaaringNative.doP2pUploadQuery(toDeviceNo, remoteFilePath, fileSize, myTransNo);
        }
        @Override
        public long doP2pUpload(long toDeviceNo, String remoteFilePath, String localFilePath,
                         long fileStartPosition, long myTransNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pUpload...");
            return SaaringNative.doP2pUpload(toDeviceNo, remoteFilePath, localFilePath,
                                             fileStartPosition, myTransNo);

        }
        @Override
        public long doP2pDownload(long toDeviceNo, String remoteFilePath, String localFilePath,
                                  long fileSize, long fileStartPosition, long myTransNo, int folderDepth,
                                  long inviteNo, long remoteFileId)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pDownload...");
            return SaaringNative.doP2pDownload(toDeviceNo, remoteFilePath, localFilePath,
                                               fileSize, fileStartPosition, myTransNo, folderDepth,
                                               inviteNo, remoteFileId);
        }
        @Override
        public long doP2pDownloadInviteSendFilesQuery(long toDeviceNo, long inviteNo, String certKey)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pDownload...");
            return SaaringNative.doP2pDownloadInviteSendFilesQuery(toDeviceNo, inviteNo, certKey);
        }
        @Override
        public long doP2pSaveSendFilesInviteInfo(long toDeviceNo,
                                                 long inviteNo, String inviteCode, String dynamicLink,
                                                 List<String> listShortPathName, List<String> listFileName, long[] arrayFileSize,
                                                 String certKey, String inviteMessage)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pSaveSendFilesInviteInfo...");

            List<Long> listFileSize = new ArrayList();
            for(long data : arrayFileSize)
            {
                listFileSize.add(data);
            }

            return SaaringNative.doP2pSaveSendFilesInviteInfo(toDeviceNo, inviteNo, inviteCode, dynamicLink,
                                                              listShortPathName, listFileName, listFileSize,
                                                              certKey, inviteMessage);
        }

        @Override
        public long doP2pProcessDownloadComplete(long toDeviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pProcessDownloadComplete...");
            return SaaringNative.doP2pProcessDownloadComplete(toDeviceNo);
        }
        @Override
        public long doP2pProcessUploadComplete(long toDeviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pProcessUploadComplete...");
            return SaaringNative.doP2pProcessUploadComplete(toDeviceNo);
        }

        @Override
        public long doP2pProcessDownloadQueueCheck(long toDeviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pProcessDownloadQueueCheck...");
            return SaaringNative.doP2pProcessDownloadQueueCheck(toDeviceNo);
        }
        @Override
        public long doP2pProcessUploadQueueCheck(long toDeviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pProcessUploadQueueCheck...");
            return SaaringNative.doP2pProcessUploadQueueCheck(toDeviceNo);
        }

        @Override
        public void doP2pOpenAllChannel(long[] arrayDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pOpenAllChannel...");

            List<Long> listDeviceNo = new ArrayList();
            for(long data : arrayDeviceNo)
            {
                listDeviceNo.add(data);
            }

            SaaringNative.doP2pOpenAllChannel(listDeviceNo);
        }
        @Override
        public void doP2pCloseChannel(long remoteDeviceNo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pCloseChannel...");
            SaaringNative.doP2pCloseChannel(remoteDeviceNo);
        }
        @Override
        public long doP2pStopAllTransfer(long[] arrayDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pStopAllTransfer...");

            List<Long> listDeviceNo = new ArrayList();
            for(long data : arrayDeviceNo)
            {
                listDeviceNo.add(data);
            }

            return SaaringNative.doP2pStopAllTransfer(listDeviceNo);
        }
        @Override
        public long doP2pCancelAllTransfer(long[] arrayDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doP2pCancelAllTransfer...");

            List<Long> listDeviceNo = new ArrayList();
            for(long data : arrayDeviceNo)
            {
                listDeviceNo.add(data);
            }

            return SaaringNative.doP2pCancelAllTransfer(listDeviceNo);
        }

        @Override
        public void listAllChannelDeviceNo(long[] arrayDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listAllChannelDeviceNo...");

            List<Long> listDeviceNo = new ArrayList();
            for(long data : arrayDeviceNo)
            {
                listDeviceNo.add(data);
            }

            SaaringNative.listAllChannelDeviceNo(listDeviceNo);
        }
        @Override
        public void updateNumLiveTransfer(int numLiveTransfer)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService updateNumLiveTransfer...");
            SaaringNative.updateNumLiveTransfer(numLiveTransfer);
        }

        @Override
        public void startRelayServer()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService startRelayServer...");
            SaaringNative.startRelayServer();
        }
        @Override
        public void stopRelayServer()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService stopRelayServer...");
            SaaringNative.stopRelayServer();
        }

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring share folder jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public void startThreadShareFolder()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService startThreadShareFolder...");
            SaaringNative.startThreadShareFolder();
        }
        @Override
        public void stopThreadShareFolder()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService stopThreadShareFolder...");
            SaaringNative.stopThreadShareFolder();
        }

        @Override
        public void shareFolderInit()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService shareFolderInit...");
            SaaringNative.shareFolderInit();
        }
        @Override
        public void shareFolderInsert(long shareNo, String sharePath, String shareName)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService shareFolderInsert...");
            SaaringNative.shareFolderInsert(shareNo, sharePath, shareName);
        }
        @Override
        public void shareFolderDelete(long shareNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService shareFolderDelete...");
            SaaringNative.shareFolderDelete(shareNo);
        }
        @Override
        public void shareFolderUpdate(String folderPath)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService shareFolderUpdate...");
            SaaringNative.shareFolderUpdate(folderPath);
        }
        @Override
        public void serverFolderDelete(long serverFolderNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService serverFolderDelete...");
            SaaringNative.serverFolderDelete(serverFolderNo);
        }

        @Override
        public int getShareFolderCount()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareFolderCount...");
            return SaaringNative.getShareFolderCount();
        }
        @Override
        public int getShareFileCount()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getShareFileCount...");
            return SaaringNative.getShareFileCount();
        }

        @Override
        public void startFileMonitor()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService startFileMonitor...");
            SaaringNative.startFileMonitor();
        }
        @Override
        public void stopFileMonitor()
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService stopFileMonitor...");
            SaaringNative.stopFileMonitor();
        }

        @Override
        public void doesNeedDbBackup()
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doesNeedDbBackup...");
            SaaringNative.doesNeedDbBackup();
        }

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring web jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public boolean listFolderOfOther(String authKey, long friendUserNo,
                                  StringData resultCode, StringData resultMessage, StringData resultData,
                                  List<ShareFolderInfo> listShareFolderInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listFolderOfOther...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.listFolderOfOther(authKey, friendUserNo,
                                                             paramResultCode, paramResultMessage, paramResultData,
                                                             listShareFolderInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean listLiveDevice(String authKey, String deviceNos,
                               StringData resultCode, StringData resultMessage, StringData resultData,
                               List<LongData> listLiveDeviceNo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listLiveDevice...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            List<Long> paramListLiveDeviceNo    = new ArrayList();

            boolean result = SaaringNative.listLiveDevice(authKey, deviceNos,
                                                          paramResultCode, paramResultMessage, paramResultData,
                                                          paramListLiveDeviceNo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            for(Long data : paramListLiveDeviceNo)
            {
                listLiveDeviceNo.add(new LongData(data.longValue()));
            }

            return result;
        }
        @Override
        public boolean getBadgeInfo(String authKey,
                             StringData resultCode, StringData resultMessage, StringData resultData,
                             BadgeInfo saaringBadgeInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getBadgeInfo...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.getBadgeInfo(authKey,
                                                        paramResultCode, paramResultMessage, paramResultData,
                                                        saaringBadgeInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        @Override
        public boolean listFriend(String authKey,
                           StringData resultCode, StringData resultMessage, StringData resultData,
                           List<FriendInfo> listFriendInfo)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listFriend...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

             boolean result = SaaringNative.listFriend(authKey,
                                                       paramResultCode, paramResultMessage, paramResultData,
                                                       listFriendInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            System.out.println("===========================>>> SaaringService listFriend resultData" + resultData.getData());
            System.out.println("===========================>>> SaaringService listFriend listFriendInfo Name" + listFriendInfo.get(0).getUserName());
            System.out.println("===========================>>> SaaringService listFriend listFriendInfo RequestStatus" + listFriendInfo.get(0).getRequestStatus());

            return result;
        }
        @Override
        public boolean requestFriendByUserNo(String authKey, long toUserNo,
                                             StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService requestFriendByUserNo...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.requestFriendByUserNo(authKey, toUserNo, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean listRequestingFriend(String authKey,
                                            StringData resultCode, StringData resultMessage, StringData resultData,
                                            List<FriendInfo> listIncomingRequestFriend,
                                            List<FriendInfo> listRequestingFriendMember,
                                            List<FriendInfo> listRequestingFriendNonMember)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listRequestingFriend...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.listRequestingFriend(authKey,
                    paramResultCode, paramResultMessage, paramResultData,
                    listIncomingRequestFriend,
                    listRequestingFriendMember,
                    listRequestingFriendNonMember);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean cancelRequestFriend(String authKey, long toUserNo, String toEmail, String toPhoneNumber,
                                           StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService requestFriendByUserNo...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.cancelRequestFriend(authKey, toUserNo, toEmail, toPhoneNumber, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean rejectRequestFriend(String authKey, long toUserNo,
                                           StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService requestFriendByUserNo...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.rejectRequestFriend(authKey, toUserNo, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean recoverRequestFriend(String authKey, long toUserNo,
                                           StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService requestFriendByUserNo...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.recoverRequestFriend(authKey, toUserNo, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean addFriend(String authKey, long friendUserNo,
                                 StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService addFriend...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.addFriend(authKey, friendUserNo, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean blockFriend(String authKey, String friendUserNos, String blockYN,
                                   StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService blockFriend...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.blockFriend(authKey, friendUserNos, blockYN, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean listBlock(String authKey,
                                 StringData resultCode, StringData resultMessage, StringData resultData,
                                 List<BlockInfo> listBlocked, List<BlockInfo> listRejected)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listBlock...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.listBlock(authKey,
                                                     paramResultCode, paramResultMessage, paramResultData,
                                                     listBlocked, listRejected);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean inviteFriendSimple(String authKey,
                                          String certKey, String inviteMessage,
                                          StringData resultCode, StringData resultMessage, StringData resultData,
                                          LongData inviteNo, StringData inviteCode, StringData dynamicLink)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService inviteFriendSimple...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            StringBuffer paramInviteNo          = new StringBuffer(String.valueOf(inviteNo.getData()));
            StringBuffer paramInviteCode        = new StringBuffer(inviteCode.getData());
            StringBuffer paramDynamicLink       = new StringBuffer(dynamicLink.getData());

            boolean result = SaaringNative.inviteFriendSimple(authKey, certKey, inviteMessage,
                                                              paramResultCode, paramResultMessage, paramResultData,
                                                              paramInviteNo, paramInviteCode, paramDynamicLink);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            inviteNo.setData(           Long.parseLong(paramInviteNo.toString()));
            inviteCode.setData(         paramInviteCode.toString());
            dynamicLink.setData(        paramDynamicLink.toString());

            return result;
        }
        @Override
        public boolean inviteFriend(String authKey,
                                    List<LongData> listToUserNo, List<String> listToUserName,
                                    List<String> listToEmail, List<String> listToPhoneNumber,
                                    String certKey, String inviteMessage,
                                    StringData resultCode, StringData resultMessage, StringData resultData,
                                    LongData inviteNo, StringData inviteCode, StringData dynamicLink,
                                    List<LongData> listFailUserNo,
                                    List<String> listSuccessEmail,
                                    List<FriendInfo> listAddedFriend,
                                    List<FriendInfo> listIncomingRequestFriend,
                                    List<FriendInfo> listRequestingFriendMember,
                                    List<FriendInfo> listRequestingFriendNonMember)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService inviteFriend...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            List<Long> paramListToUserNo        = new ArrayList<>();
            for(LongData data : listToUserNo)
            {
                paramListToUserNo.add(data.getData());
            }
            List<Long> paramListFailUserNo      = new ArrayList<>();

            StringBuffer paramInviteNo          = new StringBuffer(String.valueOf(inviteNo.getData()));
            StringBuffer paramInviteCode        = new StringBuffer(inviteCode.getData());
            StringBuffer paramDynamicLink       = new StringBuffer(dynamicLink.getData());

            boolean result = SaaringNative.inviteFriend(authKey, paramListToUserNo, listToUserName, listToEmail, listToPhoneNumber,
                                                        certKey, inviteMessage,
                                                        paramResultCode, paramResultMessage, paramResultData,
                                                        paramInviteNo, paramInviteCode, paramDynamicLink,
                                                        paramListFailUserNo,
                                                        listSuccessEmail,
                                                        listAddedFriend,
                                                        listIncomingRequestFriend,
                                                        listRequestingFriendMember,
                                                        listRequestingFriendNonMember);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            for(Long data : paramListFailUserNo)
            {
                listFailUserNo.add(new LongData(data.longValue()));
            }

            inviteNo.setData(           Long.parseLong(paramInviteNo.toString()));
            inviteCode.setData(         paramInviteCode.toString());
            dynamicLink.setData(        paramDynamicLink.toString());

            return result;
        }
        @Override
        public boolean cancelInviteFriend(String authKey, long inviteNo,
                                          StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService cancelInviteFriend...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.cancelInviteFriend(authKey, inviteNo,
                                                              paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean completeInviteFriend(String authKey, long inviteNo,
                                            StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService completeInviteFriend...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.completeInviteFriend(authKey, inviteNo,
                                                                paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean extendInviteExpiredt(String authKey, long inviteNo,
                                            StringData resultCode, StringData resultMessage, StringData resultData,
                                            InviteInfo inviteInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService extendInviteExpiredt...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.extendInviteExpiredt(authKey, inviteNo,
                                                                paramResultCode, paramResultMessage, paramResultData,
                                                                inviteInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean getInvite(String authKey, long inviteNo,
                                 StringData resultCode, StringData resultMessage, StringData resultData,
                                 InviteInfo inviteInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getInvite...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.getInvite(authKey, inviteNo, paramResultCode, paramResultMessage, paramResultData, inviteInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean getInviteByDynamicLink(String dynamicLink, String certKey,
                                              StringData resultCode, StringData resultMessage, StringData resultData,
                                              InviteInfo inviteInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getInviteByDynamicLink...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.getInviteByDynamicLink(dynamicLink, certKey, paramResultCode, paramResultMessage, paramResultData, inviteInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        @Override
        public boolean listInviteAll(String authKey,
                                     StringData resultCode, StringData resultMessage, StringData resultData,
                                     List<InviteInfo> listInviteInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listInviteAll...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.listInviteAll(authKey, paramResultCode, paramResultMessage, paramResultData, listInviteInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        @Override
        public boolean inviteShareFolderSimple(String authKey,
                                               long inviteShareFolderServerFolderNo,
                                               String certKey, String inviteMessage,
                                               StringData resultCode, StringData resultMessage, StringData resultData,
                                               LongData inviteNo, StringData inviteCode, StringData dynamicLink)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService inviteShareFolderSimple...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            StringBuffer paramInviteNo          = new StringBuffer(String.valueOf(inviteNo.getData()));
            StringBuffer paramInviteCode        = new StringBuffer(inviteCode.getData());
            StringBuffer paramDynamicLink       = new StringBuffer(dynamicLink.getData());

            boolean result = SaaringNative.inviteShareFolderSimple(authKey, inviteShareFolderServerFolderNo, certKey, inviteMessage,
                                                                   paramResultCode, paramResultMessage, paramResultData,
                                                                   paramInviteNo, paramInviteCode, paramDynamicLink);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            inviteNo.setData(           Long.parseLong(paramInviteNo.toString()));
            inviteCode.setData(         paramInviteCode.toString());
            dynamicLink.setData(        paramDynamicLink.toString());

            return result;
        }
        @Override
        public boolean inviteSendFilesSimple(String authKey,
                                             long numFile, long totalFileSize,
                                             String certKey, String inviteMessage,
                                             StringData resultCode, StringData resultMessage, StringData resultData,
                                             LongData inviteNo, StringData inviteCode, StringData dynamicLink)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService inviteSendFilesSimple...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            StringBuffer paramInviteNo          = new StringBuffer(String.valueOf(inviteNo.getData()));
            StringBuffer paramInviteCode        = new StringBuffer(inviteCode.getData());
            StringBuffer paramDynamicLink       = new StringBuffer(dynamicLink.getData());

            boolean result = SaaringNative.inviteSendFilesSimple(authKey,
                                                                 numFile, totalFileSize,
                                                                 certKey, inviteMessage,
                                                                 paramResultCode, paramResultMessage, paramResultData,
                                                                 paramInviteNo, paramInviteCode, paramDynamicLink);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            inviteNo.setData(           Long.parseLong(paramInviteNo.toString()));
            inviteCode.setData(         paramInviteCode.toString());
            dynamicLink.setData(        paramDynamicLink.toString());

            return result;
        }
        @Override
        public boolean inviteSendFilesForRemoteDeviceSimple(String authKey,
                                             long numFile, long totalFileSize,
                                             long inviteShareFolderServerFolderNo, String inviteShareFolderPath,
                                             String certKey, String inviteMessage,
                                             StringData resultCode, StringData resultMessage, StringData resultData,
                                             LongData inviteNo, StringData inviteCode, StringData dynamicLink)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService inviteSendFilesForRemoteDeviceSimple...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            StringBuffer paramInviteNo          = new StringBuffer(String.valueOf(inviteNo.getData()));
            StringBuffer paramInviteCode        = new StringBuffer(inviteCode.getData());
            StringBuffer paramDynamicLink       = new StringBuffer(dynamicLink.getData());

            boolean result = SaaringNative.inviteSendFilesForRemoteDeviceSimple(authKey,
                    numFile, totalFileSize,
                    inviteShareFolderServerFolderNo, inviteShareFolderPath,
                    certKey, inviteMessage,
                    paramResultCode, paramResultMessage, paramResultData,
                    paramInviteNo, paramInviteCode, paramDynamicLink);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            inviteNo.setData(           Long.parseLong(paramInviteNo.toString()));
            inviteCode.setData(         paramInviteCode.toString());
            dynamicLink.setData(        paramDynamicLink.toString());

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean registerInviteByDynamicLink(String authKey,
                                                   String dynamicLink, String certKey,
                                                   StringData resultCode, StringData resultMessage, StringData resultData,
                                                   InviteRegisteredInfo inviteRegisteredInfo,
                                                   InviteInfo inviteInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService registerInviteByDynamicLink...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.registerInviteByDynamicLink(authKey, dynamicLink, certKey,
                                                                       paramResultCode, paramResultMessage, paramResultData,
                                                                       inviteRegisteredInfo, inviteInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean deleteInviteRegistered(String authKey, long inviteNo,
                                              StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteInviteRegistered...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.deleteInviteRegistered(authKey, inviteNo, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean getInviteRegistered(String authKey, long inviteNo,
                                           StringData resultCode, StringData resultMessage, StringData resultData,
                                           InviteRegisteredInfo inviteRegisteredInfo,
                                           InviteInfo inviteInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService getInviteRegistered...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.getInviteRegistered(authKey, inviteNo,
                                                               paramResultCode, paramResultMessage, paramResultData,
                                                               inviteRegisteredInfo, inviteInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean listInviteRegisteredAll(String authKey,
                                               StringData resultCode, StringData resultMessage, StringData resultData,
                                               List<InviteRegisteredInfo> listInviteRegisteredInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listInviteRegisteredAll...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.listInviteRegisteredAll(authKey, paramResultCode, paramResultMessage, paramResultData, listInviteRegisteredInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean verifyFirebaseToken(String firebaseToken,
                                           StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService verifyFirebaseToken...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.verifyFirebaseToken(firebaseToken,
                                                                paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean joinMember(MemberInfo memberInfo,
                                  StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService joinMember...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.joinMember(memberInfo,
                                                      paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean loginMember(MemberInfo memberInfo,
                                   StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService loginMember...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.loginMember(memberInfo,
                                                       paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean doMe(String authKey, int deviceType,
                            StringData resultCode, StringData resultMessage, StringData resultData,
                            MemberInfo memberInfo, List<MemberProviderInfo> listMemberProviderInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService loginMember...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.doMe(authKey, deviceType, paramResultCode, paramResultMessage, paramResultData, memberInfo, listMemberProviderInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        // isEmailExist = "true" or "false"
        @Override
        public boolean isEmailExist(String email,
                                    StringData resultCode, StringData resultMessage, StringData resultData,
                                    StringData isEmailExist)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService isEmailExist...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());
            StringBuffer paramIsEmailExist      = new StringBuffer(isEmailExist.getData());

            boolean result = SaaringNative.isEmailExist(email,
                                                        paramResultCode, paramResultMessage, paramResultData,
                                                        paramIsEmailExist);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());
            isEmailExist.setData(       paramIsEmailExist.toString());

            return result;
        }
        @Override
        public boolean doUnlink(String authKey,
                                StringData resultCode, StringData resultMessage, StringData resultData)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doLogout...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.doUnlink(authKey, paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean doLogout(String authKey,
                                StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService doLogout...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.doLogout(authKey,
                                                    paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean searchUser(String authKey, String keyword,
                                  StringData resultCode, StringData resultMessage, StringData resultData,
                                  List<FriendInfo> listFriendInfo)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService searchUser...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.searchUser(authKey, keyword,
                    paramResultCode, paramResultMessage, paramResultData,
                    listFriendInfo);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean changeProfilePic(String authKey, String profilePic,
                                        StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService changeProfilePic...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.changeProfilePic(authKey, profilePic,
                                                            paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean changeEmail(String authKey, String email,
                                   StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService changeEmail...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.changeEmail(authKey, email,
                                                       paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean changeUserName(String authKey, String userName,
                                      StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService changeUserName...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.changeUserName(authKey, userName,
                                                          paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        @Override
        public boolean changeDeviceName(String authKey, String deviceName,
                                        StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService changeDeviceName...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.changeDeviceName(authKey, deviceName,
                                                            paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        @Override
        public boolean deleteFolder(String authKey, long serverFolderNo,
                                    StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService deleteFolder...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.deleteFolder(authKey, serverFolderNo,
                                                        paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean addOrUpdateFolder(String authKey, ShareFolderInfo saaringShareFolder,
                                         StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService addOrUpdateFolder...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.addOrUpdateFolder(authKey, saaringShareFolder,
                                                             paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean listFolderAll(String authKey,
                                     StringData resultCode, StringData resultMessage, StringData resultData,
                                     List<ShareFolderInfo> listFolder)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listFolderAll...");
            System.out.println("===========================>>> SaaringService authKey="+authKey);

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.listFolderAll(authKey,
                                                         paramResultCode, paramResultMessage, paramResultData,
                                                         listFolder);

            System.out.println("===========================>>> SaaringService paramResultCode="+paramResultCode);
            System.out.println("===========================>>> SaaringService paramResultMessage="+paramResultMessage);
            System.out.println("===========================>>> SaaringService paramResultData="+paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }
        @Override
        public boolean checkFolderPassword(String authKey, long serverFolderNo, String password,
                                           StringData resultCode, StringData resultMessage, StringData resultData)
            throws RemoteException
        {
            System.out.println("===========================>>> SaaringService checkFolderPassword...");

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.checkFolderPassword(authKey, serverFolderNo, password,
                                                               paramResultCode, paramResultMessage, paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        @Override
        public boolean addPhoneAddress(String authKey, List<String> listPhoneNumber,
                                       StringData resultCode, StringData resultMessage, StringData resultData,
                                       List<PhoneAddressInfo> listPhoneAddress)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService addPhoneAddress...");
            System.out.println("===========================>>> SaaringService authKey="+authKey);

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.addPhoneAddress(authKey, listPhoneNumber,
                    paramResultCode, paramResultMessage, paramResultData,
                    listPhoneAddress);

            System.out.println("===========================>>> SaaringService paramResultCode="+paramResultCode);
            System.out.println("===========================>>> SaaringService paramResultMessage="+paramResultMessage);
            System.out.println("===========================>>> SaaringService paramResultData="+paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        @Override
        public boolean listPhoneAddress(String authKey,
                                        StringData resultCode, StringData resultMessage, StringData resultData,
                                        List<PhoneAddressInfo> listPhoneAddress)
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService listPhoneAddress...");
            System.out.println("===========================>>> SaaringService authKey="+authKey);

            StringBuffer paramResultCode        = new StringBuffer(resultCode.getData());
            StringBuffer paramResultMessage     = new StringBuffer(resultMessage.getData());
            StringBuffer paramResultData        = new StringBuffer(resultData.getData());

            boolean result = SaaringNative.listPhoneAddress(authKey,
                    paramResultCode, paramResultMessage, paramResultData,
                    listPhoneAddress);

            System.out.println("===========================>>> SaaringService paramResultCode="+paramResultCode);
            System.out.println("===========================>>> SaaringService paramResultMessage="+paramResultMessage);
            System.out.println("===========================>>> SaaringService paramResultData="+paramResultData);

            resultCode.setData(         paramResultCode.toString());
            resultMessage.setData(      paramResultMessage.toString());
            resultData.setData(         paramResultData.toString());

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring.cpp jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        @Override
        public void test2()
                throws RemoteException
        {
            System.out.println("===========================>>> SaaringService test2...");
            SaaringNative.test2();
        }
    };

}
