package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class MemberProviderInfo implements Parcelable
{
    long                    userNo;

    int                     idType;
    String                  providerId          = "";
    String                  idToken             = "";
    String                  accessToken         = "";
    String                  uid                 = "";
    String                  providerYN          = "";

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////

    public static final Creator<MemberProviderInfo> CREATOR = new Creator<MemberProviderInfo>() {
        @Override
        public MemberProviderInfo createFromParcel(Parcel source) {
            return new MemberProviderInfo(source);
        }

        @Override
        public MemberProviderInfo[] newArray(int size) {
            return new MemberProviderInfo[size];
        }
    };

    public MemberProviderInfo() {
    }

    protected MemberProviderInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(                 this.userNo);
        dest.writeInt(                  this.idType);
        dest.writeString(               this.providerId);
        dest.writeString(               this.accessToken);
        dest.writeString(               this.idToken);
        dest.writeString(               this.uid);
        dest.writeString(               this.providerYN);
    }

    public void readFromParcel(Parcel in) {
        this.userNo                     = in.readLong();
        this.idType                     = in.readInt();
        this.providerId                 = in.readString();
        this.accessToken                = in.readString();
        this.idToken                    = in.readString();
        this.uid                        = in.readString();
        this.providerYN                 = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////

    public long getUserNo() {
        return userNo;
    }

    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getProviderYN() {
        return providerYN;
    }

    public void setProviderYN(String providerYN) {
        this.providerYN = providerYN;
    }
}
