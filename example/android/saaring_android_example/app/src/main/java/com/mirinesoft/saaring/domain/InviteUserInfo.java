package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class InviteUserInfo implements Parcelable
{
    long                    inviteNo;
    long                    userNo;
    long                    toUserNo;

    String                  toEmail           = "";
    String                  toPhoneNumber     = "";
    String                  regdt             = "";
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<InviteUserInfo> CREATOR = new Creator<InviteUserInfo>() {
        @Override
        public InviteUserInfo createFromParcel(Parcel source) {
            return new InviteUserInfo(source);
        }

        @Override
        public InviteUserInfo[] newArray(int size) {
            return new InviteUserInfo[size];
        }
    };

    public InviteUserInfo() {
    }

    protected InviteUserInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(         this.inviteNo);
        dest.writeLong(         this.userNo);
        dest.writeLong(         this.toUserNo);
        dest.writeString(       this.toEmail);
        dest.writeString(       this.toPhoneNumber);
        dest.writeString(       this.regdt);
    }

    public void readFromParcel(Parcel in) {
        this.inviteNo             = in.readLong();
        this.userNo               = in.readLong();
        this.toUserNo             = in.readLong();
        this.toEmail              = in.readString();
        this.toPhoneNumber        = in.readString();
        this.regdt                = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getInviteNo() {
        return inviteNo;
    }
    public void setInviteNo(long inviteNo) {
        this.inviteNo = inviteNo;
    }
    public long getUserNo() {
        return userNo;
    }
    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }
    public long getToUserNo() {
        return toUserNo;
    }
    public void setToUserNo(long toUserNo) {
        this.toUserNo = toUserNo;
    }
    public String getToEmail() {
        return toEmail;
    }
    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }
    public String getToPhoneNumber() {
        return toPhoneNumber;
    }
    public void setToPhoneNumber(String toPhoneNumber) {
        this.toPhoneNumber = toPhoneNumber;
    }
    public String getRegdt() {
        return regdt;
    }
    public void setRegdt(String regdt) {
        this.regdt = regdt;
    }
}
