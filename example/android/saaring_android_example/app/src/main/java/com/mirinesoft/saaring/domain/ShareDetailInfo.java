package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class ShareDetailInfo implements Parcelable
{
    long                    shareDetailNo;
    long                    deviceNo;
    long                    shareNo;

    String                  fullPathName                = "";
    String                  shortPathName               = "";

    String                  fileName                    = "";
    String                  fileExtension               = "";
    long                    fileSize;

    String                  updateTime                  = "";
    String                  folderYN                    = "";

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<ShareDetailInfo> CREATOR = new Creator<ShareDetailInfo>() {
        @Override
        public ShareDetailInfo createFromParcel(Parcel source) {
            return new ShareDetailInfo(source);
        }

        @Override
        public ShareDetailInfo[] newArray(int size) {
            return new ShareDetailInfo[size];
        }
    };

    public ShareDetailInfo() {
    }

    protected ShareDetailInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(                 this.shareDetailNo);
        dest.writeLong(                 this.deviceNo);
        dest.writeLong(                 this.shareNo);

        dest.writeString(               this.fullPathName);
        dest.writeString(               this.shortPathName);

        dest.writeString(               this.fileName);
        dest.writeString(               this.fileExtension);
        dest.writeLong(                 this.fileSize);

        dest.writeString(               this.updateTime);
        dest.writeString(               this.folderYN);
    }

    public void readFromParcel(Parcel in) {
        this.shareDetailNo              = in.readLong();
        this.deviceNo                   = in.readLong();
        this.shareNo                    = in.readLong();

        this.fullPathName               = in.readString();
        this.shortPathName              = in.readString();

        this.fileName                   = in.readString();
        this.fileExtension              = in.readString();
        this.fileSize                   = in.readLong();

        this.updateTime                 = in.readString();
        this.folderYN                   = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getShareDetailNo() {
        return shareDetailNo;
    }
    public void setShareDetailNo(long shareDetailNo) {
        this.shareDetailNo = shareDetailNo;
    }
    public long getDeviceNo() {
        return deviceNo;
    }
    public void setDeviceNo(long deviceNo) {
        this.deviceNo = deviceNo;
    }
    public long getShareNo() {
        return shareNo;
    }
    public void setShareNo(long shareNo) {
        this.shareNo = shareNo;
    }

    public String getFullPathName() {
        return fullPathName;
    }
    public void setFullPathName(String fullPathName) {
        this.fullPathName = fullPathName;
    }
    public String getShortPathName() {
        return shortPathName;
    }
    public void setShortPathName(String shortPathName) {
        this.shortPathName = shortPathName;
    }

    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileExtension() {
        return fileExtension;
    }
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }
    public long getFileSize() {
        return fileSize;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    public String getFolderYN() {
        return folderYN;
    }
    public void setFolderYN(String folderYN) {
        this.folderYN = folderYN;
    }
}
