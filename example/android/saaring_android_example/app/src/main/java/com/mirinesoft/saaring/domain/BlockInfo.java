package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class BlockInfo implements Parcelable
{
    long                    userNo;
    String                  email           = "";
    String                  phoneNumber     = "";
    int                     idType;
    String                  userId          = "";
    String                  userName        = "";
    String                  profilePic      = "";
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<BlockInfo> CREATOR = new Creator<BlockInfo>() {
        @Override
        public BlockInfo createFromParcel(Parcel source) {
            return new BlockInfo(source);
        }

        @Override
        public BlockInfo[] newArray(int size) {
            return new BlockInfo[size];
        }
    };

    public BlockInfo() {
    }

    protected BlockInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(         this.userNo);
        dest.writeString(       this.email);
        dest.writeString(       this.phoneNumber);
        dest.writeInt(          this.idType);
        dest.writeString(       this.userId);
        dest.writeString(       this.userName);
        dest.writeString(       this.profilePic);
    }

    public void readFromParcel(Parcel in) {
        this.userNo             = in.readLong();
        this.email              = in.readString();
        this.phoneNumber        = in.readString();
        this.idType             = in.readInt();
        this.userId             = in.readString();
        this.userName           = in.readString();
        this.profilePic         = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getUserNo() {
        return userNo;
    }
    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public int getIdType() {
        return idType;
    }
    public void setIdType(int idType) {
        this.idType = idType;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getProfilePic() {
        return profilePic;
    }
    public void setProfilePic(String userPic) {
        this.profilePic = profilePic;
    }
}
