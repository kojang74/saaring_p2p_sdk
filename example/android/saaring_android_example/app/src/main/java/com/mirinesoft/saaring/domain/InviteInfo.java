package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class InviteInfo implements Parcelable
{
    long                    inviteNo;
    String                  inviteCode              = "";

    long                    userNo;
    String                  userName                = "";

    long                    inviteDeviceNo;
    int                     inviteDeviceType;
    String                  inviteDeviceName        = "";
    int                     inviteType;

    String                  certKey                 = "";
    long                    numFile;
    long                    totalFileSize;

    long                    inviteShareFolderServerFolderNo;
    String                  inviteShareFolderName           = "";
    String                  inviteShareFolderPath           = "";
    String                  inviteShareFolderPasswordYN     = "N";

    String                  inviteMessage           = "";

    String                  dynamicLink             = "";
    String                  rejectYN                = "N";

    String                  expiredt                = "";
    String                  regdt                   = "";


    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<InviteInfo> CREATOR = new Creator<InviteInfo>() {
        @Override
        public InviteInfo createFromParcel(Parcel source) {
            return new InviteInfo(source);
        }

        @Override
        public InviteInfo[] newArray(int size) {
            return new InviteInfo[size];
        }
    };

    public InviteInfo() {
    }

    protected InviteInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(         this.inviteNo);
        dest.writeString(       this.inviteCode);

        dest.writeLong(         this.userNo);
        dest.writeString(       this.userName);

        dest.writeLong(         this.inviteDeviceNo);
        dest.writeInt(          this.inviteDeviceType);
        dest.writeString(       this.inviteDeviceName);
        dest.writeInt(          this.inviteType);

        dest.writeString(       this.certKey);
        dest.writeLong(         this.numFile);
        dest.writeLong(         this.totalFileSize);

        dest.writeLong(         this.inviteShareFolderServerFolderNo);
        dest.writeString(       this.inviteShareFolderName);
        dest.writeString(       this.inviteShareFolderPath);
        dest.writeString(       this.inviteShareFolderPasswordYN);

        dest.writeString(       this.inviteMessage);

        dest.writeString(       this.dynamicLink);
        dest.writeString(       this.rejectYN);

        dest.writeString(       this.expiredt);
        dest.writeString(       this.regdt);
    }

    public void readFromParcel(Parcel in) {
        this.inviteNo               = in.readLong();
        this.inviteCode             = in.readString();

        this.userNo                 = in.readLong();
        this.userName               = in.readString();

        this.inviteDeviceNo         = in.readLong();
        this.inviteDeviceType       = in.readInt();
        this.inviteDeviceName       = in.readString();
        this.inviteType             = in.readInt();

        this.certKey                = in.readString();
        this.numFile                = in.readLong();
        this.totalFileSize          = in.readLong();

        this.inviteShareFolderServerFolderNo    = in.readLong();
        this.inviteShareFolderName              = in.readString();
        this.inviteShareFolderPath              = in.readString();
        this.inviteShareFolderPasswordYN        = in.readString();

        this.inviteMessage          = in.readString();

        this.dynamicLink            = in.readString();
        this.rejectYN               = in.readString();

        this.expiredt               = in.readString();
        this.regdt                  = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getInviteNo() {
        return inviteNo;
    }
    public void setInviteNo(long inviteNo) {
        this.inviteNo = inviteNo;
    }
    public String getInviteCode() {
        return inviteCode;
    }
    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }
    public long getUserNo() {
        return userNo;
    }
    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public long getInviteDeviceNo() {
        return inviteDeviceNo;
    }
    public void setInviteDeviceNo(long inviteDeviceNo) {
        this.inviteDeviceNo = inviteDeviceNo;
    }
    public int getInviteDeviceType() {
        return inviteDeviceType;
    }
    public void setInviteDeviceType(int inviteDeviceType) {
        this.inviteDeviceType = inviteDeviceType;
    }
    public String getInviteDeviceName() {
        return inviteDeviceName;
    }
    public void setInviteDeviceName(String inviteDeviceName) {
        this.inviteDeviceName = inviteDeviceName;
    }
    public int getInviteType() {
        return inviteType;
    }
    public void setInviteType(int inviteType) {
        this.inviteType = inviteType;
    }

    public String getCertKey() {
        return certKey;
    }
    public void setCertKey(String certKey) {
        this.certKey = certKey;
    }
    public long getNumFile() {
        return numFile;
    }
    public void setNumFile(long numFile) {
        this.numFile = numFile;
    }
    public long getTotalFileSize() {
        return totalFileSize;
    }
    public void setTotalFileSize(long totalFileSize) {
        this.totalFileSize = totalFileSize;
    }

    public long getInviteShareFolderServerFolderNo() {
        return inviteShareFolderServerFolderNo;
    }
    public void setInviteShareFolderServerFolderNo(long inviteShareFolderServerFolderNo) {
        this.inviteShareFolderServerFolderNo = inviteShareFolderServerFolderNo;
    }
    public String getInviteShareFolderName() {
        return inviteShareFolderName;
    }
    public void setInviteShareFolderName(String inviteShareFolderName) {
        this.inviteShareFolderName = inviteShareFolderName;
    }
    public String getInviteShareFolderPath() {
        return inviteShareFolderPath;
    }
    public void setInviteShareFolderPath(String inviteShareFolderPath) {
        this.inviteShareFolderPath = inviteShareFolderPath;
    }
    public String getInviteShareFolderPasswordYN() {
        return inviteShareFolderPasswordYN;
    }
    public void setInviteShareFolderPasswordYN(String inviteShareFolderPasswordYN) {
        this.inviteShareFolderPasswordYN = inviteShareFolderPasswordYN;
    }

    public String getInviteMessage() {
        return inviteMessage;
    }
    public void setInviteMessage(String inviteMessage) {
        this.inviteMessage = inviteMessage;
    }

    public String getDynamicLink() {
        return dynamicLink;
    }
    public void setDynamicLink(String dynamicLink) {
        this.dynamicLink = dynamicLink;
    }
    public String getRejectYN() {
        return rejectYN;
    }
    public void setRejectYN(String rejectYN) {
        this.rejectYN = rejectYN;
    }

    public String getExpiredt() {
        return expiredt;
    }
    public void setExpiredt(String expiredt) {
        this.expiredt = expiredt;
    }
    public String getRegdt() {
        return regdt;
    }
    public void setRegdt(String regdt) {
        this.regdt = regdt;
    }
}
