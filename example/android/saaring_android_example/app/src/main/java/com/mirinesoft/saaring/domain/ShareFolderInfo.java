package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ShareFolderInfo implements Parcelable
{
    long                    deviceNo;
    int                     deviceType;
    String                  deviceName              = "";
    String                  liveYN                  = "";

    long                    serverFolderNo;

    String                  folderName              = "";
    long                    fileSize;
    int                     fileCount;

    int                     shareType;
    int                     downShareType;
    int                     upShareType;
    int                     shareFoaf;

    int                     numFolder;
    int                     numFile;
    long                    totalFileSize;

    String                  password                = "";
    String                  passwordYN              = "";
    String                  downloadYN              = "";
    String                  uploadYN                = "";

    List<Long>              listDownFriendUserNo    = new ArrayList();
    List<Long>              listUpFriendUserNo      = new ArrayList();

    private String fileSizeWithUnit;
    
    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<ShareFolderInfo> CREATOR = new Creator<ShareFolderInfo>() {
        @Override
        public ShareFolderInfo createFromParcel(Parcel source) {
            return new ShareFolderInfo(source);
        }

        @Override
        public ShareFolderInfo[] newArray(int size) {
            return new ShareFolderInfo[size];
        }
    };

    public ShareFolderInfo() {
    }

    protected ShareFolderInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(                 this.deviceNo);
        dest.writeInt(                  this.deviceType);
        dest.writeString(               this.deviceName);
        dest.writeString(               this.liveYN);
        dest.writeLong(                 this.serverFolderNo);
        dest.writeString(               this.folderName);
        dest.writeLong(                 this.fileSize);
        dest.writeInt(                  this.fileCount);
        dest.writeInt(                  this.shareType);
        dest.writeInt(                  this.downShareType);
        dest.writeInt(                  this.upShareType);
        dest.writeInt(                  this.shareFoaf);
        dest.writeInt(                  this.numFolder);
        dest.writeInt(                  this.numFile);
        dest.writeLong(                 this.totalFileSize);
        dest.writeString(               this.password);
        dest.writeString(               this.passwordYN);
        dest.writeString(               this.downloadYN);
        dest.writeString(               this.uploadYN);
        dest.writeList(                 this.listDownFriendUserNo);
        dest.writeList(                 this.listUpFriendUserNo);
    }

    public void readFromParcel(Parcel in) {
        this.deviceNo                   = in.readLong();
        this.deviceType                 = in.readInt();
        this.deviceName                 = in.readString();
        this.liveYN                     = in.readString();
        this.serverFolderNo             = in.readLong();
        this.folderName                 = in.readString();
        this.fileSize                   = in.readLong();
        this.fileCount                  = in.readInt();
        this.shareType                  = in.readInt();
        this.downShareType              = in.readInt();
        this.upShareType                = in.readInt();
        this.shareFoaf                  = in.readInt();
        this.numFolder                  = in.readInt();
        this.numFile                    = in.readInt();
        this.totalFileSize              = in.readLong();
        this.password                   = in.readString();
        this.passwordYN                 = in.readString();
        this.downloadYN                 = in.readString();
        this.uploadYN                   = in.readString();
        this.listDownFriendUserNo       = new ArrayList<Long>();
        in.readList(this.listDownFriendUserNo, Long.class.getClassLoader());
        this.listUpFriendUserNo         = new ArrayList<Long>();
        in.readList(this.listUpFriendUserNo, Long.class.getClassLoader());
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getDeviceNo() {
        return deviceNo;
    }
    public void setDeviceNo(long deviceNo) {
        this.deviceNo = deviceNo;
    }
    public int getDeviceType() {
        return deviceType;
    }
    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }
    public String getDeviceName() {
        return deviceName;
    }
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
    public String getLiveYN() {
        return liveYN;
    }
    public void setLiveYN(String liveYN) {
        this.liveYN = liveYN;
    }
    public long getServerFolderNo() {
        return serverFolderNo;
    }
    public void setServerFolderNo(long serverFolderNo) {
        this.serverFolderNo = serverFolderNo;
    }
    public String getFolderName() {
        return folderName;
    }
    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }
    public long getFileSize() {
        return fileSize;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public int getFileCount() {
        return fileCount;
    }
    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }
    public int getShareType() {
        return shareType;
    }
    public void setShareType(int shareType) {
        this.shareType = shareType;
    }
    public int getDownShareType() {
        return downShareType;
    }
    public void setDownShareType(int downShareType) {
        this.downShareType = downShareType;
    }
    public int getUpShareType() {
        return upShareType;
    }
    public void setUpShareType(int upShareType) {
        this.upShareType = upShareType;
    }
    public int getShareFoaf() {
        return shareFoaf;
    }
    public void setShareFoaf(int shareFoaf) {
        this.shareFoaf = shareFoaf;
    }
    public int getNumFolder() {
        return numFolder;
    }
    public void setNumFolder(int numFolder) {
        this.numFolder = numFolder;
    }
    public int getNumFile() {
        return numFile;
    }
    public void setNumFile(int numFile) {
        this.numFile = numFile;
    }
    public long getTotalFileSize() {
        return totalFileSize;
    }
    public void setTotalFileSize(long totalFileSize) {
        this.totalFileSize = totalFileSize;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPasswordYN() {
        return passwordYN;
    }
    public void setPasswordYN(String passwordYN) {
        this.passwordYN = passwordYN;
    }
    public String getDownloadYN() {
        return downloadYN;
    }
    public void setDownloadYN(String downloadYN) {
        this.downloadYN = downloadYN;
    }
    public String getUploadYN() {
        return uploadYN;
    }
    public void setUploadYN(String uploadYN) {
        this.uploadYN = uploadYN;
    }
    public List<Long> getListDownFriendUserNo() {
        return listDownFriendUserNo;
    }
    public void setListDownFriendUserNo(List<Long> listDownFriendUserNo) {
        this.listDownFriendUserNo = listDownFriendUserNo;
    }
    public List<Long> getListUpFriendUserNo() {
        return listUpFriendUserNo;
    }
    public void setListUpFriendUserNo(List<Long> listUpFriendUserNo) {
        this.listUpFriendUserNo = listUpFriendUserNo;
    }

    public String getFileSizeWithUnit() {return fileSizeWithUnit; }
    public void setFileSizeWithUnit(String fileSizeWithUnit) {this.fileSizeWithUnit = fileSizeWithUnit; }

}
