package com.mirinesoft.saaring;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;

import com.mirinesoft.saaring.domain.*;

import com.mirinesoft.saaring.ISaaringServiceBinder;
import com.mirinesoft.saaring.ISaaringServiceCallback;

/**
 * make a Activity -> Service connection
 */
public class SaaringServiceConnection implements ServiceConnection
{
    MainActivity parent;
    SaaringServiceConnection(MainActivity parent)
    {
        this.parent = parent;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service)
    {
        System.out.println("================> onServiceConnected...");

        if(service != null)
        {
            MainActivity.saaringService = ISaaringServiceBinder.Stub.asInterface(service);

            try {
                MainActivity.saaringService.registerCallback(saaringServiceCallbcak);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            MainActivity.isSaaringService = true;

        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name)
    {
        System.out.println("================> onServiceDisconnected...");
        if(MainActivity.saaringService != null && MainActivity.isSaaringService == true)
        {

            try {
                MainActivity.saaringService.unregisterCallback(saaringServiceCallbcak);
            } catch (DeadObjectException de) {
                de.printStackTrace();
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            MainActivity.saaringService = null;
            MainActivity.isSaaringService = false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // define C++ -> Java callback functions
    //
    //////////////////////////////////////////////////////////////////////////////////
    public ISaaringServiceCallback saaringServiceCallbcak = new ISaaringServiceCallback.Stub()
    {
        @Override
        public void onShareDirRefresh(StringData path)
        {
            String msg = "DEBUG onShareDirRefresh... : path="+path.getData();

            System.out.println(msg);
            parent.toast(msg);
        }

        @Override
        /**
         * Signaling Server와 접속 후 로그인에 성공했다.
         * 로그인 화면 등을 정리하고,
         * 메인화면, 첫화면을 표시해준다.
         */
        public void onSignalingConnected()
        {
            String msg = "DEBUG onSignalingConnected..........................";

            System.out.println(msg);
            parent.toast(msg);
        }

        @Override
        /**
         * Signaling Server와 접속이 끊어졌다.
         * 자동 재접속 창을 띄우고, 재접속 시도 한다.
         */
        public void onSignalingClosed() {
            String msg = "DEBUG onSignalingClosed..........................";

            System.out.println(msg);
            parent.toast(msg);
            // parent.signalingClosed();
        }

        @Override
        /**
         * Signaling Server에 로그인 실패 했다.
         * 로그인 화면으로 전환 한다.
         */
        public void onSignalingLoginFailed()
        {
            String msg = "DEBUG onSignalingLoginFailed..........................";

            System.out.println(msg);
            parent.toast(msg);
        }

        @Override
        /**
         * Singaling Server로 부터 친구 디바이스의 접속 알림을 받았다.
         * 현재 화면이 내친구 목록 화면이면,
         * 해당 친구를 접속중으로 변경 표시 한다.
         * 내 디바이스가 접속한 경우에는, 이에 맞게 화면 처리 한다.
         */
        public void onSignalingNotifyDeviceLiveYN(long fromUserNo, long fromDeviceNo, StringData liveYN)
        {
            String msg = "DEBUG onSignalingNotifyDeviceLiveYN... : fromUserNo="+fromUserNo+", fromDeviceNo="+fromDeviceNo+", liveYN"+liveYN.getData();

            System.out.println(msg);
            parent.toast(msg);
            // parent.onSignalingNotifyDeviceLiveYN(fromUserNo, fromDeviceNo, liveYN.getData());
        }

        @Override
        /**
         * Singaling Server로 부터 친구 요청 알림을 받았다.
         * 친구요청 알람 메시지를 보여주고,
         * 친구 요청 목록으로 이동해서, 수락.거절 한다.
         */
        public void onSignalingNotifyRequestFriend(StringData friendName)
        {
            String msg = "DEBUG onSignalingNotifyRequestFriend... : friendName="+friendName.getData();

            System.out.println(msg);
            parent.toast(msg);
            // parent.onSignalingNotifyRequestFriend(friendName.getData());
        }

        @Override
        /**
         * Singaling Server로 부터 친구 등록 알림을 받았다.
         * 친구 등록 알람 메시지를 보여주고,
         * 현재 화면이 친구목록 화면이면, 목록을 갱신한다.
         */
        public void onSignalingNotifyAddFriend(StringData friendName, long friendUserNo)
        {
            String msg = "DEBUG onSignalingNotifyAddFriend... : friendName="+friendName.getData()+", friendUserNo="+friendUserNo;

            System.out.println(msg);
            parent.toast(msg);
            // parent.onSignalingNotifyAddFriend(friendName.getData(), friendUserNo);
        }

        @Override
        /**
         * P2P portmap 초기화가 끝났다.
         * portmap 초기화가 끝나야, p2p 통신을위한 IP/port가 정해져서 통신을 할 수 있다.
         * 자동 재전송 처리 한다.
         */
        public void onP2pPortmapFinished()
        {
            String msg = "DEBUG onP2pPortmapFinished...";

            System.out.println(msg);
            parent.toast(msg);
            // parent.p2pPortmapFinished();
        }

        @Override
        /**
         * P2P 접속 후,
         * P2P 로그인에 성공 했다.
         * 전송 목록이 있으면, 재전송 처리한다.
         */
        public void onP2pLoginFinished(long friendDeviceNo, long toRemoteShareNo) {
            String msg = "DEBUG onP2pLoginFinished... : friendDeviceNo="+friendDeviceNo+", toRemoteShareNo="+toRemoteShareNo;

            System.out.println(msg);
            parent.toast(msg);
            // parent.p2pLoginFinished(friendDeviceNo);
        }

        @Override
        /**
         * 접속대기 message box가 있다면,
         * 메시지와 timer를 초기화 한다.
         */
        public void onP2pWakeUpFinished(boolean isSuccess)
        {
            String msg = "DEBUG onP2pWakeUpFinished... : isSuccess="+isSuccess;

            System.out.println(msg);
            parent.toast(msg);
            // parent.onP2pWakeUpFinished(isSuccess);
        }

        @Override
        /**
         * 파일 다운로드 하나가 완료 되었다.
         * 전송중인것은 전송완료 처리하고,
         * 다음 전송 진행한다.
         */
        public void onP2pFinishDownload(long transNo, long fileSize, long receivedSize)
        {
            String msg = "DEBUG onP2pFinishDownload... : transNo="+transNo+", fileSize="+fileSize+", receivedSize="+receivedSize;

            System.out.println(msg);
            parent.toast(msg);
            // parent.finishDownload(transNo, fileSize, receivedSize);
        }

        @Override
        /**
         * 파일 업로드 하나가 완료 되었다.
         * 전송중인것은 전송완료 처리하고,
         * 다음 전송 진행한다.
         */
        public void onP2pFinishUpload(long transNo, long fileSize, long sentSize)
        {
            String msg = "DEBUG onP2pFinishUpload... : transNo="+transNo+", fileSize="+fileSize+", sentSize="+sentSize;

            System.out.println(msg);
            parent.toast(msg);
            // parent.finishUpload(transNo, fileSize, sentSize);
        }

        @Override
        /**
         * 다운로드 중인 파일의 진행상태를 업데이트 한다.
         */
        public void onP2pProgressDownload(long transNo, long fileSize, long receivedSize)
        {
            String msg = "DEBUG onP2pProgressDownload... : transNo="+transNo+", fileSize="+fileSize+", receivedSize="+receivedSize;

            System.out.println(msg);
            parent.toast(msg);
            // parent.progressDownload(transNo, fileSize, receivedSize);
        }

        @Override
        /**
         * 업로드 중인 파일의 진행상태를 업데이트 한다.
         */
        public void onP2pProgressUpload(long transNo, long fileSize, long sentSize)
        {
            String msg = "DEBUG onP2pProgressUpload... : transNo="+transNo+", fileSize="+fileSize+", sentSize="+sentSize;

            System.out.println(msg);
            parent.toast(msg);
            // parent.progressUpload(transNo, fileSize, sentSize);
        }

        @Override
        /**
         * 진행중인 다운로드를 중단한다.
         */
        public void onP2pAbortDownload(long transNo)
        {
            String msg = "DEBUG onP2pAbortDownload... : transNo="+transNo;

            System.out.println(msg);
            parent.toast(msg);
            // parent.abortDownload(transNo);
        }

        @Override
        /**
         * 진행중인 업로드를 중단한다.
         */
        public void onP2pAbortUpload(long transNo)
        {
            String msg = "DEBUG onP2pAbortUpload... : transNo="+transNo;

            System.out.println(msg);
            parent.toast(msg);
            // parent.abortUpload(transNo);
        }

        @Override
        /**
         * 상대방이 파일을 다운로드 해갔다.
         */
        public void onP2pFinishDownloadOther(long jobId, long remoteDeviceNo)
        {
            String msg = "DEBUG onP2pFinishDownloadOther... : jobId="+jobId+", remoteDeviceNo="+remoteDeviceNo;

            System.out.println(msg);
            parent.toast(msg);
            // parent.finishDownloadOther(jobId);
        }

        @Override
        /**
         * 상대방 업로드 파일 하나가 완료 됐다.
         */
        public void onP2pFinishUploadOther(long jobId, long remoteDeviceNo)
        {
            String msg = "DEBUG onP2pFinishUploadOther... : jobId="+jobId+", remoteDeviceNo="+remoteDeviceNo;

            System.out.println(msg);
            parent.toast(msg);
            // parent.finishUploadOther(jobId);
        }

        @Override
        /**
         * 상대방 다운로드 진행상황을 업데이트 한다.
         */
        public void onP2pProgressDownloadOther(long jobId, long remoteDeviceNo, long fileSize, long currSize, StringData filePath, StringData serverName, StringData deviceName)
        {
            String msg = "DEBUG onP2pProgressDownloadOther... : jobId="+jobId+", remoteDeviceNo=\"+remoteDeviceNo+\", fileSize="+fileSize+", currSize="+currSize+
                         ", filePath="+filePath.getData()+", serverName="+serverName.getData()+", deviceName="+deviceName.getData();

            System.out.println(msg);
            parent.toast(msg);
            // parent.progressDownloadOther(jobId, fileSize, currSize, filePath.getData(), serverName.getData(), deviceName.getData());
        }

        @Override
        /**
         * 상대방 업로드 진행상황을 업데이트 한다.
         */
        public void onP2pProgressUploadOther(long jobId, long remoteDeviceNo, long fileSize, long currSize, StringData filePath, StringData serverName, StringData deviceName)
        {
            String msg = "DEBUG onP2pProgressUploadOther... : jobId="+jobId+", remoteDeviceNo=\"+remoteDeviceNo+\", fileSize="+fileSize+", currSize="+currSize+
                         ", filePath="+filePath.getData()+", serverName="+serverName.getData()+", deviceName="+deviceName.getData();

            System.out.println(msg);
            parent.toast(msg);
            // parent.progressUploadOther(jobId, fileSize, currSize, filePath.getData(), serverName.getData(), deviceName.getData());
        }

        @Override
        /**
         * 상대방 p2p 요청을 처리 했다.
         * 특별히 해줄일이 없다.
         */
        public void onP2pProcessedRequest(int processType, long jobId, long remoteDeviceNo, int resultCode)
        {
            String msg = "DEBUG onP2pProcessedRequest... : processType="+processType+", jobId="+jobId+", remoteDeviceNo="+remoteDeviceNo+", resultCode="+resultCode;

            System.out.println(msg);
            parent.toast(msg);
            // do nothing
        }

        @Override
        /**
         * 상대방 p2p 요청에 대한 답변을 처리 했다.
         * 특별히 해줄일이 없다.
         */
        public void onP2pProcessedResponse(int processType, long jobId, long remoteDeviceNo, int resultCode)
        {
            String msg = "DEBUG onP2pProcessedResponse... : processType="+processType+", jobId="+jobId+", remoteDeviceNo="+remoteDeviceNo+", resultCode="+resultCode;

            System.out.println(msg);
            parent.toast(msg);
            // do nothing
        }

        @Override
        /**
         * 공유폴더 목록을 업데이트 한다.
         */
        public void onP2pProcessResponseFolderList(StringData strJson)
        {
            String msg = "DEBUG onP2pProcessResponseFolderList... : strJson="+strJson;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pProcessResponseFolderList 참조
        }

        @Override
        /**
         * 공유폴더 파일 목록을 업데이트 한다.
         */
        public void onP2pProcessResponseFolderDetail(StringData strJson)
        {
            String msg = "DEBUG onP2pProcessResponseFolderDetail... : strJson="+strJson;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pProcessResponseFolderDetail 참조
        }

        @Override
        /**
         * 공유폴더 파일 목록을 업데이트 후 UI 업데이트를 위해 사용된다.
         */
        public void onP2pProcessResponseFolderDetailFinished()
        {
            String msg = "DEBUG onP2pProcessResponseFolderDetailFinished...";

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pProcessResponseFolderDetailFinished 참조
        }

        @Override
        /**
         * P2P PCC_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE 요청에 대한 응답으로,
         * P2P 폴더 다운로드 할 때, 해당 폴더의 파일.하위폴더 정보를 받은 후 이어서 다운로드 처리한다.
         */
        public void onP2pProcessResponseFolderDetailRecursive(StringData strJson)
        {
            String msg = "DEBUG onP2pProcessResponseFolderDetailRecursive... : strJson="+strJson;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pProcessResponseFolderDetailRecursive 참조
        }

        @Override
        /**
         * File Upload Qeury 에 대한 회신을 받았다.
         * p2p 파일 업로드를 시작한다.
         */
        public void onP2pProcessResponseFileUploadQuery(long transNo, long fileStartPosition)
        {
            String msg = "DEBUG onP2pProcessResponseFileUploadQuery... : transNo="+transNo+", fileStartPosition="+fileStartPosition;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pProcessResponseFileUploadQuery 참조
        }

        @Override
        /**
         * File Invite Send Files Qeury 에 대한 회신을 받았다.
         * p2p 파일 다운로드를 시작한다.
         */
        public void onP2pProcessResponseFileDownloadInviteSendFilesQuery(StringData strJson)
        {
            String msg = "DEBUG onP2pProcessResponseFileDownloadInviteSendFilesQuery... : strJson="+strJson;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pProcessResponseFileDownloadInviteSendFilesQuery
        }

        @Override
        /**
         * Save Send Files Infite Info 에 대한 회신을 받았다.
         */
        public void onP2pProcessResponseSaveSendFilesInviteInfo(StringData strJson)
        {
            String msg = "DEBUG onP2pProcessResponseSaveSendFilesInviteInfo... : strJson="+strJson;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pProcessResponseSaveSendFilesInviteInfo
        }

        @Override
        /**
         * 다운로드 대기 상태로 전환한다.
         */
        public void onP2pNotifyDownloadQueueStandBy(long remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
        {
            String msg = "DEBUG onP2pNotifyDownloadQueueStandBy... : remoteDeviceNo="+remoteDeviceNo
                    +", downloadingCount="+downloadingCount
                    +", downloadStandByCount="+downloadStandByCount
                    +", myTurn="+myTurn
                    ;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onTransmitListStop 참조
        }

        @Override
        /**
         * 다운로드 대기 상태에서, 다시 다운로드 진행한다.
         */
        public void onP2pNotifyDownloadQueueRelease(long remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
        {
            String msg = "DEBUG onP2pNotifyDownloadQueueRelease... : remoteDeviceNo="+remoteDeviceNo
                    +", downloadingCount="+downloadingCount
                    +", downloadStandByCount="+downloadStandByCount
                    +", myTurn="+myTurn
                    ;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onTransmitListStop 참조
        }

        @Override
        /**
         * 업로드 대기 상태로 전환한다.
         */
        public void onP2pNotifyUploadQueueStandBy(long remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
        {
            String msg = "DEBUG onP2pNotifyUploadQueueStandBy... : remoteDeviceNo="+remoteDeviceNo
                    +", uploadingCount="+uploadingCount
                    +", uploadStandByCount="+uploadStandByCount
                    +", myTurn="+myTurn
                    ;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onTransmitListStop 참조
        }

        @Override
        /**
         * 업로드 대기 상태에서, 다시 업로드 진행한다.
         */
        public void onP2pNotifyUploadQueueRelease(long remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
        {
            String msg = "DEBUG onP2pNotifyUploadQueueRelease... : remoteDeviceNo="+remoteDeviceNo
                    +", uploadingCount="+uploadingCount
                    +", uploadStandByCount="+uploadStandByCount
                    +", myTurn="+myTurn
                    ;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onTransmitListStop 참조
        }

        @Override
        /**
         * p2p 재접속이 이루어진다. 기존 전송을 Stop 시킨다.
         */
        public void onP2pNotifyConnectionClosed(long remoteDeviceNo)
        {
            String msg = "DEBUG onP2pNotifyConnectionClosed... : remoteDeviceNo="+remoteDeviceNo;

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pNotifyConnectionClosed 참조
        }

        @Override
        /**
         * 다음 전송을 진행한다.
         */
        public void onP2pNotifyStartNextTransfer(long remoteDeviceNo)
        {
            String msg = "DEBUG onP2pNotifyStartNextTransfer... : remoteDeviceNo="+remoteDeviceNo;

            System.out.println(msg);
            parent.toast(msg);
            // parent.selectNewTransAndRequest(deviceNo);
            // MainWindow::startNextTransfer 참조
        }

        @Override
        /**
         * 상태 메시지를 처리 한다.
         */
        public void onP2pNotifyStatusMessage(StringData text)
        {
            String msg = "DEBUG onP2pNotifyStatusMessage... : text="+text.getData();

            System.out.println(msg);
            parent.toast(msg);
            // MainWindow::onP2pNotifyStatusMessage 참조
        }

    };
}
