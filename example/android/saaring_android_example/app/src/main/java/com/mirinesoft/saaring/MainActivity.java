package com.mirinesoft.saaring;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.mirinesoft.saaring.domain.MemberInfo;
import com.mirinesoft.saaring.domain.ShareFolderInfo;
import com.mirinesoft.saaring.domain.StringData;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener
{

    // set from SaaringServiceConnection
    public static ISaaringServiceBinder saaringService = null;
    public static boolean isSaaringService = false;
    SaaringServiceConnection saaringServiceConnection = new SaaringServiceConnection(this);

    public static MemberInfo member = new MemberInfo();

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // firebase auth
        mAuth = FirebaseAuth.getInstance();

        // for buttons
        Button buttonConnect =  (Button) findViewById(R.id.buttonConnect) ;
        buttonConnect.setOnClickListener(this) ;

        Button buttonSharedFolder =  (Button) findViewById(R.id.buttonSharedFolder) ;
        buttonSharedFolder.setOnClickListener(this) ;

        Button buttonP2p =  (Button) findViewById(R.id.buttonP2p) ;
        buttonP2p.setOnClickListener(this) ;

        // 샤링 서비스 시작
        startSaaringService();
        bindSaaringService();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for listner
    //
    //////////////////////////////////////////////////////////////////////////////////

    private void onClickButtonConnect()
    {
        member.setUserId("kojang74@naver.com");
        member.setEmail("kojang74@naver.com");
        member.setPassword("1234!@#$");
        member.setIdType(0);
        member.setDeviceType(1);

        String email = member.getEmail();
        String password = member.getPassword();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user.isEmailVerified()) {
                                Log.i("SaaringExample","signInWithEmailAndPassword : success");
                                user.getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                                    @Override
                                    public void onSuccess(GetTokenResult result) {
                                        String firebaseToken = result.getToken();
                                        member.setFirebaseToken(firebaseToken);

                                        Log.i("SaaringExample","firebaseToken=" + firebaseToken);

                                        connectToSignalingServer();
                                    }
                                });
                            } else {
                                Log.i("SaaringExample","signInWithEmailAndPassword : fail");
                            }
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("activityName", "MainActivity");

        switch (v.getId()) {
            case R.id.buttonConnect:
                onClickButtonConnect();
                break;
            case R.id.buttonSharedFolder:
                getSharedFolder();
                break;
            case R.id.buttonP2p:
                connectP2p();
                break;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for service
    //
    //////////////////////////////////////////////////////////////////////////////////
    private void startSaaringService() {
        System.out.println("================> startService before...");
        Intent intent = new Intent(getApplicationContext(),
                SaaringService.class);
        startService(intent);
        System.out.println("================> startService after...");
    }

    private void stopSaaringService() {
        System.out.println("================> stopService before...");
        Intent intent = new Intent(getApplicationContext(),
                SaaringService.class);
        stopService(intent);
        System.out.println("================> stopService after...");
    }

    private void bindSaaringService() {
        System.out.println("================> bindService before... : saaringServiceConnection=" + saaringServiceConnection);
        Intent intentForBind = new Intent(getApplicationContext(), SaaringService.class);
        bindService(intentForBind, saaringServiceConnection, Context.BIND_AUTO_CREATE);
        System.out.println("================> bindService after... : saaringServiceConnection=" + saaringServiceConnection);
        System.out.println("================> bindService after... : isSaaringService=" + isSaaringService);
    }

    private void unbindSaaringService() {
        System.out.println("================> unbindService before...");
        unbindService(saaringServiceConnection);
        System.out.println("================> unbindService after...");
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for init p2p library language
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static String getDefaultLanguage()
    {
        System.out.println("getDefaultLanguage..."+Locale.getDefault().getLanguage());
        return Locale.getDefault().getLanguage();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for buttons
    //
    //////////////////////////////////////////////////////////////////////////////////
    public void toast(final String msg) {
        System.out.println("================> toast...");
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    public boolean connectToSignalingServer()
    {
        Log.i("SaaringExample","connectToSignalingServer()");

        boolean result = false;

        try {
            // web login
            MemberInfo memberInfo = new MemberInfo();
            memberInfo.setUserId(           member.getEmail());
            memberInfo.setEmail(            member.getEmail());
            memberInfo.setPassword(         member.getPassword());
            memberInfo.setIdType(           member.getIdType());
            memberInfo.setFirebaseToken(    member.getFirebaseToken());
            memberInfo.setDeviceType(       member.getDeviceType());

            StringData resultCode     = new StringData();
            StringData resultMessage  = new StringData();
            StringData resultData     = new StringData();

            // test_web
            MainActivity.saaringService.loginMember(memberInfo, resultCode, resultMessage, resultData);

            Log.i("SaaringExample","resultCode="+resultCode.getData());
            Log.i("SaaringExample","resultMessage="+resultMessage.getData());
            Log.i("SaaringExample","resultData="+resultData.getData());

            if(Integer.parseInt(resultCode.getData()) == 100) {
                JSONObject jsonResultData = new JSONObject(resultData.getData());
                Log.i("SaaringExample","jsonResultData="+jsonResultData);
                MainActivity.member.setUserId(      jsonResultData.getString("userId"));
                MainActivity.member.setAuthKey(     jsonResultData.getString("authKey"));
                MainActivity.member.setUserNo(      jsonResultData.getLong("userNo"));
                MainActivity.member.setUserName(    jsonResultData.getString("userName"));
                MainActivity.member.setDeviceNo(    jsonResultData.getLong("deviceNo"));
                MainActivity.member.setProfilePic(  jsonResultData.getString("profilePic"));

                // get version & server info.
                MainActivity.saaringService.initP2p();

                // connect to signaling server.
                MainActivity.saaringService.doSignalingConnect(MainActivity.member.getAuthKey(), false);

                result = true;
            } else {
                result = false;
            }
        }  catch(Exception e) {
            e.printStackTrace();
            result = false;
        }

        return result;
    }

    public void getSharedFolder()
    {
        Log.i("SaaringExample","getSharedFolder()");

        try {

            StringData resultCode     = new StringData();
            StringData resultMessage  = new StringData();
            StringData resultData     = new StringData();
            List<ShareFolderInfo> listShareFolderInfo = new ArrayList();

            MainActivity.saaringService.listFolderAll(MainActivity.member.getAuthKey(),
                    resultCode, resultMessage, resultData,
                    listShareFolderInfo);

            Log.i("SaaringExample","resultCode="+resultCode.getData());
            Log.i("SaaringExample","resultMessage="+resultMessage.getData());
            Log.i("SaaringExample","resultData="+resultData.getData());

            if(Integer.parseInt(resultCode.toString()) == 100) {
                // 성공
                Log.i("SaaringExample","getSharedFolder : 성공");
            } else {
                // 실패
                Log.i("SaaringExample","getSharedFolder : 실패");
            }
        }  catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void connectP2p()
    {
        Log.i("SaaringExample","connectP2p()");

        long toUserNo           = 47;
        String toUserName       = "HOHOHO";
        long toDeviceNo         = 180;
        int toDeviceType        = 4;
        String toDeviceName     = "HAHAHA";
        long toServerFolderNo   = 424;
        String toFolderName     = "hoho folder";
        boolean mustWakeUp      = false;

        try {
            MainActivity.saaringService.doP2pConnectShareFolder(toUserNo, toUserName, toDeviceNo, toDeviceType, toDeviceName, toServerFolderNo, toFolderName, mustWakeUp);
        }  catch(Exception e) {
            e.printStackTrace();
        }
    }
}