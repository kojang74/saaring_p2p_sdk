package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class StringData implements Parcelable
{
    String                  data = "";

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<StringData> CREATOR = new Creator<StringData>() {
        @Override
        public StringData createFromParcel(Parcel source) {
            return new StringData(source);
        }

        @Override
        public StringData[] newArray(int size) {
            return new StringData[size];
        }
    };

    public StringData() {
    }

    public StringData(String data) {
        this.data = data;
    }

    protected StringData(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(               this.data);
    }

    public void readFromParcel(Parcel in) {
        this.data                       = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }

    public void clear() {
        this.data = "";
    }
    public String toString() {
        return this.data;
    }
}
