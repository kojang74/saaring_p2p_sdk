package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class PhoneAddressInfo implements Parcelable
{
    long                    userNo;

    String                  phoneNumber             = "";
    int                     countryCode;
    long                    nationalNumber;

    long                    friendUserNo;
    String                  friendYN                = "N";
    String                  blockYN                 = "N";

    long                    matchingdt;
    long                    frienddt;
    long                    regdt;

    String                  userName            = "";
    String                  email               = "";
    String                  profilePic          = "";
    String                  message             = "";

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////

    public static final Creator<PhoneAddressInfo> CREATOR = new Creator<PhoneAddressInfo>() {
        @Override
        public PhoneAddressInfo createFromParcel(Parcel source) {
            return new PhoneAddressInfo(source);
        }

        @Override
        public PhoneAddressInfo[] newArray(int size) {
            return new PhoneAddressInfo[size];
        }
    };

    public PhoneAddressInfo() {
    }

    protected PhoneAddressInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(                 this.userNo);
        dest.writeString(               this.phoneNumber);
        dest.writeInt(                  this.countryCode);
        dest.writeLong(                 this.nationalNumber);
        dest.writeLong(                 this.friendUserNo);
        dest.writeString(               this.friendYN);
        dest.writeString(               this.blockYN);
        dest.writeLong(                 this.matchingdt);
        dest.writeLong(                 this.frienddt);
        dest.writeLong(                 this.regdt);
        dest.writeString(               this.userName);
        dest.writeString(               this.email);
        dest.writeString(               this.profilePic);
        dest.writeString(               this.message);
    }

    public void readFromParcel(Parcel in) {
        this.userNo                     = in.readLong();
        this.phoneNumber                = in.readString();
        this.countryCode                = in.readInt();
        this.nationalNumber             = in.readLong();
        this.friendUserNo               = in.readLong();
        this.friendYN                   = in.readString();
        this.blockYN                    = in.readString();
        this.matchingdt                 = in.readLong();
        this.frienddt                   = in.readLong();
        this.regdt                      = in.readLong();
        this.userName                   = in.readString();
        this.email                      = in.readString();
        this.profilePic                 = in.readString();
        this.message                    = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////

    public long getUserNo() {
        return userNo;
    }

    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public long getNationalNumber() {
        return nationalNumber;
    }

    public void setNationalNumber(long nationalNumber) {
        this.nationalNumber = nationalNumber;
    }

    public long getFriendUserNo() {
        return friendUserNo;
    }

    public void setFriendUserNo(long friendUserNo) {
        this.friendUserNo = friendUserNo;
    }

    public String getFriendYN() {
        return friendYN;
    }

    public void setFriendYN(String friendYN) {
        this.friendYN = friendYN;
    }

    public String getBlockYN() {
        return blockYN;
    }

    public void setBlockYN(String blockYN) {
        this.blockYN = blockYN;
    }

    public long getMatchingdt() {
        return matchingdt;
    }

    public void setMatchingdt(long matchingdt) {
        this.matchingdt = matchingdt;
    }

    public long getFrienddt() {
        return frienddt;
    }

    public void setFrienddt(long frienddt) {
        this.frienddt = frienddt;
    }

    public long getRegdt() {
        return regdt;
    }

    public void setRegdt(long regdt) {
        this.regdt = regdt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
