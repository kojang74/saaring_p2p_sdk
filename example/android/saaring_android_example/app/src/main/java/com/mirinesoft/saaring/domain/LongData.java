package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class LongData implements Parcelable
{
    Long                  data = 0L;

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<LongData> CREATOR = new Creator<LongData>() {
        @Override
        public LongData createFromParcel(Parcel source) {
            return new LongData(source);
        }

        @Override
        public LongData[] newArray(int size) {
            return new LongData[size];
        }
    };

    public LongData() {
    }

    public LongData(Long data) {
        this.data = data;
    }

    protected LongData(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(               this.data);
    }

    public void readFromParcel(Parcel in) {
        this.data                       = in.readLong();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public Long getData() {
        return data;
    }
    public void setData(Long data) {
        this.data = data;
    }

    public Long longValue() {
        return this.data;
    }
}
