package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class TransferBaseInfo implements Parcelable
{
    long                transNo;
    long                userNo;
    String              userName                = "";
    long                deviceNo;
    int                 deviceType;
    String              deviceName              = "";
    String              upDown                  = "";
    int                 transStatus;
    String              localPath               = "";
    String              remotePath              = "";
    long                fileSize;
    long                fileStartPosition;
    long                transSize;
    String              deleteYN                = "";
    int                 sortNo;
    long                updateTime;
    long                myDeviceNo;
    long                inviteNo;
    long                fileId;

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<TransferBaseInfo> CREATOR = new Creator<TransferBaseInfo>() {
        @Override
        public TransferBaseInfo createFromParcel(Parcel source) {
            return new TransferBaseInfo(source);
        }

        @Override
        public TransferBaseInfo[] newArray(int size) {
            return new TransferBaseInfo[size];
        }
    };

    public TransferBaseInfo() {
    }

    protected TransferBaseInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(                 this.transNo);
        dest.writeLong(                 this.userNo);
        dest.writeString(               this.userName);
        dest.writeLong(                 this.deviceNo);
        dest.writeInt(                  this.deviceType);
        dest.writeString(               this.deviceName);
        dest.writeString(               this.upDown);
        dest.writeInt(                  this.transStatus);
        dest.writeString(               this.localPath);
        dest.writeString(               this.remotePath);
        dest.writeLong(                 this.fileSize);
        dest.writeLong(                 this.fileStartPosition);
        dest.writeLong(                 this.transSize);
        dest.writeString(               this.deleteYN);
        dest.writeInt(                  this.sortNo);
        dest.writeLong(                 this.updateTime);
        dest.writeLong(                 this.myDeviceNo);
        dest.writeLong(                 this.inviteNo);
        dest.writeLong(                 this.fileId);
    }

    public void readFromParcel(Parcel in) {
        this.transNo                    = in.readLong();
        this.userNo                     = in.readLong();
        this.userName                   = in.readString();
        this.deviceNo                   = in.readLong();
        this.deviceType                 = in.readInt();
        this.deviceName                 = in.readString();
        this.upDown                     = in.readString();
        this.transStatus                = in.readInt();
        this.localPath                  = in.readString();
        this.remotePath                 = in.readString();
        this.fileSize                   = in.readLong();
        this.fileStartPosition          = in.readLong();
        this.transSize                  = in.readLong();
        this.deleteYN                   = in.readString();
        this.sortNo                     = in.readInt();
        this.updateTime                 = in.readLong();
        this.myDeviceNo                 = in.readLong();
        this.inviteNo                   = in.readLong();
        this.fileId                     = in.readLong();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getTransNo() {
        return transNo;
    }
    public void setTransNo(long transNo) {
        this.transNo = transNo;
    }
    public long getUserNo() {
        return userNo;
    }
    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public long getDeviceNo() {
        return deviceNo;
    }
    public void setDeviceNo(long deviceNo) {
        this.deviceNo = deviceNo;
    }
    public int getDeviceType() {
        return deviceType;
    }
    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }
    public String getDeviceName() {
        return deviceName;
    }
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
    public String getUpDown() {
        return upDown;
    }
    public void setUpDown(String upDown) {
        this.upDown = upDown;
    }
    public int getTransStatus() {
        return transStatus;
    }
    public void setTransStatus(int transStatus) {
        this.transStatus = transStatus;
    }
    public String getLocalPath() {
        return localPath;
    }
    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }
    public String getRemotePath() {
        return remotePath;
    }
    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }
    public long getFileSize() {
        return fileSize;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public long getFileStartPosition() {
        return fileStartPosition;
    }
    public void setFileStartPosition(long fileStartPosition) {
        this.fileStartPosition = fileStartPosition;
    }
    public long getTransSize() {
        return transSize;
    }
    public void setTransSize(long transSize) {
        this.transSize = transSize;
    }
    public String getDeleteYN() {
        return deleteYN;
    }
    public void setDeleteYN(String deleteYN) {
        this.deleteYN = deleteYN;
    }
    public int getSortNo() {
        return sortNo;
    }
    public void setSortNo(int sortNo) {
        this.sortNo = sortNo;
    }
    public long getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
    public long getMyDeviceNo() {
        return myDeviceNo;
    }
    public void setMyDeviceNo(long myDeviceNo) {
        this.myDeviceNo = myDeviceNo;
    }
    public long getInviteNo() {
        return inviteNo;
    }
    public void setInviteNo(long inviteNo) {
        this.inviteNo = inviteNo;
    }
    public long getFileId() {
        return fileId;
    }
    public void setFileId(long fileId) {
        this.fileId = fileId;
    }
}
