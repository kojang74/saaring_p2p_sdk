package com.mirinesoft.saaring.domain;

import android.os.Parcel;

public class FriendInfo extends MemberInfo 
{
    String                  favoriteYN = "";

    int                     numDevice;
    int                     numLiveDevice;
    int                     numOfflineMobileDevice;
    int                     numFile;
    int                     numFolder;
    long                    totalFileSize;
    String                  totalFileSizeTxt = "";

    int                     requestStatus;
    long                    requestdt;

    String                  regdt;

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<FriendInfo> CREATOR = new Creator<FriendInfo>() {
        @Override
        public FriendInfo createFromParcel(Parcel source) {
            return new FriendInfo(source);
        }

        @Override
        public FriendInfo[] newArray(int size) {
            return new FriendInfo[size];
        }
    };

    public FriendInfo() {
     }

    protected FriendInfo(Parcel in) {
        super(in);
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(               this.favoriteYN);
        dest.writeInt(                  this.numDevice);
        dest.writeInt(                  this.numLiveDevice);
        dest.writeInt(                  this.numOfflineMobileDevice);
        dest.writeInt(                  this.numFile);
        dest.writeInt(                  this.numFolder);
        dest.writeLong(                 this.totalFileSize);
        dest.writeInt(                  this.requestStatus);
        dest.writeLong(                 this.requestdt);
        dest.writeString(               this.regdt);
        dest.writeString(               this.totalFileSizeTxt);
    }

    public void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        this.favoriteYN                 = in.readString();
        this.numDevice                  = in.readInt();
        this.numLiveDevice              = in.readInt();
        this.numOfflineMobileDevice     = in.readInt();
        this.numFile                    = in.readInt();
        this.numFolder                  = in.readInt();
        this.totalFileSize              = in.readLong();
        this.requestStatus              = in.readInt();
        this.requestdt                  = in.readLong();
        this.regdt                      = in.readString();
        this.totalFileSizeTxt           = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public String getFavoriteYN() {
        return favoriteYN;
    }
    public void setFavoriteYN(String favoriteYN) {
        this.favoriteYN = favoriteYN;
    }

    public int getNumDevice() {
        return numDevice;
    }
    public void setNumDevice(int numDevice) {
        this.numDevice = numDevice;
    }

    public int getNumLiveDevice() {
        return numLiveDevice;
    }
    public void setNumLiveDevice(int numLiveDevice) {
        this.numLiveDevice = numLiveDevice;
    }

    public int getNumOfflineMobileDevice() {
        return numOfflineMobileDevice;
    }
    public void setNumOfflineMobileDevice(int numOfflineMobileDevice) { this.numOfflineMobileDevice = numOfflineMobileDevice;
    }

    public int getNumFile() {
        return numFile;
    }
    public void setNumFile(int numFile) {
        this.numFile = numFile;
    }

    public int getNumFolder() {
        return numFolder;
    }
    public void setNumFolder(int numFolder) {
        this.numFolder = numFolder;
    }

    public long getTotalFileSize() {
        return totalFileSize;
    }
    public void setTotalFileSize(long totalFileSize) {
        this.totalFileSize = totalFileSize;
    }

    public String getTotalFileSizeTxt() {
        return totalFileSizeTxt;
    }
    public void setTotalFileSizeTxt(String totalFileSizeTxt) {
        this.totalFileSizeTxt = totalFileSizeTxt;
    }

    public int getRequestStatus() {
        return requestStatus;
    }
    public void setRequestStatus(int requestStatus) {
        this.requestStatus = requestStatus;
    }

    public long getRequestdt() {
        return requestdt;
    }
    public void setRequestdt(long requestdt) {
        this.requestdt = requestdt;
    }

    public String getRegdt() {
        return regdt;
    }
    public void setRegdt(String regdt) {
        this.regdt = regdt;
    }

}
