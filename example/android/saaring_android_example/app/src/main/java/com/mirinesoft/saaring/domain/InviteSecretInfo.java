package com.mirinesoft.saaring.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class InviteSecretInfo implements Parcelable
{
    long                    userNo;

    String                  secretCode          = "";
    String                  expiredt            = "";
    String                  regdt               = "";

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for AIDL
    //
    //////////////////////////////////////////////////////////////////////////////////
    public static final Creator<InviteSecretInfo> CREATOR = new Creator<InviteSecretInfo>() {
        @Override
        public InviteSecretInfo createFromParcel(Parcel source) {
            return new InviteSecretInfo(source);
        }

        @Override
        public InviteSecretInfo[] newArray(int size) {
            return new InviteSecretInfo[size];
        }
    };

    public InviteSecretInfo() {
    }

    protected InviteSecretInfo(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(         this.userNo);

        dest.writeString(       this.secretCode);
        dest.writeString(       this.expiredt);
        dest.writeString(       this.regdt);
    }

    public void readFromParcel(Parcel in) {
        this.userNo             = in.readLong();

        this.secretCode         = in.readString();
        this.expiredt           = in.readString();
        this.regdt              = in.readString();
    }

    //////////////////////////////////////////////////////////////////////////////////
    //
    // for getter/setter
    //
    //////////////////////////////////////////////////////////////////////////////////
    public long getUserNo() {
        return userNo;
    }
    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }
    public String getSecretCode() {
        return secretCode;
    }
    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }
    public String getExpiredt() {
        return expiredt;
    }
    public void setExpiredt(String expiredt) {
        this.expiredt = expiredt;
    }
    public String getRegdt() {
        return regdt;
    }
    public void setRegdt(String regdt) {
        this.regdt = regdt;
    }
}
