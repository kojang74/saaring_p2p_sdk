package com.mirinesoft.saaring;

import java.util.List;

import com.mirinesoft.saaring.domain.LongData;
import com.mirinesoft.saaring.domain.StringData;

import com.mirinesoft.saaring.domain.BadgeInfo;
import com.mirinesoft.saaring.domain.BlockInfo;
import com.mirinesoft.saaring.domain.FriendInfo;
import com.mirinesoft.saaring.domain.InviteInfo;
import com.mirinesoft.saaring.domain.InviteFilesInfo;
import com.mirinesoft.saaring.domain.InviteRegisteredInfo;
import com.mirinesoft.saaring.domain.InviteSecretInfo;
import com.mirinesoft.saaring.domain.MemberInfo;
import com.mirinesoft.saaring.domain.MemberProviderInfo;
import com.mirinesoft.saaring.domain.ServerInfo;
import com.mirinesoft.saaring.domain.SetupInfo;
import com.mirinesoft.saaring.domain.ShareFolderInfo;
import com.mirinesoft.saaring.domain.ShareInfo;
import com.mirinesoft.saaring.domain.TransferBaseInfo;
import com.mirinesoft.saaring.domain.PhoneAddressInfo;

import com.mirinesoft.saaring.ISaaringServiceCallback;

interface ISaaringServiceBinder {

        boolean registerCallback(ISaaringServiceCallback callback);
        boolean unregisterCallback(ISaaringServiceCallback callback);

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring common jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        void test();

        double calSpeed(long oldTime, long currTime, long oldSize, long currSize);
        String leftTime(long oldTime, long currTime, long oldSize, long currSize, long totalSize, double speed);
        String getSizeWithUnit(long size);

        boolean checkEmail(String email);
        String getDuplicationFilename(String filePath, inout StringData lastLocalFilePath);
        String numberFormatWithComma(long number, String separator);

        int checkUpgrade(String lastVer, String runningVer, String currentVer);

        String getDownloadFolder();
        String getShareTopFolder();

        String changeProfileImage(String authKey, String path);
        String removeFirstPath(String path);

        String getProviderId(int idType);
        String getProviderName(int idType);
        String getProviderNameByProviderId(String providerId);

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring db jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////
        // for All
        //////////////////////////////////////////////////////////////////////////////////
        boolean isOpen();

        //////////////////////////////////////////////////////////////////////////////////
        // for login
        //////////////////////////////////////////////////////////////////////////////////
        boolean updateLoginTb(int idType, String email, String phoneNumber, String password, String isSaveEmail, String isAutoLogin, String isLastLoginEmail);
        boolean updateLoginAutoStart(int idType, String email, String phoneNumber, String isAutoStart);

        boolean updateSetSend(inout SetupInfo setupInfo);
        boolean updateSetNetwork(inout SetupInfo setupInfo);

        boolean readDbLastLogin(inout SetupInfo setupInfo);
        boolean readDbByEmailOrPhoneNumber(int idType, String email, String phoneNumber, inout SetupInfo setupInfo);

        void doAfterLogin(int idType, String email, String phoneNumber, String password,
                          String providerId, String idToken, String accessToken, String uid,
                          String isSaveEmail, String isAutoLogin);

        //////////////////////////////////////////////////////////////////////////////////
        // for transfer
        //////////////////////////////////////////////////////////////////////////////////
        long insertMyTransTbOne(inout TransferBaseInfo transferBaseInfo);
        boolean insertMyTransTbMulti(inout List<TransferBaseInfo> listTransferBaseInfo);

        boolean updateMyTransTb(inout TransferBaseInfo transferBaseInfo);
        boolean updateMyTransTbStatus(long transNo, int transStatus, long updateTime);

        boolean updateMyTransTbSortNoMulti(in long[] arrayTransNo, long updateTime);

        boolean updateMyTransTbProgress(long transNo, long fileSize, long transSize, long updateTime);
        boolean updateMyTransTbInit(long myDeviceNo);

        boolean deleteMyTransTbOne(long transNo);
        boolean deleteMyTransTbMulti(in long[] arrayTransNo);
        boolean deleteMyTransTbAll(long deviceNo);

        boolean readMyTransTbByTransNo(inout List<TransferBaseInfo> listTransferBaseInfo, long transNo);
        boolean readMyTransTbAll(inout List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo);
        boolean readMyTransTbTransferring(inout List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo);
        boolean readMyTransTbNotTransferringLastOne(inout List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo);
        boolean readMyTransTbByTransStatusLastOne(inout List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo, String deleteYN, long deviceNo, int transStatus);
        boolean readMyTransTbInit(inout List<TransferBaseInfo> listTransferBaseInfo, long myDeviceNo);

        //////////////////////////////////////////////////////////////////////////////////
        // for share
        //////////////////////////////////////////////////////////////////////////////////
        boolean insertShareTb(inout ShareInfo shareInfo);
        boolean updateShareTb(inout ShareInfo shareInfo);
        boolean deleteShareTb(long shareNo);
        boolean readShareTb(long deviceNo, inout List<ShareInfo> listShareInfo);

        long getShareNoByServerFolderNo(long serverFolderNo);
        long getServerFolderNoByShareNo(long shareNo);
        String getShareFolderPathByServerFolderNo(long serverFolderNo);

        int getShareFileCntWithShareNo(long deviceNo, long shareNo);
        long getShareFileSize(long deviceNo);
        long getShareFileSizeWithShareNo(long deviceNo, long shareNo);

        //////////////////////////////////////////////////////////////////////////////////
        // for share detail
        //////////////////////////////////////////////////////////////////////////////////

        boolean updateShareDetailShortPath(long shareNo, String shareName, String shareFolderFullPath);
        boolean deleteShareDetailTb(long deviceNo);

        void makeMemoryShareDetailTb();
        void backupMemoryShareDetailTb();

        //////////////////////////////////////////////////////////////////////////////////
        // for invite files
        //////////////////////////////////////////////////////////////////////////////////

        boolean insertInviteFiles(in InviteFilesInfo saaringInviteFilesInfo);
        boolean insertListInviteFiles(in List<InviteFilesInfo> listSaaringInviteFilesInfo);

        void deleteExpired();
        void deleteInviteFilesByInviteNo(long inviteNo);

        boolean listInviteFiles(long inviteNo, inout List<InviteFilesInfo> listSaaringInviteFilesInfo);
        boolean listInviteFilesWithCertKey(long inviteNo, in String certKey, inout List<InviteFilesInfo> listSaaringInviteFilesInfo);

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring p2p jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        void initP2p();
        void closeP2p();
        boolean isP2p();

        boolean isSignalingConnected();
        boolean isSignalingLogin();
        boolean doSignalingConnect(String authKey, boolean isRefreshServerInfo);
        void doSignalingClose();
        void reqHeartBeat();

        ServerInfo getServerInfo();
        boolean isP2pLived(long deviceNo);

        void updateShareFolders();
        void updateNetworkSendSettings();

        void doP2pConnect(long toUserNo, String toUserName, long toDeviceNo, int toDeviceType, String toDeviceName,
                          boolean mustWakeUp);
        void doP2pConnectShareFolder(long toUserNo, String toUserName, long toDeviceNo, int toDeviceType, String toDeviceName,
                                     long toServerFolderNo, String toFolderName, boolean mustWakeUp);
        void doP2pConnectCancel(long toUserNo, String toUserName, long toDeviceNo, int toDeviceType, String toDeviceName);

        long doP2pFolderList(long toDeviceNo);
        long doP2pFolderDetail(long toDeviceNo, long shareNo, long serverFolderNo, String remotePath);
        long doP2pFolderDetailRecursive(long toDeviceNo, long shareNo, String remotePath);

        long doP2pUploadQuery(long toDeviceNo, String remoteFilePath, long fileSize, long myTransNo);
        long doP2pUpload(long toDeviceNo, String remoteFilePath, String localFilePath,
                         long fileStartPosition, long myTransNo);
        long doP2pDownload(long toDeviceNo, String remoteFilePath, String localFilePath,
                           long fileSize, long fileStartPosition, long myTransNo, int folderDepth,
                           long inviteNo, long remoteFileId);
        long doP2pDownloadInviteSendFilesQuery(long toDeviceNo, long inviteNo, String certKey);
        long doP2pSaveSendFilesInviteInfo(long toDeviceNo,
                                          long inviteNo, String inviteCode, String dynamicLink,
                                          in List<String> listShortPathName, in List<String> listFileName, in long[] arrayFileSize,
                                          String certKey, String inviteMessage);

        long doP2pProcessDownloadComplete(long toDeviceNo);
        long doP2pProcessUploadComplete(long toDeviceNo);

        long doP2pProcessDownloadQueueCheck(long toDeviceNo);
        long doP2pProcessUploadQueueCheck(long toDeviceNo);

        void doP2pOpenAllChannel(in long[] arrayDeviceNo);
        void doP2pCloseChannel(long remoteDeviceNo);
        long doP2pStopAllTransfer(in long[] arrayDeviceNo);
        long doP2pCancelAllTransfer(in long[] arrayDeviceNo);

        void listAllChannelDeviceNo(in long[] arrayDeviceNo);
        void updateNumLiveTransfer(int numLiveTransfer);

        void startRelayServer();
        void stopRelayServer();

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring share folder jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        void startThreadShareFolder();
        void stopThreadShareFolder();

        void shareFolderInit();
        void shareFolderInsert(long shareNo, String sharePath, String shareName);
        void shareFolderDelete(long shareNo);
        void shareFolderUpdate(String folderPath);
        void serverFolderDelete(long serverFolderNo);

        int getShareFolderCount();
        int getShareFileCount();

        void startFileMonitor();
        void stopFileMonitor();

        void doesNeedDbBackup();

        //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring web jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        boolean listFolderOfOther(String authKey, long friendUserNo,
                                  inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                  inout List<ShareFolderInfo> listShareFolderInfo);
        boolean listLiveDevice(String authKey, String deviceNos,
                               inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                               out List<LongData> listLiveDeviceNo);
        boolean getBadgeInfo(String authKey,
                             inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                             inout BadgeInfo saaringBadgeInfo);

        boolean listFriend(String authKey,
                           inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                           inout List<FriendInfo> listFriendInfo);

        boolean requestFriendByUserNo(String authKey, long toUserNo,
                                      inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean listRequestingFriend(String authKey,
                                     inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                     inout List<FriendInfo> listIncomingRequestFriend,
                                     inout List<FriendInfo> listRequestingFriendMember,
                                     inout List<FriendInfo> listRequestingFriendNonMember);
        boolean cancelRequestFriend(String authKey, long toUserNo, String toEmail, String toPhoneNumber,
                                    inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean rejectRequestFriend(String authKey, long toUserNo,
                                    inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean recoverRequestFriend(String authKey, long toUserNo,
                                    inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        boolean addFriend(String authKey, long friendUserNo,
                          inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean blockFriend(String authKey, String friendUserNos, String blockYN,
                            inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean listBlock(String authKey,
                          inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                          inout List<BlockInfo> listBlocked, inout List<BlockInfo> listRejected);

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        boolean inviteFriendSimple(String authKey,
                                   String certKey, String inviteMessage,
                                   inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                   inout LongData inviteNo, inout StringData inviteCode, inout StringData dynamicLink);
        boolean inviteFriend(in String authKey,
                             in List<LongData> listToUserNo, in List<String> listToUserName,
                             in List<String> listToEmail, in List<String> listToPhoneNumber,
                             in String certKey, in String inviteMessage,
                             inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                             inout LongData inviteNo, inout StringData inviteCode, inout StringData dynamicLink,
                             inout List<LongData> listFailUserNo,
                             inout List<String> listSuccessEmail,
                             inout List<FriendInfo> listAddedFriend,
                             inout List<FriendInfo> listIncomingRequestFriend,
                             inout List<FriendInfo> listRequestingFriendMember,
                             inout List<FriendInfo> listRequestingFriendNonMember);
        boolean cancelInviteFriend(in String authKey, in long inviteNo,
                                   inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean completeInviteFriend(in String authKey, in long inviteNo,
                                     inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean extendInviteExpiredt(in String authKey, in long inviteNo,
                                     inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                     inout InviteInfo inviteInfo);

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        boolean getInvite(in String authKey, in long inviteNo,
                          inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                          inout InviteInfo inviteInfo);
        boolean getInviteByDynamicLink(in String dynamicLink, in String certKey,
                                       inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                       inout InviteInfo inviteInfo);

        boolean listInviteAll(in String authKey,
                              inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                              inout List<InviteInfo> listInviteInfo);

        boolean inviteShareFolderSimple(in String authKey,
                                        in long inviteShareFolderServerFolderNo,
                                        in String certKey, in String inviteMessage,
                                        inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                        inout LongData inviteNo, inout StringData inviteCode, inout StringData dynamicLink);
        boolean inviteSendFilesSimple(in String authKey,
                                      in long numFile, in long totalFileSize,
                                      in String certKey, in String inviteMessage,
                                      inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                      inout LongData inviteNo, inout StringData inviteCode, inout StringData dynamicLink);
        boolean inviteSendFilesForRemoteDeviceSimple(in String authKey,
                                      in long numFile, in long totalFileSize,
                                      in long inviteShareFolderServerFolderNo, in String inviteShareFolderPath,
                                      in String certKey, in String inviteMessage,
                                      inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                      inout LongData inviteNo, inout StringData inviteCode, inout StringData dynamicLink);

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        boolean registerInviteByDynamicLink(in String authKey,
                                            in String dynamicLink, in String certKey,
                                            inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                            inout InviteRegisteredInfo inviteRegisteredInfo,
                                            inout InviteInfo inviteInfo);
        boolean deleteInviteRegistered(in String authKey, in long inviteNo,
                                       inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean getInviteRegistered(in String authKey, in long inviteNo,
                                    inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                    inout InviteRegisteredInfo inviteRegisteredInfo,
                                    inout InviteInfo inviteInfo);
        boolean listInviteRegisteredAll(in String authKey,
                                        inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                        inout List<InviteRegisteredInfo> listInviteRegisteredInfo);

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        boolean verifyFirebaseToken(in String firebaseToken,
                                     inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean joinMember(inout MemberInfo memberInfo,
                           inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean loginMember(inout MemberInfo memberInfo,
                            inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean doMe(in String authKey, in int deviceType,
                     inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                     inout MemberInfo memberInfo, inout List<MemberProviderInfo> listMemberProviderInfo);

        // isEmailExist = "true" or "false"
        boolean isEmailExist(in String email,
                             inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                             inout StringData isEmailExist);
        boolean doUnlink(in String authKey,
                         inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean doLogout(in String authKey,
                         inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);

        boolean searchUser(in String authKey, in String keyword,
                           inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                           inout List<FriendInfo> listFriendInfo);
        boolean changeProfilePic(in String authKey, in String profilePic,
                                 inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean changeEmail(in String authKey, in String email,
                            inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean changeUserName(in String authKey, in String userName,
                               inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);

        boolean changeDeviceName(in String authKey, in String deviceName,
                                 inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);

        boolean deleteFolder(in String authKey, in long serverFolderNo,
                             inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean addOrUpdateFolder(in String authKey, inout ShareFolderInfo shareFolderInfo,
                                  inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);
        boolean listFolderAll(in String authKey,
                              inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                              inout List<ShareFolderInfo> listFolder);
        boolean checkFolderPassword(in String authKey, in long serverFolderNo, in String password,
                                    inout StringData resultCode, inout StringData resultMessage, inout StringData resultData);

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        boolean addPhoneAddress(in String authKey, in List<String> listPhoneNumber,
                                inout StringData resultCode, inout StringData resultMessage, inout StringData resultData,
                                inout List<PhoneAddressInfo> listPhoneAddress);

        boolean listPhoneAddress(in String authKey,
                                 inout StringData resultCode, inout StringData StringData, inout StringData resultData,
                                 inout List<PhoneAddressInfo> listPhoneAddress);

         //////////////////////////////////////////////////////////////////////////////////
        //
        // for saaring.cpp jni interface
        //
        //////////////////////////////////////////////////////////////////////////////////
        void test2();

}
