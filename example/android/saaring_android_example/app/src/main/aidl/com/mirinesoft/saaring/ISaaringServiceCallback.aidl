// ISaaringServiceCallback.aidl
package com.mirinesoft.saaring;

import com.mirinesoft.saaring.domain.StringData;

interface ISaaringServiceCallback {

    void onShareDirRefresh(inout StringData path);

    void onSignalingConnected();
    void onSignalingClosed();
    void onSignalingLoginFailed();

    void onSignalingNotifyDeviceLiveYN(in long fromUserNo, in long fromDeviceNo, inout StringData liveYN);
    void onSignalingNotifyRequestFriend(inout StringData friendName);
    void onSignalingNotifyAddFriend(inout StringData friendName, in long friendUserNo);

    void onP2pPortmapFinished();
    void onP2pLoginFinished(in long friendDeviceNo, in long toRemoteShareNo);
    void onP2pWakeUpFinished(in boolean isSuccess);

    void onP2pFinishDownload(in long transNo, in long fileSize, in long receivedSize);
    void onP2pFinishUpload(in long transNo, in long fileSize, in long sentSize);
    void onP2pProgressDownload(in long transNo, in long fileSize, in long receivedSize);
    void onP2pProgressUpload(in long transNo, in long fileSize, in long sentSize);
    void onP2pAbortDownload(in long transNo);
    void onP2pAbortUpload(in long transNo);

    void onP2pFinishDownloadOther(in long jobId, in long remoteDeviceNo);
    void onP2pFinishUploadOther(in long jobId, in long remoteDeviceNo);

    void onP2pProgressDownloadOther(in long jobId, in long remoteDeviceNo, in long fileSize, in long currSize, inout StringData filePath, inout StringData serverName, inout StringData deviceName);
    void onP2pProgressUploadOther(in long jobId, in long remoteDeviceNo, in long fileSize, in long currSize, inout StringData filePath, inout StringData serverName, inout StringData deviceName);

    void onP2pProcessedRequest(in int processType, in long jobId, in long remoteDeviceNo, in int resultCode);
    void onP2pProcessedResponse(in int processType, in long jobId, in long remoteDeviceNo, in int resultCode);

    void onP2pProcessResponseFolderList(inout StringData strJson);
    void onP2pProcessResponseFolderDetail(inout StringData strJson);
    void onP2pProcessResponseFolderDetailFinished();
    void onP2pProcessResponseFolderDetailRecursive(inout StringData strJson);
    void onP2pProcessResponseFileUploadQuery(in long transNo, in long fileStartPosition);
    void onP2pProcessResponseFileDownloadInviteSendFilesQuery(inout StringData strJson);
    void onP2pProcessResponseSaveSendFilesInviteInfo(inout StringData strJson);

    void onP2pNotifyDownloadQueueStandBy(in long remoteDeviceNo, in int downloadingCount, in int downloadStandByCount, in int myTurn);
    void onP2pNotifyDownloadQueueRelease(in long remoteDeviceNo, in int downloadingCount, in int downloadStandByCount, in int myTurn);
    void onP2pNotifyUploadQueueStandBy(in long remoteDeviceNo, in int uploadingCount, in int uploadStandByCount, in int myTurn);
    void onP2pNotifyUploadQueueRelease(in long remoteDeviceNo, in int uploadingCount, in int uploadStandByCount, in int myTurn);

    void onP2pNotifyConnectionClosed(in long remoteDeviceNo);
    void onP2pNotifyStartNextTransfer(in long remoteDeviceNo);
    void onP2pNotifyStatusMessage(inout StringData text);

}
