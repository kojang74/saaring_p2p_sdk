# 샤링 P2P SDK



## Summary

샤링은 멀티플랫폼을 지원하는 SaaS(Software as a Service) 형태의, P2P SDK 입니다. 샤링 P2P SDK를 이용하면 Signaling 서버, STUN 서버, TURN 서버, WAS 서버와 연동된 P2P 서비스를 개발 할 수 있습니다.

Windows x64.x86, macOS, Linux(Ubuntu), Android, iOS 를 지원합니다. 

샤링 P2P SDK는 'Qt 5.15.2' 기반으로 개발 되었습니다. 따라서, C++ 라이브러리가 기본으로 제공되고, C++ 라이브러리를 이용해서 Android 에서 사용할 수 있는 java 버전과, macOS에서 사용할 수 있는 swift버전의 라이브러리를 제공하고 있습니다.

샤링 P2P SDK를 사용하기 위해서는 P2P 시스템과 P2P 접속 프로세스에 대한 이해가 필요 합니다. 제 Blog에 있는 글을 참고 하시면 좋겠습니다.

기타 문의 사항은 메일이나 블로그로 연락 주세요.

Blog : https://blog.naver.com/kojang74
Email : kojang74@naver.com



## Directories



- Library : 각 OS별 샤링 P2P SDK 라이브러리 입니다.
- Example : 샤링 P2P SDK를 이용한, Desktop(Windows, Linux, macOS), Android, iOS 에서 작동 되는 예제 프로그램입니다.
- Library : 샤링 P2P SDK 라이브러리 세팅 방법과, 라이브러리의 각 기능별 함수 사용방법(Reference)을 정리한 문서 입니다.




## Release Note & TODO



### `2021.09.03 `

Desktop 예제 프로젝트를 추가 했습니다. Windows, Linux, macOS 에서 작동됩니다.



### `2021.09.01 `

최초 등록 했습니다.

아직 Desktop 예제가 없습니다. Qt로 만든 예제를 올릴꺼구요, Windows, Linux, macOS 에서 작동되는 통합 프로젝트가 될것입니다.



