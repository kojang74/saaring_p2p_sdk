# Common Utility Interface



## Summary

반복적으로 사용되고, 일반적으로 사용할수 있는 유틸리티 성격의 라이브러리 모음입니다.

SaaringService에서 SaaringNative.java의 JNI 함수로 제공 됩니다.



## Functions



### `double calSpeed(long oldTime, long currTime, long oldSize, long currSize);`

파일 전송 속도를 계산합니다. 시간당 전송된 파일 사이즈를 받아서, 초당 전송 byte 수를 계산해서 리턴 합니다.

전송목록 관리에서 파일 전송속도 계산할 때 사용합니다.

| return   | 초당 전송 byte 수 |
| :------- | ----------------- |
| oldTime  | 계산 시작 시간    |
| currTime | 계산 종료 시간    |
| oldSize  | 계산 시작 사이즈  |
| currSize | 계산 종료 사이즈  |



### `String leftTime(long oldTime, long currTime, long oldSize, long currSize, long totalSize, double speed);`

파일 전송을 완료하기 위해 남은 시간을 계산합니다. 전체 파일 사이즈에 대비 현재까지 전송된 파일사이즈까지 소요된 시간을 통해 계산 한다.

전송목록 관리에서 전송완료까지 남은 시간을 계산할 때 사용합니다.

| return    | 전송완료까지 남은 시간을 "0d 0h 0m 0s"문자열로 리턴합니다. |
| --------- | ---------------------------------------------------------- |
| oldTime   | 계산 시작 시간                                             |
| currTime  | 계산 종료 시간                                             |
| oldSize   | 계산 시작 사이즈                                           |
| currSize  | 계산 종료 사이즈                                           |
| totalSize | 전체 파일 사이즈                                           |
| speed     | calSpeed로 계산한 속도                                     |



### `String getSizeWithUnit(long size);`

파일 사이즈를 Byte, KB, MB, GB, TB 단위의 문자열로 리턴해준다.

폴더내 파일 목록에서 파일 사이즈를 표시하거나, 전송목록에서 파일 사이즈를 표시할때 사용합니다.

| return | Byte, KB, MB, GB, TB 단위의 문자열. |
| ------ | ----------------------------------- |
| size   | 파일 사이즈                         |



### `boolean checkEmail(String email);`

email 형식이 맞는지 체크 합니다.

로그인 또는 회원가입할때 사용합니다.

| return | Byte, KB, MB, GB, TB 단위의 문자열 |
| ------ | ---------------------------------- |
| email  | 이메일 문자열                      |



### `String getDuplicationFilename(String filePath, StringBuffer lastLocalFilePath);`

해당 path의 파일이 있는경우, 파일이름이 중복된 경우, 확장자 앞에 (1), (2)... 과 같이 숫자를 붙인 파일이름으로 새로운 path를 리턴합니다.

파일 다운로드.업로드 할때, 새 이름으로 파일을 생성할때 사용합니다.

| return            | 새로운 파일이름의 path                                       |
| ----------------- | ------------------------------------------------------------ |
| filePath          | 다운로드.업로드 할 때 로컬에 생성하려는 파일 path            |
| lastLocalFilePath | filePath 가 "c:\test\aaa.doc" 라고 할 경우,<br />return = "c:\test\aaa(1).doc" 가 되고<br />lastLocalFilePath는 "c:\test\aaa.doc" 값이 됩니다.<br />즉, lastLocalFilePath 는 return 값 바로 이전의 파일명으로, 이어받기를 하기 위한 파일명 입니다.<br />return = "c:\test\aaa(3).doc" 이면 lastLocalFilePath="c:\test\aaa(2).doc" 입니다. |



### `String numberFormatWithComma(long number, String separator);`

파일 사이즈 등을 표시 할 때, 1,000단위에 구분자로 "," 를 넣은 문자열을 리턴한다.

폴더 정보에서 파일 사이즈를 표시하거나, 전송 목록에서 파일 크기 등을 표시할때 사용됩니다.

| return    | 1,000단위에 구분자가 포함되어 포메팅된 문자열 |
| --------- | --------------------------------------------- |
| number    | 포메팅할 숫자                                 |
| separator | 구분자. default는 "," 콤마다.                 |



### `int checkUpgrade(String lastVer, String runningVer, String currentVer);`

버전 업그레이드 여부를 확인합니다. 버전은 "1.1.1" 과 같이 3단계(Major version, Minor version, Patch version) 버전을 사용합니다.

로그인 후 전송 받은 lastVer, runningVer 정보와, 현재 앱(프로그램)의 버전을 보내서, 업그레이드 여부를 판단합니다. 프로그램 실행하자 마자 호출해서, 리턴값에 따라 강제 업데이트 할지? 선택적으로 할지? 그냥 넘어갈지? 판단합니다.

| return     | -1 = 이미 최신 버전입니다. 넘어갑니다.<br /> 0 = 필수적으로 업그레이드 해야 합니다. 강제업데이트 합니다.<br /> 1 = 선택적으로 업그레이드 합니다. |
| ---------- | ------------------------------------------------------------ |
| lastVer    | 현재 배포된 최신 버전 정보.                                  |
| runningVer | 필수 업그레이드의 기준이 되는 버전 정보.                     |
| currentVer | 현재 실행중인 앱(프로그램)의 버전 정보.                      |



### `String changeProfileImage(String authKey, String path);`

이미지 파일을 서버에 업로드 하고, 프로필 이미지로 DB를 update 합니다.

환경설정에서 프로필 이미지를 세팅할 때 사용합니다.

| return  | 업로드된 프로필 이미지의 URL      |
| ------- | --------------------------------- |
| authKey | 로그인 후 전송받은, auth key      |
| path    | 업로드 할 이미지 파일의 로컬 path |




### `String getDownloadFolder();`

기본 공유폴더의 상위폴더 위치를 리턴합니다. 기본 공유폴더는 QStandardPaths::DocumentsLocation 위치 입니다.

모바일에서 공유폴더 위치를 찾을 때 사용합니다.



### `String getShareTopFolder();`

기본 다운로드 폴더 위치를 리턴 합니다. 기본 다운로드폴더는 QStandardPaths::DocumentsLocation + "/Downloads" 위치 입니다.

모바일에서 다운로드폴더 위치를 찾을 때 사용합니다.



### `String removeFirstPath(String path);`

parameter로 받은 path 에서 첫번째 folder명을 제거합니다.
path로 받은 string 에서 "/" 구분자를 기준으로, 첫번째 path를 제거한 값을 리턴합니다.
("/" 없이 리턴합니다.)

다음 두 경우 모두 "b/saaring_window.ico"를 리턴 할 수 있어야 합니다.
QString path1("a/b/saaring_window.ico");
QString path2("/a/b/saaring_window.ico");

상대방 공유폴더의 "/a/b/saaring_window.ico" 파일을 다운로드 할때, 첫번째 path는 실제 path가 아니라, 공유폴더명이기 때문에, 첫번째 공유폴더 명을 제거 한 후, ''현재위치의 local path + '/' + 'b/saaring_window.ico'" 이런식으로 다운로드 받는 파일 path를 지정할 때 사용됩니다.



### `String getProviderId(int idType);`

Firebase의 provider ID, provider Name 간의 상호 변환 유딜리티 입니다.

| return | firebase에서 사용하는 privider id<br />"password", "phonenumber", "facebook.com", "google.com", "apple.com", "naver.com", "kakao.com", "twitter.com", "wechat.com" |
| ------ | ------------------------------------------------------------ |
| idType | (아래 참조)                                                  |

```
enum class SaaringIdType {
    NONE                    = -1,               // 아무것도 아닌 상태.
    PASSWORD                = 0,                // email & password
    PHONENUMBER             = 1,                // phone number
    FACEBOOK                = 2,                // facebook 회원
    GOOGLE                  = 3,                // google 회원
    APPLE                   = 4,                // apple 회원
    NAVER                   = 5,                // Naver
    KAKAO                   = 6,                // Kakao
    TWITTER                 = 7,                // twitter
    WECHAT                  = 8                 // wechat
};

```



### `String getProviderName(int idType);`

Firebase의 provider ID, provider Name 간의 상호 변환 유딜리티 입니다.

| return | tr("Email"), tr("Phone number"), tr("Facebook"), tr("Google"), tr("Apple"), tr("Naver"), tr("Kakao"), tr("Twitter"), tr("WeChat") |
| ------ | ------------------------------------------------------------ |
| idType | (위 참조)                                                    |



### `String getProviderNameByProviderId(String providerId);`

Firebase의 provider ID, provider Name 간의 상호 변환 유딜리티 입니다.

| return | firebase에서 사용하는 privider id<br />"password", "phonenumber", "facebook.com", "google.com", "apple.com", "naver.com", "kakao.com", "twitter.com", "wechat.com" |
| ------ | ------------------------------------------------------------ |
| idType | 앱에서 사용하는 로그인 방식에 따른 서비스 이름<br />tr("Email"), tr("Phone number"), tr("Facebook"), tr("Google"), tr("Apple"), tr("Naver"), tr("Kakao"), tr("Twitter"), tr("WeChat") |

