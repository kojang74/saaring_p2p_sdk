# P2P Connection Interface



## Summary

P2P 엔진에서 제공하는 루틴입니다.

P2P 접속 및 전문 전송, 파일 전송과 관련된 기능을 제공합니다.

SaaringService에서 SaaringNative.java의 JNI 함수로 제공 됩니다.



## Functions



### `void doP2pConnect(long toUserNo, String toUserName, long toDeviceNo, int toDeviceType, String toDeviceName, boolean mustWakeUp);`

p2p 접속을 시도 합니다. p2p 접속이후 내부적으로 p2p login 단계도 진행되기 때문에, 그냥 접속만 해서는 안됩니다. 접속을 하기 위해서는 아래 parameter 정보를 알아야 하는데, 공유폴더 목록 등에는 항상 userNo, deviceNo 정보가 포함되어 있습니다.

자동재전송, 전송목록에서 전송시도, 초대링크 실행시 p2p 접속중이 아닌경우, 이때 사용됩니다.



p2p 접속 후, p2p 로그인 성공하면, 다음 callback을 받는다.

```java
    public static final int MSG_SR_ON_P2P_LOGIN_FINISHED                                            = 20001;
    public static final int MSG_SR_ON_P2P_WAKEUP_FINISHED                                           = 20002;

```



| toUserNo     | 상대방의 고유 회원번호                                       |
| ------------ | ------------------------------------------------------------ |
| toUserName   | 상대방의 회원가입시 입력한 이름                              |
| toDeviceNo   | 상대방 디바이스의 고유 번호                                  |
| toDeviceType | 디바이스 종류 (아래 참조)                                    |
| toDeviceName | 상대방이 설정에서 입력한 디바이스의 이름                     |
| mustWakeUp   | 상대 디바이스가 모바일인 경우에는 바로 접속을 할 수 없습니다. 따라서, push 메시지를 보내서 해당 폰을 깨우는 일을 먼저 해야 합니다. 모바일이고 mustWakeUp == true 이면 push 메시지를 보내고, push 메시지를 받고 해당 디바이스에 샤링이 실행되면, 그때 바로 이어서 p2p 접속을 이어서 진행합니다. |



```c++
enum class SaaringDeviceType {
    SDT_UNKNOWN             = 0,
    SDT_ANDROID             = 1,
    SDT_IOS                 = 2,
    SDT_WEB                 = 3,
    SDT_WINDOWS             = 4,
    SDT_OSX                 = 5,
    SDT_LINUX               = 6
};

```



### `void doP2pConnectShareFolder(long toUserNo, String toUserName, long toDeviceNo, int toDeviceType, String toDeviceName, long toServerFolderNo, String toFolderName, boolean mustWakeUp);`

p2p 접속후 이어서 공유폴더에 접속해서, 해당 공유폴더 정보까지 요청 합니다.

공유폴더 목록에서 해당 공유폴더로 바로 접속할 때 사용합니다.



```java
    public static final int MSG_SR_ON_P2P_LOGIN_FINISHED                                            = 20001;
    public static final int MSG_SR_ON_P2P_WAKEUP_FINISHED                                           = 20002;

    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL                                     = 22003;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED                            = 22004;

```



| toUserNo         | 상대방의 고유 회원번호                                       |
| ---------------- | ------------------------------------------------------------ |
| toUserName       | 상대방의 회원가입시 입력한 이름                              |
| toDeviceNo       | 상대방 디바이스의 고유 번호                                  |
| toDeviceType     | 디바이스 종류 (아래 참조)                                    |
| toDeviceName     | 상대방이 설정에서 입력한 디바이스의 이름                     |
| toServerFolderNo | p2p 접속 후, 바로 접속하려는 공유폴더의 공유번호             |
| toFolderName | 접속하려는 공유폴더 이름. 푸시 등을 보낼때 공유폴더 이름이 사용된다. |
| mustWakeUp       | 상대 디바이스가 모바일인 경우에는 바로 접속을 할 수 없습니다. 따라서, push 메시지를 보내서 해당 폰을 깨우는 일을 먼저 해야 합니다. 모바일이고 mustWakeUp == true 이면 push 메시지를 보내고, push 메시지를 받고 해당 디바이스에 샤링이 실행되면, 그때 바로 이어서 p2p 접속을 이어서 진행합니다. |




### `void doP2pConnectCancel(long toUserNo, String toUserName, long toDeviceNo, int toDeviceType, String toDeviceName);`

p2p 접속 요청 중이면 접속 요청을 취소 하고, 접속중이면 접속을 끊는다.

| toUserNo         | 상대방의 고유 회원번호                                       |
| ---------------- | ------------------------------------------------------------ |
| toUserName       | 상대방의 회원가입시 입력한 이름                              |
| toDeviceNo       | 상대방 디바이스의 고유 번호                                  |
| toDeviceType     | 디바이스 종류 (위 참조)                                      |
| toDeviceName     | 상대방이 설정에서 입력한 디바이스의 이름                     |



### `long doP2pFolderList(long toDeviceNo);`

공유폴더 정보를 요청합니다. (특정 deviceNo의 공유폴더만 가져올일이 없을것 같은데, 사용여부 확인해봐야함.)

다음 callback을 받습니다.

```java
public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_LIST                                       = 22002;
```

| return     | jobId 를 리턴합니다.<br />jobId 는 p2p 요청할때마다 증가 되는 sequence 값입니다.<br />요청에 실패하면 jobId < 0 값(-1)을 리턴합니다. |
| ---------- | ------------------------------------------------------------ |
| toDeviceNo | 공유폴더 목록을 받을 디바이스 번호.                          |



### `long doP2pFolderDetail(long toDeviceNo, long shareNo, long serverFolderNo, String remotePath);`

해당 공유폴더의 정보를 받습니다.

doP2pConnectShareFolder 에서 받는 callback 과 같은 callback을 받습니다.

```java
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL                                     = 22003;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED                            = 22004;

```

| return         | jobId                                                        |
| -------------- | ------------------------------------------------------------ |
| toDeviceNo     | 공유폴더가 있는 디바이스 번호                                |
| shareNo        | 공유폴더 정보는 local DB에 저장되는데, local DB에 저장될때 PK가 되는 값이다. |
| serverFolderNo | 공유폴더 정보는 remote DB에도 저장되는데, remove DB에 저장될때 PK가 되는 값이다.<br />shareNo 만 있어도 되기 때문에, shareNo가 있는경우에는, serverFolderNo=0 값을 넣어주면 된다. 내부적으로는 serverFolderNo가 있으면 해당 정보로 shareNo를 얻어온 후 사용한다. |
| remotePath     | 공유폴더 path<br />ex) '음악/아이유'<br />'음악'은 최상위 공유폴더이름이고 '아이유'는 공유폴더 밑에 있는 하위 폴더 이름입니다. |



### `long doP2pFolderDetailRecursive(long toDeviceNo, long shareNo, String remotePath);`

해당폴더의 모든 하위 폴더의 정보까지 읽어온다.

공유폴더안의 폴더를 다운로드 하려고 할때, recursive 하게 하위 폴더 정보까지 읽어와서 모두 다운로드 처리 하려고 할때 사용된다.

return, parameter는 doP2pFolderDetail와 같다.

| return      | jobId                                                        |
| ----------- | ------------------------------------------------------------ |
| toDeviceNo  | 공유폴더가 있는 디바이스 번호                                |
| shareNo     | 공유폴더 정보는 local DB에 저장되는데, local DB에 저장될때 PK가 되는 값이다. |
| remotePaths | 공유폴더 path<br />ex) '음악/아이유'<br />'음악'은 최상위 공유폴더이름이고 '아이유'는 공유폴더 밑에 있는 하위 폴더 이름입니다. |



### `long doP2pUploadQuery(long toDeviceNo, String remoteFilePath, long fileSize, long myTransNo);`

파일 업로드를 하려고 할때, 해당 위치에 파일 업로드가 가능한지(폴더가 유효한지? 권한이 있는지? 업로드 순번체크? ) 여부를 체크합니다.

다음 callback을 받고, 이어서 doP2pUpload를 호출한다.

```java
    public static final int MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY                                 = 22006;

```

| return         | jobId                                                        |
| -------------- | ------------------------------------------------------------ |
| toDeviceNo     | 공유폴더가 있는 디바이스 번호                                |
| remoteFilePath | 업로드 하려는 'remote 공유폴더 path' + 업로드 하려는 'filename'<br />ex) '영화/aaa/SetupEditPadLite.exe'<br />'영화'은 최상위 공유폴더이름이고 'aaa'는 공유폴더 밑에 있는 하위 폴더 이름이고, 'SetupEditPadLite.exe' 는 업로드 하려는 파일 이름입니다. |
| fileSize       | 업로드 하려는 파일의 크기 입니다. 이어서 올리기 체크를 위해 필요합니다. |
| myTransNo      | 다운로드, 업로드 전에 insertMyTransTbOne 를 호출해서 전송정보를 관리하는 local DB에 insert 합니다.<br />local DB에 insert 하면서 생성된 PK 값입니다.<br />화면상의 전송목록과 local DB상의 전송목록은 서로 매칭되어야 합니다. 따라서, local DB에 insert 한 후에는, 화면상의 전송목록에서 추가 해줘야 합니다. |



### `long doP2pUpload(long toDeviceNo, String remoteFilePath, String localFilePath, long fileStartPosition, long myTransNo);`

파일 업로드를 진행합니다. MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY callback을 통해 얻어온 정보들을 이용해서, 업로드 요청합니다.

MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY callback에서 받은 transNo를 이용해서 readMyTransTbByTransNo를 호출해서 local DB상의 전송 정보를 읽어와서 toDeviceNo, remoteFilepath, localFilePath를 얻어올 수 있고, fileStartPosition은 MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY callback으로 전달 받은 값입니다.

| return            | jobId                                                        |
| ----------------- | ------------------------------------------------------------ |
| toDeviceNo        | readMyTransTbByTransNo에서 얻어올수 있습니다.<br />업로드 하려는 디바이스 번호입니다. |
| remoteFilePath    | readMyTransTbByTransNo에서 얻어올 수 있습니다.<br />업로드 되는 공유폴더의 file path 입니다. |
| localFilePath     | 업로드할 파일의 local file path 입니다.                      |
| fileStartPosition | 이어올리기를 하지 않으면 0 입니다.<br />0보다 큰 값이면, 업로드할 파일의 해당 위치에서 부터 이어올리기를 합니다. |
| myTransNo         | 본 업로드 전송건에 대한 local 전송 번호 입니다.              |



### `long doP2pDownload(long toDeviceNo, String remoteFilePath, String localFilePath, long fileSize, long fileStartPosition, long myTransNo, int folderDepth, long inviteNo, long remoteFileId);`

파일을 다운로드 합니다. 업로드와 달리 바로 다운로드 진행합니다.

일반 파일 다운로드를 할 경우와, 파일보내기 초대링크를 실행해서 파일을 다운로드 할 때 사용합니다. 파일 보내기 초대 링그인 경우에는 초대링크를 통해 얻어온 inviteNo, remoteFileId 값을 사용합니다. 일반 다운로드인 경우에는 모두 값이 0 입니다.

| return            | jobId                                                        |
| ----------------- | ------------------------------------------------------------ |
| toDeviceNo        | 다운로드 하려는 공유폴더가 있는 디바이스 번호                |
| remoteFilePath    | 다운로드 하려는 공유폴더의 파일 path<br />ex) '영화/aaa/SetupEditPadLite.exe' |
| localFilePath     | 다운로드 되는 local 파일 path<br />ex) 'C:/Users/saaring/Downloads/SetupEditPadLite(1).exe'<br />이미 같은 이름의 파일이 있으면, 다른 이름으로 다운로드 합니다. |
| fileSize          | 파일 크기                                                    |
| fileStartPosition | 0이 아니면, 해당 위치에서 부터 이어받기를 진행합니다.<br />이어받기가 아니면 0 |
| myTransNo         | 전송 번호                                                    |
| folderDepth       | '영화' -> 'aaa' 폴더인 경우에는 '2'                          |
| inviteNo          | 파일보내기 초대링크를 통해 얻어온 초대 번호<br />일반 다운로드인 경우에는 0 |
| remoteFileId      | 파일보내기 초대링크를 통해 얻어온 remote 파일 ID<br />파일 정보를 remote에 저장되어 있고, 초대링크를 통해 얻어올 수 있습니다.<br />일반 다운로드인 경우에는 0 |



### `long doP2pDownloadInviteSendFilesQuery(long toDeviceNo, long inviteNo, String certKey);`

파일보내기 초대링크를 통해 파일을 다운로드 하는 경우에는, 초대링크를 통해 얻어온 inviteNo와 상대방으로 메신저 등으로 부터 알아낸 비밀번호 cert key 가 필요 합니다.

inviteNo와 certKey가 인증 되면, 다운로드 할 수 있는 파일 정보를 얻어와서, 파일 다운로드를 진행합니다.

다음 callback을 받은 후, doP2pDownload를 통해서 파일을 다운로드 합니다.

```java
public static final int MSG_SR_ON_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY             = 22007;
```

| return     | jobId                                              |
| ---------- | -------------------------------------------------- |
| toDeviceNo | 다운로드 할 디바이스 번호                          |
| inviteNo   | callback 으로 받은 초대 번호                       |
| certKey    | 메신저 등으로 친구로부터 별도로 전달 받은 비밀번호 |



### `long doP2pSaveSendFilesInviteInfo(long toDeviceNo, long inviteNo, String inviteCode, String dynamicLink, List<String> listShortPathName, List<String> listFileName, List<Long> listFileSize, String certKey, String inviteMessage);`

다른 내 디바이스의 공유폴더의 선택한 파일을 대상으로, 파일보내기 초대를 할때 사용합니다.

즉, 내 핸드폰에서 집에 실행중인 내 PC의 공유폴더에 있는 파일을 선택해서 파일보내기 초대링크를 생성하려고 할때 사용합니다.

listShortPathName, listFileName, listFileSize는 파일 선택 과정에서 알수 있고, certKey, inviteMessage는 초대링크 생성과정에서 입력합니다. inviteNo, inviteCode, dynamicLink 는 inviteSendFilesForRemoteDeviceSimple를 호출해서 초대링크 정보를 remote DB에 등록하면서 얻어냅니다.

| return            | jobId                                                        |
| ----------------- | ------------------------------------------------------------ |
| toDeviceNo        | 보내려는 파일이 있는 디바이스 번호                           |
| inviteNo          | 생성된 초대링크의 초대 번호                                  |
| inviteCode        | 생성된 초대링크의 초대 코드 번호                             |
| dynamicLink       | 생성된 초대링크                                              |
| listShortPathName | 파일 path 목록<br />["음악/01-barbra_streisand-woman_in_love.mp3","음악/01. SIGNAL.mp3"] |
| listFileName      | 파일 이름 목록<br />["01-barbra_streisand-woman_in_love.mp3","01. SIGNAL.mp3"] |
| listFileSize      | 파일 크기 목록<br />[5804797,8012582]                        |
| certKey           | 초대링크 비밀번호                                            |
| inviteMessage     | 초대링크의 초대 메시지.                                      |

```json
"certKey":"abcdefg","controlCode":2400,"dynamicLink":"https://l.saaring.com/links/fX3UbLQjtDKBHsAGA","inviteCode":"21e2d9eb271fa9b74e7fed4c9bf3c8c886961b87096e9c8d196bfc90aa749cf0","inviteMessage":"01-barbra_streisand-woman_in_love.mp3 (+1 ??)","inviteNo":"61","jobId":"40","listFileName":["01-barbra_streisand-woman_in_love.mp3","01. SIGNAL.mp3"],"listFileSize":[5804797,8012582],"listShortPathName":["???/01-barbra_streisand-woman_in_love.mp3","???/01. SIGNAL.mp3"]} , bodyData= "eyJjZXJ0S2V5IjoiYWJjZGVmZyIsImNvbnRyb2xDb2RlIjoyNDAwLCJkeW5hbWljTGluayI6Imh0dHBzOi8vbC5zYWFyaW5nLmNvbS9saW5rcy9mWDNVYkxRanRES0JIc0FHQSIsImludml0ZUNvZGUiOiIyMWUyZDllYjI3MWZhOWI3NGU3ZmVkNGM5YmYzYzhjODg2OTYxYjg3MDk2ZTljOGQxOTZiZmM5MGFhNzQ5Y2YwIiwiaW52aXRlTWVzc2FnZSI6IjAxLWJhcmJyYV9zdHJlaXNhbmQtd29tYW5faW5fbG92ZS5tcDMgKCsxIOqwnCkiLCJpbnZpdGVObyI6IjYxIiwiam9iSWQiOiI0MCIsImxpc3RGaWxlTmFtZSI6WyIwMS1iYXJicmFfc3RyZWlzYW5kLXdvbWFuX2luX2xvdmUubXAzIiwiMDEuIFNJR05BTC5tcDMiXSwibGlzdEZpbGVTaXplIjpbNTgwNDc5Nyw4MDEyNTgyXSwibGlzdFNob3J0UGF0aE5hbWUiOlsi7JiB7ZmULzAxLWJhcmJyYV9zdHJlaXNhbmQtd29tYW5faW5fbG92ZS5tcDMiLCLsmIHtmZQvMDEuIFNJR05BTC5tcDMiXX0="
```



### `long doP2pProcessDownloadComplete(long toDeviceNo);`

더이상 다운로드 할게 없으면, 다운로드 완료 메시지를 전송한다. 상대편 download queue 관리를 위해 필요하다.

상대방은 특별히 처리하는일 없다. 다운로드 큐만 정리한다. 상대방도 정기적으로 다운로드 큐를 정리하긴 하지만, 바로 처리 할 수 있도록 보내준다.

| return     | jobId                                   |
| ---------- | --------------------------------------- |
| toDeviceNo | 다운로드 완료 처리 하려는 디바이스 번호 |



### `long doP2pProcessUploadComplete(long toDeviceNo);`

더이상 업로드 할게 없으면, 업로드 완료 메시지를 전송한다. 상대편 upload queue 관리를 위해 필요하다.

상대방은 특별히 처리하는일 없다. 업로드 큐만 정리한다. 상대방도 정기적으로 업로드 큐를 정리하긴 하지만, 바로 처리 할 수 있도록 보내준다.

| return     | jobId                                 |
| ---------- | ------------------------------------- |
| toDeviceNo | 업로드 완료 처리 하려는 디바이스 번호 |



### `long doP2pProcessDownloadQueueCheck(long toDeviceNo);`

toDeviceNo 의 디바이스의 다운로드가 가능한지 순번 체크한다.

이 함수를 호출하면 다음 callback 이 호출된다.

```java
    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STANDBY                             = 30000;
    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                             = 30001;

```

| return     | jobId                                 |
| ---------- | ------------------------------------- |
| toDeviceNo | 다운로드 큐 체크 하려는 디바이스 번호 |



### `long doP2pProcessUploadQueueCheck(long toDeviceNo);`

toDeviceNo 의 디바이스의 업로드가 가능한지 순번 체크한다.

이 함수를 호출하면 다음 callback 이 호출된다.

```java
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STANDBY                               = 30002;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                               = 30003;
```

| return     | jobId                               |
| ---------- | ----------------------------------- |
| toDeviceNo | 업로드 큐 체크 하려는 디바이스 번호 |



### `void doP2pOpenAllChannel(List<Long> listDeviceNo);`

p2p 전송(다운로드.업로드)를 재개 한다. 전송목록에서 pause/stop 되어 있는것을 resume하거나, 순번대기 중이다가 순번을 받아서 전송을 재개 할때 사용한다.

이 함수를 호출하면 다음 callback 이 호출된다.

```c++
saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_NOTIFY_START_NEXT_TRANSFER
```

또한, 다음 callback 이 왔을때 이 함수를 호출 해서 전송을 재개 하면 된다.

```java
    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STANDBY                             = 30000;
    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                             = 30001;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STANDBY                               = 30002;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                               = 30003;
```

| return       | jobId                                 |
| ------------ | ------------------------------------- |
| listDeviceNo | 업로드 완료 처리 하려는 디바이스 번호 |



### `void doP2pCloseChannel(long remoteDeviceNo);`

모든 p2p 접속을 끊는다.

상대방이 로그아웃, 또는 접속이 종료 됐을때, 아래 callback을 받게 되는데, 이때 이 함수로 모든 p2p 접속을 종료하면 된다.

```java
    public static final int MSG_SR_ON_SIGNALING_NOTIFY_DEVICE_LIVEYN                                = 11000;
```

| return         | jobId                              |
| -------------- | ---------------------------------- |
| remoteDeviceNo | 접속 종료 처리하려는 디바이스 번호 |



### `long doP2pStopAllTransfer(List<Long> listDeviceNo);`

해당 디바이스의 모든 전송(다운로드.업로드)을 멈춥니다. 내부적으로는 pause 처리 됩니다.

의미적으로 stop은 접속을 끊고, 전송을 완전 중단해야겠지만, 실질적으로는 pause로 처리됩니다. 어렵게 p2p 접속 했는데, 굳이 끊을 필요까지 없기 때문입니다.

| return       | jobId                               |
| ------------ | ----------------------------------- |
| listDeviceNo | 전송 중지 처리 하려는 디바이스 번호 |



### `long doP2pCancelAllTransfer(List<Long> listDeviceNo);`

해당 디바이스의 전송건에 대해, 전송을 취소하고 전송 목록에서 제거 합니다. p2p 접속을 끊지는 않습니다.

전송목록에서 삭제를 눌렀을때 전송목록 UI에서도 제거하고, deleteMyTransTbMulti 로 local DB에서도 삭제 하고, p2p 전송목록에서도 제거 합니다.

| return       | jobId                               |
| ------------ | ----------------------------------- |
| listDeviceNo | 전송 종료 처리 하려는 디바이스 번호 |



### `void listAllChannelDeviceNo(List<Long> listDeviceNo);`

현재 p2p 엔진에서 관리하고 있는 p2p 접속중인 디바이스의 디바이스 번호 목록을 가져옵니다.

로그아웃을 하거나, Signaling Server와 접속이 끊어졌을때, 

| return       | jobId                                        |
| ------------ | -------------------------------------------- |
| listDeviceNo | p2p 접속중인 디바이스 번호목록을 받아올 list |



### `void updateNumLiveTransfer(int numLiveTransfer);`

현재 전송중인 모든 전송 건수를 p2p엔진에 알려준다. 해당 정보는 local relay 서버 정보를 위해 사용되며, remote DB에 업데이트 된다.

| numLiveTransfer | 현재 전송중인 모든 전송 건수 |
| --------------- | ---------------------------- |



