# Signaling, Relay, Rendezvous Server Interface



## Summary

각종 서버 정보를 얻어오는 기능.

Signaling Server에 접속하고, 로그인 하기 위한 루틴.

Relay Server를 실행.종료 하고 그 정보를 확인하는 루틴.

SaaringService에서 SaaringNative.java의 JNI 함수로 제공 됩니다.



## Functions



### `SaaringServerInfo getServerInfo();`

saaring 에서 운영하고 있는, 다음의 각종 서버 정보를 얻어옵니다.

relayIp, relayPort도 saaring에서 운영하고 있는 Relay Server의 IP/port 정보 입니다.

```java
    String          signalingIp                  = "";
    int             signalingPort;
    String          rendezvousIp                 = "";
    int             rendezvousPort;
    String          relayIp                      = "";
    int             relayPort;
    String          runningVersion               = "";
    String          lastVersion                  = "";

```



### `boolean isSignalingConnected();`

Signaling Server와 접속 됐는지 여부를 확인 합니다.

Signaling Server에 접속 시도하기 전에, 확인차 호출합니다.



### `boolean isSignalingLogin();`

Signaling Server에 로그인 되어 있는지 여부를 확인 합니다.

Signaling Server에 로그인 하기 전에, 확인차 호출합니다.



### `boolean doSignalingConnect(String authKey, boolean isRefreshServerInfo);`

Signaling Server에 접속을 시도합니다. Web 로그인 후, 얻어온 auth key를 이용해서 로그인  시도합니다.

프로그램 최초 실행시와, 자동 재접속시 사용합니다.

접속 결과는 onSignalingConnected callback 으로 받아서 처리 합니다.

| return              | true = 호출 성공<br />false = 호출 실패                      |
| ------------------- | ------------------------------------------------------------ |
| authKey             | Web 로그인 후 얻어온 auth key 값                             |
| isRefreshServerInfo | true = 서버정보를 새로 받아서 재접속 시도 합니다. 자동 재접속을 하는 경우에 true를 이용합니다.<br />false = 프로그램 처음 실행하면서 접속을 시도 할때에는, false 값을 사용합니다. |



### `void doSignalingClose();`

Signaling Server와의 접속을 끊습니다.

접속이 끊어지면, onSignalingClosed callback 으로 받아서 처리합니다.



### `void reqHeartBeat();`
Signaling Server와 접속을 유지 하기 위한 Heart Beat을 보냅니다.

내부적으로 4분에 1회 보내서, 접속을 유지하지만, 수동으로 직접 보낼 수 도 있습니다. 25분 동안 heart beat 이 없으면, Signaling Server 가 접속을 끊습니다.



### `bool isP2pLived(qint64 deviceNo);`

해당 deviceNo 의 디바이스와 내가 p2p 접속중인지 여부를 확인합니다.

파일보내기 초대링크를 실행할때, 해당 디바이스가 접속중이면 바로 다운로드 진행하고, 해당 디바이스가 접속중이 아니면 파일보내기 초대링크 정보를 저장하고, 일단 p2p 접속부터 시도한다.

p2p 접속 시도할때는 p2p 엔진에서 알아서 처리하기 때문에, 굳이 신경쓰지 않아도 되지만, 파일보내기 초대링크 실행시에는 이 함수로 확인한다.

