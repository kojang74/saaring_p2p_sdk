# Signaling Server Callback



## Summary

Signaling Server 접속 이후, 접속 상태 및 각종 알림을 callback으로 비동기적으로 받아서 처리 합니다.

샤링 p2p 엔진에서 SaaringNative.java 에 있는 static 메소드 호출을 통해서 callback 이 이뤄 지고, SaaringNative 에서는 다시 AIDL의 Handler를 통해서 callback 이 이뤄집니다.



```java
    public static final int MSG_SR_ON_SHARE_DIR_REFRESH                                             = 1;

    public static final int MSG_SR_ON_SIGNALING_CONNECTED                                           = 10000;
    public static final int MSG_SR_ON_SIGNALING_CLOSED                                              = 10001;
    public static final int MSG_SR_ON_SIGNALING_LOGIN_FAILED                                        = 10002;

    public static final int MSG_SR_ON_SIGNALING_NOTIFY_DEVICE_LIVEYN                                = 11000;
    public static final int MSG_SR_ON_SIGNALING_NOTIFY_REQUEST_FRIEND                               = 11001;
    public static final int MSG_SR_ON_SIGNALING_NOTIFY_ADD_FRIEND                                   = 11002;

    public static final int MSG_SR_ON_P2P_PORTMAP_FINISHED                                          = 20000;
    public static final int MSG_SR_ON_P2P_LOGIN_FINISHED                                            = 20001;
    public static final int MSG_SR_ON_P2P_WAKEUP_FINISHED                                           = 20002;

    public static final int MSG_SR_ON_P2P_FINISH_DOWNLOAD                                           = 21000;
    public static final int MSG_SR_ON_P2P_FINISH_UPLOAD                                             = 21001;
    public static final int MSG_SR_ON_P2P_PROGRESS_DOWNLOAD                                         = 21002;
    public static final int MSG_SR_ON_P2P_PROGRESS_UPLOAD                                           = 21003;
    public static final int MSG_SR_ON_P2P_ABORT_DOWNLOAD                                            = 21004;
    public static final int MSG_SR_ON_P2P_ABORT_UPLOAD                                              = 21005;
    public static final int MSG_SR_ON_P2P_FINISH_DOWNLOAD_OTHER                                     = 21006;
    public static final int MSG_SR_ON_P2P_FINISH_UPLOAD_OTHER                                       = 21007;
    public static final int MSG_SR_ON_P2P_PROGRESS_DOWNLOAD_OTHER                                   = 21008;
    public static final int MSG_SR_ON_P2P_PROGRESS_UPLOAD_OTHER                                     = 21009;

    public static final int MSG_SR_ON_P2P_PROCESSED_REQUEST                                         = 22000;
    public static final int MSG_SR_ON_P2P_PROCESSED_RESPONSE                                        = 22001;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_LIST                                       = 22002;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL                                     = 22003;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED                            = 22004;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE                           = 22005;
    public static final int MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY                                 = 22006;
    public static final int MSG_SR_ON_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY             = 22007;
    public static final int MSG_SR_ON_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO                       = 22008;

    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STANDBY                             = 30000;
    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                             = 30001;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STANDBY                               = 30002;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                               = 30003;

    public static final int MSG_SR_ON_P2P_NOTIFY_CONNECTION_CLOSED                                  = 40000;
    public static final int MSG_SR_ON_P2P_NOTIFY_START_NEXT_TRANSFER                                = 40001;
    public static final int MSG_SR_ON_P2P_NOTIFY_STATUS_MESSAGE                                     = 40002;

```

## Callback



### `MSG_SR_ON_SIGNALING_CONNECTED`

```java
public static void onSignalingConnected()
```

Web 로그인 후, Signaling Server에 socket 접속 후 로그인에 성공 했습니다.

모든 로그인 프로세스가 끝났기 때문에, 메인 프로그램을 실행 합니다.



### `MSG_SR_ON_SIGNALING_CLOSED`

```java
public static void onSignalingClosed()
```

Signaling Server와 접속이 끊어졌습니다. 로그아웃 처리는 Signaling Server에서 처리 됐을것이기 때문에, Local 상황만 정리하면 됩니다.

메인 프로그램을 종료하고, Local Relay 서버가 실행중이라면 종료하고, 자동 재접속 모드로 넘어갑니다.



### `MSG_SR_ON_SIGNALING_LOGIN_FAILED`

```java
public static void onSignalingLoginFailed()
```

Signaling Server에 로그인에 실패 했습니다. 처음부터 다시 로그인을 시도 합니다.



### `MSG_SR_ON_SIGNALING_NOTIFY_DEVICE_LIVEYN`

```java
public static void onSignalingNotifyDeviceLiveYN(long fromUserNo, long fromDeviceNo, String liveYN
```

해당 친구의 디바이스가 접속됐음.접속종료됐음을 알려줍니다.

현재 화면이, 친구목록 또는 친구의 공유폴더 목록이면, 해당 친구와 디바이스의 공유폴더를 접속 가능 상태로 변경해줍니다.

| fromUserNo   | long   | 접속한 회원 번호           |
| ------------ | ------ | -------------------------- |
| fromDeviceNo | long   | 접속한 디바이스 번호       |
| liveYN       | String | Y=접속됐음, N=접속종료됐음 |



### `MSG_SR_ON_SIGNALING_NOTIFY_REQUEST_FRIEND`

```java
public static void onSignalingNotifyRequestFriend(String friendName)
```

친구 요청이 있습니다.

friendName의 친구가 친구 요청이 있음을 notibar에 표시해주고, 정확한 친구요청 목록을 읽어와서, '수락', '거절' 할 수 있도록 합니다.

| friendName | String | 친구 요청을 한 친구의 이름 |
| ---------- | ------ | -------------------------- |



### `MSG_SR_ON_SIGNALING_NOTIFY_ADD_FRIEND`

```java
public static void onSignalingNotifyAddFriend(String friendName, long friendUserNo)
```

친구 등록 알림. 친구 요청에 대해 수락 해서, 친구로 등록되었음을 알려주는 알림입니다.

알림을 터치하면 친구목록으로 이동해줍니다.

| friendName   | String | 친구의 이름                |
| ------------ | ------ | -------------------------- |
| friendUserNo | long   | 친구의 회원번호            |

