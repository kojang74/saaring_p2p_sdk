# Tutorial - Invite Link



## Summary

샤링에서 파일을 보내거나, 폴더를 공유하려면, 친구를 맺어야 합니다. 그런데, 비회원인 경우에는 샤링 앱을 설치하고, 가입까지 해야 하기 때문에, 프로세스가 길고 진입장벽이 높습니다. 이 문제를 해결 하기 위해, 초대링크 기능을 제공합니다.

초대링크는 Firebase Dynamic Link로 만들어 집니다. 초대링크를 클릭하면, 해당 플랫폼에 맞는 앱을 설치하도록 합니다. 이미 앱이 설치 되어 있으면, 앱을 실행시킵니다. 앱 실행 후에는 회원가입을 해야 하고, 이미 회원가입이 되어 있으면 자동로그인 되어, 초대링크 등록 화면으로 이동해야 합니다. 초대링크는 자동으로 입력 되어, 등록 할 수 있도록 해야 합니다.

초대링크를 등록하게 되면, 자동으로 초대링크를 만든 사람과 친구가 맺어집니다. 초대링크에는 비밀번호를 포함하고 있을 수 있습니다. 초대링크가 유출돼서 아무나 친구가 맺어지면 곤란해지기 때문입니다. 초대링크 비밀번호는 링크와 함께 별도로 알려줘야 합니다. 또한, 초대링크는 유효기간이 있습니다. 시간이 지나면 유효하지 않고, 유효기간은 연장이 가능합니다. 초대링크를 삭제 하면 역시 사용할 수 없는데, 삭제한 초대링크는 되살릴 수 없습니다.

초대링크는 '일반 초대링크, 폴더공유 초대링크, 파일보내기 초대링크' 3가지가 있습니다. 일반 초대링크는 친구까지만 맺어줍니다. 폴더공유 초대링크는 친구맺고, 해당 공유폴더까지 이동 합니다. 파일보내기 초대링크는 친구 맺고, 포함된 파일을 다운로드 합니다.





## Functions



### `일반 초대링크 만들기`

일반 초대링크는 아무 정보 없이, 초대 메시지와 비밀번호만 설정해서 생성할 수 있습니다.
saaringService.inviteFriendSimple를 호출해서 초대링크를 생성하고, 생성된 초대링크 정보를 받을 수 있습니다.
내가 만든 초대링크는 saaringService.listInviteAll 를 호출해서 목록을 가져와서 보여줍니다.



### `공유폴더 초대링크 만들기`

공유폴더 초대링크를 만들기 위해서는 먼저 공유폴더 목록에서 공유폴더를 선택 해야 합니다. 이후 화면에서 초대메시지와 비밀번호를 입력 받아서, saaringService.inviteShareFolderSimple 를 호출해서, 공유폴더 초대링크를 생성할 수 있습니다.



### `파일보내기 초대링크 만들기`

파일보내기 초대링크는 현재 내 디바이스에 있는 파일을 선택해서 파일보내기 초대링크를 생성하는 방법과, 내 다른 디바이스의 파일을 선택해서 파일보내기 초대링크를 생성하는 방법이 있습니다.
현재 내 디바이스의 파일보내기 초대링크를 만들기 위해서는, 현재 디바이스의 파일을 먼저 선택하고, 이후 초대메시지와 비밀번호를 입력 받은 후 saaringService.inviteSendFilesSimple 를 호출해서 초대링크를 생성합니다.
내 다른 디바이스의 파일보내기 초대링크를 선택할 때에는 해당 디바이스에 접속한 상태에서 파일을 선택후, saaringService.inviteSendFilesForRemoteDeviceSimple를 호출해서 초대링크를 생성할 수 있습니다.
파일보내기 초대링크는 해당 초대링크에 포함된 파일 정보를 해당 디바이스의 local DB에만 저장을 하기 때문에, 다른 디바이스의 파일보내기 초대링크 생성을 위해서는 해당 파일 정보를 saaringService.inviteSendFilesForRemoteDeviceSimple 를 호출해서 저장해야 합니다. 파일정보는 saaringService.insertInviteFiles를 호출해서 local DB에 저장합니다.
saaringService.inviteSendFilesForRemoteDeviceSimple 호출 경과는 MSG_SR_ON_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO callback으로 회신이 옵니다. 따라서, 생성중인 초대링크 정보는 별도로 저장하고 있어야 하면, callback을 받은 다음에는, saaringService.completeInviteFriend 를 호출해서 파일보내기 초대링크 생성을 완료해줘야 합니다.



### `초대링크 등록.목록.연장.삭제.상세정보 `

초대링크 등록은 saaringService.registerInviteByDynamicLink 를 호출해서 등록합니다.
등록한 초대링크는 saaringService.listInviteRegisteredAll 를 호출해서 목록을 받아올 수 있습니다.
saaringService.deleteInviteRegistered 를 호출해서 등록한 초대 링크를 삭제 할수 있습니다.

내가 만들 초대링크는 saaringService.listInviteAll 를 호출해서 목록을 받아올 수 있습니다.
saaringService.getInviteRegistered 를 호출해서 상세 정보를 얻어올 수 있습니다. 해당 링크를 실행하거나, 유효기간을 연장할 때 사용합니다.
내가 만든 초대링크는 유효기간을 연장할 수 있는데, saaringService.extendInviteExpiredt 를 호출해서 기간을 연장 할 수 있습니다. 또한, 삭제 할때에는 saaringService.cancelInviteFriend 를 호출해서 삭제 합니다.

