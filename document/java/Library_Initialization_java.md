# Library Initialization



## Summary

샤링 P2P Java 라이브러리는 내부적으로 Qt C++ 라이브러리를 기반으로 되어 있기 때문에 JNI 형태로 제공됩니다. 

Qt 5.15.2 버전 기준입니다.

샤링 P2P Java 라이브러리를 Android에서 사용하려면, 샤링 P2P 엔진은 멀티스레드로 작동 되기 때문에, AIDL을 이용해서 개발 합니다. 샤링 P2P엔진은 Android Service로 작동 되면서, Binder를 통해서 JNI 함수를 호출할수 있게 되고, Service에서 Callback을 받아 Message를 sendMessage하면, Handler에서 다시 ServiceConnection의 메소드를 callback으로 호출해서 Activity 로 까지 전달 됩니다.

AIDL 프로그래밍과 관련한 사항은 아래 글을 참조 합니다.

https://oysu.tistory.com/17

https://developer.android.com/guide/components/aidl?hl=ko

전체적인 시스템 구조는 다음과 같습니다.

![saaring_aidl](./images/saaring_aidl.png)





## Functions

샤링 P2P Android Java 라이브러리를 초기화 하기 위해서 별도로 처리해줄 필요는 없고, Qt 용 라이브러리를 Android Service로 사용하기 위해, 다음과같이 AndroidManifest.xml 파일에 Service를 추가해주고, 제공되는 합니다.

앱의 기본이 되는 Main Activity를 GatewayActivity 로 정하고, GatewayActivity에서는 언어 설정을 위해 "String getDefaultLanguage()"  함수를 선언해야 되는데, 이런 제약 사항은 향후 변경될 수 있습니다.

좀 더 자세한 사항은 https://wiki.qt.io/AndroidServices 를 참고하면 됩니다.



```
<?xml version='1.0' encoding='utf-8'?>
<manifest package="com.mirinesoft.saaring" xmlns:android="http://schemas.android.com/apk/res/android" android:versionName="1.0" android:versionCode="1" android:installLocation="auto">
    <uses-sdk android:minSdkVersion="21" android:targetSdkVersion="28"/>

    <!-- The following comment will be replaced upon deployment with default permissions based on the dependencies of the application.
         Remove the comment if you do not require these default permissions. -->
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.CAMERA" />

    <!-- The following comment will be replaced upon deployment with default features based on the dependencies of the application.
         Remove the comment if you do not require these default features. -->


    <supports-screens android:largeScreens="true" android:normalScreens="true" android:anyDensity="true" android:smallScreens="true"/>

    <application
        android:hardwareAccelerated="true"
        android:name="org.qtproject.qt5.android.bindings.QtApplication"
        android:label="Saaring"
        android:extractNativeLibs="true"
        android:supportsRtl="true"
        android:usesCleartextTraffic="true">

        <service
            android:name=".SaaringService"
            android:enabled="true"
            android:process=":saaringP2p">

            <!-- <service android:process=":qt" android:enabled="true" android:name="org.qtproject.qt5.android.bindings.QtService"> -->
            <!-- android:process=":qt" is needed to force the service to run on a separate process than the Activity -->


            <!-- Application arguments -->
            <meta-data
                android:name="android.app.arguments"
                android:value="-service" />
            <!-- Application arguments -->


            <meta-data android:name="android.app.lib_name" android:value="sanet"/>
            <meta-data android:name="android.app.qt_sources_resource_id" android:resource="@array/qt_sources"/>
            <meta-data android:name="android.app.repository" android:value="default"/>
            <meta-data android:name="android.app.qt_libs_resource_id" android:resource="@array/qt_libs"/>
            <meta-data android:name="android.app.bundled_libs_resource_id" android:resource="@array/bundled_libs"/>
            <!-- Deploy Qt libs as part of package -->
            <meta-data android:name="android.app.bundle_local_qt_libs" android:value="1"/>

            <!-- Run with local libs -->
            <meta-data android:name="android.app.use_local_qt_libs" android:value="1"/>
            <meta-data android:name="android.app.libs_prefix" android:value="/data/local/tmp/qt/"/>
            <meta-data android:name="android.app.load_local_libs_resource_id" android:resource="@array/load_local_libs"/>
            <meta-data android:name="android.app.load_local_jars" android:value="jar/QtAndroid.jar:jar/QtAndroidExtras.jar:jar/QtAndroidBearer.jar"/>
            <meta-data android:name="android.app.static_init_classes" android:value=""/>
            <!-- Used to specify custom system library path to run with local system libs -->
            <!-- <meta-data android:name="android.app.system_libs_prefix" android:value="/system/lib/"/> -->
            <!--  Messages maps -->
            <meta-data android:value="@string/ministro_not_found_msg" android:name="android.app.ministro_not_found_msg"/>
            <meta-data android:value="@string/ministro_needed_msg" android:name="android.app.ministro_needed_msg"/>
            <meta-data android:value="@string/fatal_error_msg" android:name="android.app.fatal_error_msg"/>
            <meta-data android:value="@string/unsupported_android_version" android:name="android.app.unsupported_android_version"/>
            <!--  Messages maps -->


            <!-- Background running -->
            <meta-data
                android:name="android.app.background_running"
                android:value="true" />
            <!-- Background running -->

            <intent-filter>
                <action android:name="com.mirinesoft.saaring.SaaringService.ACTION" />
            </intent-filter>
        </service>

        ......

        <activity android:configChanges="orientation|uiMode|screenLayout|screenSize|smallestScreenSize|layoutDirection|locale|fontScale|keyboard|keyboardHidden|navigation|mcc|mnc|density"
            android:name=".GatewayActivity"
            android:label="Saaring"
            android:screenOrientation="unspecified"
            android:launchMode="singleTop"
            android:theme="@style/Theme.Saaring">>
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.LAUNCHER"/>
            </intent-filter>

            <!-- Application arguments -->
            <!-- meta-data android:name="android.app.arguments" android:value="arg1 arg2 arg3"/ -->
            <!-- Application arguments -->

            <meta-data android:name="android.app.lib_name" android:value="sanet"/>
            <meta-data android:name="android.app.qt_sources_resource_id" android:resource="@array/qt_sources"/>
            <meta-data android:name="android.app.repository" android:value="default"/>
            <meta-data android:name="android.app.qt_libs_resource_id" android:resource="@array/qt_libs"/>
            <meta-data android:name="android.app.bundled_libs_resource_id" android:resource="@array/bundled_libs"/>
            <!-- Deploy Qt libs as part of package -->
            <meta-data android:name="android.app.bundle_local_qt_libs" android:value="1"/>

            <!-- Run with local libs -->
            <meta-data android:name="android.app.use_local_qt_libs" android:value="1"/>
            <meta-data android:name="android.app.libs_prefix" android:value="/data/local/tmp/qt/"/>
            <meta-data android:name="android.app.load_local_libs_resource_id" android:resource="@array/load_local_libs"/>
            <meta-data android:name="android.app.load_local_jars" android:value="jar/QtAndroid.jar:jar/QtAndroidExtras.jar:jar/QtAndroidBearer.jar"/>
            <meta-data android:name="android.app.static_init_classes" android:value=""/>
            <!-- Used to specify custom system library path to run with local system libs -->
            <!-- <meta-data android:name="android.app.system_libs_prefix" android:value="/system/lib/"/> -->
            <!--  Messages maps -->
            <meta-data android:value="@string/ministro_not_found_msg" android:name="android.app.ministro_not_found_msg"/>
            <meta-data android:value="@string/ministro_needed_msg" android:name="android.app.ministro_needed_msg"/>
            <meta-data android:value="@string/fatal_error_msg" android:name="android.app.fatal_error_msg"/>
            <meta-data android:value="@string/unsupported_android_version" android:name="android.app.unsupported_android_version"/>
            <!--  Messages maps -->

            <!-- Splash screen -->
            <!-- Orientation-specific (portrait/landscape) data is checked first. If not available for current orientation,
                 then android.app.splash_screen_drawable. For best results, use together with splash_screen_sticky and
                 use hideSplashScreen() with a fade-out animation from Qt Android Extras to hide the splash screen when you
                 are done populating your window with content. -->
            <!-- meta-data android:name="android.app.splash_screen_drawable_portrait" android:resource="@drawable/logo_portrait" / -->
            <!-- meta-data android:name="android.app.splash_screen_drawable_landscape" android:resource="@drawable/logo_landscape" / -->
            <!-- meta-data android:name="android.app.splash_screen_drawable" android:resource="@drawable/logo"/ -->
            <!-- meta-data android:name="android.app.splash_screen_sticky" android:value="true"/ -->
            <!-- Splash screen -->

            <!-- Background running -->
            <!-- Warning: changing this value to true may cause unexpected crashes if the
                          application still try to draw after
                          "applicationStateChanged(Qt::ApplicationSuspended)"
                          signal is sent! -->
            <meta-data android:name="android.app.background_running" android:value="false"/>
            <!-- Background running -->

            <!-- auto screen scale factor -->
            <meta-data android:name="android.app.auto_screen_scale_factor" android:value="false"/>
            <!-- auto screen scale factor -->

            <!-- extract android style -->
            <!-- available android:values :
                * default - In most cases this will be the same as "full", but it can also be something else if needed, e.g., for compatibility reasons
                * full - useful QWidget & Quick Controls 1 apps
                * minimal - useful for Quick Controls 2 apps, it is much faster than "full"
                * none - useful for apps that don't use any of the above Qt modules
                -->
            <meta-data android:name="android.app.extract_android_style" android:value="default"/>
            <!-- extract android style -->
        </activity>

        <!-- For adding service(s) please check: https://wiki.qt.io/AndroidServices -->
        
        ......

    </application>

</manifest>

```

