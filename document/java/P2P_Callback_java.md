# Share Folder Callback



## Summary

P2p 모듈은 비동기적으로 처리 됩니다.  P2p 접속 및 P2p 요청에 대한 회신을 모두 callback 으로 비동기적으로 회신 받아서 처리 합니다.

샤링 p2p 엔진에서 SaaringNative.java 에 있는 static 메소드 호출을 통해서 callback 이 이뤄 지고, SaaringNative 에서는 다시 AIDL의 Handler를 통해서 callback 이 이뤄집니다.



```java
    public static final int MSG_SR_ON_SHARE_DIR_REFRESH                                             = 1;

    public static final int MSG_SR_ON_SIGNALING_CONNECTED                                           = 10000;
    public static final int MSG_SR_ON_SIGNALING_CLOSED                                              = 10001;
    public static final int MSG_SR_ON_SIGNALING_LOGIN_FAILED                                        = 10002;

    public static final int MSG_SR_ON_SIGNALING_NOTIFY_DEVICE_LIVEYN                                = 11000;
    public static final int MSG_SR_ON_SIGNALING_NOTIFY_REQUEST_FRIEND                               = 11001;
    public static final int MSG_SR_ON_SIGNALING_NOTIFY_ADD_FRIEND                                   = 11002;

    public static final int MSG_SR_ON_P2P_PORTMAP_FINISHED                                          = 20000;
    public static final int MSG_SR_ON_P2P_LOGIN_FINISHED                                            = 20001;
    public static final int MSG_SR_ON_P2P_WAKEUP_FINISHED                                           = 20002;

    public static final int MSG_SR_ON_P2P_FINISH_DOWNLOAD                                           = 21000;
    public static final int MSG_SR_ON_P2P_FINISH_UPLOAD                                             = 21001;
    public static final int MSG_SR_ON_P2P_PROGRESS_DOWNLOAD                                         = 21002;
    public static final int MSG_SR_ON_P2P_PROGRESS_UPLOAD                                           = 21003;
    public static final int MSG_SR_ON_P2P_ABORT_DOWNLOAD                                            = 21004;
    public static final int MSG_SR_ON_P2P_ABORT_UPLOAD                                              = 21005;
    public static final int MSG_SR_ON_P2P_FINISH_DOWNLOAD_OTHER                                     = 21006;
    public static final int MSG_SR_ON_P2P_FINISH_UPLOAD_OTHER                                       = 21007;
    public static final int MSG_SR_ON_P2P_PROGRESS_DOWNLOAD_OTHER                                   = 21008;
    public static final int MSG_SR_ON_P2P_PROGRESS_UPLOAD_OTHER                                     = 21009;

    public static final int MSG_SR_ON_P2P_PROCESSED_REQUEST                                         = 22000;
    public static final int MSG_SR_ON_P2P_PROCESSED_RESPONSE                                        = 22001;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_LIST                                       = 22002;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL                                     = 22003;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED                            = 22004;
    public static final int MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE                           = 22005;
    public static final int MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY                                 = 22006;
    public static final int MSG_SR_ON_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY             = 22007;
    public static final int MSG_SR_ON_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO                       = 22008;

    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STANDBY                             = 30000;
    public static final int MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                             = 30001;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STANDBY                               = 30002;
    public static final int MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                               = 30003;

    public static final int MSG_SR_ON_P2P_NOTIFY_CONNECTION_CLOSED                                  = 40000;
    public static final int MSG_SR_ON_P2P_NOTIFY_START_NEXT_TRANSFER                                = 40001;
    public static final int MSG_SR_ON_P2P_NOTIFY_STATUS_MESSAGE                                     = 40002;

```

## Callback



### `MSG_SR_ON_P2P_PORTMAP_FINISHED`

```java
public static void onP2pPortmapFinished()
```

P2P portmap 초기화가 끝났음을 callback으로 알림을 받습니다. portmap 초기화가 끝나야, TCP p2p 통신을위한 IP/port가 정해져서 통신을 할 수 있습니다.

자동 재접속을 시도하고, Local Relay 서버를 실행시킵니다.



### `MSG_SR_ON_P2P_LOGIN_FINISHED`

```java
public static void onP2pLoginFinished(long friendDeviceNo, long toRemoteShareNo)
```

P2p 접속 후, p2p 로그인 까지 성공 했습니다. 

자동 재전송 여부에 따라, 자동 재전송 합니다. 파일보내기 초대링크 실행 여부에 따라, 이어서 파일 다운로드를 진행한다.

| friendDeviceNo  | qint64 | 접속 된 deviceNo                             |
| --------------- | ------ | -------------------------------------------- |
| toRemoteShareNo | qint64 | 폴더로 바로 접속한 경우, 해당 폴더의 shareNo |



### `MSG_SR_ON_P2P_WAKEUP_FINISHED`

```java
public static void onP2pWakeUpFinished(boolean isSuccess)
```

P2p 접속을 위해, 모바일 단말기를 깨우기 위해 Wake-up 요청을 했고, Wake-up에 대한 결과를 받는 callback 입니다.

실패한 경우에는 현재 진행중인 p2p 접속을 중단하고, 성공한 경우에는 이어서 p2p 접속이 진행중이기 때문에 접속 문구 등을 변경해줍니다.

| isSuccess | bool | 0 = false = 실패, 그밖의 값 = true = 성공 |
| --------- | ---- | ----------------------------------------- |



### `MSG_SR_ON_P2P_FINISH_DOWNLOAD`

```java
public static void onP2pFinishDownload(long transNo, long fileSize, long receivedSize)
```

다운로드 완료 되었습니다. 전체 파일을 모두 다운로드 했을 수 도 있고. 중지 됐을 수 도 있습니다.

filesize == receivedSize 이면 전송 상태는 SEND_STATUS_DOWNLOAD_FINISHED. 서로 다르면 SEND_STATUS_DOWNLOAD_STOPPED

다운로드 완료인 경우 다음 다운로드를 진행합니다.

| transNo      | qint64 | 전송 번호                    |
| ------------ | ------ | ---------------------------- |
| fileSize     | qint64 | 파일 사이즈.                 |
| receivedSize | qint64 | 다운로드 전송 완료된 사이즈. |



전송상태는 다음의 값으로 관리됩니다.

```java
public  static  final  int SEND_STATUS_NONE                 = 0;
public  static  final  int SEND_STATUS_DOWNLOAD_WAITING     = 1;
public  static  final  int SEND_STATUS_DOWNLOAD_STAND_BY    = 2;
public  static  final  int SEND_STATUS_DOWNLOADING          = 3;
public  static  final  int SEND_STATUS_DOWNLOAD_STOPPED     = 10;
public  static  final  int SEND_STATUS_DOWNLOAD_FAILED      = 11;
public  static  final  int SEND_STATUS_DOWNLOAD_FINISHED    = 99;
public  static  final  int SEND_STATUS_UPLOAD_WAITING       = 101;
public  static  final  int SEND_STATUS_UPLOAD_STAND_BY      = 102;
public  static  final  int SEND_STATUS_UPLOADING            = 103;
public  static  final  int SEND_STATUS_UPLOAD_STOPPED       = 110;
public  static  final  int SEND_STATUS_UPLOAD_FAILED        = 111;
public  static  final  int SEND_STATUS_UPLOAD_FINISHED      = 199;
```



### `MSG_SR_ON_P2P_FINISH_UPLOAD`

```java
public static void onP2pFinishUpload(long transNo, long fileSize, long sentSize)
```

업로드 완료 되었습니다. 전체 파일을 모두 업로드 했을 수 도 있고. 중지 됐을 수 도 있습니다.

filesize == receivedSize 이면 전송 상태는 SEND_STATUS_UPLOAD_FINISHED. 서로 다르면 SEND_STATUS_UPLOAD_STOPPED

업로드 완료인 경우 다음 업로드를 진행합니다.

| transNo  | qint64 | 전송 번호                  |
| -------- | ------ | -------------------------- |
| fileSize | qint64 | 파일 사이즈.               |
| sentSize | qint64 | 업로드 전송 완료된 사이즈. |



### `MSG_SR_ON_P2P_PROGRESS_DOWNLOAD`

```java
public static void onP2pProgressDownload(long transNo, long fileSize, long receivedSize)
```

다운로드 중입니다. 다운로드 상태를 progressbar 로 표시합니다.

| transNo      | qint64 | 전송 번호                    |
| ------------ | ------ | ---------------------------- |
| fileSize     | qint64 | 파일 사이즈.                 |
| receivedSize | qint64 | 다운로드 전송 완료된 사이즈. |



### `MSG_SR_ON_P2P_PROGRESS_UPLOAD`

```java
public static void onP2pProgressUpload(long transNo, long fileSize, long sentSize)
```

업로드 중입니다. 업로드 상태를 progressbar 로 표시합니다.

| transNo  | qint64 | 전송 번호                  |
| -------- | ------ | -------------------------- |
| fileSize | qint64 | 파일 사이즈.               |
| sentSize | qint64 | 업로드 전송 완료된 사이즈. |



### `MSG_SR_ON_P2P_ABORT_DOWNLOAD`

```java
public static void onP2pAbortDownload(long transNo)
```

다운로드를 중단 했습니다. 전송 목록에서 다운로드 중단 처리 합니다.

| transNo | qint64 | 전송 번호 |
| ------- | ------ | --------- |



### `MSG_SR_ON_P2P_ABORT_UPLOAD`

```java
public static void onP2pAbortUpload(long transNo)
```

업로드를 중단 했습니다. 전송 목록에서 업로드 중단 처리 합니다.

| transNo | qint64 | 전송 번호 |
| ------- | ------ | --------- |



### `MSG_SR_ON_P2P_FINISH_DOWNLOAD_OTHER`

```java
public static void onP2pFinishDownloadOther(long jobId, long remoteDeviceNo)
```

상대방이 파일을 다운로드 해갔습니다. 전송목록에서 제거 합니다.

| jobId | qint64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ----- | ------ | ------ |
| remoteDeviceNo | qint64 | 다운로드 완료한 디바이스 번호 |



### `MSG_SR_ON_P2P_FINISH_UPLOAD_OTHER`

```java
public static void onP2pFinishUploadOther(long jobId, long remoteDeviceNo)
```

상대방이 파일을 다운로드 해갔습니다. 전송목록에서 제거 합니다.

| jobId | qint64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ----- | ------ | ------ |
| remoteDeviceNo | qint64 | 업로드 완료한 디바이스 번호 |



### `MSG_SR_ON_P2P_PROGRESS_DOWNLOAD_OTHER`

```java
public static void onP2pProgressDownloadOther(long jobId, long remoteDeviceNo, long fileSize, long currSize, String filePath, String serverName, String deviceName)
```

상대방이 파일을 다운로드 중이다. 진행상태를 업데이트 해준다. 전송목록에 없으면 추가해준다.

| jobId      | qint64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ---------- | ------ | ------------------------------------------------------------ |
| remoteDeviceNo | qint64 | 업로드 완료한 디바이스 번호 |
| fileSize   | qint64 | 전체 파일 사이즈                                             |
| currSize   | qint64 | 현재까지 전송된 파일 사이즈                                  |
| filePath   | String | 전송목록에 표시할 local file path                            |
| serverName | String | 다운로드 해가는 사용자 이름. 사용자이름을 서버처럼 사용한다. |
| deviceName | String | 다운로드 해가는 디바이스 이름                                |



### `MSG_SR_ON_P2P_PROGRESS_UPLOAD_OTHER`

```java
public static void onP2pProgressUploadOther(long jobId, long remoteDeviceNo, long fileSize, long currSize, String filePath, String serverName, String deviceName)
```

상대방이 파일을 업로드 중이다. 진행상태를 업데이트 해준다. 전송목록에 없으면 추가해준다.

| jobId      | qint64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ---------- | ------ | ------------------------------------------------------------ |
| remoteDeviceNo | qint64 | 업로드 완료한 디바이스 번호 |
| fileSize   | qint64 | 전체 파일 사이즈                                             |
| currSize   | qint64 | 현재까지 전송된 파일 사이즈                                  |
| filePath   | String | 전송목록에 표시할 local file path                            |
| serverName | String | 업로드 하는 사용자 이름. 사용자이름을 서버처럼 사용한다.     |
| deviceName | String | 업로드 하는 디바이스 이름                                    |



### `MSG_SR_ON_P2P_PROCESSED_REQUEST`

```java
public static void onP2pProcessedRequest(int processType, long jobId, long remoteDeviceNo, int resultCode)
```

A가 B에게 request를 보냈을때, B는 request를 받아서 다시 A에게 회신을 한 후, B는 MSG_SR_ON_P2P_PROCESSED_REQUEST callback 을 받는다.

saaring::SaaringPacketProcessType P2P 요청에 대해, 내가 답변처리했음에 대한 결과를 callback 받는다.
성공인 경우에는 별로 할것은 없고, 실패인 경우에는 실패 처리를 위해서 사용한다.

예를 들어, 다운로드 요청을 한 다음에, 다운로드 대기 상태에 있을텐데, p2p 요청 자체를 실패 하면, 다운로드 대기 상태에 있는것을, 정지 상태로 변경한다. 업로드도 마찬가지다. 폴더 목록을 요청한 경우에는, wait dialog를 없앤다.



| processType    | int    |
| -------------- | ------ |
| jobId          | qint64 |
| remoteDeviceNo | qint64 |
| resultCode     | int    |



SaaringPacketProcessType은 saaring.h 에 다음과 같이 정의되어 있습니다.

```c++
enum SaaringPacketProcessType {
    PROCESSTYPE_REQ_PING                                            = 0,
    PROCESSTYPE_REQ_LOGIN                                           = 1,
    PROCESSTYPE_REQ_CHAT                                            = 2,
    PROCESSTYPE_REQ_FOLDER_LIST                                     = 3,
    PROCESSTYPE_REQ_FOLDER_DETAIL                                   = 4,
    PROCESSTYPE_REQ_FOLDER_DETAIL_RECURSIVE                         = 5,
    PROCESSTYPE_REQ_FILE_DOWNLOAD                                   = 6,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_STOP                              = 7,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_CANCEL                            = 8,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_COMPLETE                          = 9,
    PROCESSTYPE_REQ_FILE_UPLOAD_QUERY                               = 10,
    PROCESSTYPE_REQ_FILE_UPLOAD                                     = 11,
    PROCESSTYPE_REQ_FILE_UPLOAD_STOP                                = 12,
    PROCESSTYPE_REQ_FILE_UPLOAD_CANCEL                              = 13,
    PROCESSTYPE_REQ_FILE_UPLOAD_COMPLETE                            = 14,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 15,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 16,
    PROCESSTYPE_REQ_SAVE_SEND_FILES_INVITE_INFO                     = 17,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_CHECK                            = 18,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_STAND_BY                         = 19,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_RELEASE                          = 20,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_CHECK                              = 21,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_STAND_BY                           = 22,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_RELEASE                            = 23,

    PROCESSTYPE_RES_PING                                            = 10000,
    PROCESSTYPE_RES_LOGIN                                           = 10001,
    PROCESSTYPE_RES_CHAT                                            = 10002,
    PROCESSTYPE_RES_FOLDER_LIST                                     = 10003,
    PROCESSTYPE_RES_FOLDER_DETAIL                                   = 10004,
    PROCESSTYPE_RES_FOLDER_DETAIL_RECURSIVE                         = 10005,
    PROCESSTYPE_RES_FILE_DOWNLOAD                                   = 10006,
    PROCESSTYPE_RES_FILE_DOWNLOAD_STOP                              = 10007,
    PROCESSTYPE_RES_FILE_DOWNLOAD_CANCEL                            = 10008,
    PROCESSTYPE_RES_FILE_DOWNLOAD_COMPLETE                          = 10009,
    PROCESSTYPE_RES_FILE_UPLOAD_QUERY                               = 10010,
    PROCESSTYPE_RES_FILE_UPLOAD                                     = 10011,
    PROCESSTYPE_RES_FILE_UPLOAD_STOP                                = 10012,
    PROCESSTYPE_RES_FILE_UPLOAD_CANCEL                              = 10013,
    PROCESSTYPE_RES_FILE_UPLOAD_COMPLETE                            = 10014,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 10015,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 10016,
    PROCESSTYPE_RES_SAVE_SEND_FILES_INVITE_INFO                     = 10017,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_CHECK                            = 10018,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_STAND_BY                         = 10019,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_RELEASE                          = 10020,
    PROCESSTYPE_RES_UPLOAD_QUEUE_CHECK                              = 10021,
    PROCESSTYPE_RES_UPLOAD_QUEUE_STAND_BY                           = 10022,
    PROCESSTYPE_RES_UPLOAD_QUEUE_RELEASE                            = 10023
};

```



### `MSG_SR_ON_P2P_PROCESSED_RESPONSE`

```java
public static void onP2pProcessedResponse(int processType, long jobId, long remoteDeviceNo, int resultCode)
```

B가 A에서 response를 보냈을때, A는 response를 받아서 정상적으로 처리를 했으면, A는 MSG_SR_ON_P2P_PROCESSED_RESPONSE callback을 받는다.

saaring::SaaringPacketProcessType P2P 요청에 대해, 상대방 peer의 답변을 처리 했음을 callback으로 받는다. 성공인 경우에는 별 문제 없지만, 실패인 경우에는 실패 처리를 위해서 사용한다.

예를 들어, 다운로드 요청을 한 다음에, 다운로드 대기 상태에 있을텐데, p2p 요청 자체를 실패 하면, 다운로드 대기 상태에 있는것을, 정지 상태로 변경한다. 업로드도 마찬가지다. 폴더 목록을 요청한 경우에는, wait dialog를 없앤다.

| processType    | int    |
| -------------- | ------ |
| jobId          | qint64 |
| remoteDeviceNo | qint64 |
| resultCode     | int    |



### `MSG_SR_ON_P2P_PROCESS_FOLDER_LIST`

```java
public static void onP2pProcessResponseFolderList(String strJson)
```

특정 디바이스의 공유폴더 목록을 받아서 보여줍니다. 현재 특정 디바이스에 대한 공유폴더 목록을 요청하는 곳은 없기 때문에, 사용되지 않습니다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL`

```java
public static void onP2pProcessResponseFolderDetail(String strJson)
```

특정 공유폴더의 파일, 폴더 내용을 보여줍니다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_FINISHED`

```java
public static void onP2pProcessResponseFolderDetailFinished()
```

MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL 처리에 이어서, 바로 호출 된다.

데이터 세팅과 UI 처리를 분리하기 위해서 두번 호출한다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `MSG_SR_ON_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE`

```java
public static void onP2pProcessResponseFolderRecursive(String strJson)
```

해당 폴더의 하위폴더까지 포함된 정보를 전송 받았다.

폴더 다운로드 할 때 사용되며,  해당 폴더의 파일.하위폴더 정보를 받아 다운로드 처리한다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `MSG_SR_ON_P2P_PROCESS_FILE_UPLOAD_QUERY`

```java
public static void onP2pProcessResponseFileUploadQuery(long transNo, long fileStartPosition)
```

File Upload Qeury 에 대한 회신을 받았다.

요청이 성공적이면,  transNo로 g_db->readMyTransTbByTransNo 호출해서 전송정보를 받아서, g_p2p->doP2pUpload 호출해서, 업로드 진행한다.


| transNo           | qint64 | 전송 번호        |
| ----------------- | ------ | ---------------- |
| fileStartPosition | qint64 | 업로드 시작 위치 |



### `MSG_SR_ON_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY`

```java
public static void onP2pProcessResponseFileDownloadInviteSendFilesQuery(String strJson)
```

File Invite Send Files Qeury 에 대한 회신을 받았다.
다운로드 받을수 있는 정보가 json 에 모두 포함되어 있기 때문에, 해당 정보로 전송목록에 추가 할 수 있는 정보를 구성해서 추가하고, 다운로드 진행한다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `MSG_SR_ON_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO`

```java
public static void onP2pProcessResponseSaveSendFilesInviteInfo(String strJson)
```

내 다른 디바이스의 공유폴더에 있는 파일로, 파일 보내기 링크 만들기 요청에 대한 회신을 받았다.
g_web->completeInviteFriend 호출해서 초대링크 만들기를 완료 하고, 완료 메시지를 보여준다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY`

```java
public static void onP2pNotifyDownloadQueueStandBy(long remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
```

다운로드 대기 상태가 됐다.

전송을 중단하고, 대기 정보를 보여준다.

| toDeviceNo           | qint64 | 대기상태가 된 디바이스 번호 |
| -------------------- | ------ | --------------------------- |
| downloadingCount     | int    | 현재 다운로드 수            |
| downloadStandByCount | int    | 전체 대기자 수              |
| myTurn               | int    | 대기자 중 내 순번           |



### `MSG_SR_ON_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE`

```java
public static void onP2pNotifyDownloadQueueRelease(long remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn)
```

다운로드 대기 상태에서, 다운로드 가능한 순번이 됐다.

다운로드 진행한다.

| toDeviceNo           | qint64 | 대기상태가 된 디바이스 번호 |
| -------------------- | ------ | --------------------------- |
| downloadingCount     | int    | 현재 다운로드 수            |
| downloadStandByCount | int    | 전체 대기자 수              |
| myTurn               | int    | 대기자 중 내 순번           |



### `MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY`

```java
public static void onP2pNotifyUploadQueueStandBy(long remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
```

업로드 대기 상태가 됐다.

전송을 중단하고, 대기 정보를 보여준다.

| toDeviceNo         | qint64 | 대기상태가 된 디바이스 번호 |
| ------------------ | ------ | --------------------------- |
| uploadingCount     | int    | 현재 업로드 수              |
| uploadStandByCount | int    | 전체 대기자 수              |
| myTurn             | int    | 대기자 중 내 순번           |



### `MSG_SR_ON_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE`

```java
public static void onP2pNotifyUploadQueueRelease(long remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn)
```

업로드 대기 상태에서, 업로드 가능한 순번이 됐다.

업로드 진행한다.

| toDeviceNo         | qint64 | 대기상태가 된 디바이스 번호 |
| ------------------ | ------ | --------------------------- |
| uploadingCount     | int    | 현재 업로드 수              |
| uploadStandByCount | int    | 전체 대기자 수              |
| myTurn             | int    | 대기자 중 내 순번           |



### `MSG_SR_ON_P2P_NOTIFY_CONNECTION_CLOSED`

```java
public static void onP2pNotifyConnectionClosed(long remoteDeviceNo)
```

p2p 접속이 끊어졌습니다. 해당 디바이스의 전송을 중지 합니다.

| remoteDeviceNo | qint64 | p2p 접속이 끊어진 디바이스 번호 |
| -------------- | ------ | ------------------------------- |



### `MSG_SR_ON_P2P_NOTIFY_START_NEXT_TRANSFER`

```java
public static void onP2pNotifyStartNextTransfer(long remoteDeviceNo)
```

기존 전송이 끝났거나, 새롭게 p2p 접속이 되는등, 새로 전송 진행 가능한 상태가 됐습니다. 전송목록의 다음 건을 전송 진행합니다.

| remoteDeviceNo | qint64 | p2p 접속이 끊어진 디바이스 번호 |
| -------------- | ------ | ------------------------------- |



### `MSG_SR_ON_P2P_NOTIFY_STATUS_MESSAGE`

```java
public static void onP2pNotifyStatusMessage(String text)
```

P2P 엔진이 P2P 접속.전송상태에 대한 메시지를 받았습니다. 선택적으로 화면 또는 로그로 메시지 남기면 됩니다.

| text | String | 메시지 |
| ---- | ------ | ------ |

