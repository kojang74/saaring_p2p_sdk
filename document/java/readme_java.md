# Saaring P2P Library : Java SDK Reference



샤링 p2p 엔진을 이용한 Android 프로그램을 개발할수 있는, Java SDK 개발 문서 입니다.



Java SDK 라이브러리를 사용하기 위한 초기화 방법입니다.

- [Library Initialization](Library_Initialization_java.md)



각 모듈별 함수 사용 방법입니다.

- [Common Utility Interface](Common_Utility_Interface_java.md)
- [Signaling, Relay, Rendzvous Server Interface](Signaling_Relay_Rendzvous_Server_Interface_java.md)
- [P2P Initialization Interface](P2P_Initialization_Interface_java.md)
- [P2P Connection Interface](P2P_Connection_Interface_java.md)
- [Share Folder Interface](Share_Folder_Interface_java.md)
- [Web Interface](Web_Interface_java.md)
- [DB Interface](DB_Interface_java.md)



Callback

- [Signaling Server Callback](Signaling_Server_Callback_java.md)
- [Share Folder Callback](Share_Folder_Callback_java.md)
- [P2P Callback](P2P_Callback_java.md)



기능별 프로그래밍 방법

- [login 방법](Tutorial_Login_java.md)
- [공유폴더 설정 방법](Tutorial_Share_Folder_Setting_java.md)
- [친구 요청 방법](Tutorial_Friend_Request_java.md)
- [초대링크 생성방법](Tutorial_Invite_Link_java.md)
- [p2p 접속 방법](Tutorial_P2p_Connect_java.md)

