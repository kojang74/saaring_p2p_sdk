# Saaring P2P Library : C++ SDK Reference



샤링 p2p 엔진을 이용한 프로그램을 개발할수 있는, C++ SDK 개발 문서 입니다.



C++ SDK 라이브러리를 사용하기 위한 초기화 방법입니다.

- [Library Initialization](Library_Initialization.md)



각 모듈별 함수 사용 방법입니다.

- [Common Utility Interface](Common_Utility_Interface.md)
- [Signaling, Relay, Rendzvous Server Interface](Signaling_Relay_Rendzvous_Server_Interface.md)
- [P2P Initialization Interface](P2P_Initialization_Interface.md)
- [P2P Connection Interface](P2P_Connection_Interface.md)
- [Share Folder Interface](Share_Folder_Interface.md)
- [Web Interface](Web_Interface.md)
- [DB Interface](DB_Interface.md)



Callback

- [Signaling Server Callback](Signaling_Server_Callback.md)
- [Share Folder Callback](Share_Folder_Callback.md)
- [P2P Callback](P2P_Callback.md)



기능별 프로그래밍 방법

- [login 방법](Tutorial_Login.md)
- [공유폴더 설정 방법](Tutorial_Share_Folder_Setting.md)
- [친구 요청 방법](Tutorial_Friend_Request.md)
- [초대링크 생성방법](Tutorial_Invite_Link.md)
- [p2p 접속 방법](Tutorial_P2p_Connect.md)

