# Library Initialization



## Summary

샤링 P2P C++ 라이브러리를 사용하기 위해서, 다음과 같은 라이브러리 초기화 과정이 필요합니다.

Qt 5.15.2 버전 기준입니다.



## Functions



### 필수 header files

샤링 P2P C++ 라이브러리를 사용하기 위해서, 다음 header파일을 필수적으로 include 해야 합니다.

```c++
#include "saaring.h"
#include "saaringapi.h"
```



### `Global 변수 선언`

saaringglobal.h 에 다음과 같이 extern 변수들을 선언합니다.

```
// Global static 변수
namespace saaring {

extern saaring::SaaringMemberInfo g_member;
extern QString g_styleSheet;
extern QString g_saaringCurrVersion;

extern QString g_saaringApiDomain;
extern QString g_saaringImageDomain;
extern QString g_saaringServiceDomain;

} // namespace ssaring

```



따라서, main.cpp 에서 saaringglobal.h를 include 한 후, global 변수로 정의해줍니다. saaringglobal.h 는 모듈 사용을 위해서, saaring.h, saaringapi.h 파일과 함께 항상 같이 include 해줍니다.

```
namespace saaring {

SaaringCommonInterface* g_common            = Q_NULLPTR;
SaaringDbInterface* g_db                    = Q_NULLPTR;
SaaringP2pInterface* g_p2p                  = Q_NULLPTR;
SaaringShareFolderInterface* g_shareFolder  = Q_NULLPTR;
SaaringWebInterface* g_web                  = Q_NULLPTR;

// Global static 변수
SaaringMemberInfo g_member;
QString g_styleSheet                        = ":/stylesheet/stylesheet.qss";
QString g_saaringCurrVersion                = SAARING_VERSION;

QString g_saaringApiDomain                  = "";
QString g_saaringImageDomain                = "";
QString g_saaringServiceDomain              = "";

} // namespace ssaring

```



### `dll loading`

QLibrary를 이용해서, DLL 파일을 로딩 합니다.

```
    QLibrary library(filePath);

#elif defined(Q_OS_OSX)
    QString pwd = QDir::currentPath();
    qDebug() << "pwd=" << pwd;

    QLibrary library("libsanet.dylib");
#else
    QString pwd = QDir::currentPath();
    qDebug() << "pwd=" << pwd;
    QLibrary library(pwd+"/libsanet.so");
#endif

    if(!library.load())
    {
        qDebug() << library.errorString();
        QApplication::exit(0);
    }
    else
    {
        qDebug() << "library loaded...";
    }

```



### `saaring::SaaringCommonInterface 모듈 생성`

C++ 모듈은 'PRODUCTION, REAL, TEST, DEV' 모드에 따라서 작동하게 되며, 실행 모드에 따라 각 서버의 domain이 변경됩니다.

```c++
enum SAARING_RUNNING_MODE {
    PRODUCTION = 0,
    REAL=99999999,
    TEST=99999998,
    DEV=99999997
};
int g_runningMode = SAARING_RUNNING_MODE::PRODUCTION;
```



실행 모드를 전달해서, Common 객체를 생성한 후, 각 서버의 domain 정보를 얻어와서, 나머지 모듈을 생성해야 합니다.

```
    typedef saaring::SaaringCommonInterface *(*CreateCommon)(int type);
    typedef saaring::SaaringDbInterface *(*CreateDb)(int type);
    typedef saaring::SaaringP2pInterface *(*CreateP2p)(int type);
    typedef saaring::SaaringShareFolderInterface *(*CreateShareFolder)();
    typedef saaring::SaaringWebInterface *(*CreateWeb)(QString apiDomain);
    qDebug() << "g_runningMode=" << g_runningMode;

    CreateCommon createCommon = (CreateCommon)library.resolve("Common");
    if(createCommon != Q_NULLPTR)
    {
        qDebug("createCommon Good...");
        saaring::g_common = createCommon(g_runningMode);
        if(saaring::g_common == Q_NULLPTR)
        {
            qDebug("g_common Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createCommon Bad...");
        QApplication::exit(0);
    }

    // domain 정보 세팅
    // 중요함.
    saaring::g_saaringApiDomain         = saaring::g_common->getSaaringApiDomain();
    saaring::g_saaringImageDomain       = saaring::g_common->getSaaringImageDomain();
    saaring::g_saaringServiceDomain     = saaring::g_common->getSaaringServiceDomain();
    qDebug() << "saaring::g_saaringApiDomain=" << saaring::g_saaringApiDomain;
    qDebug() << "saaring::g_saaringImageDomain=" << saaring::g_saaringImageDomain;
    qDebug() << "saaring::g_saaringApiDomain=" << saaring::g_saaringServiceDomain;

```



이제 나머지 모듈도 생성합니다.

```c++
    CreateDb createDb = (CreateDb)library.resolve("Db");
    if(createDb != Q_NULLPTR)
    {
        qDebug("createDb Good...");
        saaring::g_db = createDb(g_runningMode);
        if(saaring::g_db == Q_NULLPTR)
        {
            qDebug("g_db Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createDb Bad...");
        QApplication::exit(0);
    }

    CreateP2p createP2p = (CreateP2p)library.resolve("P2p");
    if(createP2p != Q_NULLPTR)
    {
        qDebug("createP2p Good...");
        saaring::g_p2p = createP2p(g_runningMode);
        if(saaring::g_p2p == Q_NULLPTR)
        {
            qDebug("g_P2p Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createP2p Bad...");
        QApplication::exit(0);
    }

    CreateShareFolder createShareFolder = (CreateShareFolder)library.resolve("ShareFolder");
    if(createShareFolder != Q_NULLPTR)
    {
        qDebug("createShareFolder Good...");
        saaring::g_shareFolder = createShareFolder();
        if(saaring::g_shareFolder == Q_NULLPTR)
        {
            qDebug("g_ShareFolder Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createShareFolder Bad...");
        QApplication::exit(0);
    }

    CreateWeb createWeb = (CreateWeb)library.resolve("Web");
    if(createWeb != Q_NULLPTR)
    {
        qDebug("createWeb Good...");
        saaring::g_web = createWeb(saaring::g_saaringApiDomain);
        if(saaring::g_web == Q_NULLPTR)
        {
            qDebug("g_Web Bad...");
            QApplication::exit(0);
        }
    }
    else
    {
        qDebug("createWeb Bad...");
        QApplication::exit(0);
    }

```



이렇게 생성된 saaring::g_common, saaring::g_db, saaring::g_p2p, saaring::g_shareFolder, saaring::g_web  global 변수를 이용해서, C++ 모듈을 사용하면 됩니다.



### `library release`

프로그램이 종료 될때에는 생성했던 모듈을 모두 제거 해서 메모리를 반납 합니다.

```
void releaseLibrary()
{
    if(saaring::g_common != Q_NULLPTR)
    {
        delete saaring::g_common;
        *(&saaring::g_common) = Q_NULLPTR;
    }
    if(saaring::g_db != Q_NULLPTR)
    {
        delete saaring::g_db;
        *(&saaring::g_db) = Q_NULLPTR;
    }
    if(saaring::g_p2p != Q_NULLPTR)
    {
        delete saaring::g_p2p;
        *(&saaring::g_p2p) = Q_NULLPTR;
    }
    if(saaring::g_shareFolder != Q_NULLPTR)
    {
        delete saaring::g_shareFolder;
        *(&saaring::g_shareFolder) = Q_NULLPTR;
    }
    if(saaring::g_web != Q_NULLPTR)
    {
        delete saaring::g_web;
        *(&saaring::g_web) = Q_NULLPTR;
    }

    // for firebase
    if(saaring::g_firebaseUtil != Q_NULLPTR)
    {
        delete saaring::g_firebaseUtil;
        *(&saaring::g_firebaseUtil) = Q_NULLPTR;
    }
}

```



