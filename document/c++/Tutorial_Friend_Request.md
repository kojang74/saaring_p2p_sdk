# Tutorial - Friend Request



## Summary

샤링은 내 디바이스간 공유는 자유롭게 할 수 있지만, 친구를 맺어야만 공유를 할 수 있습니다. 따라서, 친구 맺는게 중요 합니다.

친구를 맺기 위해서는, 샤링 회원을 대상으로 친구 요청을 보내고 수락하는 방법이 있고, 비회원인 경우에는 초대링크를 보내고 상대방이 초대링크를 등록하면서 자동으로 친구가 맺어지는 방법이 있습니다.

[초대링크를 통해 친구 맺는 방법](Tutorial_Invite_Link.md) 은 별도로 설명하기로 하고, 여기에서는 샤링 회원을 대상으로 친구 요청을 보내고 수락.거절.차단 하는 방법에 대해서 설명하겠습니다.





## Functions



### `회원 찾기 `

g_web->searchUser 를 호출해서, keyword로 전달한 email과 전화번호의 회원을 검색합니다.

검색 결과에서 userNo(회원번호)를 알 수 있어, 다음 단계에서 친구 요청을 보낼 수 있습니다.



### `친구 요청.조회.추가.등록.삭제.거절.해제 `

g_web->requestFriendByUserNo 를 호출해서, 친구 요청을 합니다.

친구 요청을 받는 쪽에서는, PCC_SIGNALING_NOTIFY_REQUEST_FRIEND callback으로 알림이 갑니다. 모바일의 경우에는 푸시 메시지가 날아갑니다.

g_web->listRequestingFriend를 호출해서 친구 요청 목록을 확인합니다.

g_web->addFriend를 호출해서 친구 추가를 합니다. 친구가 추가 되면, 친구 요청을 한 쪽에 PCC_SIGNALING_NOTIFY_ADD_FRIEND callback으로 친구요청이 수락돼서 추가 됐음을 알려줍니다. 물론 모바일에서는 푸시 메시지가 날아갑니다.

g_web->cancelRequestFriend를 호출해서, 친구 요청을 단순 삭제 할 수 있습니다. 단순 삭제를 한 경우에는 상대방이 다시 친구요청을 보낼 수 있습니다.

g_web->rejectRequestFriend를 호출해서, 친구 요청을 거절 할 수 있습니다. 거절한 경우에는 상대방이 다시 친구요청을 보내도 정상적으로 친구요청이 전달되지 않습니다.

g_web->listBlock를 호출해서, 거절 목록을 확인 할수 있고, g_web->recoverRequestFriend를 호출해서 거절을 해제 할수 있습니다. 거절을 해제 하면, 다시 친구요청 목록에 친구요청이 보입니다.



### `친구 목록.차단.해제 `

g_web->listFriend를 호출해서 친구 목록을 가져올 수 있습니다.

등록된 친구를 차단하려면, g_web->blockFriend 를 호출하면 됩니다. 차단하면 친구목록에서 빠지고, 차단 목록은 g_web->listBlock 에서 확인 할 수 있습니다. 차단 해제 할때에도 g_web->blockFriend 를 호출하는데, parameter중 blockYN="Y"로 보내면, 차단해지 됩니다.



