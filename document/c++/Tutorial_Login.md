# Tutorial - Login



## Summary

로그인을 위해서는 우선적으로 Firebase Auth를 통해 로그인 해야 합니다. 로그인에 성공하면 Firebase Token 을 받아서, 샤링 웹을 통해 샤링에 로그인을 합니다. 사링 로그인에 성공하면, authKey 를 받게 되는데, 받은 authKey로 Signaling Server에 접속합니다.





## Functions



### `Firebase Auth. 로그인 `

다음 URL에서 각 플랫폼에 문서를 보면 됩니다.

https://firebase.google.com/docs/reference

C++의 경우 다음 문서를 보면 됩니다.

https://firebase.google.com/docs/reference/cpp/namespace/firebase/auth

Firebase Auth. 를 이용해서 가입(계정생성), 로그인 후 Firebase Token을 얻어와야 합니다. Email.Password를 통한 계정은 email 인증을 해야 하고, 핸드폰을 통한 계정은 핸드폰 인증을 합니다. 또한, 각 SNS 계정은 OAuth2 를 통해 인증을 마치고, idToken, accessToken 의 정보가 필요합니다.

firebase::auth::Auth::CreateUserWithEmailAndPassword, firebase::auth::Auth::SignInWithCredential 통해서 가입(계정생성), 로그인에 성공하면, firebase::auth::User 객체를 얻어올 수 있고, firebase::auth::Use::GetToken 을 이용해서 Firebase Token 값을 얻어옵니다.



### `샤링 웹 가입, 로그인 `

g_web->loginMember를 호출해서 로그인 합니다.

saaringMember.lastVersion, saaringMember.runningVersion 정보를 g_common->checkUpgrade 통해서 버전 업그레이드 여부를 체크합니다.

그밖의 로그인 정보를 저장합니다.

```c++
            saaring::g_member.firebaseToken = saaringMember.firebaseToken;
            saaring::g_member.authKey       = saaringMember.authKey;
            saaring::g_member.userNo        = saaringMember.userNo;
            saaring::g_member.joinStatus    = saaringMember.joinStatus;

            saaring::g_member.deviceNo      = saaringMember.deviceNo;
            saaring::g_member.deviceName    = saaringMember.deviceName;

            saaring::g_member.userId        = saaringMember.userId;
            saaring::g_member.userName      = saaringMember.userName;

            saaring::g_member.email         = saaringMember.email;
            saaring::g_member.phoneNumber   = saaringMember.phoneNumber;

            saaring::g_member.password      = password;

            saaring::g_member.firstName     = saaringMember.firstName;
            saaring::g_member.lastName      = saaringMember.lastName;

            saaring::g_member.profilePic    = saaringMember.profilePic;
            saaring::g_member.message       = saaringMember.message;

            saaring::g_member.idType        = (int)saaring::SaaringIdType::PASSWORD;
            saaring::g_member.providerId    = saaringMember.providerId;
            saaring::g_member.idToken       = saaringMember.idToken;
            saaring::g_member.accessToken   = saaringMember.accessToken;
            saaring::g_member.uid           = saaringMember.uid;
            saaring::g_member.providerYN    = saaringMember.providerYN;
            saaring::g_member.numProvider   = saaringMember.numProvider;

```

로그인 정보는 자동로그인을 위해 g_db->doAfterLogin를 호출해서 local DB에 별도로 저장해 놓습니다.

g_shareFolder->startThreadShareFolder, g_shareFolder->shareFolderInit, g_shareFolder->startFileMonitor를 호출해서 공유폴더 관련 스레드를 실행해서 초기화 합니다.



### `Signaling Server 접속 `

Web Login을 통해서 받아온 g_member 정보를 이용해서, g_p2p->initP2p를 호출해서 [p2p 모듈을 초기화](P2P_Initialization_Interface.md) 하고, g_p2p->doSignalingConnect를 통해서 Signaling Server에 접속합니다.

Signaling Server접속&로그인 결과는 initP2p 호출할때 전달한 callback 함수를 통해서 PCC_SIGNALING_CONNECTED, PCC_SIGNALING_CLOSED, PCC_SIGNALING_LOGIN_FAILED 등의 알림을 받습니다.

그밖의 [Signaling Server를 통한 callback](Signaling_Server_Callback.md) 기능 처리도 모두 해줍니다.



### `Local Relay Server 실행 `

PCC_P2P_PORTMAP_FINISHED callback을 받아서, g_p2p->startRelayServer를 호출해서 Relay Server를 실행시킵니다.

