# Signaling Server Callback



## Summary

Signaling Server 접속 이후, 접속 상태 및 각종 알림을 callback으로 비동기적으로 받아서 처리 합니다.

P2p 모듈 초기화 하면서 전달한 callback 에서 처리 합니다.



```
    enum P2pCallbackCommand {
        PCC_SIGNALING_CONNECTED                                 = 10000,
        PCC_SIGNALING_CLOSED                                    = 10001,
        PCC_SIGNALING_LOGIN_FAILED                              = 10002,

        PCC_SIGNALING_NOTIFY_DEVICE_LIVEYN                      = 11000,
        PCC_SIGNALING_NOTIFY_REQUEST_FRIEND                     = 11001,
        PCC_SIGNALING_NOTIFY_ADD_FRIEND                         = 11002,

        PCC_P2P_PORTMAP_FINISHED                                = 20000,
        PCC_P2P_LOGIN_FINISHED                                  = 20001,
        PCC_P2P_WAKEUP_FINISHED                                 = 20002,

        PCC_P2P_FINISH_DOWNLOAD                                 = 21000,
        PCC_P2P_FINISH_UPLOAD                                   = 21001,
        PCC_P2P_PROGRESS_DOWNLOAD                               = 21002,
        PCC_P2P_PROGRESS_UPLOAD                                 = 21003,
        PCC_P2P_ABORT_DOWNLOAD                                  = 21004,
        PCC_P2P_ABORT_UPLOAD                                    = 21005,
        PCC_P2P_FINISH_DOWNLOAD_OTHER                           = 21006,
        PCC_P2P_FINISH_UPLOAD_OTHER                             = 21007,
        PCC_P2P_PROGRESS_DOWNLOAD_OTHER                         = 21008,
        PCC_P2P_PROGRESS_UPLOAD_OTHER                           = 21009,

        PCC_P2P_PROCESSED_REQUEST                               = 22000,
        PCC_P2P_PROCESSED_RESPONSE                              = 22001,
        PCC_P2P_PROCESS_ALL_SHARED_FOLDERS                      = 22002,
        PCC_P2P_PROCESS_FOLDER_DETAIL                           = 22003,
        PCC_P2P_PROCESS_FOLDER_DETAIL_FINISHED                  = 22004,
        PCC_P2P_PROCESS_FOLDER_DETAIL_INCLUDING_ALL_SUBFOLDERS  = 22005,
        PCC_P2P_PROCESS_FILE_UPLOAD_QUERY                       = 22006,
        PCC_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY   = 22007,
        PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO             = 22008,

        PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY                  = 23000,
        PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                   = 23001,
        PCC_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY                    = 23002,
        PCC_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                     = 23003,

        PCC_P2P_NOTIFY_TIMEOUT_RECONNECT                        = 30000,
        PCC_P2P_NOTIFY_START_NEXT_TRANSFER                      = 30001,
        PCC_P2P_NOTIFY_STATUS_MESSAGE                           = 30002,
    };

```

## Callback



### `PCC_SIGNALING_CONNECTED`

Web 로그인 후, Signaling Server에 socket 접속 후 로그인에 성공 했습니다.

모든 로그인 프로세스가 끝났기 때문에, 메인 프로그램을 실행 합니다.



### `PCC_SIGNALING_CLOSED`

Signaling Server와 접속이 끊어졌습니다. 로그아웃 처리는 Signaling Server에서 처리 됐을것이기 때문에, Local 상황만 정리하면 됩니다.

메인 프로그램을 종료하고, Local Relay 서버가 실행중이라면 종료하고, 자동 재접속 모드로 넘어갑니다.



### `PCC_SIGNALING_LOGIN_FAILED`

Signaling Server에 로그인에 실패 했습니다. 처음부터 다시 로그인을 시도 합니다.



### `PCC_SIGNALING_NOTIFY_DEVICE_LIVEYN`

해당 친구의 디바이스가 접속됐음.접속종료됐음을 알려줍니다.

현재 화면이, 친구목록 또는 친구의 공유폴더 목록이면, 해당 친구와 디바이스의 공유폴더를 접속 가능 상태로 변경해줍니다.

| fromUserNo   | long   | 접속한 회원 번호           |
| ------------ | ------ | -------------------------- |
| fromDeviceNo | long   | 접속한 디바이스 번호       |
| liveYN       | String | Y=접속됐음, N=접속종료됐음 |



### `PCC_SIGNALING_NOTIFY_REQUEST_FRIEND`

친구 요청이 있습니다.

friendName의 친구가 친구 요청이 있음을 notibar에 표시해주고, 정확한 친구요청 목록을 읽어와서, '수락', '거절' 할 수 있도록 합니다.

| friendName | String | 친구 요청을 한 친구의 이름 |
| ---------- | ------ | -------------------------- |



### `PCC_SIGNALING_NOTIFY_ADD_FRIEND`

친구 등록 알림. 친구 요청에 대해 수락 해서, 친구로 등록되었음을 알려주는 알림입니다.

알림을 터치하면 친구목록으로 이동해줍니다.

| friendName   | String | 친구의 이름                |
| ------------ | ------ | -------------------------- |
| friendUserNo | long   | 친구의 회원번호            |

