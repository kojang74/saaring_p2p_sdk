# Share Folder Callback



## Summary

공유폴더안의 내용 변경 알림, 공유폴더 인터페이스 호출에 대한 처리 결과 알림을 받아 처리 합니다.



```
    enum ShareDirCallbackCommand {
        SDCC_DIRECTORY_CHANGED,
        SDCC_JOB_FINISHED,
        SDCC_JOB_ERROR
    };
```

## Callback



### `SDCC_DIRECTORY_CHANGED`

공유폴더의 내용이 업데이트 됐음을 알려줍니다. 특별히 처리할것은 없습니다.



### `SDCC_JOB_FINISHED`

공유폴더 관리 스레드에게 공유폴더 추가.삭제.수정에 대한 요청이 완료 되었습니다. 특별히 처리할 것은 없습니다.



### `SDCC_JOB_ERROR`

공유폴더 관리 스레드에게 공유폴더 추가.삭제.수정에 대한 요청에 실패 했습니다. 특별히 처리할 것은 없습니다.


