# P2P Initialization Interface



## Summary

P2P 엔진(라이브러리) 를 초기화하기 위한 루틴이다.

P2P 엔진을 먼저 초기화 한 후에, Signaling.Relay.Rendezvous Server Interface와 P2P 전체 Interface를 사용할 수 있다.



## Functions



### `void initP2p(void *context, P2pCallback callback, SaaringMemberInfo &member);`

P2p 엔진을 초기화 합니다. P2p 관련 요청은 비동기적으로 작동 됩니다. 따라서, 요청을 하면 callback 으로 비동기적으로 회신을 받아서 처리 합니다. 그런데, callback 함수는 static 함수라서, 객체지향적으로 프로그래밍 할수 없습니다. 따라서, callback 함수가 요청을 받아서 다시 요청을 처리할 객체의 포인터인 context를 보냅니다. 로그인 후, P2p 엔진을 사용하기전 프로그램 맨 처음에 호출해서 초기화 해야 합니다.

callback 함수는 아래와 같습니다.

```c++
typedef void (*P2pCallback)(void *context, int command, QStringList &param);
```

| context  | callback 함수가 요청을 받아, 다시 요청을 처리할 객체의 포인터 입니다. MainWindow의 포인터를 전달 하면 됩니다. |
| -------- | ------------------------------------------------------------ |
| callback | callback 처리 함수<br />initP2p를 통해 전달했던 context를 다시 돌려 받습니다. |
| member   | 현재 로그인 된 회원 정보를 전송합니다.                       |



### `void closeP2p();`

P2p 엔진 사용을 종료합니다. 내부적으로는 모든 p2p 접속을 끊고, p2p 객체를 제거 합니다.




### `bool isP2p();`

P2p 엔진 초기화 여부를 확인합니다.

initP2p, closeP2p 호출 하기전에 확인합니다.




### `SaaringServerInfo getServerInfo();`

saaring 에서 운영하고 있는, 다음의 각종 서버 정보를 얻어옵니다.

rendezvousIp, rendezvousPort, relayIp, relayPort도 모두 saaring에서 운영하고 있는 Relay Server의 IP/port 정보 입니다.

```
struct SaaringServerInfo
{
    QString                 signalingIp;
    int                     signalingPort;
    QString                 rendezvousIp;
    int                     rendezvousPort;
    QString                 relayIp;
    int                     relayPort;
    QString                 runningVersion;
    QString                 lastVersion;
};

```

