# Share Folder Interface



## Summary

공유폴더 설정 및 공유폴더 관리를 위한 스레드 초기화 루틴.

공유폴더는 별도의 스레드로 작동되며, 공유폴더로 지정한 폴더는 변경될 때 마다 변경여부를 notify 받을 수 있도록 모니터링 하고 있습니다. 또한, 공유폴더 안의 파일과 폴더는 local DB에 저장해서 매번 해당 폴더를 스캔하지 않아 성능상에 문제가 없도록 작동되고 있습니다.



## Functions



### `void initThreadShareDir(void *context, ThreadShareDirCallback callback, SaaringMemberInfo &member);`

공유폴더 스레드를 초기화 합니다. 공유폴더의 변경사항은 비동기적으로 작동되며, 그결과는 callback 으로 회신을 받아서 처리 합니다. callback 함수는 static 함수라서, 객체지향적으로 프로그래밍 할수 없습니다. 따라서, callback 함수가 요청을 받아서 다시 요청을 처리할 객체의 포인터인 context를 보냅니다. MainWindow constructor 프로그램 맨 처음에 호출해서 초기화 합니다.

callback 함수는 아래와 같습니다.

```c++
typedef void (*ThreadShareDirCallback)(void *context, int command, QStringList &param);
```

| context  | callback 함수가 요청을 받아, 다시 요청을 처리할 객체의 포인터 입니다. MainWindow의 포인터를 전달 하면 됩니다. |
| -------- | ------------------------------------------------------------ |
| callback | callback 처리 함수<br />initThreadShareDir를 통해 전달했던 context를 다시 돌려 받습니다. |
| member   | 현재 로그인 된 회원 정보를 전송합니다.                       |



### `void startThreadShareFolder();`

공유폴더 관리를 위한 스레드를 실행합니다.

initThreadShareDir에서는 스레드를 생성만 합니다. 로그인 이후 호출 합니다.



### `void stopThreadShareFolder();`

공유폴더 관리를 위한 스레드를 종료합니다.

로그아웃 할 때 호출 합니다.



### `void shareFolderInit();`

공유폴더 정보는 서버에도 저장됩니다. 공유폴더 안의 정보까지 저장하진 않고, 공유폴더 이름과 공유폴더에 있는 파일갯수와 용량정도만 remote DB에 저장합니다.

한편, local DB에는 공유폴더 안의 모든 파일 정보까지 저장되는데, remote DB와 local DB가 다를 수 도 있습니다. shareFolderInit을 통해서, local DB 정보를 기준으로 remote DB와 동기화를 합니다.

로그인 이후, startThreadShareFolder 호출 한 다음 바로 이어서 호출 합니다.



### `void shareFolderInsert(qint64 shareNo, QString sharePath, QString shareName);`

공유폴더를 추가 할때, addOrUpdateFolder를 호출해서 remote DB에 insert 하면, serverFolderNo를 얻어올 수 있습니다. 이어서, insertShareTb를 통해 local DB에 insert 하면 shareNo를 얻어올 수 있습니다. 또한, getShareNoByServerFolderNo 를 호출해서 serverFolderNo로 shareNo를 얻어올 수 있습니다.

shareFolderInsert는 공유폴더가 추가 됐으니, 공유폴더 관리 스레드에게 새로 추가된 공유폴더 안의 파일 정보를 수집해서 업데이트를 요청하는 기능입니다.

| shareNo   | 추가된 공유폴더 고유번호                                    |
| --------- | ----------------------------------------------------------- |
| sharePath | 공유폴더의 local path<br />ex) 'C:/Users/saaring/Downloads' |
| shareName | 공유폴더 이름<br />ex) 'Downloads'                          |



### `void shareFolderDelete(qint64 shareNo);`

공유폴더 삭제를 위해서, deleteFolder를 호출해서 remote DB를 삭제하고, local DB 삭제는 deleteShareDetailTb, deleteShareTb를 호출해서 삭제 합니다.

공유폴더 관리 스레드에게도 shareFolderDelete를 호출해서 공유폴더가 삭제 됐다고 알려준다.

| shareNo | 삭제된 공유폴더 고유번호 |
| ------- | ------------------------ |



### `void shareFolderUpdate(QString folderPath);`

다음 callback 으로 공유폴더안의 파일이나 폴더에 변경이 확인 되면, 공유폴더 관리 스레드에게도 알려줘야 한다.

```
saaring::SaaringShareFolderInterface::ShareDirCallbackCommand::SDCC_DIRECTORY_CHANGED
```

| folderPath | 내용이 변경된 공유폴더의 local path<br />ex) 'C:/Users/saaring/Downloads' |
| ---------- | ------------------------------------------------------------ |



### `void serverFolderDelete(qint64 serverFolderNo);`

공유폴더 local DB정보와 remote DB정보가 다르면 rescan후 서버 정보를 update 해야 하는데, 이때 remote DB에는 공유폴더 정보가 있는데, local DB에는 없는 경우, remote DB의 공유폴더 정보를 삭제 해야 되는데, 이때 호출 한다.

local DB의 공유폴더 정보는 readShareTb를 호출해서 읽어오고, remote DB의 공유폴더 정보는 listFolderAll 을 호출해서 받아올 수 있는데, listFolderAll 호출 이후에 이어서 바로 local DB와 remote DB를 비교하면서, 필요한 경우 호출한다.

| serverFolderNo | 삭제하려는 서버 공유폴더 고유번호 |
| -------------- | --------------------------------- |



### `int getShareFolderCount();`

공유폴더내 공유되고 있는 폴더 갯수

| return | 공유폴더내 공유되고 있는 폴더 갯수 |
| ------ | ---------------------------------- |



### `int getShareFileCount();`

공유폴더내 공유되고 있는 파일 갯수

| return | 공유폴더내 공유되고 있는 파일 갯수 |
| ------ | ---------------------------------- |



### `void startFileMonitor();`

공유폴더를 모니터링 하기 위한 스레드를 실행합니다. 폴더 모니터링 하는것은 시스템 리소스를 많이 사용할 수 도 있어서, 선택적으로 사용할 수 있도록, 별도로 제공하고 있습니다.

로그인 이후 startThreadShareFolder, shareFolderInit 호출 이후 이어서 호출합니다.



### `void stopFileMonitor();`

공유폴더를 모니터링 하기 위한 스레드를 종료합니다.

로그아웃 이후 stopThreadShareFolder 호출 이후 이어서 호출합니다.



### `bool doesNeedDbBackup();`

공유폴더 정보는 ‘임시 DB’와 ‘영구 DB’ 2개로 구성되어 있습니다. 공유폴더에 변경이 발생 했을때 일단 ‘임시 DB’에 ‘추가.삭제.변경＇을 한 후, 작업이 모두 끝나면, ‘영구 DB’로 한번에 복제 하는데, 화면에 보여주는 정보는 ‘영구 DB’를 기준으로 보여줍니다.

프로그램 종료할 때, 만약 임시 DB의 정보가 영구 DB에 복제가 안되어 있으면, 복제를 해줘야 하는데, 그 여부를 확인하기 위해 사용됩니다. 

영구 DB복제 하려면 backupMemoryShareDetailTb 를 호출하면 됩니다.

| return | true = 영구DB로 복제가 필요함.<br />false = 복제가 필요없음. |
| ------ | ------------------------------------------------------------ |
