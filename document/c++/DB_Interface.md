# DB Interface



## Summary

로그인 정보, 전송 정보, 공유폴더 정보, 초대링크 정보를 local DB에도 저장합니다.

이들 정보는 프로그램이 실행.종료 되면서 다시 불러와서 사용해야 하는 정보들 입니다.

local DB shema와 데이터는, 샤링 Desktop 버전에서 '로그인, 공유폴더 추가, 파일보내기 초대링크 생성'을 하게 되면, "C:\Users\username\AppData\Local\Mirinesoft\Saaring" 폴더의 *.db 파일에 저장되므로, 해당 파일을 열어서 확인해보면 됩니다.



## < Common >

DB 파일 오픈 여부 체크

### `bool isOpen();`

로그인 후 호출해서, 로그인 정보를  저
| return | true = DB open 성공, false = DB open실패 |
| ----------- | ------------------------------------------------------------ |



## < Login >

로그인, 환경설정과 관련된 루틴.



### `void doAfterLogin(int idType, QString email, QString phoneNumber, QString password, QString providerId, QString idToken, QString accessToken, QString uid, QString isSaveEmail, QString isAutoLogin);`

로그인 후 호출해서, 로그인 정보를  저장합니다. 로그인 정보는 다음에 다시 로그인 할때, 자동 로그인 할 때 사용 됩니다.

| idType      | saaring::SaaringIdType 의 int 값                             |
| ----------- | ------------------------------------------------------------ |
| email       | 로그인에 사용한 email                                        |
| phoneNumber | 로그인에 사용한 전화번호                                     |
| password    | email 로그인시 등록한 비밀번호                               |
| providerId  | Firebase Provider ID<br />saaring.h 의 PROVIDERID_* 참조     |
| idToken     | OAuth2 로그인, Google에서 사용됨.                            |
| accessToken | OAuth2 로그인, Google.Facebook에서 사용됨.                   |
| uid         | Firebase UID                                                 |
| isSaveEmail | Y = 로그인 정봉 Email.전화번호 저장, N=저장하지 않음.<br />모바일은 default "Y" |
| isAutoLogin | Y = 자동로그인, N=자동로그인 하지 않음.<br />모바일은 default "Y" |



### `bool updateLoginTb(QString idType, QString email, QString phoneNumber, QString password, QString isSaveEmail, QString isAutoLogin, QString isLastLoginEmail);`

설정에서 로그인 정보를 변경했을때 호출 한다.

| return | true = 호출 성공, false = 호출 실패<br />이후 return 값이 bool이고, 성공.실패인 경우에는 별도로 표시 하지 않겠습니다. |
| ----------- | ------------------------------------------------------------ |
| isLastLoginEmail      | "1" 값을 사용합니다.<br />내부적으로 모든 row의 값을 "0"으로 변경한 후, updateLoginTb 를 호출해서 "1"로 업데이트해 놓으면, 자동로그인 할때 readDbLastLogin 를 호출해서 최근(최종적으로) 로그인한 계정 정보를 찾을때 사용합니다. |




### `bool updateLoginAutoStart(QString idType, QString email, QString phoneNumber, QString isAutoStart);`

자동로그인 여부 설정합니다.

| isAutoStart | "Y" = 자동로그인 설정함. "N" = 자동로그인 설정 안함. |
| ----------- | ------------------------------------------------------------ |



### `bool updateSetSend(saaring::SaaringSetupInfo &saaringSetupInfo);`

'설정 > 전송관리' 관련 사항을 설정합니다.


```
    saaring::SaaringSetupInfo setupInfo;

    setupInfo.email                         = saaring::g_member.email;
    setupInfo.phoneNumber                   = saaring::g_member.phoneNumber;
    setupInfo.idType                        = QString::number(saaring::g_member.idType);
    setupInfo.isTransferListAutoResend      = digSend->getIsTransferListAutoResend();
    setupInfo.isTransferListAutoDelete      = digSend->getIsTransferListAutoDelete();
    setupInfo.transferListResendMinute      = digSend->getTransferListResendMinute();
    setupInfo.isDisplayTraySendStatus       = digSend->getIsDisplayTraySendStatus();
    setupInfo.isContinueReceiveFile         = digSend->getIsContinueReceiveFile();

    saaring::g_db->updateSetSend(setupInfo);

```



### `bool updateSetNetwork(saaring::SaaringSetupInfo &saaringSetupInfo);`

'설정 > 네트워크설정' 관련 사항을 설정합니다.

```c++
    saaring::SaaringSetupInfo setupInfo;

    setupInfo.email                         = saaring::g_member.email;
    setupInfo.phoneNumber                   = saaring::g_member.phoneNumber;
    setupInfo.idType                        = QString::number(saaring::g_member.idType);
    setupInfo.isRelayServer                 = "1";
    setupInfo.relayServerPort               = 0;
    setupInfo.otherDownloadCount            = digNetwork->getOtherDownloadCount();
    setupInfo.otherDownloadSpeed            = digNetwork->getOtherDownloadSpeed();
    setupInfo.myDownloadSpeed               = digNetwork->getMyDownloadSpeed();
    setupInfo.otherUploadCount              = digNetwork->getOtherUploadCount();
    setupInfo.otherUploadSpeed              = digNetwork->getOtherUploadSpeed();
    setupInfo.myUploadSpeed                 = digNetwork->getMyUploadSpeed();

    saaring::g_db->updateSetNetwork(setupInfo);
```



### `bool readDbLastLogin(saaring::SaaringSetupInfo &saaringSetupInfo);`

최근(최종적으로) 로그인한 계정 정보를 읽어옵니다. 자동로그인 할 때 계정정보 읽어올때 사용합니다.



### `bool readDbByEmailOrPhoneNumber(QString idType, QString email, QString phoneNumber, saaring::SaaringSetupInfo &saaringSetupInfo);`

idType에 따른 email 또는 전화번호 회원의 설정 정보를 가져올때 사용합니다. saaringSetupInfo에 모든 설정정보가 세팅되어 전달 됩니다.






## < Transfer >

전송목록 관리를 위한 루틴.



### `qint64 insertMyTransTbOne(saaring::SaaringTransferBaseInfo &saaringTransfer);`

전송정보 하나를 추가 합니다. DB에 insert 하고 받은 transNo를 PK로 사용합니다.

saaringTransfer.transNo 에도 값을 세팅해줍니다.

| return | transNo 값을 리턴한다.<br />0 = 호출 실패 |
| ----------- | ------------------------------------------------------------ |



### `bool insertMyTransTbMulti(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer);`

전송정보 여러개를 추가 합니다. DB에 insert 하고 받은 transNo를 PK로 사용합니다.

saaringTransfer.transNo 에도 값을 세팅해줍니다.



### `bool updateMyTransTb(saaring::SaaringTransferBaseInfo &saaringTransfer);`

전송 정보를 변경한다. 일반적으로 전송상태 변경은 updateMyTransTbStatus 를 사용 하면되고, 다운로드.업로드 완료 된경우에는  전송 처리한 사이즈, 시간 등도 같이 업데이트 해야 해서 updateMyTransTb를 사용합니다.



### `bool updateMyTransTbStatus(qint64 transNo, int transStatus, QDateTime updateTime);`

전송완료를 제외한, 그밖의 경우 전송상태가 변경 됐을때 사용합니다.



### `bool updateMyTransTbSortNoMulti(QList<qint64> &paramListTransNo, QDateTime updateTime);`

전송목록 순서를 변경합니다.



### `bool updateMyTransTbProgress(qint64 transNo, qint64 fileSize, qint64 transSize, QDateTime updateTime);`

전송 진행상태에 따른 정보를 업데이트 합니다. 다운로드 할때는 사용하지 않고, 업로드 할때 사용합니다. (업로드도 굳이 필요 없을것 같긴 한데 테스트를 해보면 좋겠습니다.)



### `bool updateMyTransTbInit(qint64 myDeviceNo);`

프로그램 실행시, 기존에 전송 진행중이었던 상태가 있었다면, 모두 초기 상태로 변경합니다.



### `bool deleteMyTransTbOne(qint64 transNo);`

해당 전송건 하나를 삭제 합니다.



### `bool deleteMyTransTbMulti(QList<qint64> &paramListTransNo);`

해당 전송건 전체를 삭제 합니다.



### `bool deleteMyTransTbAll(qint64 deviceNo);`

해당 디바이스 전체의 전송건을 삭제 합니다.



### `bool readMyTransTbByTransNo(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 transNo);`

전송 정보 한건을 읽어옵니다.



### `bool readMyTransTbTransferring(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo, QString deleteYN, qint64 deviceNo);`

myDeviceNo = 내 디바이스 번호, deviceNo = 상대 디바이스 번호, deleteYN = "N" 삭제 안된, 전송상태가 saaring::SaaringTransferStatusType::DOWNLOADING, saaring::SaaringTransferStatusType::UPLOADING 인것을 읽어옵니다.



### `bool readMyTransTbNotTransferringLastOne(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo, QString deleteYN, qint64 deviceNo);`

해당 디바이스의 전송중이 아닌건 중, sortNo로 소팅해서 첫번째 것을 읽어옵니다. 다음 전송을 진행하기 위해 다음 하나를 선택할때 사용합니다.




### `bool readMyTransTbInit(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo);`

초기화 상태의 모든 전송 목록을 읽어온다.

updateMyTransTbInit 호출해서 초기화 한후, readMyTransTbInit 호출해서 초기화된 전송목록을 읽어 온후, UI상의 전송목록에 추가 한다. 이후, readMyTransTbTransferring 호출해서 전송중인게 없으면, readMyTransTbNotTransferringLastOne 호출해서 첫번째 건에 대해 전송을 진행한다..





## < Share >

공유폴더 설정과 관련한 루틴.



### `bool insertShareTb(saaring::SaaringShareInfo &saaringShare);`

공유폴더를 추가 합니다.



### `bool updateShareTb(saaring::SaaringShareInfo &saaringShare);`

공유폴더의 모든 정보를 변경합니다.



### `bool deleteShareTb(qint64 shareNo);`

해당 공유폴더를 삭제합니다.



### `bool readShareTb(qint64 deviceNo, QList<saaring::SaaringShareInfo> &listSaaringShare);`

해당 디바이스 번호의 모든 공유폴더 정보를 읽어옵니다.



### `qint64 getServerFolderNoByShareNo(qint64 shareNo);`

shareNo로 serverFolderNo를 찾습니다. 

shareNo로 local DB 삭제 후, remote DB도 삭제하기 위해 serverFolderNo 을 알아야 할때 사용됩니다.

| return | serverFolderNo |
| ------ | -------------- |



### `QString getShareFolderPathByServerFolderNo(qint64 serverFolderNo);`

serverFolderNo로 공유폴더 Path를 얻어옵니다.

공유폴더 목록에서, 해당 공유폴더를 클릭해서 해당 폴더의 정보를 보고자 할때, remote 디바이스인 경우에는 p2p 접속후, p2p 전문을 통해 공유폴더 정보를 보여줘야겠지만, 해당 공유폴더가 현재 내 디바이스의 공유폴더인 경우에는 해당 path의 파일시스템 정보를 읽어와서 바로 보여주면 된다. 이러한 경우, 해당 공유폴더의 local path를 알고자 할때 사용된다.

| return | 공유폴더의 local path |
| ------ | --------------------- |



### `int getShareFileCntWithShareNo(qint64 deviceNo, qint64 shareNo);`

해당 공유폴더의 파일 갯수를 얻어온다. 파일시스템에서 바로 읽어오지 않고, share detail 테이블에서 읽어온다.

| return | 파일 갯수 |
| ------ | --------- |



### `qint64 getShareFileSize(qint64 deviceNo);`

해당 디바이스의 전체 용량을 읽어온다. 파일시스템에서 바로 읽어오지 않고, share detail 테이블에서 읽어온다.

| return | 전체 (파일의) 용량 |
| ------ | ------------------ |



### `qint64 getShareFileSizeWithShareNo(qint64 deviceNo, qint64 shareNo);`

해당 공유폴더의 전체 용량을 읽어온다. 파일시스템에서 바로 읽어오지 않고, share detail 테이블에서 읽어온다.

| return | 전체 (파일의) 용량 |
| ------ | ------------------ |





## < Share Detail >

공유폴더 내 폴더와 파일 정보.



### `bool updateShareDetailShortPath(qint64 shareNo, QString shareName, QString shareFolderFullPath);`

공유폴더 변경에서, 공유폴더 이름만 변경된 경우, share detail 테이블의 상대 path 정보를 변경해준다.

shareDetail.fullPathName = local full path가 들어가 있고, shareDetail.shortPathName = "/공유폴더이름/상대path"가 들어가 있다.

예를 들어 공유폴더 이름이 '문서' 에서 '내문서'로 이름을 변경한 경우.

"/문서/20210101/my_document.txt" 를 "/내문서/20210101/mydocument.txt" 로 변경합니다.

| shareNo             | local 공유폴더 번호                                |
| ------------------- | -------------------------------------------------- |
| shareName           | 공유폴더 이름<br />예) "내문서"                    |
| shareFolderFullPath | 공유폴더의 local full Path<br />예) "c:/Documents" |



### `bool deleteShareDetailTb(qint64 shareNo);`

공유폴더 삭제 할때 deleteShareTb 호출하고, 이어서 호출해서 공유폴더내 정보도 삭제 합니다.



### `void makeMemoryShareDetailTb();`

공유폴더 내 폴더와 파일 정보는 별도의 스레드가 파일시스템 정보를 수집해서 '임시DB'에 저장한 다음, 정보가 모두 수집되면 '운영DB'에 복제 합니다.

프로그램이 처음 실행될때 호출해서 '임시DB'를 초기화 할 때 사용합니다.



### `void backupMemoryShareDetailTb();`

'임시DB'는 변경된 사항이 있는 경우에 주기적으로 '운영DB'로 복제를 합니다. 그런데, 복제를 하기 전에 미처 복제를 못하고 프로그램이 종료하려는 경우, 프로그램 종료 전에 g_shareFolder->isDbBackup를 호출해서 백업이 필요한지 체크해서, 백업이 필요하면 호출해서 백업(복제) 합니다.





## < Invite Files >

파일보내기 초대 관련한 루틴.



### `bool insertInviteFiles(saaring::SaaringInviteFilesInfo &saaringInviteFilesInfo);`

파일보내기 초대링크에 포함된 파일 정보를 추가 합니다.




### `bool insertListInviteFiles(QList<saaring::SaaringInviteFilesInfo> &listSaaringInviteFilesInfo);`

파일보내기 초대링크에 포함된 파일 정보를 리스트로 추가 합니다.



### `void deleteExpired();`

기간 만료된 파일보내기 초대링크에 포함된 파일 정보를 삭제 합니다.



### `void deleteInviteFilesByInviteNo(qint64 inviteNo);`

해당 inviteNo의 파일보내기 초대링크에 포함된 파일 정보를 삭제 합니다.



### `bool listInviteFiles(qint64 inviteNo, QList<saaring::SaaringInviteFilesInfo> &listSaaringInviteFilesInfo);`

해당 inviteNo의 파일보내기 초대링크에 포함된 파일 정보를 얻어옵니다. 현재 내 디바이스의 local DB에 포함된 파일 정보만 볼 수 있고, 다른 디바이스의 파일보내기 초대링크에 포함된 파일 정보를 가져오는것은 아닙니다.




### `bool listInviteFilesWithCertKey(qint64 inviteNo, QString certKey, QList<saaring::SaaringInviteFilesInfo> &listSaaringInviteFilesInfo);`

해당 inviteNo와 certKey의 파일보내기 초대링크에 포함된 파일 정보를 얻어옵니다. 현재 내 디바이스의 local DB에 포함된 파일 정보만 볼 수 있고, 다른 디바이스의 파일보내기 초대링크에 포함된 파일 정보를 가져오는것은 아닙니다.

