# Tutorial - P2p Connect



## Summary

P2p 접속 단계에 대한 가이드 입니다.

샤링은 TCP 방식의 p2p로 우선 접속을 시도 합니다. TCP 방식의 p2p 통신을 위해서는 공유기를 통과하기 위해 자동 portmapping 을 해야 합니다. 

TCP 방식으로 접속이 안되면, UDP 방식의 p2p로 접속을 시도 합니다. TDP 방식의 p2p 통신을 위해서는 hole-punching을 해야 해야 합니다.

TCP 방식과 UDP 방식 모두 실패하면, Relay Server를 통해 통신을 합니다.

즉, p2p 통신을 하기 위해서는 사전에 portmapping과 hole-punching을 해야 해서 접속하는데 조금 시간이 걸리기 때문에, 화면상에 접속중 표시를 해주고 기다릴 수 있도록 해야 합니다.



## Functions



### `PCC_P2P_PORTMAP_FINISHED `

내부적으로 Signaling Server에 접속이되면, portmapping 이 시작되고, portmapping 이 끝나면, callback 을 받습니다.

공유기와 네트워크 상태에 따라서, 시간이 좀 걸릴 수 있어서, 별도의 스레드로 포트매핑이 진행됩니다.

특별히 해줄건 없고, portmapping 이 성공적이면, local Relay Server를 실행시킵니다. Local Relay Server는 TCP 환경에서 작동되기 때문입니다.



### `g_web->listFolderOfOther`

친구의 공유폴더 목록을 가져옵니다.

p2p 접속을 위해서는, 친구의 디바이스 정보를 알아야 하는데, 공유폴더 목록을 가져오면, p2p 접속에 필요한 정보가 모두 있습니다.



### `g_p2p->doP2pConnect `

p2p 접속을 시도 합니다. p2p 접속단계는 내부적으로 복잡하고 다양한 단계가 진행되고, 시간도 걸리기 때문에 wait cursor 로 대기하라고 표시해줘야 합니다.

해당 디바이스가 접속중인지 체크하기 위해 g_web->listLiveDevice를 호출해서 확인합니다. 해당 디바이스가 모바일이고 비접속 중이면 mustWakeUp=true 로 전달해서, 푸시메시지를 전송해서 해당 단말기를 깨운 다음 샤링 앱이 실행되면 이후 접속이 진행 됩니다.

Wake Up 에 성공하면 PCC_P2P_WAKEUP_FINISHED callback 을 받게 되고, 실패 했을때는 접속 진행을 중단하면 되지만, 성공한 경우에는 메시지 정도만 변경해주고, 특별히 처리해줄건 없습니다.

P2p 접속에 성공하면, 내부적으로는 인증단계가 진행 됩니다. 인증단계가 끝나면 PCC_P2P_LOGIN_FINISHED callback을 받게 되는데, 이 callback을 받게 되면, 비로소 모든 p2p 접속이 프로세스가 끝난 것입니다.



### `P2P Protocol `

P2P 접속이후, 한쪽이 request를 보내면, 상대편에서 response로 회신을 해줍니다.

모든 전문은 json 으로 주고 받게 되고, 내부적으로는 접속 유지를 위해 ping (또는 heart beat)을 보내서 접속을 유지 합니다.

p2p protocol 은 saaring.h 에 다음과 같이 선언되어 있습니다.

```
enum SaaringPacketProcessType {
    PROCESSTYPE_REQ_PING                                            = 0,
    PROCESSTYPE_REQ_LOGIN                                           = 1,
    PROCESSTYPE_REQ_CHAT                                            = 2,
    PROCESSTYPE_REQ_FOLDER_LIST                                     = 3,
    PROCESSTYPE_REQ_FOLDER_DETAIL                                   = 4,
    PROCESSTYPE_REQ_FOLDER_DETAIL_RECURSIVE                         = 5,
    PROCESSTYPE_REQ_FILE_DOWNLOAD                                   = 6,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_STOP                              = 7,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_CANCEL                            = 8,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_COMPLETE                          = 9,
    PROCESSTYPE_REQ_FILE_UPLOAD_QUERY                               = 10,
    PROCESSTYPE_REQ_FILE_UPLOAD                                     = 11,
    PROCESSTYPE_REQ_FILE_UPLOAD_STOP                                = 12,
    PROCESSTYPE_REQ_FILE_UPLOAD_CANCEL                              = 13,
    PROCESSTYPE_REQ_FILE_UPLOAD_COMPLETE                            = 14,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 15,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 16,
    PROCESSTYPE_REQ_SAVE_SEND_FILES_INVITE_INFO                     = 17,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_CHECK                            = 18,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_STAND_BY                         = 19,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_RELEASE                          = 20,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_CHECK                              = 21,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_STAND_BY                           = 22,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_RELEASE                            = 23,

    PROCESSTYPE_RES_PING                                            = 10000,
    PROCESSTYPE_RES_LOGIN                                           = 10001,
    PROCESSTYPE_RES_CHAT                                            = 10002,
    PROCESSTYPE_RES_FOLDER_LIST                                     = 10003,
    PROCESSTYPE_RES_FOLDER_DETAIL                                   = 10004,
    PROCESSTYPE_RES_FOLDER_DETAIL_RECURSIVE                         = 10005,
    PROCESSTYPE_RES_FILE_DOWNLOAD                                   = 10006,
    PROCESSTYPE_RES_FILE_DOWNLOAD_STOP                              = 10007,
    PROCESSTYPE_RES_FILE_DOWNLOAD_CANCEL                            = 10008,
    PROCESSTYPE_RES_FILE_DOWNLOAD_COMPLETE                          = 10009,
    PROCESSTYPE_RES_FILE_UPLOAD_QUERY                               = 10010,
    PROCESSTYPE_RES_FILE_UPLOAD                                     = 10011,
    PROCESSTYPE_RES_FILE_UPLOAD_STOP                                = 10012,
    PROCESSTYPE_RES_FILE_UPLOAD_CANCEL                              = 10013,
    PROCESSTYPE_RES_FILE_UPLOAD_COMPLETE                            = 10014,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 10015,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 10016,
    PROCESSTYPE_RES_SAVE_SEND_FILES_INVITE_INFO                     = 10017,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_CHECK                            = 10018,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_STAND_BY                         = 10019,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_RELEASE                          = 10020,
    PROCESSTYPE_RES_UPLOAD_QUEUE_CHECK                              = 10021,
    PROCESSTYPE_RES_UPLOAD_QUEUE_STAND_BY                           = 10022,
    PROCESSTYPE_RES_UPLOAD_QUEUE_RELEASE                            = 10023
};


```

내부적으로는 REQ_CHAT 과 같은 채팅 메시지를 주고 받을 수 있도록 해놨지만, 아직 사용하고 있진 않습니다.

A가 B에게 request를 보냈을때, B는 request를 받아서 다시 A에게 회신을 한 후, B는 PCC_P2P_PROCESSED_REQUEST callback 을 받는다.

B가 A에서 response를 보냈을때, A는 response를 받아서 정상적으로 처리를 했으면, A는 PCC_P2P_PROCESSED_RESPONSE callback을 받는다.















