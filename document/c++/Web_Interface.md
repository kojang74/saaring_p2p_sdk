# Web Interface



## Summary

회원가입, 로그인, 친구관계 설정, 공유폴더 설정, 초대링크 생성 등과 관련한 정보 관리를 위해, remote DB를 운영하고 있고, 이를 위한 web api 사용을 위한 인터페이스를 제공합니다.





## Functions



### `bool doPost(QString &resultCode, QString &resultMessage, QJsonObject &resultData, QString strUrl, QUrlQuery urlQuery);`

POST 방식의 웹 URL을 호출합니다.

결과는 json 형식인데, resultCode, resultMessage, resultData 정보를 1차파싱해서 전달합니다.

| result        | true = 호출 성공<br />false = 네트워크 오류 등 호출자체를 못한 경우 |
| ------------- | ------------------------------------------------------------ |
| resultCode    | 결과 code<br />100 = 성공<br />그밖의 값은 에러. 특히, 0 = 시스템에러 |
| resultMessage | resultCode 에 따른 결과(에러) 메시지.                        |
| resultData    | 처리 결과로 받은 데이터. json 형식입니다.                    |
| strUrl        | 호출 URL                                                     |
| urlQuery      | post query Data                                              |



### `bool doPost(QJsonObject &resultData, QString strUrl, QUrlQuery urlQuery);`

POST 방식의 웹 URL을 호출합니다.

결과는 json 형식인데, 결과 전체를 resultData에 그대로 전달합니다.

| result        | true = 호출 성공<br />false = 네트워크 오류 등 호출자체를 못한 경우 |
| ------------- | ------------------------------------------------------------ |
| resultData    | 처리 결과로 받은 데이터. json 형식입니다.                    |
| strUrl        | 호출 URL                                                     |
| urlQuery      | post query Data                                              |



### `bool doGet(QJsonObject &resultData, QString strUrl);`

GET 방식의 웹 URL을 호출합니다.

결과는 json 형식인데, 결과 전체를 resultData에 그대로 전달합니다.

| result     | true = 호출 성공<br />false = 네트워크 오류 등 호출자체를 못한 경우 |
| ---------- | ------------------------------------------------------------ |
| resultData | 처리 결과로 받은 데이터. json 형식입니다.                    |
| strUrl     | 호출 URL                                                     |



### `bool listFolderOfOther(QString authKey, qint64 friendUserNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<saaring::SaaringShareFolderInfo> &listFolder);`

친구의 공유폴더 목록을 받아옵니다.

친구목록에서 친구를 클릭해서 친구의 공유폴더 목록을 보여줄때 사용합니다.

| result        | true = 호출 성공, false = 호출 실패                          |
| ------------- | ------------------------------------------------------------ |
| authKey       | 로그인 할때 받은 authKey 를 전송합니다.                      |
| friendUserNo  | 공유폴더 목록을 받아올 친구의 회원번호.<br />친구의 회원번호는 친구 목록을 받아오면 알 수 있습니다. |
| resultCode    | 결과 코드를 받아옵니다.                                      |
| resultMessage | 결과 메시지를 받아옵니다.                                    |
| resultData    | 결과 데이터를 받아옵니다.                                    |
| listFolder    | resultData를 파싱해서 ShareFolder의 List로 받아옵니다.       |

```c++
struct SaaringShareFolderInfo
{
    qint64                  deviceNo;
    int                     deviceType;
    QString                 deviceName;
    QString                 liveYN;

    qint64                  serverFolderNo;

    QString                 folderName;
    qint64                  fileSize;
    int                     fileCount;

    int                     shareType;
    int                     downShareType;
    int                     upShareType;
    int                     shareFoaf;

    int                     numFolder;
    int                     numFile;
    qint64                  totalFileSize;

    QString                 password;
    QString                 passwordYN;
    QString                 downloadYN;
    QString                 uploadYN;

    QList<qint64>           listDownFriendUserNo;
    QList<qint64>           listUpFriendUserNo;
};
```



##### 다음 전문 부터는 항상 중복되는 authKey, resultCode, resultMessage, resultData는 생략합니다.



### `bool listLiveDevice(QString authKey, QString deviceNos, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<qint64> &listLiveDevice);`

전송받은 deviceNos 목록에서 접속중인 디바이스 번호 목록을 받아옵니다.

data type 처리에 대한 이슈 때문에, 디바이스 번호를 String으로 보냅니다. 구분자로는 "," 콤마를 사용합니다. 다른 전문에서도 이렇게 String으로 보낼때는 구분자로 ","를 사용합니다.

p2p 접속 시도하기 전에 본 전문을 호출해서, 접속하려는 디바이스가 접속중인지 확인합니다. p2p 접속은 복잡한 과정을 거치고 시간도 많이 걸리기 때문에 빠르게 먼저 확인해보기 위함입니다.

또한, 전송목록이 있는 상태에서 프로그램을 종료했다가, 다시 실행하면, 전송목록에서 접속중인 디바이스만 찾아서 자동 재전송 처리를 해줘야 할텐데, 이때 접속중인 디바이스를 찾을때도 사용합니다.



| result         | true = 호출 성공, false = 호출 실패                          |
| -------------- | ------------------------------------------------------------ |
| deviceNos      | 접속여부를 확인하고자 하는 디바이스 번호 목록을 구분자로 "," 를 이용해서 전달합니다. |
| listLiveDevice | 공유폴더 목록을 받아올 친구의 회원번호.<br />친구의 회원번호는 친구 목록을 받아오면 알 수 있습니다. |



### `bool getBadgeInfo(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, saaring::SaaringBadgeInfo &saaringBadgeInfo);`

뱃지로 표시할 알림이 있는지 체크합니다. 현재는 공지사항 여부, 친구요청 여부 2가지만 확인합니다.

홈(메인) 화면으로 전환 될때마다 정기적으로 호출해서 체크합니다.



| result           | true = 호출 성공, false = 호출 실패                          |
| ---------------- | ------------------------------------------------------------ |
| saaringBadgeInfo | 접속여부를 확인하고자 하는 디바이스 번호 목록을 구분자로 "," 를 이용해서 전달합니다. |

```c++
struct SaaringBadgeInfo
{
    int                     numNewRequestFriend;
    int                     numNewNotice;
};
```



### `bool listFriend(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<SaaringFriendInfo> &listSaaringFriendInfo);`

친구 목록을 얻어옵니다.

| result                | true = 호출 성공, false = 호출 실패 |
| --------------------- | ----------------------------------- |
| listSaaringFriendInfo | 내 친구 목록                        |



```
struct SaaringFriendInfo : public SaaringMemberInfo
{
    QString                 favoriteYN;
    QString                 regdt;

    int                     numDevice;
    int                     numLiveDevice;
    int                     numOfflineMobileDevice;
    int                     numFile;
    int                     numFolder;
    qint64                  totalFileSize;

    int                     requestStatus;
    QDateTime               requestdt;
};
```



### `bool requestFriendByUserNo(QString authKey, qint64 toUserNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

해당 샤링 회원에게 친구 요청을 보냅니다.

| toPhoneNumber | 친구 요청.초대 할 전화번호 |
| ------------- | -------------------------- |



### `bool listRequestingFriend(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<saaring::SaaringFriendInfo> &listSaaringIncomingRequestFriend, QList<saaring::SaaringFriendInfo> &listSaaringRequestingFriendMember, QList<saaring::SaaringFriendInfo> &listSaaringRequestingFriendNonMember);`

내가 받은 친구요청 목록, 내가 보낸 친구요청 목록, 내가 보낸 비회원 친구초대 목록을 얻어옵니다.

요청을 확인.승인.거절.취소 할때 사용됩니다.

| listSaaringIncomingRequestFriend     | 내가 받은 친구요청 목록        |
| ------------------------------------ | ------------------------------ |
| listSaaringRequestingFriendMember    | 내가 보낸 친구요청 목록        |
| listSaaringRequestingFriendNonMember | 내가 보낸 비회원 친구초대 목록 |



### `bool cancelRequestFriend(QString authKey, qint64 toUserNo, QString toEmail, QString toPhoneNumber, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

내가 보낸 친구 요청에서, 회원번호, 이메일, 전화번호를 기준으로 보낸 친구 요청을 취소.삭제 합니다. 삭제를 하면 상대방은 알지 못합니다. 한편, 요청을 삭제만 했기 때문에, 상대방은 다시 요청 할 수 있습니다.

| toUserNo      | 해당 회원번호로 보낸 친구 요청을 취소.삭제 합니다. |
| ------------- | -------------------------------------------------- |
| toEmail       | 해당 이메일로 보낸 친구 요청을 취소.삭제 합니다.   |
| toPhoneNumber | 해당 전화번호로 보낸 친구 요청을 취소.삭제 합니다. |



### `bool rejectRequestFriend(QString authKey, qint64 toUserNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

내가 받은 친구 요청에서, 회원번호를 기준으로 친구 요청을 거절 합니다.

| toUserNo      | 해당 회원번호가 보낸 친구 요청을 거절 합니다.      |
| ------------- | -------------------------------------------------- |



### `bool recoverRequestFriend(QString authKey, qint64 toUserNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

친구 요청 거절을 취소하고, 다시 요청 받은 상태로 변경 합니다.

| toUserNo | 친구 요청을 보구 하려는 회원번호 |
| -------- | -------------------------------- |



### `bool addFriend(QString authKey, qint64 friendUserNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData); `

해당 회원번호의 회원을 친구를 추가 합니다. 친구 추가를 위해서는 회원번호를 알아야 하는데, 해당 회원의 회원번호는 친구 요청을 받으면 알수 있습니다.

또한, 회원검색을 통해서 알아낼 수도 있지만, 스팸성 친구요청 이슈로 현재는 제공하지 않습니다.

내가 받은 친구요청에서 수락해서 친구 추가 할때 사용합니다.

| friendUserNo | 친구 추가 하려는 회원의 회원번호 |
| ------------ | -------------------------------- |



### `bool blockFriend(QString authKey, QString friendUserNos, QString blockYN, QString &resultCode, QString &resultMessage, QJsonObject &resultData); `

해당 회원을 차단 합니다. 회원을 차단하면 더이상 친구요청이 오지 않습니다.

| friendUserNos | 차단할 회원번호 목록<br />구분자로 "," 콤마를 이용해서 String으로 전달합니다. |
| ------------- | ------------------------------------------------------------ |
| blockYN | Y = 차단, N = 차단해지 |



### `bool listBlock(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<SaaringBlockInfo> &listSaaringBlocked, QList<SaaringBlockInfo> &listSaaringRejected);`

친구 차단 목록과, 친구 요청 거절 목록을 가져옵니다.

| listSaaringBlocked  | 친구 차단 목록<br />친구로 등록 한 후 차단한 목록.          |
| ------------------- | ----------------------------------------------------------- |
| listSaaringRejected | 친구 요청 거절 목록<br />친구 추가 전에 요청을 차단한 목록. |



### `bool inviteFriendSimple(QString authKey, QString certKey, QString inviteMessage, QString &resultCode, QString &resultMessage, QJsonObject &resultData, qint64 &inviteNo, QString &inviteCode, QString &dynamicLink); `

친구 초대 링크를 만듭니다. 샤링 회원 또는 비회원에게, 초대할 수 있는 링크를 생성하는 기능입니다. 초대 링크를 입력하면 회원가입.로그인 후 친구까지 맺어집니다.

| certKey       | 초대링크 등록시 필요한 비밀번호<br />없으면 "" |
| ------------- | ---------------------------------------------- |
| inviteMessage | 어떤 초대링크인지 초대링크에 표시될 설명.      |
| inviteNo      | 초대링크 등록 후 생성된 초대등록 번호          |
| inviteCode    | 초대링크 등록 후 생성된 초대코드               |
| dynamicLink   | 초대링크                                       |



### `bool cancelInviteFriend(QString authKey, qint64 inviteNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

내가 생성한 초대 링크를 제거한다. 내가 만든 초대링크 목록을 읽어와서 inviteNo 를 알아낼 수 있다.

초대링크가 제거 되면서 초대링크에 따른 초대도 취소 된다.

| inviteNo | 삭제할 초대링크 번호 |
| -------- | -------------------- |



### `bool completeInviteFriend(QString authKey, qint64 inviteNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

내 다른 디바이스의 파일로, 파일보내기 초대링크를 생성할때에는, 해당 정보를 해당 디바이스에 저장후 회신을 받은 다음, 최종적으로 초대링크 생성을 완료한다.

다음 callback을 받은 후 호출해서 파일보내기 초대링크 생성을 완료한다.

```
saaring::SaaringP2pInterface::P2pCallbackCommand::PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO
```

| inviteNo | 생성 완료할 초대링크 번호 |
| -------- | ------------------------- |



### `bool extendInviteExpiredt(QString authKey, qint64 inviteNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData, saaring::SaaringInviteInfo &saaringInviteInfo);`

초대링크의 유효기간을 연장한다.

| inviteNo | 기간 연장할 초대링크 번호 |
| -------- | ------------------------- |
| saaringInviteInfo | 기간 연장된 초대링크 정보 |



### `bool getInvite(QString authKey, qint64 inviteNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData, saaring::SaaringInviteInfo &saaringInviteInfo);`

inviteNo 로 초대 정보를 얻어옵니다.

| inviteNo | 초대 번호 |
| -------- | --------- |

```c++
struct SaaringInviteInfo
{
    qint64                  inviteNo;
    QString                 inviteCode;

    qint64                  userNo;
    QString                 userName;

    qint64                  inviteDeviceNo;
    int                     inviteDeviceType;
    QString                 inviteDeviceName;
    int                     inviteType;

    QList<SaaringInviteUsersInfo>  listInviteUsers;
    QList<SaaringInviteFilesInfo>  listInviteFiles;

    QString                 certKey;
    qint64                  numFile;
    qint64                  totalFileSize;

    qint64                  inviteShareFolderServerFolderNo;
    QString                 inviteShareFolderName;
    QString                 inviteShareFolderPath;
    QString                 inviteShareFolderPasswordYN;

    QString                 inviteMessage;

    QString                 dynamicLink;
    QString                 rejectYN;

    QDateTime               expiredt;
    QDateTime               regdt;
};

```



### `bool getInviteByDynamicLink(QString dynamicLink, QString certKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, saaring::SaaringInviteInfo &saaringInviteInfo);`

초대 링크로 초대 정보를 얻어옵니다.

| dynamicLink | 초대링크 |
| ----------- | -------- |
| certKey | 초대비밀번호 |



### `bool listInviteAll(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<saaring::SaaringInviteInfo> &listSaaringInviteInfo);`

내가 만든 초대링크 목록을 가져옵니다.

| listSaaringInviteInfo | 초대 정보 리스트 |
| --------------------- | ---------------- |



### `bool inviteShareFolderSimple(QString authKey, qint64 inviteShareFolderServerFolderNo, QString inviteShareFolderPath, QString certKey, QString inviteMessage, QString &resultCode, QString &resultMessage, QJsonObject &resultData, qint64 &inviteNo, QString &inviteCode, QString &dynamicLink);`

공유폴더 초대링크를 생성합니다.

| inviteShareFolderServerFolderNo | 공유폴더 serverFolderNo                                      |
| ------------------------------- | ------------------------------------------------------------ |
| inviteShareFolderPath           | 공유폴더 path<br />ex) '음악/트와이스(TWICE)/TWICE (트와이스) - What is Love' |



### `bool inviteSendFilesSimple(QString authKey, qint64 numFile, qint64 totalFileSize, QString certKey, QString inviteMessage, QString &resultCode, QString &resultMessage, QJsonObject &resultData, qint64 &inviteNo, QString &inviteCode, QString &dynamicLink);`

내 디바이스에 있는 파일을 대상으로 파일보내기 초대링크를 생성합니다.

remote DB를 통해서 초대링크가 생성돼서 inviteNo를 받은 다음, 파일 정보와 함께 g_db->insertInviteFiles를 호출해서 local DB에 저장해서 초대링크 생성을 마무리 합니다.

서버에는 파일갯수와 총 파일사이즈만 저장합니다.

| numFile       | 파일 갯수      |
| ------------- | -------------- |
| totalFileSize | 총 파일 사이즈 |



### `bool inviteSendFilesForRemoteDeviceSimple(QString authKey, qint64 numFile, qint64 totalFileSize, qint64 inviteShareFolderServerFolderNo, QString inviteShareFolderPath, QString certKey, QString inviteMessage, QString &resultCode, QString &resultMessage, QJsonObject &resultData, qint64 &inviteNo, QString &inviteCode, QString &dynamicLink);` 

내 다른 디바이스의 파일를 대상으로 초대링크를 생성합니다.

이때에는 디바이스가 다르기 때문에, 프로세스도 길고 방법도 조금 다릅니다.  remote DB에 초대링크 생성후, g_p2p->doP2pSaveSendFilesInviteInfo를 호출해서 파일 정보를 저장하고, PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO callback 받아서 회신이 오면, g_web->completeInviteFriend 를 호출해서 초대링크 생성을 마무리 합니다.

| numFile       | 파일 갯수      |
| ------------- | -------------- |
| totalFileSize | 총 파일 사이즈 |
| totalFileSize | 총 파일 사이즈 |
| totalFileSize | 총 파일 사이즈 |



### `bool registerInviteByDynamicLink(QString authKey, QString dynamicLink, QString certKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, saaring::SaaringInviteRegisteredInfo &saaringInviteRegisteredInfo, saaring::SaaringInviteInfo &saaringInviteInfo);`

친구로부터 받은 초대링크를 등록합니다. 초대링크 등록을 위해서는 비밀번호가 필요할 수 있습니다. 비밀번호는 메신저 등을 통해서 별도로 받아야 합니다.

초대링크가 정상적으로 등록이 되면, 자동을 친구가 맺어집니다. 그리고, 초대링크 종류에 따라, 초대링크에 포함되어 있는 공유폴더에 접속할 수 도 있고, 파일을 다운로드 할 수 도 있습니다.

| dynamicLink                 | 등록하려는 초대링크             |
| --------------------------- | ------------------------------- |
| certKey                     | 초대링크 등록에 필요한 비밀번호 |
| saaringInviteRegisteredInfo | 내가 등록한 초대링크 정보       |
| saaringInviteInfo           | 초대링크에 대한 초대 정보       |



### `bool deleteInviteRegistered(QString authKey, qint64 inviteNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

등록한 초대링크를 삭제 한다.

| inviteNo | 삭제 하려는 등록한 초대링크의 초대번호 |
| -------- | -------------------------------------- |



### `bool getInviteRegistered(QString authKey, qint64 inviteNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData, saaring::SaaringInviteRegisteredInfo &saaringInviteRegisteredInfo, saaring::SaaringInviteInfo &saaringInviteInfo);`

등록한 초대링크 정보를 얻어옵니다.

| inviteNo | 등록한 초대링크의 초대번호 |
| -------- | ------ |
| saaringInviteRegisteredInfo | 내가 등록한 초대링크 정보       |
| saaringInviteInfo           | 초대링크에 대한 초대 정보       |



### `bool listInviteRegisteredAll(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<saaring::SaaringInviteRegisteredInfo> &listSaaringInviteRegisteredInfo);`

내가 등록한 모든 초대 링크 목록을 가져옵니다.

| listSaaringInviteRegisteredInfo | 내가 등록한 모든 초대 링크 |
| ------------------------------- | -------------------------- |



### `bool verifyFirebaseToken(QString firebaseToken, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

firebaseToken 의 유효성을 체크 합니다. 자동로그인 할때 firebaseToken을 local DB에 저장해 놨을 경우 사용되는데, 어차피 실패하면 다시 로그인 해야 하는데, 다시 로그인 하는데 보안적인 이슈로, 오래 걸리지 않는다면, 매번 로그인 하도록 합니다.

| firebaseToken | firebase token |
| ------------- | -------------- |



### `bool joinMember(SaaringMemberInfo &saaringMember, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

회원가입 처리를 합니다. 회원가입은 email 기반 회원가입 할때만 사용합니다.

firebaseToken 의 유효성을 검증하기 때문에, 회원가입을 위해서는 우선적으로 Firebase에 가입(등록) 되어야 합니다. 또한, email 가입의 경우 email 인증도 마무리 되어 있어야 합니다.

| saaringMember | 회원 가입 정보 |
| ------------- | -------------- |

다음의 정보를 세팅해줘야 합니다.

```
        saaring::SaaringMemberInfo saaringMember;
        saaringMember.firebaseToken = firebaseToken;
        saaringMember.email         = email;
        saaringMember.password      = password;
        saaringMember.userName      = userName;
        saaringMember.lastName      = "";
        saaringMember.firstName     = "";
        saaringMember.message       = "";
        saaringMember.idType        = idType;
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_WINDOWS;
#ifdef Q_OS_LINUX
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_LINUX;
#elif defined (Q_OS_MACOS)
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_OSX;
#endif        
```

회원가입 후 다음 정보를 세팅한 후, g_db->doAfterLogin를 호출해서 local DB에도 저장해줍니다.

```
        saaring::g_member.firebaseToken = saaringMember.firebaseToken;
        saaring::g_member.authKey       = saaringMember.authKey;
        saaring::g_member.userNo        = saaringMember.userNo;
        saaring::g_member.joinStatus    = saaringMember.joinStatus;

        saaring::g_member.deviceNo      = saaringMember.deviceNo;
        saaring::g_member.deviceName    = saaringMember.deviceName;
        saaring::g_member.deviceType    = saaringMember.deviceType;

        saaring::g_member.userId        = saaringMember.userId;
        saaring::g_member.userName      = saaringMember.userName;

        saaring::g_member.email         = saaringMember.email;
        saaring::g_member.phoneNumber   = saaringMember.phoneNumber;

        saaring::g_member.password      = password;

        saaring::g_member.firstName     = saaringMember.firstName;
        saaring::g_member.lastName      = saaringMember.lastName;

        saaring::g_member.profilePic    = saaringMember.profilePic;
        saaring::g_member.message       = saaringMember.message;

        saaring::g_member.idType        = (int)saaring::SaaringIdType::PASSWORD;
        saaring::g_member.providerId    = saaringMember.providerId;
        saaring::g_member.idToken       = saaringMember.idToken;
        saaring::g_member.accessToken   = saaringMember.accessToken;
        saaring::g_member.uid           = saaringMember.uid;
        saaring::g_member.providerYN    = saaringMember.providerYN;
        saaring::g_member.numProvider   = saaringMember.numProvider;

```



### `bool loginMember(SaaringMemberInfo &saaringMember, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

로그인 합니다.

joinMember가 있지만, SNS 로그인의 경우 로그인과 동시에 가입처리 합니다. 따라서, loginMember를 호출하면 회원가입 되어 있지 않으면, 회원가입 후 로그인 처리 합니다.

| saaringMember | 로그인.회원가입 정보 |
| ------------- | -------------------- |

이메일 로그인의 경우 다음 정보를 세팅해줘야 합니다.

```c++
    	saaring::SaaringMemberInfo saaringMember;
    	saaringMember.firebaseToken = firebaseToken;
    	saaringMember.email         = email;
    	saaringMember.password      = password;
    	saaringMember.userName      = this->userName;
    	saaringMember.idType        = (int)saaring::SaaringIdType::PASSWORD;
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_WINDOWS;
#ifdef Q_OS_LINUX
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_LINUX;
#elif defined (Q_OS_MACOS)
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_OSX;
#endif        
```

SNS 로그인의 경우 다음 정보를 세팅해줘야 합니다.

```c++
    	saaring::SaaringMemberInfo saaringMember;
    	saaringMember.idType        = idType;
    	saaringMember.accessToken   = accessToken;
    	saaringMember.idToken       = idToken;
    	saaringMember.uid           = uid;
    	saaringMember.userName      = userName;
    	saaringMember.email         = email;
    	saaringMember.profilePic    = profilePic;
    	saaringMember.firebaseToken = firebaseToken;
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_WINDOWS;
#ifdef Q_OS_LINUX
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_LINUX;
#elif defined (Q_OS_MACOS)
        saaringMember.deviceType    = (int)saaring::SaaringDeviceType::SDT_OSX;
#endif 
```

로그인 후에도, 회원가입 후 처럼 다음 정보를 세팅한 후, g_db->doAfterLogin를 호출해서 local DB에도 저장해줍니다.

```
        saaring::g_member.firebaseToken = saaringMember.firebaseToken;
        saaring::g_member.authKey       = saaringMember.authKey;
        saaring::g_member.userNo        = saaringMember.userNo;
        saaring::g_member.joinStatus    = saaringMember.joinStatus;

        saaring::g_member.deviceNo      = saaringMember.deviceNo;
        saaring::g_member.deviceName    = saaringMember.deviceName;
        saaring::g_member.deviceType    = saaringMember.deviceType;

        saaring::g_member.userId        = saaringMember.userId;
        saaring::g_member.userName      = saaringMember.userName;

        saaring::g_member.email         = saaringMember.email;
        saaring::g_member.phoneNumber   = saaringMember.phoneNumber;

        saaring::g_member.password      = password;

        saaring::g_member.firstName     = saaringMember.firstName;
        saaring::g_member.lastName      = saaringMember.lastName;

        saaring::g_member.profilePic    = saaringMember.profilePic;
        saaring::g_member.message       = saaringMember.message;

        saaring::g_member.idType        = (int)saaring::SaaringIdType::PASSWORD;
        saaring::g_member.providerId    = saaringMember.providerId;
        saaring::g_member.idToken       = saaringMember.idToken;
        saaring::g_member.accessToken   = saaringMember.accessToken;
        saaring::g_member.uid           = saaringMember.uid;
        saaring::g_member.providerYN    = saaringMember.providerYN;
        saaring::g_member.numProvider   = saaringMember.numProvider;
```



### `bool doMe(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, SaaringMemberInfo &saaringMember, QList<saaring::SaaringMemberProviderInfo> &listSaaringMemberProvider);`

내 정보를 다시 읽어옵니다. 특히, link 되어 있을 경우, Firebase provider 목록을 가져올때 사용합니다.



### `bool isEmailExist(QString email, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QVariant &isEmailExist);`

email 가입.로그인 전에 해당 email로 회원가입이 되어 있는지 체크합니다.

| email        | 확인 하려는 이메일                                           |
| ------------ | ------------------------------------------------------------ |
| isEmailExist | bool type의 Variant 입니다.<br />true = 이미 가입되어 있습니다. false = 가입되어 있지 않습니다. |



### `bool doUnlink(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData); `

Firebase 계정은 서로 링크 할 수 있습니다.
회원가입 단계에서 링크 할수 있고, 환경설정에서 firebase SDK를 이용해서 링크를 끊은 다음, 본 함수를 이용해서 remote DB에도 업데이트 해줘야 합니다.

링크를 끊은 다음에는 이어서 바로 로그아웃 합니다.

| authKey | 현재 로그인 중인 회원 정보를 확인해서 remote DB상의 로그인 정보를 unlink 처리 합니다. |
| ------- | ------------------------------------------------------------ |



### `bool doLogout(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

프로그램을 종료 하거나 로그아웃을 하려면, 우선 Signaling Server와 접속을 끊고, remote DB에도 로그아웃 처리를 해줘야 합니다.

| authKey | 현재 로그인 중인 회원 정보를 확인해서 remote DB상에 로그아웃 처리 합니다. |
| ------- | ------------------------------------------------------------ |



### `bool searchUser(QString authKey, QString keyword, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<SaaringFriendInfo> &listSaaringFriendInfo);`

email 이나 전화번호로 회원을 검색 합니다. 개인정보 및 스팸 방지를 위해, like 검색을 하지 않고, equal 검색을 합니다.

| keyword | 검색하려는 email 또는 전화번호<br />전화번호는 국가코드포함, 숫자로만 구성되어야 합니다. |
| ------- | ------------------------------------------------------------ |



### `bool changeProfilePic(QString authKey, QString &profilePic, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

호출 하기 전에 이미지를 서버에 업로드 해야 합니다. 업로드한 이미지의 임시 path를 changeProfilePic로 전달하면, changeProfilePic에 변경된 path를 remote DB에 저장하고 해당 값으로 변경해서 다시 보내줍니다.

local 파일을 서버에 업로드 하고, changeProfilePic 함수를 내부적으로 호출해서 최종적으로 업로드된 이미지를 리턴해주는 함수가, g_common->changeProfileImage 입니다. changeProfileImage를 사용하세요.

| profilePic | 업로드한 이미지의 임시 path를 changeProfilePic로 전달하면, changeProfilePic에 변경된 path를 remote DB에 저장하고 해당 값으로 변경해서 다시 보내줍니다. |
| ---------- | ------------------------------------------------------------ |



### `bool changeEmail(QString authKey, QString email, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

이메일 로그인인 경우, 이메일을 변경합니다. 이메일 변경을 위해서는 firebase 이메일 변경부터 해야 하는데, firebase 이메일 변경을 하려면, 먼저 로그인부터 해야 합니다. 만약, 변경하려는 이메일이 이미 다른 계정과 연결되어 있으면, 이메일을 변경할 수 없습니다.

firebase를 통해 이메일 변경이 완료된 이후에는 remote DB에도 업데이트 해야 하는데, 이때 호출합니다. 이메일이 변경되면, 로그아웃 처리가 되어야 합니다. 다시 로그인을 하려고 하면, 변경된 이메일 인증도 새로 해야 합니다.

| email | 변경된 이메일 |
| ----- | ------------- |



### `bool changeUserName(QString authKey, QString userName, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

회원가입시 등록한 이름을 변경합니다. firebase auth를 이용해서 먼저 이름을 변경하고, 본 함수를 호출해서 remote DB의 이름도 같이 변경합니다.

| userName | 변경 할 이름 |
| -------- | ------------ |



### `bool changeDeviceName(QString authKey, QString deviceName, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

현재 로그인한 디바이스의 이름을 변경합니다. firebase와 관계 없기 때문에, remote DB만 변경합니다.

| deviceName | 변경 할 디바이스 이름 |
| ---------- | --------------------- |



### `bool deleteFolder(QString authKey, qint64 serverFolderNo, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

등록한 공유폴더를 삭제 합니다.

g_db->deleteShareDetailTb, g_db->deleteShareTb 를 호출해서, local DB에서도 삭제해줍니다.

| serverFolderNo | remote DB의 등록된 공유폴더 번호 |
| -------------- | -------------------------------- |



### `bool addOrUpdateFolder(QString authKey, SaaringShareFolderInfo &saaringShareFolder, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

공유폴더 정보를 추가 또는 변경 합니다.

saaringShareFolder.serverFolderNo = 0 이면 추가 하고, 0 이상의 값이면 변경합니다.

| saaringShareFolder | 추가 또는 변경 하려는 공유폴더 정보 |
| ------------------ | ----------------------------------- |



### `bool listFolderAll(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<saaring::SaaringShareFolderInfo> &listFolder);`

내 전체 공유폴더 목록을 받아옵니다.

| listFolder | 공유폴더 목록 |
| ---------- | ------------- |



### `bool checkFolderPassword(QString authKey, qint64 serverFolderNo, QString password, QString &resultCode, QString &resultMessage, QJsonObject &resultData);`

친구의 공유폴더에 접속하려고 할때, 해당 공유폴더에 비밀번호가 설정 되어 있으면, 비밀번호를 입력받아서 먼저 확인 받습니다.

| serverFolderNo | 비빌번호를 확인 하려는 공유폴더 번호 |
| -------------- | ------------------------------------ |
| password       | 입력받은 비밀번호                    |
| resultCode     | 100 = 성공 = 비밀번호 확인 완료      |



### `bool addPhoneAddress(QString authKey, QStringList listPhoneNumber, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<saaring::SaaringPhoneAddressInfo> &listSaaringPhoneAddress);`

핸드폰에 저장되어 있는 전화번호 목록을 remote DB에 등록해서, 이중 샤링 회원으로 등록되어 있는 목록을 찾아서, 친구 요청을 하기 위한 기능입니다.

매번 모든 목록을 전송해서 확인하면 시간도 오래 걸리고, 서버에 부하가 가기 때문에,  서버에 등록했던 시점을 저장해놨다가, 다음에 호출할 때에는 이후에 update된 전화번호 목록만 보내도록 합니다.

전화번호를 remote DB에 등록하고, 회원여부 매칭 하고, 내 친구로 등록되어 있는지 여부를 체크하고, 친구 차단 여부까지 체크해서, 회원 목록에 대해서만 '친구, 비친구 목록'을 구분해서 받아옵니다. 차단 된 친구는 목록에서 제외합니다.

| listPhoneNumber         | E164 format 전화번호 (+821053776913) 목록.                   |
| ----------------------- | ------------------------------------------------------------ |
| listSaaringPhoneAddress | 내 핸드폰에 저장된 전화번호 목록 중, 샤링 회원 목록을 내려보내준다. friendYN == "N" 인 회원을 대상으로 친구요청 하면 됩니다. |

```c++
struct SaaringPhoneAddressInfo
{
    qint64                  userNo;

    QString                 phoneNumber;
    int                     countryCode;
    qint64                  nationalNumber;

    qint64                  friendUserNo;
    QString                 friendYN;
    QString                 blockYN;

    qint64                  matchingdt;
    qint64                  frienddt;
    qint64                  regdt;

    QString                 userName;
    QString                 email;
    QString                 profilePic;
    QString                 message;
};

```



### `bool listPhoneAddress(QString authKey, QString &resultCode, QString &resultMessage, QJsonObject &resultData, QList<saaring::SaaringPhoneAddressInfo> &listSaaringPhoneAddress);`

핸드폰에 저장되어 있는 전화번호 목록을 보내지 않고, 이미 등록되어 있는 전화번호 기준, 샤링 회원으로 등록되어 있는 목록을 찾아서, 친구 요청을 하기 위한 기능입니다.

호출 할때 마다 회원여부, 친구여부, 차단 여부를 체그 해서 읽어옵니다.

| listSaaringPhoneAddress | 내 핸드폰에 저장된 전화번호 목록 중, 샤링 회원 목록을 내려보내준다. friendYN == "N" 인 회원을 대상으로 친구요청 하면 됩니다. |
| ----------------------- | ------------------------------------------------------------ |

