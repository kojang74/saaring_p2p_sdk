# Share Folder Callback



## Summary

P2p 모듈은 비동기적으로 처리 됩니다.  P2p 접속 및 P2p 요청에 대한 회신을 모두 callback 으로 비동기적으로 회신 받아서 처리 합니다.

샤링 p2p 엔진에서 SaaringService -> SaaringWrapper -> SaaringSwiftContext 로 호출되며 callback 이 이뤄집니다.



```swift
enum P2pCallbackCommand : Int {
    case PCC_SIGNALING_CONNECTED                                 = 10000
    case PCC_SIGNALING_CLOSED                                    = 10001
    case PCC_SIGNALING_LOGIN_FAILED                              = 10002
    
    case PCC_SIGNALING_NOTIFY_DEVICE_LIVEYN                      = 11000
    case PCC_SIGNALING_NOTIFY_REQUEST_FRIEND                     = 11001
    case PCC_SIGNALING_NOTIFY_ADD_FRIEND                         = 11002
    
    case PCC_P2P_PORTMAP_FINISHED                                = 20000
    case PCC_P2P_LOGIN_FINISHED                                  = 20001
    case PCC_P2P_WAKEUP_FINISHED                                 = 20002
    
    case PCC_P2P_FINISH_DOWNLOAD                                 = 21000
    case PCC_P2P_FINISH_UPLOAD                                   = 21001
    case PCC_P2P_PROGRESS_DOWNLOAD                               = 21002
    case PCC_P2P_PROGRESS_UPLOAD                                 = 21003
    case PCC_P2P_ABORT_DOWNLOAD                                  = 21004
    case PCC_P2P_ABORT_UPLOAD                                    = 21005
    case PCC_P2P_FINISH_DOWNLOAD_OTHER                           = 21006
    case PCC_P2P_FINISH_UPLOAD_OTHER                             = 21007
    case PCC_P2P_PROGRESS_DOWNLOAD_OTHER                         = 21008
    case PCC_P2P_PROGRESS_UPLOAD_OTHER                           = 21009
    
    case PCC_P2P_PROCESSED_REQUEST                               = 22000
    case PCC_P2P_PROCESSED_RESPONSE                              = 22001
    case PCC_P2P_PROCESS_FOLDER_LIST                             = 22002
    case PCC_P2P_PROCESS_FOLDER_DETAIL                           = 22003
    case PCC_P2P_PROCESS_FOLDER_DETAIL_FINISHED                  = 22004
    case PCC_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE                 = 22005
    case PCC_P2P_PROCESS_FILE_UPLOAD_QUERY                       = 22006
    case PCC_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY   = 22007
    case PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO             = 22008
    
    case PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY                  = 30000
    case PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                   = 30001
    case PCC_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY                    = 30002
    case PCC_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                     = 30003
    
    case PCC_P2P_NOTIFY_CONNECTION_CLOSED                        = 40000
    case PCC_P2P_NOTIFY_START_NEXT_TRANSFER                      = 40001
    case PCC_P2P_NOTIFY_STATUS_MESSAGE                           = 40002
};
```



## Callback



### `PCC_P2P_PORTMAP_FINISHED`

```swift
func onPortmapFinished()
```

P2P portmap 초기화가 끝났음을 callback으로 알림을 받습니다. portmap 초기화가 끝나야, TCP p2p 통신을위한 IP/port가 정해져서 통신을 할 수 있습니다.

자동 재접속을 시도하고, Local Relay 서버를 실행시킵니다.



### `PCC_P2P_LOGIN_FINISHED`

```swift
func onP2pLoginFinish(friendDeviceNo:Int64, toRemoteShareNo:Int64)
```

P2p 접속 후, p2p 로그인 까지 성공 했습니다. 

자동 재전송 여부에 따라, 자동 재전송 합니다. 파일보내기 초대링크 실행 여부에 따라, 이어서 파일 다운로드를 진행한다.

| friendDeviceNo  | Int64 | 접속 된 deviceNo                             |
| --------------- | ----- | -------------------------------------------- |
| toRemoteShareNo | Int64 | 폴더로 바로 접속한 경우, 해당 폴더의 shareNo |



### `PCC_P2P_WAKEUP_FINISHED`

```swift
func onP2pWakeUpFinish(isSuccess:Bool)
```

P2p 접속을 위해, 모바일 단말기를 깨우기 위해 Wake-up 요청을 했고, Wake-up에 대한 결과를 받는 callback 입니다.

실패한 경우에는 현재 진행중인 p2p 접속을 중단하고, 성공한 경우에는 이어서 p2p 접속이 진행중이기 때문에 접속 문구 등을 변경해줍니다.

| isSuccess | bool | 0 = false = 실패, 그밖의 값 = true = 성공 |
| --------- | ---- | ----------------------------------------- |



### `PCC_P2P_FINISH_DOWNLOAD`

```swift
func onP2pFinishDownload(transNo:Int64, fileSize:Int64, receivedSize:Int64)
```

다운로드 완료 되었습니다. 전체 파일을 모두 다운로드 했을 수 도 있고. 중지 됐을 수 도 있습니다.

filesize == receivedSize 이면 전송 상태는 SEND_STATUS_DOWNLOAD_FINISHED. 서로 다르면 SEND_STATUS_DOWNLOAD_STOPPED

다운로드 완료인 경우 다음 다운로드를 진행합니다.

| transNo      | Int64 | 전송 번호                    |
| ------------ | ----- | ---------------------------- |
| fileSize     | Int64 | 파일 사이즈.                 |
| receivedSize | Int64 | 다운로드 전송 완료된 사이즈. |



전송상태는 다음의 값으로 관리됩니다.

```c++
enum class SaaringTransferStatusType {
    NONE                    = 0,                // 아무것도 진행하지 않는 상태
    DOWNLOAD_WAITING        = 1,                // 전송 대기
    DOWNLOAD_STAND_BY       = 2,                // 순번 대기
    DOWNLOADING             = 3,                // 전송중(다운로드 중, 업로드 중)
    DOWNLOAD_STOPPED        = 10,               // 중단(정지, 일시 정지)
    DOWNLOAD_FAILED         = 11,               // 실패, 에러
    DOWNLOAD_FINISHED       = 99,               // 전송 완료
    UPLOAD_WAITING          = 101,              // 전송 대기
    UPLOAD_STAND_BY         = 102,              // 순번 대기
    UPLOADING               = 103,              // 전송중(다운로드 중, 업로드 중)
    UPLOAD_STOPPED          = 110,              // 중단(정지, 일시 정지)
    UPLOAD_FAILED           = 111,              // 실패, 에러
    UPLOAD_FINISHED         = 199,              // 전송 완료
};
```



### `PCC_P2P_FINISH_UPLOAD`

```swift
func onP2pFinishUpload(transNo:Int64, fileSize:Int64, sentSize:Int64)
```

업로드 완료 되었습니다. 전체 파일을 모두 업로드 했을 수 도 있고. 중지 됐을 수 도 있습니다.

filesize == receivedSize 이면 전송 상태는 UPLOAD_FINISHED. 서로 다르면 UPLOAD_STOPPED

업로드 완료인 경우 다음 업로드를 진행합니다.

| transNo  | Int64 | 전송 번호                  |
| -------- | ----- | -------------------------- |
| fileSize | Int64 | 파일 사이즈.               |
| sentSize | Int64 | 업로드 전송 완료된 사이즈. |



### `PCC_P2P_PROGRESS_DOWNLOAD`

```swift
func onP2pProgressDownload(transNo:Int64, fileSize:Int64, receivedSize:Int64)
```

다운로드 중입니다. 다운로드 상태를 progressbar 로 표시합니다.

| transNo      | Int64 | 전송 번호                    |
| ------------ | ----- | ---------------------------- |
| fileSize     | Int64 | 파일 사이즈.                 |
| receivedSize | Int64 | 다운로드 전송 완료된 사이즈. |



### `PCC_P2P_PROGRESS_UPLOAD`

```swift
func onP2pProgressUpload(transNo:Int64, fileSize:Int64, sentSize:Int64)
```

업로드 중입니다. 업로드 상태를 progressbar 로 표시합니다.

| transNo  | Int64 | 전송 번호                  |
| -------- | ----- | -------------------------- |
| fileSize | Int64 | 파일 사이즈.               |
| sentSize | Int64 | 업로드 전송 완료된 사이즈. |



### `PCC_P2P_ABORT_DOWNLOAD`

```swift
func onP2pAbortDownload(transNo:Int64)
```

다운로드를 중단 했습니다. 전송 목록에서 다운로드 중단 처리 합니다.

| transNo | Int64 | 전송 번호 |
| ------- | ----- | --------- |



### `PCC_P2P_ABORT_UPLOAD`

```swift
func onP2pAbortUpload(transNo:Int64)
```

업로드를 중단 했습니다. 전송 목록에서 업로드 중단 처리 합니다.

| transNo | Int64 | 전송 번호 |
| ------- | ----- | --------- |



### `PCC_P2P_FINISH_DOWNLOAD_OTHER`

```swift
func onP2pFinishDownloadOther(jobId:Int64, remoteDeviceNo:Int64)
```

상대방이 파일을 다운로드 해갔습니다. 전송목록에서 제거 합니다.

| jobId | Int64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ----- | ------ | ------ |
| remoteDeviceNo | Int64 | 다운로드 완료한 디바이스 번호 |



### `PCC_P2P_FINISH_UPLOAD_OTHER`

```swift
func onP2pFinishUploadOther(jobId:Int64, remoteDeviceNo:Int64)
```

상대방이 파일을 다운로드 해갔습니다. 전송목록에서 제거 합니다.

| jobId | Int64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ----- | ------ | ------ |
| remoteDeviceNo | Int64 | 업로드 완료한 디바이스 번호 |



### `PCC_P2P_PROGRESS_DOWNLOAD_OTHER`

```swift
func onP2pProgressDownloadOther(jobId:Int64, remoteDeviceNo:Int64, fileSize:Int64, currSize:Int64,
                                filePath:String, serverName:String, deviceName:String)
```

상대방이 파일을 다운로드 중이다. 진행상태를 업데이트 해준다. 전송목록에 없으면 추가해준다.

| jobId      | Int64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ---------- | ------ | ------------------------------------------------------------ |
| remoteDeviceNo | Int64 | 업로드 완료한 디바이스 번호 |
| fileSize   | Int64 | 전체 파일 사이즈                                             |
| currSize   | Int64 | 현재까지 전송된 파일 사이즈                                  |
| filePath   | String | 전송목록에 표시할 local file path                            |
| serverName | String | 다운로드 해가는 사용자 이름. 사용자이름을 서버처럼 사용한다. |
| deviceName | String | 다운로드 해가는 디바이스 이름                                |



### `PCC_P2P_PROGRESS_UPLOAD_OTHER`

```sw
func onP2pProgressUploadOther(jobId:Int64, remoteDeviceNo:Int64, fileSize:Int64, currSize:Int64,
                              filePath:String, serverName:String, deviceName:String)
```

상대방이 파일을 업로드 중이다. 진행상태를 업데이트 해준다. 전송목록에 없으면 추가해준다.

| jobId      | Int64 | job ID.<br />transNo 를 만들수 없어, transNo 대신 jobId 를 transNo 로 사용한다. |
| ---------- | ------ | ------------------------------------------------------------ |
| remoteDeviceNo | Int64 | 업로드 완료한 디바이스 번호 |
| fileSize   | Int64 | 전체 파일 사이즈                                             |
| currSize   | Int64 | 현재까지 전송된 파일 사이즈                                  |
| filePath   | String | 전송목록에 표시할 local file path                            |
| serverName | String | 업로드 하는 사용자 이름. 사용자이름을 서버처럼 사용한다.     |
| deviceName | String | 업로드 하는 디바이스 이름                                    |



### `PCC_P2P_PROCESSED_REQUEST`

```swift
func onProgressedRequest(processType:Int, jobId:Int64, remoteDeviceNo:Int64, resultCode:Int)
```

A가 B에게 request를 보냈을때, B는 request를 받아서 다시 A에게 회신을 한 후, B는 PCC_P2P_PROCESSED_REQUEST callback 을 받는다.

saaring::SaaringPacketProcessType P2P 요청에 대해, 내가 답변처리했음에 대한 결과를 callback 받는다.
성공인 경우에는 별로 할것은 없고, 실패인 경우에는 실패 처리를 위해서 사용한다.

예를 들어, 다운로드 요청을 한 다음에, 다운로드 대기 상태에 있을텐데, p2p 요청 자체를 실패 하면, 다운로드 대기 상태에 있는것을, 정지 상태로 변경한다. 업로드도 마찬가지다. 폴더 목록을 요청한 경우에는, wait dialog를 없앤다.



| processType    | int   |
| -------------- | ----- |
| jobId          | Int64 |
| remoteDeviceNo | Int64 |
| resultCode     | int   |



SaaringPacketProcessType은 saaring.h 에 다음과 같이 정의되어 있습니다.

```c++
enum SaaringPacketProcessType {
    PROCESSTYPE_REQ_PING                                            = 0,
    PROCESSTYPE_REQ_LOGIN                                           = 1,
    PROCESSTYPE_REQ_CHAT                                            = 2,
    PROCESSTYPE_REQ_FOLDER_LIST                                     = 3,
    PROCESSTYPE_REQ_FOLDER_DETAIL                                   = 4,
    PROCESSTYPE_REQ_FOLDER_DETAIL_RECURSIVE                         = 5,
    PROCESSTYPE_REQ_FILE_DOWNLOAD                                   = 6,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_STOP                              = 7,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_CANCEL                            = 8,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_COMPLETE                          = 9,
    PROCESSTYPE_REQ_FILE_UPLOAD_QUERY                               = 10,
    PROCESSTYPE_REQ_FILE_UPLOAD                                     = 11,
    PROCESSTYPE_REQ_FILE_UPLOAD_STOP                                = 12,
    PROCESSTYPE_REQ_FILE_UPLOAD_CANCEL                              = 13,
    PROCESSTYPE_REQ_FILE_UPLOAD_COMPLETE                            = 14,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 15,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 16,
    PROCESSTYPE_REQ_SAVE_SEND_FILES_INVITE_INFO                     = 17,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_CHECK                            = 18,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_STAND_BY                         = 19,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_RELEASE                          = 20,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_CHECK                              = 21,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_STAND_BY                           = 22,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_RELEASE                            = 23,

    PROCESSTYPE_RES_PING                                            = 10000,
    PROCESSTYPE_RES_LOGIN                                           = 10001,
    PROCESSTYPE_RES_CHAT                                            = 10002,
    PROCESSTYPE_RES_FOLDER_LIST                                     = 10003,
    PROCESSTYPE_RES_FOLDER_DETAIL                                   = 10004,
    PROCESSTYPE_RES_FOLDER_DETAIL_RECURSIVE                         = 10005,
    PROCESSTYPE_RES_FILE_DOWNLOAD                                   = 10006,
    PROCESSTYPE_RES_FILE_DOWNLOAD_STOP                              = 10007,
    PROCESSTYPE_RES_FILE_DOWNLOAD_CANCEL                            = 10008,
    PROCESSTYPE_RES_FILE_DOWNLOAD_COMPLETE                          = 10009,
    PROCESSTYPE_RES_FILE_UPLOAD_QUERY                               = 10010,
    PROCESSTYPE_RES_FILE_UPLOAD                                     = 10011,
    PROCESSTYPE_RES_FILE_UPLOAD_STOP                                = 10012,
    PROCESSTYPE_RES_FILE_UPLOAD_CANCEL                              = 10013,
    PROCESSTYPE_RES_FILE_UPLOAD_COMPLETE                            = 10014,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 10015,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 10016,
    PROCESSTYPE_RES_SAVE_SEND_FILES_INVITE_INFO                     = 10017,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_CHECK                            = 10018,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_STAND_BY                         = 10019,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_RELEASE                          = 10020,
    PROCESSTYPE_RES_UPLOAD_QUEUE_CHECK                              = 10021,
    PROCESSTYPE_RES_UPLOAD_QUEUE_STAND_BY                           = 10022,
    PROCESSTYPE_RES_UPLOAD_QUEUE_RELEASE                            = 10023
};

```



### `PCC_P2P_PROCESSED_RESPONSE`

```swift
func onProgressedResponse(processType:Int, jobId:Int64, remoteDeviceNo:Int64, resultCode:Int)
```

B가 A에서 response를 보냈을때, A는 response를 받아서 정상적으로 처리를 했으면, A는 PCC_P2P_PROCESSED_RESPONSE callback을 받는다.

saaring::SaaringPacketProcessType P2P 요청에 대해, 상대방 peer의 답변을 처리 했음을 callback으로 받는다. 성공인 경우에는 별 문제 없지만, 실패인 경우에는 실패 처리를 위해서 사용한다.

예를 들어, 다운로드 요청을 한 다음에, 다운로드 대기 상태에 있을텐데, p2p 요청 자체를 실패 하면, 다운로드 대기 상태에 있는것을, 정지 상태로 변경한다. 업로드도 마찬가지다. 폴더 목록을 요청한 경우에는, wait dialog를 없앤다.

| processType    | int   |
| -------------- | ----- |
| jobId          | Int64 |
| remoteDeviceNo | Int64 |
| resultCode     | int   |



### `PCC_P2P_PROCESS_FOLDER_LIST`

```swift
func onP2pProcessResponseFolderList(strJson:String)
```

특정 디바이스의 공유폴더 목록을 받아서 보여줍니다. 현재 특정 디바이스에 대한 공유폴더 목록을 요청하는 곳은 없기 때문에, 사용되지 않습니다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `PCC_P2P_PROCESS_FOLDER_DETAIL`

```swift
func onP2pProcessResponseFolderDetail(strJson:String)
```

특정 공유폴더의 파일, 폴더 내용을 보여줍니다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `PCC_P2P_PROCESS_FOLDER_DETAIL_FINISHED`

```swift
func onP2pProcessResponseFolderDetailFinished()
```

PCC_P2P_PROCESS_FOLDER_DETAIL 처리에 이어서, 바로 호출 된다.

데이터 세팅과 UI 처리를 분리하기 위해서 두번 호출한다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `PCC_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE`

```swift
func onP2pProcessResponseFolderDetailRecursive(strJson:String)
```

해당 폴더의 하위폴더까지 포함된 정보를 전송 받았다.

폴더 다운로드 할 때 사용되며,  해당 폴더의 파일.하위폴더 정보를 받아 다운로드 처리한다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `PCC_P2P_PROCESS_FILE_UPLOAD_QUERY`

```swift
func onP2pProcessResponseFileUploadQuery(transNo:Int64, fileStartPosition:Int64)
```

File Upload Qeury 에 대한 회신을 받았다.

요청이 성공적이면,  transNo로 g_db->readMyTransTbByTransNo 호출해서 전송정보를 받아서, g_p2p->doP2pUpload 호출해서, 업로드 진행한다.


| transNo           | Int64 | 전송 번호        |
| ----------------- | ----- | ---------------- |
| fileStartPosition | Int64 | 업로드 시작 위치 |



### `PCC_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY`

```swift
func onP2pProcessResponseFileDownloadInviteSendFilesQuery(strJson:String)
```

File Invite Send Files Qeury 에 대한 회신을 받았다.
다운로드 받을수 있는 정보가 json 에 모두 포함되어 있기 때문에, 해당 정보로 전송목록에 추가 할 수 있는 정보를 구성해서 추가하고, 다운로드 진행한다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO`

```swift
func onP2pProcessResponseSaveSendFilesInviteInfo(strJson:String)
```

내 다른 디바이스의 공유폴더에 있는 파일로, 파일 보내기 링크 만들기 요청에 대한 회신을 받았다.
g_web->completeInviteFriend 호출해서 초대링크 만들기를 완료 하고, 완료 메시지를 보여준다.

| strJson | String | 결과를 json 전체로 받는다. |
| ------- | ------ | -------------------------- |



### `PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY`

```swift
func onP2pNotifyDownloadQueueStandBy(remoteDeviceNo:Int64, downloadingCount:Int, downloadStandByCount:Int, myTurn:Int)
```

다운로드 대기 상태가 됐다.

전송을 중단하고, 대기 정보를 보여준다.

| toDeviceNo           | Int64 | 대기상태가 된 디바이스 번호 |
| -------------------- | ----- | --------------------------- |
| downloadingCount     | int   | 현재 다운로드 수            |
| downloadStandByCount | int   | 전체 대기자 수              |
| myTurn               | int   | 대기자 중 내 순번           |



### `PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE`

```swift
func onP2pNotifyDownloadQueueRelease(remoteDeviceNo:Int64, downloadingCount:Int, downloadStandByCount:Int, myTurn:Int)
```

다운로드 대기 상태에서, 다운로드 가능한 순번이 됐다.

다운로드 진행한다.

| toDeviceNo           | Int64 | 대기상태가 된 디바이스 번호 |
| -------------------- | ----- | --------------------------- |
| downloadingCount     | int   | 현재 다운로드 수            |
| downloadStandByCount | int   | 전체 대기자 수              |
| myTurn               | int   | 대기자 중 내 순번           |



### `PCC_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY`

```swift
func onP2pNotifyUploadQueueStandBy(remoteDeviceNo:Int64, uploadingCount:Int, uploadStandByCount:Int, myTurn:Int)
```

업로드 대기 상태가 됐다.

전송을 중단하고, 대기 정보를 보여준다.

| toDeviceNo         | Int64 | 대기상태가 된 디바이스 번호 |
| ------------------ | ----- | --------------------------- |
| uploadingCount     | int   | 현재 업로드 수              |
| uploadStandByCount | int   | 전체 대기자 수              |
| myTurn             | int   | 대기자 중 내 순번           |



### `PCC_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE`

```swift
func onP2pNotifyUploadQueueRelease(remoteDeviceNo:Int64, uploadingCount:Int, uploadStandByCount:Int, myTurn:Int)
```

업로드 대기 상태에서, 업로드 가능한 순번이 됐다.

업로드 진행한다.

| toDeviceNo         | Int64 | 대기상태가 된 디바이스 번호 |
| ------------------ | ----- | --------------------------- |
| uploadingCount     | int   | 현재 업로드 수              |
| uploadStandByCount | int   | 전체 대기자 수              |
| myTurn             | int   | 대기자 중 내 순번           |



### `PCC_P2P_NOTIFY_CONNECTION_CLOSED`

```swift
func onTransmitListStop(remoteDeviceNo:Int64)
```

p2p 접속이 끊어졌습니다. 해당 디바이스의 전송을 중지 합니다.

| remoteDeviceNo | Int64 | p2p 접속이 끊어진 디바이스 번호 |
| -------------- | ----- | ------------------------------- |



### `PCC_P2P_NOTIFY_START_NEXT_TRANSFER`

```swift
func startNextTransfer(remoteDeviceNo:Int64)
```

기존 전송이 끝났거나, 새롭게 p2p 접속이 되는등, 새로 전송 진행 가능한 상태가 됐습니다. 전송목록의 다음 건을 전송 진행합니다.

| remoteDeviceNo | Int64 | p2p 접속이 끊어진 디바이스 번호 |
| -------------- | ----- | ------------------------------- |



### `PCC_P2P_NOTIFY_STATUS_MESSAGE`

```swift
func onP2pNotifyStatusMessage(text:String)
```

P2P 엔진이 P2P 접속.전송상태에 대한 메시지를 받았습니다. 선택적으로 화면 또는 로그로 메시지 남기면 됩니다.

| text | String | 메시지 |
| ---- | ------ | ------ |

