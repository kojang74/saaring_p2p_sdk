# Share Folder Callback



## Summary

공유폴더안의 내용 변경 알림, 공유폴더 인터페이스 호출에 대한 처리 결과 알림을 받아 처리 합니다.






## Callback



### `SDCC_SHARE_DIR_REFRESH`

```swift
enum ShareDirCallbackCommand : Int {
    case SDCC_SHARE_DIR_REFRESH                                  = 1
};
```

```swift
func onShareDirRefresh(path:String)
```

공유폴더의 내용이 업데이트 됐음을 알려줍니다. 특별히 처리할것은 없습니다.

| path | String | 변경된 폴더의 local full path |
| ---- | ------ | ----------------------------- |

