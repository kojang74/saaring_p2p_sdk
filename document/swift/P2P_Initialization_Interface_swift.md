# P2P Initialization Interface



## Summary

P2P 엔진(라이브러리) 를 초기화하기 위한 루틴이다.

P2P 엔진을 먼저 초기화 한 후에, Signaling.Relay.Rendezvous Server Interface와 P2P 전체 Interface를 사용할 수 있다.

SaaringService에서 SaaringNative.java의 JNI 함수로 제공 됩니다.



## Functions



### `void initP2p();`

P2p 엔진을 초기화 합니다.

앱 실행시 호출 합니다.



### `void closeP2p();`

P2p 엔진 사용을 종료합니다. 내부적으로는 모든 p2p 접속을 끊고, p2p 객체를 제거 합니다.

앱 종료시 호출 합니다.




### `bool isP2p();`

P2p 엔진 초기화 여부를 확인합니다.

initP2p, closeP2p 호출 하기전에 확인합니다.




### `SaaringServerInfo getServerInfo();`

saaring 에서 운영하고 있는, 다음의 각종 서버 정보를 얻어옵니다.

rendezvousIp, rendezvousPort, relayIp, relayPort도 모두 saaring에서 운영하고 있는 Relay Server의 IP/port 정보 입니다.

```java
    String          signalingIp                  = "";
    int             signalingPort;
    String          rendezvousIp                 = "";
    int             rendezvousPort;
    String          relayIp                      = "";
    int             relayPort;
    String          runningVersion               = "";
    String          lastVersion                  = "";
```

