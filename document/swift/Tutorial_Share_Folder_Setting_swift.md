# Tutorial - Share Folder Setting



## Summary

공유폴더 추가, 삭제, 변경 방법을 설명합니다.

폴더 정보를 실시간으로 파일시스템을 검사해서 사용하게 되면, 시스템이 느려질 수 있고, 프로그램이 멈출수 있습니다. 따라서, 폴더 정보는 별도의 Thread가 파일시스템을 검색해서 ''임시DB'에 저장합니다. 모든 파일 시스템 정보가 '임시DB'에 저장되어 수집되면, '운영DB'에 복제(백업)합니다.

모든 폴더 정보는 매번 파일시스템에서 정보를 수집해서 전송하지 않고, '운영DB'에서 select 해서 전송해주기 때문에 빠르게 작동합니다. 대신 파일시스템과 '운영DB' 정보 sync 를 위해, 공유폴더는 Monitor를 생성해서 변경 여부를 체크 합니다.

Qt에는 QFileSystemWatcher 를 제공하지만 성능이 썩 좋지 않습니다. 따라서, Win32 시스템에서는, FindFirstChangeNotification를 통해서 모니터링하고, 그밖의 시스템에서는 QFileSystemWatcher를 사용합니다.

모니터를 통해, 공유폴더에 변경이 생기면, SDCC_SHARE_DIR_REFRESH callback을 받아, 변경된 폴더의 정보를 다시 수집해서 '임시DB'에 저장하고, 정보수집이 완료 되면, '운영DB'에 복제 합니다.





## Functions



### `초기화 `

로그인에 성공하면, SaaringSwiftContext.startThreadShareFolder, SaaringSwiftContext.shareFolderInit, SaaringSwiftContext.startFileMonitor를 호출해서 공유폴더 관련 스레드를 실행해서 초기화 합니다.



### `종료 `

SaaringSwiftContext.stopThreadShareFolder, SaaringSwiftContext.stopFileMonitor를 호출해서 공유폴더 관련 스레드를 실행 종료합니다.



### `callback `

callback 함수는 SaaringSwiftContext.onShareDirRefresh를 통해서 호출됩니다.

```
public static void onShareDirRefresh(String path)
```



### `DB - 공유폴더`

공유폴더는 다음과 같은 저장됩니다. 공유폴더의 local DB의 PK는 shareNo 이고, remote DB의 PK는 serverFolderNo 입니다.
권한은 다운로드.업로드 에 대해, '나 or 모든 친구 or 특정친구'에게 권한을 줄 수 있습니다.
공유폴더에 비밀번호를 설정하면, 접속할 때 비밀번호를 물어봅니다.

```java
    long                    shareNo;
    long                    deviceNo;
    String                  shareFolderPath                     = "";
    String                  shareName                           = "";

    // 서버에서발행하는 folderNo
    long                    serverFolderNo;
    int                     fileCount;
    long                    fileSize;

    int                     downKind;
    List<Long>              listDownFriendUserNo		= new ArrayList();;
    List<String>            listDownFriendUserName		= new ArrayList();;
    int                     upKind;
    List<Long>              listUpFriendUserNo			= new ArrayList();;
    List<String>            listUpFriendUserName		= new ArrayList();;

    String                  downYN                              = "";
    String                  upYN                                = "";
    String                  password                            = "";
    String                  passwordYN                          = "";

```



### `DB - 공유폴더내 파일`

공유폴더내 폴더.파일 정보는 다음과 같이 별도의 DB에 저장됩니다. local에만 저장되고, local DB의 PK는 shareDetailNo 입니다.

```java
    long                    shareDetailNo;
    long                    deviceNo;
    long                    shareNo;

    String                  fullPathName                = "";
    String                  shortPathName               = "";

    String                  fileName                    = "";
    String                  fileExtension               = "";
    long                    fileSize;

    String                  updateTime                  = "";
    String                  folderYN                    = "";

```



### `Backup `

Local DB는 '임시DB'와 '운영DB' 2개로 운영됩니다.

파일시스템 정보를 수집해서 '임시DB'에 저장하지만, 수시로 '운영DB'에 복제 하진 않습니다. 어느정도 시간이 지난 경우에 '임시DB'에서 '운영DB'로 복제를 하는데, 실컷 '임시DB'에 파일시스템을 수집 완료 해놨는데, 마침 프로그램이 종료되면서 '운영DB'에 복제를 해놓지 않으면 안되겠죠.

따라서, 프로그램 종료 할때 다음과 같이 DB 백업이 필요한지 확인 해서, DB를 백업.복제 합니다.

```java
        if(SaaringSwiftContext.isDbBackup() == true)
        {
            SaaringSwiftContext.backupMemoryShareDetailTb();
        }

```

