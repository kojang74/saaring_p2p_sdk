# Library Initialization



## Summary

샤링 P2P Swift 라이브러리는 내부적으로 Qt C++ 라이브러리를 기반으로 개발 되었습니다. Qt 5.15.2 버전을 사용하고 있습니다.

Qt C++로 개발된, 샤링 p2p 라이브러리를 Swift로 사용하기 위해, 우선 C++을 Objective-C 로 Wrapping 했고, Objective-C를 다시 Swift로 Wrapping 했습니다.

따라서, 샤링 P2p Swift 라이브러리를 사용하기 위해서는, XCode Project 설정에서 다음과 같이 Qt include, library를 설정해줘야 합니다.



Qt 5.15.2의 include 폴더와, 샤링 P2p 라이브러리의 include 폴더를 포함해야 합니다.

![](./images/setup_001.png)

Qt 5.15.2의 library 폴더와, 샤링 P2p 라이브러리의 library 폴더를 포함해야 합니다.

![](./images/setup_002.png)

Qt 5.15.2의 library의 package와, 샤링 P2p 라이브러리의 package를 포함해야 합니다.

![](./images/setup_003.png)



다음 git의 예제 프로그램을 참고하세요.





## Modules



### SaaringService

샤링 C++ 라이브러리를 초기화 합니다.

생성된 샤링 C++ 라이브러리 객체를 통해, C++ 라이브러리의 함수들을 모두 사용할수 있습니다.

라이브러리를 초기화 하면서, C++ callback 함수도 세팅 합니다. C++ callback 함수에서는 받은 callback을, Objective-C의 Block을 이용해서 Objective-C의 callback 함수를 호출하고, Objective-C의 callback 함수에서는 다시 @selector 를 이용해서, Swift의 함수를 호출 합니다.



### SaaringWrapper

SaaringService를 생성해서, SaarigService에 정의되어 있는 C++ 라이브러리 객체를 통해 C++ 라이브러리의 함수들을 호출할 수 있도록,  Objective-C 함수로 재정의(Wrapping) 해놨습니다.

Objective-C의 callback가 정의되어 있어서, SaaringService 생성후 C++ 라이브러리를 초기화 하면서 Block으로 Objective-C callback 함수를 등록합니다. Objective-C의 callback  함수에서는 다시 @selector를 이용해서, Swift의 함수를 호출 합니다.



### SaaringSwiftContext

SaaringWrapper를 생성해서, SaaringWrapper의 SaaringService를 통해 Swift로 샤링 C++ 라이브러리의 함수를 사용할수 있도록 다시한번 재정의(Wrapping) 해놨습니다.

SaaringWrapper에서 callback받아서 호출할 수 있는 Selector 함수가 선언되어 있어, callback 처리를 합니다.

결과적으로 XCode의 Swift 프로젝트에서는 SaaringSwiftContext를 이용해서, 샤링 p2p 라이브러리를 사용하면 됩니다.



### SaaringDomainWrapper

Qt C++ struct를 Objective-C의 Class로 변환 합니다.



### SaaringUtility

Qt의 QDateTime, QString, QList<qint64>, QStringList를 long long, NSString, NSMutableArray로 변환하는 유딜리티 함수를 제공합니다.











 
