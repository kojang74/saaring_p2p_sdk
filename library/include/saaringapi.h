#ifndef SAARINGAPI_H
#define SAARINGAPI_H

#include "api/saaringcommoninterface.h"
#include "api/saaringdbinterface.h"
#include "api/saaringdomain.h"
#include "api/saaringp2pinterface.h"
#include "api/saaringsharefolderinterface.h"
#include "api/saaringwebinterface.h"

namespace saaring {

extern SaaringCommonInterface* g_common;
extern SaaringDbInterface* g_db;
extern SaaringP2pInterface* g_p2p;
extern SaaringShareFolderInterface* g_shareFolder;
extern SaaringWebInterface* g_web;

} // namespace ssaring

#endif // SAARINGAPI_H
