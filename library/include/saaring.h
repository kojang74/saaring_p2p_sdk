﻿#ifndef __SAARING_H__
#define __SAARING_H__

// /////////////////////////////////////////////////////////////////////////////
// for dynamic library
// /////////////////////////////////////////////////////////////////////////////

#include <QtCore/qglobal.h>

#if defined(SAARINGNETDYNAMICLIBRARY_LIBRARY)
#  define SAARINGNETDYNAMICLIBRARYSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SAARINGNETDYNAMICLIBRARYSHARED_EXPORT Q_DECL_IMPORT
#endif

// /////////////////////////////////////////////////////////////////////////////
// for win32 network library
// /////////////////////////////////////////////////////////////////////////////

#ifdef WIN32
// Explicitly define 32-bit and 64-bit numbers
typedef __int16 int16_t;
typedef unsigned __int16 uint16_t;
#endif

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib,"ws2_32.lib") //Winsock Library
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <errno.h>
//#include <cstdlib>
//#include <cstring>
#include <netdb.h>
#include <signal.h>
#include <unistd.h>
#endif
#include <fcntl.h>
//#include <fstream>
//#include <iostream>
//#include <cstring>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <string>
//#include <sstream>
#include <vector>
#include <map>
#include <deque>

using namespace std;

#include <QObject>
#include <QString>
#include <QStringLiteral>

// /////////////////////////////////////////////////////////////////////////////
// transfer status string
// /////////////////////////////////////////////////////////////////////////////

#define TRANSFER_STATUS_STRING_NONE                     QObject::tr("Preparing")            // "대기중";       // 아무것도 진행하지 않는 상태

#define TRANSFER_STATUS_STRING_DOWNLOAD_WAITING         QObject::tr("Download waiting")     // "다운로드 준비중";
#define TRANSFER_STATUS_STRING_DOWNLOAD_STAND_BY        QObject::tr("Download stand by")    // "다운로드 순번 대기중";
#define TRANSFER_STATUS_STRING_DOWNLOADING              QObject::tr("Downloading")          // "다운로드 중";
#define TRANSFER_STATUS_STRING_DOWNLOAD_STOPPED         QObject::tr("Download stopped")     // "다운로드 중지";
#define TRANSFER_STATUS_STRING_DOWNLOAD_FAILED          QObject::tr("Download failed")      // "다운로드 실패";
#define TRANSFER_STATUS_STRING_DOWNLOAD_FINISHED        QObject::tr("Download finished")    // "다운로드 완료";

#define TRANSFER_STATUS_STRING_UPLOAD_WAITING           QObject::tr("Upload waiting")       // "업로드 준비중";
#define TRANSFER_STATUS_STRING_UPLOAD_STAND_BY          QObject::tr("Upload stand by")      // "업로드 순번 대기중";
#define TRANSFER_STATUS_STRING_UPLOADING                QObject::tr("Uploading")            // "업로드 중";
#define TRANSFER_STATUS_STRING_UPLOAD_STOPPED           QObject::tr("Upload stopped")       // "업로드 중지";
#define TRANSFER_STATUS_STRING_UPLOAD_FAILED            QObject::tr("Upload failed")        // "업로드 실패";
#define TRANSFER_STATUS_STRING_UPLOAD_FINISHED          QObject::tr("Upload finished")      // "업로드 완료";

#define TRANSFER_STATUS_STRING_WAITING                  QObject::tr("Waiting")              // "준비중";
#define TRANSFER_STATUS_STRING_STAND_BY                 QObject::tr("Stand by")             // "순번 대기중";
#define TRANSFER_STATUS_STRING_TRANSFERRING             QObject::tr("Transferring")         // "전송중";
#define TRANSFER_STATUS_STRING_STOPPED                  QObject::tr("Stopped")              // "중지";
#define TRANSFER_STATUS_STRING_FAILED                   QObject::tr("Failed")               // "실패";
#define TRANSFER_STATUS_STRING_FINISHED                 QObject::tr("Finished")             // "전송완료";

// p2p 정보
#define MAX_P2P_CONNECTION_TIMEOUT_SECOND                   60
#define MAX_P2P_CONNECTION_TIMEOUT_MILLISECOND              60*1000

// p2p wakeup 정보
#define MAX_P2P_CONNECTION_MOBILE_TIMEOUT_SECOND            60
#define MAX_P2P_CONNECTION_MOBILE_TIMEOUT_MILLISECOND       60*1000

// max portmap wait timeout = 20초
#define MAX_PORTMAP_WAIT_TIMEOUT_SECOND                     20
#define MAX_PORTMAP_WAIT_TIMEOUT_MILLISECOND                20*1000

// relay 정보 update = 12분
#define RELAY_CHECK_TIMEOUT_MILLISECOND                     1000*60*12
// relay 정보 refresh = 12시간
#define RELAY_REFRESH_MILLISECOND                           1000*60*60*12

// 초대링크 유효기간 = 10일
#define INVITATION_LINK_VALIDITY                            10

// /////////////////////////////////////////////////////////////////////////////
// for saaring global
// /////////////////////////////////////////////////////////////////////////////

namespace saaring {

static const QString g_saaringApplicationName               = "Saaring";
static const QString g_saaringOrganizationName              = "Mirinesoft";
static const QString g_saaringOrganizationDomain            = "saaring.com";

//Language
static const QString g_saaringLanguageKorean                = "ko";
static const QString g_saaringLanguageEnglish               = "en";

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

//idType
enum class SaaringIdType {
    NONE                    = -1,               // 아무것도 아닌 상태.
    PASSWORD                = 0,                // email & password
    PHONENUMBER             = 1,                // phone number
    FACEBOOK                = 2,                // facebook 회원
    GOOGLE                  = 3,                // google 회원
    APPLE                   = 4,                // apple 회원
    NAVER                   = 5,                // Naver
    KAKAO                   = 6,                // Kakao
    TWITTER                 = 7,                // twitter
    WECHAT                  = 8                 // wechat
};

//providerId
static const QString PROVIDERID_PASSWORD        = "password";
static const QString PROVIDERID_PHONENUMBER     = "phonenumber";
static const QString PROVIDERID_FACEBOOK        = "facebook.com";
static const QString PROVIDERID_GOOGLE          = "google.com";
static const QString PROVIDERID_APPLE           = "apple.com";
static const QString PROVIDERID_NAVER           = "naver.com";
static const QString PROVIDERID_KAKAO           = "kakao.com";
static const QString PROVIDERID_TWITTER         = "twitter.com";
static const QString PROVIDERID_WECHAT          = "wechat.com";

//deviceType
//0:Unknown, 1:Android, 2:iOS, 3:Web, 4:Windows(Desktop), 5:OSX(Desktop), 6:Linux(Desktop)
enum class SaaringDeviceType {
    SDT_UNKNOWN             = 0,
    SDT_ANDROID             = 1,
    SDT_IOS                 = 2,
    SDT_WEB                 = 3,
    SDT_WINDOWS             = 4,
    SDT_OSX                 = 5,
    SDT_LINUX               = 6
};

// Share folder Type
// 0:My share folder, 1:Friend's share folder
enum class SaaringShareFolderType {
    ME                      = 0,
    FRIEND                  = 1
};

// file type
enum class SaaringFileType {
    VIDEO                   = 0,
    AUDIO                   = 1,
    IMAGE                   = 2,
    DOC                     = 3,
    COMPRESSED              = 4,
    CDIMAGE                 = 5,
    ETC                     = 6
};

// share type
enum class SaaringShareType {
    OPEN_ONLY_ME            = 0,    // 나만
    OPEN_EVERYONE           = 1,    // 모두에게 오픈
    OPEN_ALL_FRIEND         = 2,    // 모든친구에게오픈
    OPEN_SPECIAL_FRIEND     = 3,    // 특정친구에게오픈
    OPEN_GROUP              = 4     // 그룹공유
};

// share type
enum class SaaringRequestStatusType {
    YET                    = 0,    // 미결정
    ACCEPT                 = 1,    // 승인
    REJECT                 = 2     // 거절
};

// transfer status type
// 상태: “전송 대기중” or “전송중 = 다운로드 중 or 업로드 중” or “전송 완료” or “전송 실패” or “전송 중지” 등의 전송 상태
enum class SaaringTransferStatusType {
    NONE                    = 0,                // 아무것도 진행하지 않는 상태
    DOWNLOAD_WAITING        = 1,                // 전송 대기
    DOWNLOAD_STAND_BY       = 2,                // 순번 대기
    DOWNLOADING             = 3,                // 전송중(다운로드 중, 업로드 중)
    DOWNLOAD_STOPPED        = 10,               // 중단(정지, 일시 정지)
    DOWNLOAD_FAILED         = 11,               // 실패, 에러
    DOWNLOAD_FINISHED       = 99,               // 전송 완료
    UPLOAD_WAITING          = 101,              // 전송 대기
    UPLOAD_STAND_BY         = 102,              // 순번 대기
    UPLOADING               = 103,              // 전송중(다운로드 중, 업로드 중)
    UPLOAD_STOPPED          = 110,              // 중단(정지, 일시 정지)
    UPLOAD_FAILED           = 111,              // 실패, 에러
    UPLOAD_FINISHED         = 199,              // 전송 완료
};

// share type
enum class SaaringInviteType {
    JUST_INVITE           = 0,    // 친구 초대
    SHARE_FOLDER_INVITE   = 1,    // 폴더공유 초대
    SEND_FILES            = 2,    // 파일보내기 초대
    ETC                   = 99    // 기타 초대
};

enum class SaaringPacketResultCode {
    SYSTEM_ERROR                 = 0,
    SUCCESS                      = 100,
    PARAMETER_ERROR              = 200,
    PROCESS_FAIL                 = 300,
    NOTHING_DATA                 = 400,
    INVALID_DATA                 = 500,
    SYNC_END                     = 600,
    OAUTHKEY_FAIL                = 1000,
    PASSWORD_FAIL                = 1001,
    JOINSTATUS_FAIL              = 1002,
    EMAIL_CERT_FAIL              = 1100,
    ERROR_SERVER_ERROR           = 90000,
    ERROR_DOUBLE_CONNECTION      = 90001,
    ERROR_LOGIN_FAIL             = 90002,
    ERROR_USER_NOT_CONNECTED     = 90003,
    ERROR_SENDING_               = 90004,
    ERROR_STAND_BY               = 90005
};

enum SaaringPacketProcessType {
    PROCESSTYPE_REQ_PING                                            = 0,
    PROCESSTYPE_REQ_LOGIN                                           = 1,
    PROCESSTYPE_REQ_CHAT                                            = 2,
    PROCESSTYPE_REQ_FOLDER_LIST                                     = 3,
    PROCESSTYPE_REQ_FOLDER_DETAIL                                   = 4,
    PROCESSTYPE_REQ_FOLDER_DETAIL_RECURSIVE                         = 5,
    PROCESSTYPE_REQ_FILE_DOWNLOAD                                   = 6,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_STOP                              = 7,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_CANCEL                            = 8,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_COMPLETE                          = 9,
    PROCESSTYPE_REQ_FILE_UPLOAD_QUERY                               = 10,
    PROCESSTYPE_REQ_FILE_UPLOAD                                     = 11,
    PROCESSTYPE_REQ_FILE_UPLOAD_STOP                                = 12,
    PROCESSTYPE_REQ_FILE_UPLOAD_CANCEL                              = 13,
    PROCESSTYPE_REQ_FILE_UPLOAD_COMPLETE                            = 14,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 15,
    PROCESSTYPE_REQ_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 16,
    PROCESSTYPE_REQ_SAVE_SEND_FILES_INVITE_INFO                     = 17,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_CHECK                            = 18,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_STAND_BY                         = 19,
    PROCESSTYPE_REQ_DOWNLOAD_QUEUE_RELEASE                          = 20,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_CHECK                              = 21,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_STAND_BY                           = 22,
    PROCESSTYPE_REQ_UPLOAD_QUEUE_RELEASE                            = 23,

    PROCESSTYPE_RES_PING                                            = 10000,
    PROCESSTYPE_RES_LOGIN                                           = 10001,
    PROCESSTYPE_RES_CHAT                                            = 10002,
    PROCESSTYPE_RES_FOLDER_LIST                                     = 10003,
    PROCESSTYPE_RES_FOLDER_DETAIL                                   = 10004,
    PROCESSTYPE_RES_FOLDER_DETAIL_RECURSIVE                         = 10005,
    PROCESSTYPE_RES_FILE_DOWNLOAD                                   = 10006,
    PROCESSTYPE_RES_FILE_DOWNLOAD_STOP                              = 10007,
    PROCESSTYPE_RES_FILE_DOWNLOAD_CANCEL                            = 10008,
    PROCESSTYPE_RES_FILE_DOWNLOAD_COMPLETE                          = 10009,
    PROCESSTYPE_RES_FILE_UPLOAD_QUERY                               = 10010,
    PROCESSTYPE_RES_FILE_UPLOAD                                     = 10011,
    PROCESSTYPE_RES_FILE_UPLOAD_STOP                                = 10012,
    PROCESSTYPE_RES_FILE_UPLOAD_CANCEL                              = 10013,
    PROCESSTYPE_RES_FILE_UPLOAD_COMPLETE                            = 10014,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY           = 10015,
    PROCESSTYPE_RES_FILE_DOWNLOAD_INVITE_SEND_FILES                 = 10016,
    PROCESSTYPE_RES_SAVE_SEND_FILES_INVITE_INFO                     = 10017,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_CHECK                            = 10018,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_STAND_BY                         = 10019,
    PROCESSTYPE_RES_DOWNLOAD_QUEUE_RELEASE                          = 10020,
    PROCESSTYPE_RES_UPLOAD_QUEUE_CHECK                              = 10021,
    PROCESSTYPE_RES_UPLOAD_QUEUE_STAND_BY                           = 10022,
    PROCESSTYPE_RES_UPLOAD_QUEUE_RELEASE                            = 10023
};

enum SaaringLoginResult {
    LOGIN_RESULT_SUCCESS        = 0,
    LOGIN_RESULT_FAIL           = 1,
    LOGIN_RESULT_CANCEL         = 2,
    LOGIN_RESULT_GO_JOIN        = 3
};

#define SAARING_STRING_SHORT_DOWNLOAD                 "D"
#define SAARING_STRING_SHORT_UPLOAD                   "U"

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// web service response code & message
// /////////////////////////////////////////////////////////////////////////////

const QString service_resultCode_error                  = "000";
const QString service_resultCode_success                = "100";

const QString service_resultValue_yes                   = "Y";
const QString service_resultValue_no                    = "N";

// 통신상태가 원활하지 않습니다. 안정적인 네트워크 환경에서 실행시켜 주세요.
#define QNETWORK_ERROR_MESSAGE  QObject::tr("Connection error. Please check your network connection and try again.")
// 서버에서 응답이 없습니다. 잠시 후 다시 시도 하세요.
#define QWEBSERVER_ERROR_MESSAGE  QObject::tr("No response from the server. Please try again later.")

const QString service_resultMessage_networkError        = QNETWORK_ERROR_MESSAGE;
const QString service_resultMessage_webServerError      = QWEBSERVER_ERROR_MESSAGE;

// /////////////////////////////////////////////////////////////////////////////
// style sheet
// /////////////////////////////////////////////////////////////////////////////

const QString g_folderSeparator                         = "/";

} // namespace ssaring

#endif // __SAARING_H__
