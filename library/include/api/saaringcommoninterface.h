#ifndef SAARINGCOMMONINTERFACE_H
#define SAARINGCOMMONINTERFACE_H

#include "saaring.h"

#include "api/saaringdomain.h"

#include <QObject>
#include <QLabel>

namespace saaring {

class SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringCommonInterface : public QObject
{
    Q_OBJECT

public:

    SaaringCommonInterface(int type);
    virtual ~SaaringCommonInterface();

    static SaaringCommonInterface* instance;

    virtual QString getSaaringServiceDomain();
    virtual QString getSaaringApiDomain();
    virtual QString getSaaringImageDomain();

    virtual int getScreenWidth();
    virtual int getScreenHeight();

    virtual double calSpeed(QDateTime oldTime, QDateTime currTime, qint64 oldSize, qint64 currSize);
    virtual QString leftTime(QDateTime oldTime, QDateTime currTime, qint64 oldSize, qint64 currSize, qint64 totalSize, double speed);
    virtual QString getSizeWithUnit(qint64 size);

    virtual bool checkEmail(QString email);
    virtual QString getDuplicationFilename(QString filePath, QString &lastLocalFilePath);
    virtual QString numberFormatWithComma(quint64 number, QString separator = ",");
    virtual void clear(QJsonObject &obj);

    virtual int checkUpgrade(QString lastVer, QString runningVer, QString currentVer);

    virtual void showMessageBox(QString str);
    virtual bool confirmMessageBox(QString str);

    virtual void loadImage(QString strImg, QLabel *label);
    virtual QString changeProfileImage(QString authKey, QString path);

    /** for file utility */
    virtual QString getDownloadFolder();
    virtual QString getShareTopFolder();
    virtual QString removeFirstPath(QString path);

    /** for provider id */
    virtual QString getProviderId(SaaringIdType idType);
    virtual QString getProviderName(SaaringIdType idType);
    virtual QString getProviderNameByProviderId(QString providerId);
};

extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringCommonInterface* Common(int type);
extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT void releaseCommon();

} // namespace ssaring

#endif // SAARINGCOMMONINTERFACE_H
