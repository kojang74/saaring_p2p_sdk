#ifndef SAARINGDOMAIN_H
#define SAARINGDOMAIN_H

#include "saaring.h"

#include <QObject>
#include <QDateTime>
#include <QFileInfo>
#include <QDir>

class SetupInfo;
class Member;
class MemberProvider;
class FriendInfo;
class Block;
class Transfer;
class TransferInfo;
class Share;
class ShareDetail;
class ShareFolder;
class BadgeInfo;
class ServerInfo;
class InviteUsersInfo;
class InviteFilesInfo;
class InviteSecretInfo;
class InviteInfo;
class InviteRegisteredInfo;
class PhoneAddressInfo;

namespace saaring {

struct SaaringSetupInfo
{
    QString             idType;                         // 0 email, 1 phone
    QString             email;                          // email
    QString             phoneNumber;                    // phoneNumber
    QString             password;                       // encrypt
    QString             providerId;                     // SNS 연결, providerId
    QString             idToken;                        // SNS 연결, idToken
    QString             accessToken;                    // SNS 연결, accessToken
    QString             uid;                            // SNS 연결, uid. encrypt
    QString             isUseMobileNetwork;             // 3G/4G 모바일 네트웍 사용여부
    QString             isSaveEmail;                    // 로그인시, 아이디 저장여부.       0 unchecked, 1 checkecd
    QString             isAutoLogin;                    // 로그인시, 자동로그인 여부.       0 unchecked, 1 checkecd
    QString             isLastLoginEmail;               // 최종 로그인한 아이디인지 여부     0 nomal, 1 last login (최종 로그인한 ID만 1로 세팅되어 있음)
    QString             isAutoStart;                    // 컴퓨터 부팅시, 자동실행 여부
    QString             isTransferListAutoResend;       // 전송목록에서 자동 재전송 여부
    QString             isTransferListAutoDelete;       // 전송목록에서 전송완료 목록 자동 삭제 여부
    QString             transferListResendMinute;       // 전송목록에서 자동 재전송 체크 시간 간격
    QString             isDisplayTraySendStatus;        // Tray에 전송상태 표시여부
    QString             isContinueReceiveFile;          // 파일 전송시, 이어받기 여부
    QString             isRelayServer;                  // 중계서버로 사용할지 여부(미사용
    int                 relayServerPort;                // 중계서버로 사용될때, 중계서버의 port
    int                 otherDownloadCount;             // 최대 다운로드 허용 수 (나에게 다운로드 해갈 수 있는 peer의 최대 갯수)
    int                 otherDownloadSpeed;             // 최대 다운로드 속도 (나에게 다운로드 해갈 수 있는 peer의 최대 전송 속도)
    int                 myDownloadSpeed;                // 내가 다운로드 받을때의 최대 다운로드 속도 (다운로드 갯수는 제한하지 않는다. 느리면 중단하면 되기 때문에)
    int                 otherUploadCount;               // 최대 업로드 허용 수 (나에게 업로드 할 수 있는 peer의 최대 갯수)
    int                 otherUploadSpeed;               // 최대 업로드 속도 (나에게 업로드 할 수 있는 peer의 최대 전송 속도)
    int                 myUploadSpeed;                  // 내가 업로드 할때의 최대 업로드 속도 (업로드 갯수는 제한하지 않는다. 느리면 중단하면 되기 때문에)
    QString             deviceOpenFriendUserNo;         // 디바이스 공개 수준에서, 특정 친구에게 오픈할경우, 회원번호 리스트 (미사용)
    QString             deviceOpenFriendUserName;       // 디바이스 공개 수준에서, 특정 친구에게 오픈할경우, 이름 리스트 (미사용)
    int                 portmapWanCtrlPort;             // 포트매핑 정보. Control 용
    int                 portmapLanCtrlPort;             // 포트매핑 정보. Control 용
    int                 portmapWanFilePort;             // 포트매핑 정보. Filer 용
    int                 portmapLanFilePort;             // 포트매핑 정보. Filer 용
    int                 portmapWanRelayPort;            // 포트매핑 정보. Filer 용
    int                 portmapLanRelayPort;            // 포트매핑 정보. Filer 용
    QString             localIpAddress;                 // 포트매핑 정보. 로컬 IP
};

struct SaaringMemberInfo
{
    QString                 authKey;

    QString                 runningVersion;
    QString                 lastVersion;

    qint64                  deviceNo;
    int                     deviceType;
    QString                 deviceModel;
    QString                 deviceName;

    QString                 language;

    QString                 osName;
    QString                 osVersion;

    qint64                  userNo;
    QString                 userId;
    QString                 userName;

    QString                 password;

    QString                 email;
    QString                 phoneNumber;

    QString                 firstName;
    QString                 lastName;

    QString                 profilePic;
    QString                 message;

    int                     idType;
    QString                 providerId;
    QString                 idToken;
    QString                 accessToken;
    QString                 uid;
    QString                 providerYN;
    int                     numProvider;

    qint64                  defaultServerFolderNo;

    int                     joinStatus;
    QString                 pushKey;
    QString                 loginKey;

    // for firebase
    QString                 firebaseToken;

    // id/pw확인
    qint64                  certNo;
    qint64                  certType;
    QString                 certKey;

    // additial member
    bool                    isNewUser   = false;
};

struct SaaringMemberProviderInfo
{
    qint64                  userNo;

    int                     idType;
    QString                 providerId;
    QString                 idToken;
    QString                 accessToken;
    QString                 uid;
    QString                 providerYN;

};

struct SaaringFriendInfo : public SaaringMemberInfo
{
    QString                 favoriteYN;
    QString                 regdt;

    int                     numDevice;
    int                     numLiveDevice;
    int                     numOfflineMobileDevice;
    int                     numFile;
    int                     numFolder;
    qint64                  totalFileSize;

    int                     requestStatus;
    QDateTime               requestdt;
};

struct SaaringInviteUsersInfo
{
    qint64              inviteNo;
    qint64              userNo;
    qint64              toUserNo;

    QString             toEmail;
    QString             toPhoneNumber;
    QDateTime           regdt;
};

struct SaaringInviteFilesInfo
{
    qint64              fileId;
    qint64              inviteNo;
    QString             certKey;
    QString             localPath;
    QString             indirectPath;
    QString             fileName;
    qint64              fileSize;
    QDateTime           expiredt;
    QDateTime           regdt;
};

struct SaaringInviteSecretInfo
{
    qint64              userNo;
    QString             secretCode;
    QDateTime           expiredt;
    QDateTime           regdt;
};

struct SaaringInviteInfo
{
    qint64                  inviteNo;
    QString                 inviteCode;

    qint64                  userNo;
    QString                 userName;

    qint64                  inviteDeviceNo;
    int                     inviteDeviceType;
    QString                 inviteDeviceName;
    int                     inviteType;

    QList<SaaringInviteUsersInfo>  listInviteUsers;
    QList<SaaringInviteFilesInfo>  listInviteFiles;

    QString                 certKey;
    qint64                  numFile;
    qint64                  totalFileSize;

    qint64                  inviteShareFolderServerFolderNo;
    QString                 inviteShareFolderName;
    QString                 inviteShareFolderPath;
    QString                 inviteShareFolderPasswordYN;

    QString                 inviteMessage;

    QString                 dynamicLink;
    QString                 rejectYN;

    QDateTime               expiredt;
    QDateTime               regdt;
};

struct SaaringInviteRegisteredInfo
{
    qint64                  userNo;
    qint64                  inviteNo;
    QString                 inviteCode;

    int                     inviteType;
    QString                 certKey;

    qint64                  fromUserNo;
    QString                 fromUserName;

    QString                 inviteMessage;
    QString                 dynamicLink;

    QDateTime               expiredt;
    QDateTime               regdt;
};

struct SaaringBlockInfo
{
    qint64                  userNo;
    QString                 email;
    QString                 phoneNumber;
    int                     idType;
    QString                 userId;
    QString                 userName;
    QString                 profilePic;
};

struct SaaringTransferBaseInfo
{
    qint64              transNo;
    qint64              userNo;
    QString             userName;
    qint64              deviceNo;
    int                 deviceType;
    QString             deviceName;
    QString             upDown;
    int                 transStatus;
    QString             localPath;
    QString             remotePath;
    qint64              fileSize;
    qint64              fileStartPosition;
    qint64              transSize;
    QString             deleteYN;
    int                 sortNo;
    QDateTime           updateTime;
    qint64              myDeviceNo;
    qint64              inviteNo;
    qint64              fileId;

    // utility
    QString getLocalFolderPath()
    {
        QFileInfo fi(this->localPath);

        QString folderPath = fi.path();

        if(folderPath.right(1) == "/") {
            folderPath = folderPath.left(folderPath.length() - 1);
        }

        folderPath = folderPath.replace("/", QDir::separator());

        return folderPath;
    }
    QString getLocalPathForDisplay()
    {
        QString path(this->localPath);

        return path.replace("/", QDir::separator());;
    }
};

struct SaaringTransferInfo
{
    QString                 fileName;   // 파일이름
    SaaringTransferStatusType      status;     // 상태
    qint64                  fileSize;   // 파일사이즈
    QString                 sendSpeed;  // 전송속도
    QString                 leftTime;   // 남은시간
    qint64                  transNo;
    qint64                  currSize;   // 현재까지 전송된 사이즈
    QString                 currTime;   // 현재까지 마지막으로 전송한 시간. YYYYMMddHHmmsszzz
    qint64                  userNo;     // toUserNo
    QString                 userName;
    qint64                  deviceNo;
    int                     deviceType;
    QString                 deviceName;
    QString                 upDown;     // U or D
    bool                    isMyAction; // 내가 down/up 이면 true, 상대방이 down/up 이면 false
    QString                 folderName; // 공유폴더명
    QString                 remotePath;
    qint64                  inviteNo;
    qint64                  fileId;

    // utility
    QString getStatusString()
    {
        QString strStatus = "";

        switch(status)
        {
        case saaring::SaaringTransferStatusType::NONE:
            strStatus = TRANSFER_STATUS_STRING_NONE;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_WAITING:
            strStatus = TRANSFER_STATUS_STRING_DOWNLOAD_WAITING;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_STAND_BY:
            strStatus = TRANSFER_STATUS_STRING_DOWNLOAD_STAND_BY;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOADING:
            strStatus = TRANSFER_STATUS_STRING_DOWNLOADING;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_STOPPED:
            strStatus = TRANSFER_STATUS_STRING_DOWNLOAD_STOPPED;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_FAILED:
            strStatus = TRANSFER_STATUS_STRING_DOWNLOAD_FAILED;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_FINISHED:
            strStatus = TRANSFER_STATUS_STRING_DOWNLOAD_FINISHED;
            break;
        case saaring::SaaringTransferStatusType::UPLOAD_WAITING:
            strStatus = TRANSFER_STATUS_STRING_UPLOAD_WAITING;
            break;
        case saaring::SaaringTransferStatusType::UPLOAD_STAND_BY:
            strStatus = TRANSFER_STATUS_STRING_UPLOAD_STAND_BY;
            break;
            break;
        case saaring::SaaringTransferStatusType::UPLOADING:
            strStatus = TRANSFER_STATUS_STRING_UPLOADING;
            break;
        case saaring::SaaringTransferStatusType::UPLOAD_STOPPED:
            strStatus = TRANSFER_STATUS_STRING_UPLOAD_STOPPED;
            break;
        case saaring::SaaringTransferStatusType::UPLOAD_FAILED:
            strStatus = TRANSFER_STATUS_STRING_UPLOAD_FAILED;
            break;
        case saaring::SaaringTransferStatusType::UPLOAD_FINISHED:
            strStatus = TRANSFER_STATUS_STRING_UPLOAD_FINISHED;
            break;
        }

        return strStatus;
    }
    QString getShortStatusString()
    {
        QString strStatus = "";

        switch(status)
        {
        case saaring::SaaringTransferStatusType::NONE:
            strStatus = TRANSFER_STATUS_STRING_NONE;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_WAITING:
        case saaring::SaaringTransferStatusType::UPLOAD_WAITING:
            strStatus = TRANSFER_STATUS_STRING_WAITING;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_STAND_BY:
        case saaring::SaaringTransferStatusType::UPLOAD_STAND_BY:
            strStatus = TRANSFER_STATUS_STRING_STAND_BY;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOADING:
        case saaring::SaaringTransferStatusType::UPLOADING:
            strStatus = TRANSFER_STATUS_STRING_TRANSFERRING;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_STOPPED:
        case saaring::SaaringTransferStatusType::UPLOAD_STOPPED:
            strStatus = TRANSFER_STATUS_STRING_STOPPED;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_FAILED:
        case saaring::SaaringTransferStatusType::UPLOAD_FAILED:
            strStatus = TRANSFER_STATUS_STRING_FAILED;
            break;
        case saaring::SaaringTransferStatusType::DOWNLOAD_FINISHED:
        case saaring::SaaringTransferStatusType::UPLOAD_FINISHED:
            strStatus = TRANSFER_STATUS_STRING_FINISHED;
            break;
        }

        return strStatus;
    }
    double getTransferPercent()
    {
        return (double)this->currSize/(double)this->fileSize;
    }
};

struct SaaringShareInfo
{
    qint64                  shareNo;
    qint64                  deviceNo;
    QString                 shareFolderPath;
    QString                 shareName;

    // 서버에서발행하는 folderno
    qint64                  serverFolderNo;
    int                     fileCount;
    qint64                  fileSize;

    int                     downKind;
    QList<qint64>           listDownFriendUserNo;
    QStringList             listDownFriendUserName;
    int                     upKind;
    QList<qint64>           listUpFriendUserNo;
    QStringList             listUpFriendUserName;

    QString                 downYN;
    QString                 upYN;
    QString                 password;
    QString                 passwordYN;
};

struct SaaringShareDetailInfo
{
    qint64                  shareDetailNo;
    qint64                  deviceNo;
    qint64                  shareNo;
    QString                 fullPathName;
    QString                 shortPathName;
    QString                 fileName;
    QString                 fileExtension;
    qint64                  fileSize;
    QDateTime               updateTime;
    QString                 folderYN;       // Y(Yes) or N(No)

    // utility
    bool isFolder()
    {
        return this->folderYN == "Y" ? true : false;
    }
};

struct SaaringShareFolderInfo
{
    qint64                  deviceNo;
    int                     deviceType;
    QString                 deviceName;
    QString                 liveYN;

    qint64                  serverFolderNo;

    QString                 folderName;
    qint64                  fileSize;
    int                     fileCount;

    int                     shareType;
    int                     downShareType;
    int                     upShareType;
    int                     shareFoaf;

    int                     numFolder;
    int                     numFile;
    qint64                  totalFileSize;

    QString                 password;
    QString                 passwordYN;
    QString                 downloadYN;
    QString                 uploadYN;

    QList<qint64>           listDownFriendUserNo;
    QList<qint64>           listUpFriendUserNo;
};

struct SaaringBadgeInfo
{
    int                     numNewRequestFriend;
    int                     numNewNotice;
};

struct SaaringServerInfo
{
    QString                 signalingIp;
    int                     signalingPort;
    QString                 rendezvousIp;
    int                     rendezvousPort;
    QString                 relayIp;
    int                     relayPort;
    QString                 runningVersion;
    QString                 lastVersion;
};

struct SaaringPhoneAddressInfo
{
    qint64                  userNo;

    QString                 phoneNumber;
    int                     countryCode;
    qint64                  nationalNumber;

    qint64                  friendUserNo;
    QString                 friendYN;
    QString                 blockYN;

    qint64                  matchingdt;
    qint64                  frienddt;
    qint64                  regdt;

    QString                 userName;
    QString                 email;
    QString                 profilePic;
    QString                 message;
};

} // namespace ssaring

#endif // SAARINGDOMAIN_H
