#ifndef SAARINGP2PINTERFACE_H
#define SAARINGP2PINTERFACE_H

#include "saaring.h"

#include "api/saaringdomain.h"

#include <QObject>
#include <QList>

#include <QWaitCondition>
#include <QMutex>
#include <QThread>
#include <QTimer>

namespace saaring {

class SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringP2pInterface : public QObject
{
    Q_OBJECT

public:

    SaaringP2pInterface(int type);
    virtual ~SaaringP2pInterface();

    /** for signaling & p2p */
    enum P2pCallbackCommand {
        PCC_SIGNALING_CONNECTED                                 = 10000,
        PCC_SIGNALING_CLOSED                                    = 10001,
        PCC_SIGNALING_LOGIN_FAILED                              = 10002,

        PCC_SIGNALING_NOTIFY_DEVICE_LIVEYN                      = 11000,
        PCC_SIGNALING_NOTIFY_REQUEST_FRIEND                     = 11001,
        PCC_SIGNALING_NOTIFY_ADD_FRIEND                         = 11002,

        PCC_P2P_PORTMAP_FINISHED                                = 20000,
        PCC_P2P_LOGIN_FINISHED                                  = 20001,
        PCC_P2P_WAKEUP_FINISHED                                 = 20002,

        PCC_P2P_FINISH_DOWNLOAD                                 = 21000,
        PCC_P2P_FINISH_UPLOAD                                   = 21001,
        PCC_P2P_PROGRESS_DOWNLOAD                               = 21002,
        PCC_P2P_PROGRESS_UPLOAD                                 = 21003,
        PCC_P2P_ABORT_DOWNLOAD                                  = 21004,
        PCC_P2P_ABORT_UPLOAD                                    = 21005,
        PCC_P2P_FINISH_DOWNLOAD_OTHER                           = 21006,
        PCC_P2P_FINISH_UPLOAD_OTHER                             = 21007,
        PCC_P2P_PROGRESS_DOWNLOAD_OTHER                         = 21008,
        PCC_P2P_PROGRESS_UPLOAD_OTHER                           = 21009,

        PCC_P2P_PROCESSED_REQUEST                               = 22000,
        PCC_P2P_PROCESSED_RESPONSE                              = 22001,
        PCC_P2P_PROCESS_FOLDER_LIST                             = 22002,
        PCC_P2P_PROCESS_FOLDER_DETAIL                           = 22003,
        PCC_P2P_PROCESS_FOLDER_DETAIL_FINISHED                  = 22004,
        PCC_P2P_PROCESS_FOLDER_DETAIL_RECURSIVE                 = 22005,
        PCC_P2P_PROCESS_FILE_UPLOAD_QUERY                       = 22006,
        PCC_P2P_PROCESS_FILE_DOWNLOAD_INVITE_SEND_FILES_QUERY   = 22007,
        PCC_P2P_PROCESS_SAVE_SEND_FILES_INVITE_INFO             = 22008,

        PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_STAND_BY                  = 30000,
        PCC_P2P_NOTIFY_DOWNLOAD_QUEUE_RELEASE                   = 30001,
        PCC_P2P_NOTIFY_UPLOAD_QUEUE_STAND_BY                    = 30002,
        PCC_P2P_NOTIFY_UPLOAD_QUEUE_RELEASE                     = 30003,

        PCC_P2P_NOTIFY_CONNECTION_CLOSED                        = 40000,
        PCC_P2P_NOTIFY_START_NEXT_TRANSFER                      = 40001,
        PCC_P2P_NOTIFY_STATUS_MESSAGE                           = 40002,
    };

    typedef void (*P2pCallback)(void *context, int command, QStringList &param);

    virtual void initP2p(void *context, P2pCallback callback, SaaringMemberInfo &member);
    virtual void closeP2p();
    virtual bool isP2p();

    virtual bool isSignalingConnected();
    virtual bool isSignalingLogin();
    virtual bool doSignalingConnect(QString &authKey, bool isRefreshServerInfo = false);
    virtual void doSignalingClose();
    virtual void reqHeartBeat();

    virtual SaaringServerInfo getServerInfo();
    virtual int getWanRelayPort();
    virtual int getLanRelayPort();
    virtual bool isPortmapFinished();                   // 미사용
    virtual bool isUpnpSupport();                       // 미사용
    virtual bool isP2pLived(qint64 deviceNo);

    virtual void updateShareFolders();
    virtual void updateNetworkSendSettings();

    virtual void doP2pConnect(qint64 toUserNo, QString toUserName, qint64 toDeviceNo, saaring::SaaringDeviceType toDeviceType, QString toDeviceName, bool mustWakeUp);
    virtual void doP2pConnectShareFolder(qint64 toUserNo, QString toUserName, qint64 toDeviceNo, saaring::SaaringDeviceType toDeviceType, QString toDeviceName, qint64 toServerFolderNo, QString toFolderName, bool mustWakeUp);
    virtual void doP2pConnectCancel(qint64 toUserNo, QString toUserName, qint64 toDeviceNo, saaring::SaaringDeviceType toDeviceType, QString toDeviceName);

    virtual qint64 doP2pFolderList(qint64 toDeviceNo);
    virtual qint64 doP2pFolderDetail(qint64 toDeviceNo, qint64 shareNo, qint64 serverFolderNo, QString remotePath);
    virtual qint64 doP2pFolderDetailRecursive(qint64 toDeviceNo, qint64 shareNo, QString remotePath);

    virtual qint64 doP2pUploadQuery(qint64 toDeviceNo, QString remoteFilePath, qint64 fileSize, qint64 myTransNo);
    virtual qint64 doP2pUpload(qint64 toDeviceNo, QString remoteFilePath, QString localFilePath , qint64 fileStartPosition, qint64 myTransNo);
    virtual qint64 doP2pDownload(qint64 toDeviceNo,
                                 QString remoteFilePath, QString localFilePath,
                                 qint64 fileSize, qint64 fileStartPosition, qint64 myTransNo, int folderDepth,
                                 qint64 inviteNo, qint64 remoteFileId);
    virtual qint64 doP2pDownloadInviteSendFilesQuery(qint64 toDeviceNo, qint64 inviteNo, QString certKey);
    virtual qint64 doP2pSaveSendFilesInviteInfo(qint64 toDeviceNo,
                                                qint64 inviteNo, QString inviteCode, QString dynamicLink,
                                                QStringList listShortPathName, QStringList listFileName, QList<qint64> listFileSize,
                                                QString certKey, QString inviteMessage);

    virtual qint64 doP2pProcessDownloadComplete(qint64 toDeviceNo);
    virtual qint64 doP2pProcessUploadComplete(qint64 toDeviceNo);

    virtual qint64 doP2pProcessDownloadQueueCheck(qint64 toDeviceNo);
    virtual qint64 doP2pProcessUploadQueueCheck(qint64 toDeviceNo);

    virtual void doP2pOpenAllChannel(QList<qint64> listDeviceNo);
    virtual void doP2pCloseChannel(qint64 remoteDeviceNo);
    virtual qint64 doP2pStopAllTransfer(QList<qint64> listDeviceNo);
    virtual qint64 doP2pCancelAllTransfer(QList<qint64> listDeviceNo);

    virtual void listAllChannelDeviceNo(QList<qint64> &list);
    virtual void updateNumLiveTransfer(int numLiveTransfer);

    // for relay server
    virtual void startRelayServer();
    virtual void stopRelayServer();

private slots:
    void onSignalingConnected();
    void onSignalingClosed();
    void onSignalingLoginFailed();
    void onSignalingNotifyDeviceLiveYN(qint64 fromUserNo, qint64 fromDeviceNo, QString liveYN);
    void onSignalingNotifyRequestFriend(QString friendName);
    void onSignalingNotifyAddFriend(QString friendName, qint64 friendUserNo);

    void onPortmapFinished();
    void onP2pLoginFinished(qint64 friendDeviceNo, qint64 toRemoteShareNo);
    void onP2pWakeUpFinished(bool isSuccess);

    void onP2pFinishDownload(qint64 transNo, qint64 fileSize, qint64 receivedSize);
    void onP2pFinishUpload(qint64 transNo, qint64 fileSize, qint64 sentSize);

    void onP2pProgressDownload(qint64 transNo, qint64 fileSize, qint64 receivedSize);
    void onP2pProgressUpload(qint64 transNo, qint64 fileSize, qint64 sentSize);

    void onP2pAbortDownload(qint64 transNo);
    void onP2pAbortUpload(qint64 transNo);

    void onP2pFinishDownloadOther(qint64 jobId, qint64 remoteDeviceNo);
    void onP2pFinishUploadOther(qint64 jobId, qint64 remoteDeviceNo);

    void onP2pProgressDownloadOther(qint64 jobId, qint64 remoteDeviceNo, qint64 fileSize, qint64 currSize, QString filePath, QString serverName, QString deviceName);
    void onP2pProgressUploadOther(qint64 jobId, qint64 remoteDeviceNo, qint64 fileSize, qint64 currSize, QString filePath, QString serverName, QString deviceName);

    void onP2pProcessedRequest(int processType, qint64 jobId, qint64 remoteDeviceNo, int resultCode);
    void onP2pProcessedResponse(int processType, qint64 jobId, qint64 remoteDeviceNo, int resultCode);

    // additional slots
    void onP2pProcessResponseFolderList(QByteArray val);
    void onP2pProcessResponseFolderDetail(QByteArray val);
    void onP2pProcessResponseFolderDetailFinished();
    void onP2pProcessResponseFolderDetailRecursive(QByteArray val);
    void onP2pProcessResponseFileUploadQuery(qint64 transNo, qint64 fileStartPosition);
    void onP2pProcessResponseFileDownloadInviteSendFilesQuery(QByteArray val);
    void onP2pProcessResponseSaveSendFilesInviteInfo(QByteArray val);

    void onP2pNotifyDownloadQueueStandBy(qint64 remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn);
    void onP2pNotifyDownloadQueueRelease(qint64 remoteDeviceNo, int downloadingCount, int downloadStandByCount, int myTurn);
    void onP2pNotifyUploadQueueStandBy(qint64 remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn);
    void onP2pNotifyUploadQueueRelease(qint64 remoteDeviceNo, int uploadingCount, int uploadStandByCount, int myTurn);

    void onP2pNotifyConnectionClosed(qint64 remoteDeviceNo);
    void onP2pNotifyStartNextTransfer(qint64 remoteDeviceNo);
    void onP2pNotifyStatusMessage(QString text);

};

extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringP2pInterface* P2p(int type);
extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT void releaseP2p();

} // namespace ssaring

#endif // SAARINGP2PINTERFACE_H
