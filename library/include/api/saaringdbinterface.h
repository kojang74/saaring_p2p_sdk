#ifndef SAARINGDBINTERFACE_H
#define SAARINGDBINTERFACE_H

#include "saaring.h"

#include "api/saaringdomain.h"

#include <QObject>

namespace saaring {

class SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringDbInterface : public QObject
{
    Q_OBJECT

public:

    SaaringDbInterface(int type);
    virtual ~SaaringDbInterface();

    static SaaringDbInterface* instance;

    //////////////////////////////////////////////////////////////////////////////////
    // for All
    //////////////////////////////////////////////////////////////////////////////////
    virtual bool isOpen();

    //////////////////////////////////////////////////////////////////////////////////
    // for login
    //////////////////////////////////////////////////////////////////////////////////
    virtual void doAfterLogin(int idType, QString email, QString phoneNumber, QString password, QString providerId, QString idToken, QString accessToken, QString uid, QString isSaveEmail, QString isAutoLogin);

    virtual bool updateLoginTb(QString idType, QString email, QString phoneNumber, QString password, QString isSaveEmail, QString isAutoLogin, QString isLastLoginEmail);
    virtual bool updateLoginAutoStart(QString idType, QString email, QString phoneNumber, QString isAutoStart);

    virtual bool updateSetSend(saaring::SaaringSetupInfo &saaringSetupInfo);
    virtual bool updateSetNetwork(saaring::SaaringSetupInfo &saaringSetupInfo);

    virtual bool readDbLastLogin(saaring::SaaringSetupInfo &saaringSetupInfo);
    virtual bool readDbByEmailOrPhoneNumber(QString idType, QString email, QString phoneNumber, saaring::SaaringSetupInfo &saaringSetupInfo);

    //////////////////////////////////////////////////////////////////////////////////
    // for transfer
    //////////////////////////////////////////////////////////////////////////////////
    virtual qint64 insertMyTransTbOne(saaring::SaaringTransferBaseInfo &saaringTransfer);
    virtual bool insertMyTransTbMulti(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer);

    virtual bool updateMyTransTb(saaring::SaaringTransferBaseInfo &saaringTransfer);
    virtual bool updateMyTransTbStatus(qint64 transNo, int transStatus, QDateTime updateTime);

    virtual bool updateMyTransTbSortNoMulti(QList<qint64> &paramListTransNo, QDateTime updateTime);

    virtual bool updateMyTransTbProgress(qint64 transNo, qint64 fileSize, qint64 transSize, QDateTime updateTime);
    virtual bool updateMyTransTbInit(qint64 myDeviceNo);

    virtual bool deleteMyTransTbOne(qint64 transNo);
    virtual bool deleteMyTransTbMulti(QList<qint64> &paramListTransNo);
    virtual bool deleteMyTransTbAll(qint64 deviceNo);

    virtual bool readMyTransTbByTransNo(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 transNo);
    virtual bool readMyTransTbAll(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo, QString deleteYN, qint64 deviceNo);
    virtual bool readMyTransTbTransferring(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo, QString deleteYN, qint64 deviceNo);
    virtual bool readMyTransTbNotTransferringLastOne(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo, QString deleteYN, qint64 deviceNo);
    virtual bool readMyTransTbByTransStatusLastOne(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo, QString deleteYN, qint64 deviceNo, int transStatus);
    virtual bool readMyTransTbInit(QList<saaring::SaaringTransferBaseInfo> &listSaaringTransfer, qint64 myDeviceNo);

    //////////////////////////////////////////////////////////////////////////////////
    // for share
    //////////////////////////////////////////////////////////////////////////////////
    virtual bool insertShareTb(saaring::SaaringShareInfo &saaringShare);
    virtual bool updateShareTb(saaring::SaaringShareInfo &saaringShare);
    virtual bool deleteShareTb(qint64 shareNo);

    virtual bool readShareTb(qint64 deviceNo, QList<saaring::SaaringShareInfo> &listSaaringShare);

    virtual qint64 getShareNoByServerFolderNo(qint64 serverFolderNo);
    virtual qint64 getServerFolderNoByShareNo(qint64 shareNo);
    virtual QString getShareFolderPathByServerFolderNo(qint64 serverFolderNo);

    virtual int getShareFileCntWithShareNo(qint64 deviceNo, qint64 shareNo);
    virtual qint64 getShareFileSize(qint64 deviceNo);
    virtual qint64 getShareFileSizeWithShareNo(qint64 deviceNo, qint64 shareNo);

    //////////////////////////////////////////////////////////////////////////////////
    // for share detail
    //////////////////////////////////////////////////////////////////////////////////
    virtual bool updateShareDetailShortPath(qint64 shareNo, QString shareName, QString shareFolderLocalFullPath);
    virtual bool deleteShareDetailTb(qint64 shareNo);

    virtual void makeMemoryShareDetailTb();
    virtual void backupMemoryShareDetailTb();

    //////////////////////////////////////////////////////////////////////////////////
    // for invite files
    //////////////////////////////////////////////////////////////////////////////////
    virtual bool insertInviteFiles(saaring::SaaringInviteFilesInfo &saaringInviteFilesInfo);
    virtual bool insertListInviteFiles(QList<saaring::SaaringInviteFilesInfo> &listSaaringInviteFilesInfo);

    virtual bool deleteExpired();
    virtual bool deleteInviteFilesByInviteNo(qint64 inviteNo);

    virtual bool listInviteFiles(qint64 inviteNo, QList<saaring::SaaringInviteFilesInfo> &listSaaringInviteFilesInfo);
    virtual bool listInviteFilesWithCertKey(qint64 inviteNo, QString certKey, QList<saaring::SaaringInviteFilesInfo> &listSaaringInviteFilesInfo);

    //////////////////////////////////////////////////////////////////////////////////
    // for test
    //////////////////////////////////////////////////////////////////////////////////
    virtual void test();

};

extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringDbInterface* Db(int type);
extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT void releaseDb();

} // namespace ssaring

#endif // SAARINGDBINTERFACE_H
