#ifndef SAARINGSHAREFOLDERINTERFACE_H
#define SAARINGSHAREFOLDERINTERFACE_H

#include "saaring.h"

#include "api/saaringdomain.h"

#include <QObject>

namespace saaring {

class SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringShareFolderInterface : public QObject
{
    Q_OBJECT

public:

    SaaringShareFolderInterface();
    virtual ~SaaringShareFolderInterface();

    static SaaringShareFolderInterface* instance;

    /** for share dir monitor & job */
    enum ShareDirCallbackCommand {
        SDCC_DIRECTORY_CHANGED      = 0,
        SDCC_JOB_FINISHED           = 1,
        SDCC_JOB_ERROR              = 2
    };

    typedef void (*ThreadShareDirCallback)(void *context, int command, QStringList &param);

    virtual void initThreadShareDir(void *context, ThreadShareDirCallback callback, SaaringMemberInfo &member);

    virtual void startThreadShareFolder();
    virtual void stopThreadShareFolder();

    virtual void shareFolderInit();
    virtual void shareFolderInsert(qint64 shareNo, QString sharePath, QString shareName);
    virtual void shareFolderDelete(qint64 shareNo);
    virtual void shareFolderUpdate(QString folderPath);
    virtual void serverFolderDelete(qint64 serverFolderNo);

    virtual int getShareFolderCount();
    virtual int getShareFileCount();

    virtual void startFileMonitor();
    virtual void stopFileMonitor();

    virtual bool doesNeedDbBackup();

private slots:
    void onShareDirRefresh(const QString &path);

};

extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringShareFolderInterface* ShareFolder();
extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT void releaseShareFolder();

} // namespace ssaring

#endif // SAARINGSHAREFOLDERINTERFACE_H
