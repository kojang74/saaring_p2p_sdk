#ifndef SAARINGWEBINTERFACE_H
#define SAARINGWEBINTERFACE_H

#include "saaring.h"

#include "api/saaringdomain.h"

#include <QObject>
#include <QUrl>
#include <QUrlQuery>

namespace saaring {

class SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringWebInterface : public QObject
{
    Q_OBJECT

public:

    SaaringWebInterface(QString apiDomain);
    virtual ~SaaringWebInterface();

    static SaaringWebInterface* instance;

    // ////////////////////////////////////////////////////////////////
    // Utility
    // ////////////////////////////////////////////////////////////////

    virtual bool doPost(QString &resultCode, QString &resultMessage, QJsonObject &resultData, QString strUrl, QUrlQuery urlQuery);

    virtual bool doPost(QJsonObject &resultData, QString strUrl, QUrlQuery urlQuery);

    virtual bool doGet(QJsonObject &resultData, QString strUrl);

    // ////////////////////////////////////////////////////////////////
    // Alarm
    // ////////////////////////////////////////////////////////////////

    // ////////////////////////////////////////////////////////////////
    // Connect
    // ////////////////////////////////////////////////////////////////

    virtual bool listFolderOfOther(QString authKey, qint64 friendUserNo,
                                   QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                   QList<saaring::SaaringShareFolderInfo> &listFolder);

    virtual bool listLiveDevice(QString authKey, QString deviceNos,
                                QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                QList<qint64> &listLiveDevice);

    virtual bool getBadgeInfo(QString authKey,
                              QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                              saaring::SaaringBadgeInfo &saaringBadgeInfo);

    // ////////////////////////////////////////////////////////////////
    // Friend
    // ////////////////////////////////////////////////////////////////

    virtual bool listFriend(QString authKey,
                            QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                            QList<SaaringFriendInfo> &listSaaringFriendInfo);

    virtual bool requestFriendByUserNo(QString authKey, qint64 toUserNo,
                                       QString &resultCode, QString &resultMessage, QJsonObject &resultData);
    virtual bool listRequestingFriend(QString authKey,
                                      QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                      QList<saaring::SaaringFriendInfo> &listSaaringIncomingRequestFriend,
                                      QList<saaring::SaaringFriendInfo> &listSaaringRequestingFriendMember,
                                      QList<saaring::SaaringFriendInfo> &listSaaringRequestingFriendNonMember);
    virtual bool cancelRequestFriend(QString authKey, qint64 toUserNo, QString toEmail, QString toPhoneNumber,
                                     QString &resultCode, QString &resultMessage, QJsonObject &resultData);
    virtual bool rejectRequestFriend(QString authKey, qint64 toUserNo,
                                     QString &resultCode, QString &resultMessage, QJsonObject &resultData);
    virtual bool recoverRequestFriend(QString authKey, qint64 toUserNo,
                                      QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool addFriend(QString authKey, qint64 friendUserNo,
                           QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool blockFriend(QString authKey, QString friendUserNos, QString blockYN,
                             QString &resultCode, QString &resultMessage, QJsonObject &resultData);
    virtual bool listBlock(QString authKey,
                           QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                           QList<SaaringBlockInfo> &listSaaringBlocked, QList<SaaringBlockInfo> &listSaaringRejected);

    // ////////////////////////////////////////////////////////////////
    // invite
    // ////////////////////////////////////////////////////////////////

    virtual bool inviteFriendSimple(QString authKey,
                                    QString certKey, QString inviteMessage,
                                    QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                    qint64 &inviteNo, QString &inviteCode, QString &dynamicLink);
    virtual bool inviteFriend(QString authKey, QList<qint64> listToUserNo, QStringList listToUserName,
                              QStringList listToEmail, QStringList listToPhoneNumber,
                              QString certKey, QString inviteMessage,
                              QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                              qint64 &inviteNo, QString &inviteCode, QString &dynamicLink,
                              QList<qint64> &listFailUserNo, QStringList &listSuccessEmail,
                              QList<saaring::SaaringFriendInfo> &listSaaringAddedFriend,
                              QList<saaring::SaaringFriendInfo> &listSaaringIncomingRequestFriend,
                              QList<saaring::SaaringFriendInfo> &listSaaringRequestingFriendMember,
                              QList<saaring::SaaringFriendInfo> &listSaaringRequestingFriendNonMember);
    virtual bool cancelInviteFriend(QString authKey, qint64 inviteNo,
                                    QString &resultCode, QString &resultMessage, QJsonObject &resultData);
    virtual bool completeInviteFriend(QString authKey, qint64 inviteNo,
                                      QString &resultCode, QString &resultMessage, QJsonObject &resultData);
    virtual bool extendInviteExpiredt(QString authKey, qint64 inviteNo,
                                      QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                      saaring::SaaringInviteInfo &saaringInviteInfo);

    virtual bool getInvite(QString authKey, qint64 inviteNo,
                           QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                           saaring::SaaringInviteInfo &saaringInviteInfo);
    virtual bool getInviteByDynamicLink(QString dynamicLink, QString certKey,
                                        QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                       saaring::SaaringInviteInfo &saaringInviteInfo);

    virtual bool listInviteAll(QString authKey,
                               QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                               QList<saaring::SaaringInviteInfo> &listSaaringInviteInfo);

    virtual bool inviteShareFolderSimple(QString authKey,
                                         qint64 inviteShareFolderServerFolderNo,
                                         QString inviteShareFolderPath,
                                         QString certKey, QString inviteMessage,
                                         QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                         qint64 &inviteNo, QString &inviteCode, QString &dynamicLink);
    virtual bool inviteSendFilesSimple(QString authKey,
                                       qint64 numFile, qint64 totalFileSize,
                                       QString certKey, QString inviteMessage,
                                       QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                       qint64 &inviteNo, QString &inviteCode, QString &dynamicLink);
    virtual bool inviteSendFilesForRemoteDeviceSimple(QString authKey,
                                                      qint64 numFile, qint64 totalFileSize,
                                                      qint64 inviteShareFolderServerFolderNo,
                                                      QString inviteShareFolderPath,
                                                      QString certKey, QString inviteMessage,
                                                      QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                                      qint64 &inviteNo, QString &inviteCode, QString &dynamicLink);


    virtual bool registerInviteByDynamicLink(QString authKey,
                                             QString dynamicLink, QString certKey,
                                             QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                             saaring::SaaringInviteRegisteredInfo &saaringInviteRegisteredInfo,
                                             saaring::SaaringInviteInfo &saaringInviteInfo);
    virtual bool deleteInviteRegistered(QString authKey, qint64 inviteNo,
                                        QString &resultCode, QString &resultMessage, QJsonObject &resultData);
    virtual bool getInviteRegistered(QString authKey, qint64 inviteNo,
                                     QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                     saaring::SaaringInviteRegisteredInfo &saaringInviteRegisteredInfo,
                                     saaring::SaaringInviteInfo &saaringInviteInfo);
    virtual bool listInviteRegisteredAll(QString authKey,
                                         QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                         QList<saaring::SaaringInviteRegisteredInfo> &listSaaringInviteRegisteredInfo);

    // ////////////////////////////////////////////////////////////////
    // Member
    // ////////////////////////////////////////////////////////////////

    virtual bool verifyFirebaseToken(QString firebaseToken,
                            QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool joinMember(SaaringMemberInfo &saaringMember,
                            QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool loginMember(SaaringMemberInfo &saaringMember,
                             QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool doMe(QString authKey, int deviceType,
                      QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                      SaaringMemberInfo &saaringMember, QList<saaring::SaaringMemberProviderInfo> &listSaaringMemberProvider);

    virtual bool isEmailExist(QString email,
                              QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                              QVariant &isEmailExist);

    virtual bool doUnlink(QString authKey,
                          QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool doLogout(QString authKey,
                          QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool searchUser(QString authKey, QString keyword,
                            QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                            QList<SaaringFriendInfo> &listSaaringFriendInfo);

    virtual bool changeProfilePic(QString authKey, QString &profilePic,
                                  QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool changeEmail(QString authKey, QString email,
                             QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool changeUserName(QString authKey, QString userName,
                                QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    // ////////////////////////////////////////////////////////////////
    // Setup
    // ////////////////////////////////////////////////////////////////

    virtual bool changeDeviceName(QString authKey, QString deviceName,
                                  QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    // ////////////////////////////////////////////////////////////////
    // Folder
    // ////////////////////////////////////////////////////////////////

    virtual bool deleteFolder(QString authKey, qint64 serverFolderNo,
                              QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool addOrUpdateFolder(QString authKey, SaaringShareFolderInfo &saaringShareFolder,
                                   QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    virtual bool listFolderAll(QString authKey,
                               QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                               QList<saaring::SaaringShareFolderInfo> &listFolder);

    virtual bool checkFolderPassword(QString authKey, qint64 serverFolderNo, QString password,
                                     QString &resultCode, QString &resultMessage, QJsonObject &resultData);

    // ////////////////////////////////////////////////////////////////
    // 핸드폰 전화번호 연동
    // ////////////////////////////////////////////////////////////////

    virtual bool addPhoneAddress(QString authKey, QStringList listPhoneNumber,
                                 QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                 QList<saaring::SaaringPhoneAddressInfo> &listSaaringPhoneAddress);

    virtual bool listPhoneAddress(QString authKey,
                                  QString &resultCode, QString &resultMessage, QJsonObject &resultData,
                                  QList<saaring::SaaringPhoneAddressInfo> &listSaaringPhoneAddress);

};

extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT SaaringWebInterface* Web(QString apiDomain);
extern "C" SAARINGNETDYNAMICLIBRARYSHARED_EXPORT void releaseWeb();

} // namespace ssaring

#endif // SAARINGWEBINTERFACE_H
